/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var watchID;

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.

    onDeviceReady: function() 
    {
        this.receivedEvent('deviceready');
        var ref = cordova.InAppBrowser.open('https://singularitystorm.com/ChildMonitor/user.php?stme=VAG', '_blank','location=no,zoom=no');
        // window.open('http://serverapps.tk/ChildMonitor/user.php?stme=VAG', '_self ', 'location=yes');
        watchID = navigator.geolocation.watchPosition(onSuccessGPS, onErrorGPS, { enableHighAccuracy: true, timeout: 15000, maximumAge: 0 });
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();

////////////////////////////////////////////////////////////////////////////////
function onSuccessGPS(position) 
{     
    // .toLocaleFormat(formatString);     
    var dateLoc = new Date(position.timestamp);     
    // .toLocaleFormat('Y-m-d H:i:s')         
    var buffer =  DateFormat(dateLoc) + '<br>' +                   'Lat: '          + 
    position.coords.latitude          + '<br>' +                   'Lng: '         + 
    position.coords.longitude         + '<br>' +                   'Alt: '          + 
    position.coords.altitude          + '<br>' +                   'Acc: '          + 
    position.coords.accuracy          + '<br>' +                   'Heading: '           + 
    position.coords.heading           + '<br>' +                   'Speed: '             + 
    position.coords.speed;       

    // $("#position").html(buffer);   
    // alert(buffer);
}
////////////////////////////////////////////////////////////////////////////////
function onErrorGPS(error) 
{     
    // alert('code: '    + error.code    + '\n' +           'message: ' + error.message + '\n'); 
}
////////////////////////////////////////////////////////////////////////////////
