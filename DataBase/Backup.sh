#!/bin/bash
data_atual=`date +%d/%m/%Y-%T`
data_atual_arq=`date +%d_%m_%Y-%T`

/bin/echo "ChildMonitor backup de $data_atual" > /tmp/output.txt
/opt/lampp/bin/mysqldump -h 127.0.0.1 -v -u root  --opt --routines --triggers ChildMonitor | /bin/gzip  > "/tmp/ChildMonitorBakBD_$data_atual_arq.sql.gz" 
/usr/bin/mutt  andrerezende_br@yahoo.com.br -s "ChildMonitor backup de $data_atual" -a "/tmp/ChildMonitorBakBD_$data_atual_arq.sql.gz" < /tmp/output.txt
rm "/tmp/ChildMonitorBakBD_$data_atual_arq.sql.gz"
rm /tmp/output.txt
