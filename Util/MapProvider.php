<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

////////////////////////////////////////////////////////////////////////////////
function MapProvider() 
{
    
    if (ismobile()) {
        $top = "45px";
        $heightz = "45";

    } else {
        $top = "100px";
        $heightz = "85";
    }
    
    
    // Mapzen key search-EzrHuLH
    $idUser = 0;
    $headersM = HeadersMap();
    $divStyle= "  z-index: 1; opacity: 0.5; background:    #000; background:    -webkit-linear-gradient(#000, #011629); background:    linear-gradient(#000, #011629);  border-radius: 5px; box-shadow:    0 0px 0 0 #444; color:  #fff; display:       inline-block; padding: 0px 3px 7px 3px; text-align:    center; text-shadow:   1px 1px 0 #000;";
      
    $dataSearch = <<<EOT
    $headersM
         
    <script src="/ChildMonitor/Util/Util.min.js"></script>  
        
    <body class="bodyForm" style="overflow:hidden;"  >   
   
    <div style=" position:absolute; z-index:1; top:$top; left:0; width:100%; height:100; "  id="map1"> </div> <br> 

    <div style=" position:absolute; z-index:2; top:$top; left:10px; width:99vw; $divStyle "  id="divAddress" /> <p style="font-size:10px" id="idAddress" >    </div>
    <div style=" position:absolute; z-index:2; top:$top; left:0;" width:10px; height:10px; id="gpsLoc"> <img src="Img/GPS.png" alt="" height="30" width="30"> </div> <br> 
        
    </body>          
    <script type="text/javascript">        
 
    ////////////////////////////////////         
    var markerGlb=null;  
    var markerMe=null; 
    var markerOri=null;  
    var markerDst=null;  
    var controlRoute=null; 
    var idOrilatGlbOld=null;
    var idOrilonGlbOld=null;
    var idDstlatGlbOld=null;
    var idDstlonGlbOld=null;
    /////////////////////////////////////
            
    var altura_tela = $(window).height(); /*cria variável com valor do altura da janela*/
    var largura_tela = $(window).width(); 
 
    largura = largura_tela-6;
    $("#divAddress").css('width', largura);
    $("#divAddress").css('left', 0);              
            
    $("#gpsLoc").css('top',altura_tela-55);
    $("#gpsLoc").css('left', largura_tela-50);
    $('#gpsLoc').on( "click", function() {
        SetMarkerOnMyLocation();
    });
    
    // avoid showing scrollbars on main window
    $(".bodyForm").css('overflow','hidden');
    
       
    $("#map1").height(altura_tela-$heightz); /* aplica a variável a altura da div*/ 
    $('#divAddress').show();


            
    ////////////////////////////////////////////////////////////////////////////      
    var bTimerLoc = 0; 
   
    glbLatNow = 0.0;    
    glbLngNow = 0.0;          
    setTimeout(function() 
    {
       SetMarkerOnMyLocation();
       SetMapDragActions();     
    }, 5000);
      
    
       // alert(glbLat);
     
    initialize();

    //////////////////////////////////// 
    function SetMapDragActions() 
    {
        map.on('move', function () {
           // StopMapTimer();
            if(markerMe!=null) 
               markerMe.setLatLng(map.getCenter());
            
            pos = map.getCenter()
         
	});
        
	//Dragstart event of map for update marker position
	map.on('dragstart', function(e) {  
            StopMapTimer();
            ActionOnControls(["#start","#divOriDst"],"hide");          
        });
            
            
	//Dragend event of map for update marker position
	map.on('dragend', function(e) {
            CreateMapTimer();
            ActionOnControls(["#start","#divOriDst"],"show");
            
            var cnt = map.getCenter();
            if(markerMe!=null) 
               var position = markerMe.getLatLng();
            lat = Number(position['lat']).toFixed(5);
            lng = Number(position['lng']).toFixed(5);
            //console.log(position);
            // setLeafLatLong(lat, lng);
            WriteAddressOnDestiny(); 
            
	});
            
            
    }
    //////////////////////////////////// 
    function CleanMapDragActions() 
    {
        map.off('move', function (e) {
            
	});
           
	//Dragend event of map for update marker position
	map.off('dragend', function(e) {
            
	});          
    }
    //////////////////////////////////// 
    function initialize() 
    {
        GetDeviceId();
        map = ShowOpenMap('map1',glbLatNow,glbLngNow);     
        // map.on('click', onMapClick);     
    
            
        CreateMapTimer();
        myTimer(); 
    }
    ////////////////////////////////////   
    function CreateMapTimer()
    { 
       iTimer=setInterval(function () {myTimer()}, 3000); // 5 segundos 
    }
    ////////////////////////////////////   
    function StopMapTimer()
    {
        clearInterval(iTimer);    
    }
    ////////////////////////////////////         
    function onMapClick(e) 
    {
       alert("You clicked the map at " + e.latlng);
    }
    //////////////////////////////////// 
    function SetMarkerOnMyLocation()
    {
        // getLocation();      
        // alert(GetOpenMapAddress(glbLatNow,glbLngNow));  
            
        getLocation();
        glbLatNow =   glbLat;    
        glbLngNow =   glbLng;    
            
        map.setView(new L.LatLng(glbLatNow, glbLngNow)); 
            
        // markerMe = PutMarker(markerMe, map,"Img/Black_Marker.png",20,33,glbLatNow,glbLngNow);
        // alert(glbHeading); 
        markerMe  = PutCarMarker(markerMe, map,glbLatNow,glbLngNow,glbHeading); 
            
    }        
    //////////////////////////////////// 
    function SetMarkerOnNewLocation(glbLatNow,glbLngNow)
    {
        // getLocation();      
        // alert(GetOpenMapAddress(glbLatNow,glbLngNow));   
            
        map.setView(new L.LatLng(glbLatNow, glbLngNow)); 
            
        // markerMe = PutMarker(markerMe, map,"Img/Black_Marker.png",20,33,glbLatNow,glbLngNow);
        markerMe = PutMarkerLocalization(markerMe, map,glbLatNow,glbLngNow);
      
    }        

    /////////////////////////////////////////////////////////////////////////// 
    function SetupRoute(latOri,lonOri,latDest,lonDest)
    { 
        // alert(latOri+"-"+lonOri+","+latDest+"-"+lonDest);
        error=false;
        if( controlRoute != null)
           controlRoute.setWaypoints([]); 

        controlRoute = L.Routing.control({
               fitSelectedRoutes: true,
               createMarker: function() { return null; },
               lineOptions: { styles: [{color: 'green', opacity: 1, weight: 5}] },
               waypoints: [ L.latLng(latOri,lonOri), L.latLng(latDest,lonDest)]}
        ).addTo(map)
        .on('routingerror', function (e) 
        { 
            ShowErrorToast("Routing error"); 
            error=true;
            if(map!=null)
            {
               map.fitBounds([ [latOri,lonOri],[latDest,lonDest]]);             
            }
        });
         
        if(error==false)
           setTimeout(function(){ map.setZoom(map.getZoom()-1); }, 3000); 
            
        markerOri = PutMarker(markerOri, map,"Img/MapMarker_Marker_Outside_Chartreuse.png",16,16,latOri,lonOri);
        markerDst = PutMarker(markerDst, map,"Img/MapMarker_Marker_Outside_Pink.png",16,16,latDest,lonDest); 
        $('.leaflet-routing-alternatives-container').hide();
        $('.leaflet-top').hide();
        $('.leaflet-left').hide();
     }   

    ///////////////////////////////////////////////////////////////////////////
    function WriteAddressOnDestiny()
    {
        var updateAddress = function(strbuf,country,city)
        {
            glbCity = city; 
            glbCountry = country; 
  
            if (strbuf === undefined || strbuf == "") 
            {
                // $('#divAddress').slideUp();  
                //$('#idAddress').text("");
            }    
            else
            {    
                $('#idOri').val(strbuf.substring(0, 400));     
            }    
        };        
        
        var updateAddressGoogle = function(strbuf)
        {

            
            if (strbuf === undefined || strbuf == "") 
            {
                // $('#divAddress').slideUp();  
                // $('#idAddress').text("");
            }    
            else
            {    
                // $('#divAddress').show();  
                // $('#divAddress').slideUp(); 
                // $('#idAddress').text(strbuf.substring(0, 400));  
                
                $('#idOri').val(strbuf.substring(0, 400)); 
            }    
        };   
            
        // strbuf = GetOpenMapAddress(glbLatNow,glbLngNow,updateAddress);   
            
        strbuf = GetGoogleAddress(glbLatNow,glbLngNow,updateAddressGoogle);    
 
 
    }
    ///////////////////////////////////////////////////////////////////////////
    function myTimer()
    { 
        // GetDeviceId();
        console.log("Timer");
        SetMarkerOnMyLocation();  
        PostGeoServiceLastPos($idUser,glbDeviceId,glbLatNow,glbLngNow,glbHeading,"CarTransport");
        
    }
              
    ////////////////////////////////////
    function GetAjaxData()
    {
        // XXXXXXXXXX
        console.log("Data:"+$( "#datepicker" ).val());
        $.ajax
        ({
            url: "Util/GetGlobalPosition.php",
           data: { idUser: $idUser, idDevice: $('#selDevice').val (), FilterDate: $( "#datepicker" ).val(),RealTime: RealTime  },
           type: "GET",
           async: false,
           dataType : "json",
            success: function( json ) {
                console.log("Depurando JSON"+json.vet[0].idDevice);

                lLat = parseFloat(json.vet[0].Lat);
                lLong = parseFloat(json.vet[0].Long);
                idDevice = json.vet[0].idDevice;
                dTimeStamp = json.vet[0].TimeStamp;


                processJsonData(json);           
                if(RealTime==0)
                   dlgWait1.dialog("close");    
                // console.log(lLat);
                // console.log(lLong);
                // console.log(dTimeStamp);                       
                    },
            error: function( xhr, status, errorThrown ) {

                bJsonReady=true; 
                if(RealTime==0)
                   dlgWait1.dialog("close");   
                // console.log( "Erro json :" + errorThrown );
                // console.dir( xhr );
                // alert("Erro ajax");
            },
            complete: function( xhr, status ) 
            { 
                 bJsonReady=true; 
                if(RealTime==0)
                   dlgWait1.dialog("close");   
                 // alert("Ajax OK");
            }
        });           

    }
    ////////////////////////////////////
                
    </script>               
EOT;
    echo $dataSearch;
}
////////////////////////////////////////////////////////////////////////////////
