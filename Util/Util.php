<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once 'Multilanguage.php';

include_once 'FormsFunctions.php';

////////////////////////////////////////////////////////////////////////////////
// Report all PHP errors
// error_reporting(E_ALL);
// error_reporting(E_ERROR | E_WARNING | E_PARSE);
// error_reporting(E_ERROR  | E_PARSE);
// Turn off all error reporting

error_reporting(0);

// Report simple running errors
// error_reporting(E_ERROR | E_WARNING | E_PARSE);
// Reporting E_NOTICE can be good too (to report uninitialized
// variables or catch variable name misspellings ...)
// error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
////////////////////////////////////////////////////////////////////////////////
// require 'Util/GlobalVars.php';

$host = "127.0.0.1";
$port = "5432";
$dbname = "ChildMonitor";
// $user="postgres";
// $password="andreztp5000";
$user = "root"; //Mysql
$password = "";

//Mysql no hostgator
/*
  $host = "localhost";
  $dbname="child185_ChildMonitor";
  $user="child185_root";
  $password="andreztp5000";
 */

$showSubHeader = true;
// $msgWait="<div id=\\\"divMsg\\\" ><img src=\\\"Img\/Wait.gif\\\"  height=\\\"50\\\" width=\\\"50\\\" alt=\\\"Wait\\\">  </div>";

session_start();

// pgsql:host=localhost;port=5432;dbname=testdb;user=bruce;password=mypass
// 
// 
// 
////////////////////////////////////////////////////////////////////////////////

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
////////////////////////////////////////////////////////////////////////////////
function TesteCielo() 
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_URL, "http://websiteURL");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "XML=".$xmlcontent."&password=".$password."&etc=etc");
    $content=curl_exec($ch);
}
////////////////////////////////////////////////////////////////////////////////
function LogoutApp() 
{
    session_start();
    $_SESSION['userId']="";
    $_SESSION['userEmail']="";  
    echo <<<EOE
    <script type="text/javascript">
    if (navigator.cookieEnabled)
    {  
         deleteCookie('userEmailProviderParkFit');
         deleteCookie('userEmailStaffParkFit'); 
         deleteCookie('userEmailParkFit'); 
         document.cookie = "userId=";
         document.cookie = "userEmail=";
         document.cookie = "fbId=";                     
    } 
    window.location.href="index.php";
    </script>
EOE;

}
//////////////////////////////////////////////////////////////////////////////// 
function GetCountryCities() 
{
    // if ( !defined('BASEPATH')) exit('No direct script access allowed');

    // $str = file_get_contents('http://127.0.0.1/ChildMonitor/Util/countries.json');
    $str = file('http://127.0.0.1/ChildMonitor/Util/countriesToCities.json');

    // echo print_r($str, true);
    echo mb_detect_encoding($str);
    echo mb_detect_encoding($str[0]);
    
    $json = json_decode($str[0], true);
    dbg($json[0][15]);
    dbg($json['Brazil'][16]);
    dbg($json['Brazil'][17]);
    dbg($json['Brazil'][18]);
    dbg($json['Brazil'][19]);
    
    // echo print_r($json, true);
}
//////////////////////////////////////////////////////////////////////////////// 
function SelectCountry($id) 
{
    $idHid = $_GET[$id."Hid"];
    $buffer =  <<<EOT
       <script src="/ChildMonitor/Util/jquery.geocomplete.js"></script> 
       <input class="form-control" style="text-transform: uppercase" type="text" name="$id" id="$id" value="$idHid" />     
            
        <script type="text/javascript"> 
            
        /* $('#$id').autocomplete({
            source: "/ChildMonitor/Util/countries.json"
        }); */
        
        var list = [];
        var ind=0;    

        $.getJSON("/ChildMonitor/Util/countries.json" , function (data) {
                    $.map(data, function (value) {
                        // console.log({ label: value.name });
                        list[ind]=value.name;
                        ind++;  
                        
                    })
                });

         $('#$id').autocomplete({
            source: list
        });
        </script>    
	<style>

	</style>

            
EOT;
     return $buffer;
} 
//////////////////////////////////////////////////////////////////////////////// 
function SelectCountryCity($id) 
{
    $buffer =  <<<EOT
       <script src="/ChildMonitor/Util/jquery.geocomplete.js"></script> 
       <input class="form-control" type="text" name="$id" id="$id" />     
            
        <script type="text/javascript"> 
            
        /* $('#$id').autocomplete({
            source: "/ChildMonitor/Util/countriesToCities.json"
        }); */
        
        var list = [];
        var ind=0;    

        $.getJSON("/ChildMonitor/Util/countriesToCities.json" , function (data) {
                    $.map(data, function (value) {
                        // console.log({ label: value.name });
                        list[ind]=value.name;
                        ind++;  
                        
                    })
                });

         $('#$id').autocomplete({
            source: list
        });
        </script>    
	<style>

	</style>

            
EOT;
     return $buffer;
} 
//////////////////////////////////////////////////////////////////////////////// 
function UploadFile() 
{
    $uploaddir = "uploads/";
    $uploadfile = $uploaddir . basename($_FILES['file']['name']);

    if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
        echo "The file has been uploaded successfully";
    } else {
        echo "There was an error uploading the file";
    }
}
//////////////////////////////////////////////////////////////////////////////// 
function dbg($linha) {
    // if($_SESSION['userEmail']!="chu@chu.com")
    //            return;
    $temp = $linha;
    echo "debug - [{$temp}]<br>\n";
    // dbglog($linha);
}

//////////////////////////////////////////////////////////////////////////////// 
function dbglog($linha) {
    return;
    require 'GlobalVars.php';

    $tmstp = GetDateTimeNTZO();

    $change = array("'", "`", "´",",");
    $buffer = str_replace($change, "|", $linha);

    $query = "INSERT INTO `Log`(`DateTime`, `Info`) VALUES ('$tmstp','$buffer')";
    // echo "$query<br>\n";
    db_query_log($query);
    PurgeOldLog();
}

//////////////////////////////////////////////////////////////////////////////// 
function PurgeOldLog() {
    require 'GlobalVars.php';

    // select * FROM `Log` WHERE DATE_SUB(CURDATE(),INTERVAL 30 DAY) >= DateTime;
    $query = "DELETE FROM `Log` WHERE DATE_SUB(CURDATE(),INTERVAL 30 DAY) >= DateTime;";

    db_query_log($query);
}

//////////////////////////////////////////////////////////////////////////////// 
function log_file($filename, $data) {
    $myfile = fopen($filename, "a+") or die("Unable to open log file! $filename");
    fwrite($myfile, $data);
    fclose($myfile);
}

////////////////////////////////////////////////////////////////////////////////  
/*
  function login($email, $password, $mysqli) {
  // Usando definições pré-estabelecidas significa que a injeção de SQL (um tipo de ataque) não é possível.
  if ($stmt = $mysqli->prepare("SELECT id, username, password, salt
  FROM members
  WHERE email = ?
  LIMIT 1")) {
  $stmt->bind_param('s', $email);  // Relaciona  "$email" ao parâmetro.
  $stmt->execute();    // Executa a tarefa estabelecida.
  $stmt->store_result();

  // obtém variáveis a partir dos resultados.
  $stmt->bind_result($user_id, $username, $db_password, $salt);
  $stmt->fetch();

  // faz o hash da senha com um salt excusivo.
  $password = hash('sha512', $password . $salt);
  if ($stmt->num_rows == 1) {
  // Caso o usuário exista, conferimos se a conta está bloqueada
  // devido ao limite de tentativas de login ter sido ultrapassado

  if (checkbrute($user_id, $mysqli) == true) {
  // A conta está bloqueada
  // Envia um email ao usuário informando que a conta está bloqueada
  return false;
  } else {
  // Verifica se a senha confere com o que consta no banco de dados
  // a senha do usuário é enviada.
  if ($db_password == $password) {
  // A senha está correta!
  // Obtém o string usuário-agente do usuário.
  $user_browser = $_SERVER['HTTP_USER_AGENT'];
  // proteção XSS conforme imprimimos este valor
  $user_id = preg_replace("/[^0-9]+/", "", $user_id);
  $_SESSION['user_id'] = $user_id;
  // proteção XSS conforme imprimimos este valor
  $username = preg_replace("/[^a-zA-Z0-9_\-]+/",
  "",
  $username);
  $_SESSION['username'] = $username;
  $_SESSION['login_string'] = hash('sha512',
  $password . $user_browser);
  // Login concluído com sucesso.
  return true;
  } else {
  // A senha não está correta
  // Registramos essa tentativa no banco de dados
  $now = time();
  $mysqli->query("INSERT INTO login_attempts(user_id, time)
  VALUES ('$user_id', '$now')");
  return false;
  }
  }
  } else {
  // Tal usuário não existe.
  return false;
  }
  }
  }

 */

////////////////////////////////////////////////////////////////////////////////
function ColorTableDef() {
    global $AppTheme;
    if ($AppTheme == "myfather" || $AppTheme == "listaescolar" ) {
        $buffer = <<<EOT
        <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
        .tg td{font: 90% "Trebuchet MS", sans-serif;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#bbb;color:#594F4F;background-color:#E0FFEB;}
        .tg th{font: 90% "Trebuchet MS", sans-serif;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#bbb;color:#493F3F;background-color:#9DE0AD;}
        .tg .tg-lightcenter{background-color:#c7d1e2; text-align:center}
        .tg .tg-darkcenter{background-color:#9fb6e2;text-align:center}
        .tg .tg-lightright{background-color:#c7d1e2; text-align:right}
        .tg .tg-darkright{background-color:#9fb6e2;text-align:right}
        .tg .tg-header{background-color:#7096db;text-align:center}        
        </style>
EOT;
        return $buffer;
    } 
    if ($AppTheme== "truck") {
        $buffer = <<<EOT
        <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
        .tg td{font: 90% "Trebuchet MS", sans-serif;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#bbb;color:#594F4F;background-color:#E0FFEB;}
        .tg th{font: 90% "Trebuchet MS", sans-serif;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#bbb;color:#493F3F;background-color:#9DE0AD;}
        .tg .tg-lightcenter{background-color:#cacaca; text-align:center}
        .tg .tg-darkcenter{background-color:#8f8f8f;text-align:center}
        .tg .tg-lightright{background-color:#cacaca; text-align:right}
        .tg .tg-darkright{background-color:#8f8f8f;text-align:right}
        .tg .tg-header{background-color:#6b6b6b;text-align:center}        
        </style>
EOT;
        return $buffer;
    } 
    
    $buffer = <<<EOT
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;border-color:#bbb;}
    .tg td{font: 90% "Trebuchet MS", sans-serif;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#bbb;color:#594F4F;background-color:#E0FFEB;}
    .tg th{font: 90% "Trebuchet MS", sans-serif;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#bbb;color:#493F3F;background-color:#9DE0AD;}
    .tg .tg-lightcenter{text-align:center}
    .tg .tg-darkcenter{background-color:#C2FFD6;text-align:center}
    .tg .tg-lightright{text-align:right}
    .tg .tg-darkright{background-color:#C2FFD6;text-align:right}
    .tg .tg-header{background-color:#50d48b;text-align:center}       
    </style> 
EOT;

    return $buffer;
}

////////////////////////////////////////////////////////////////////////////////
function GreenTable($title, $table, $id = "",$indStart=0) {
    if ($table[0][0] == "")
        return "";
    if (ismobile())
        $width = "97.5%";
    echo ColorTableDef();
    $buffer = <<<EOT
        <table width="$width" class="tg" id="$id" >
          <tr>
            <th class="tg-header" colspan="50">$title</th>
          </tr>

EOT;
    $indLin = 0;
    $indCol = $indStart;
    
    // Logic for an empty table
    if($table[$indLin+1][$indCol] == "")
    {
        $buffer = $buffer . "<tr>";
        while ($table[$indLin][$indCol] != "") {
            $tmp = $table[$indLin][$indCol];
            $indBuf = $table[$indLin][0];
            if ($indLin % 2 == 0)
                $buffer = $buffer . "<td id=\"$indBuf" . "#" . "$indCol\" class=\"tg-darkcenter\">$tmp</td>";
            else
                $buffer = $buffer . "<td id=\"$indBuf" . "#" . "$indCol\" class=\"tg-lightcenter\">$tmp</td>";
            $indCol++;
        }
        $buffer = $buffer . "<tr>";
        
        $buffer = $buffer . <<<EOT

          
         <th class="tg-lightcenter" colspan="50"  >Empty</th>
          
          </table>   

EOT;
        return $buffer;
    }    
    
    while ($table[$indLin][$indCol] != "") {
        $buffer = $buffer . "<tr>";
        while ($table[$indLin][$indCol] != "") {
            $tmp = $table[$indLin][$indCol];
            $indBuf = $table[$indLin][0];
            if ($indLin % 2 == 0)
                $buffer = $buffer . "<td id=\"$indBuf" . "#" . "$indCol\" class=\"tg-darkcenter\">$tmp</td>";
            else
                $buffer = $buffer . "<td id=\"$indBuf" . "#" . "$indCol\" class=\"tg-lightcenter\">$tmp</td>";
            $indCol++;
        }
        $buffer = $buffer . "<tr>";
        $indCol = $indStart;
        $indLin++;
    }
    $buffer = $buffer . <<<EOT
        </table>
EOT;

    return $buffer;
}
////////////////////////////////////////////////////////////////////////////////
function ShowDataTable($title, $table, $id = "",$indStart=0) {
    if ($table[0][0] == "")
        return "";
    //if (ismobile())
    //    $width = "97.5%";

    $buffer = <<<EOT
    <table style="width:90%">
EOT;
    $indLin = 0;
    $indCol = $indStart;
    
   
    while ($table[$indLin][$indCol] != "") {
        $buffer = $buffer . "<tr>";
        while ($table[$indLin][$indCol] != "") {
            $tmp = $table[$indLin][$indCol];
            $indBuf = $table[$indLin][0];
            if ($indLin % 2 == 0)
                $buffer = $buffer . "<td id=\"$indBuf" . "#" . "$indCol\" ><tx class=\"tx\">$tmp</tx></td>";
            else
                $buffer = $buffer . "<td id=\"$indBuf" . "#" . "$indCol\" ><tx class=\"tx\">$tmp</tx></td>";
            $indCol++;
        }
        $buffer = $buffer . "<tr>";
        $indCol = $indStart;
        $indLin++;
    }
    $buffer = $buffer . <<<EOT
        </table>
EOT;

    return $buffer;
}
////////////////////////////////////////////////////////////////////////////////
function ShowData($title, $table, $id = "",$indStart=0) {
    if ($table[0][0] == "")
        return "";
    //if (ismobile())
    //    $width = "97.5%";

    $buffer = <<<EOT
    <table style="width:90%">
EOT;
    $indLin = 0;
    $indCol = $indStart;
    
   
    $buffer = $buffer . "<tr>";
    while ($table[0][$indCol] != "") 
    {
        $tmp = $table[0][$indCol];
        $tmp2 = $table[1][$indCol];
        $buffer = $buffer . "<tr  ><tx class=\"txb\">$tmp</tx> <br> <tx class=\"tx\">$tmp2</tx><br></tr><br> \n";
        // $buffer = $buffer . "<tr  ><tx class=\"txb\">$tmp</tx> <br> $tmp2<br></tr><br>";
        $indCol++;
    }
    $buffer = $buffer . "<tr>";

    $buffer = $buffer . <<<EOT
        </table>
EOT;

    return $buffer;
}

////////////////////////////////////////////////////////////////////////////////
function MenuFloating() {
    echo <<<EOT
    <link rel="stylesheet" href="/ChildMonitor/Util/Util.min.css">
EOT;
    echo divSemSombra("<div onclick=\"smallmenuclick()\"  style=\"position:absolute; top:8px; left:8px; borderadius:30px; \" > <img src='Img/mobile_menu.png' alt='Menu' height='30' width='30'> </div>", $color = "linear-gradient(rgba(180,180,180,0.4), rgba(80,80,80,0.1))", $opacity = "", $top = "5px", $left = "5px", $width = "45px", $height = "45px", $borderadius = "30px", $z_index = "10", $position = "fixed", $id = "smallmenufloat");

    $buffer .= "<div id=\"menusData\" style=\"position:fixed; top:5px; left:5px;\" >";

    $buffer .= ItemMenuGlossy("Home", "id1", "index.php");
    if (isLogged()) {
        $useremail = $_SESSION['userEmail'];
        $buffer .= ItemMenuGlossy("Real Time Find", "id2", "mapaRT.php");
        $buffer .= ItemMenuGlossy("Locate", "id3", "mapa.php");
        $buffer .= ItemMenuGlossy("Places", "id4", "userPlaces.php");
        $buffer .= ItemMenuGlossy("Report", "id5", "userReport.php");
        $buffer .= ItemMenuGlossy("Profile", "id6", "userProfile.php");
        if ($_SESSION['AppLogin'] != 1) {
            $buffer .= ItemMenuGlossy("Logout [$useremail]", "id7", "formLogout.php");
        }
    } else {
        $buffer .= ItemMenuGlossy("Sign In", "id8", "formSignin.php");
        $buffer .= ItemMenuGlossy("Login", "id9", "formLogin.php");
    }
    $buffer .= "</div>";
    echo $buffer;

    echo <<<EOT
    <script>
       // document.getElementById("demo").innerHTML = "Hello JavaScript!";
      $("#menusData").hide();
       function smallmenuclick()
       {
          $("#smallmenufloat").hide();
          $("#menusData").show();
          
       }
    
    
        $("#smallmenufloat").hover(function(){
            $("#smallmenufloat").css("background", "linear-gradient(rgba(80,80,180,0.4), rgba(80,80,80,0.1))");
            }, function(){
            $("#smallmenufloat").css("background", "linear-gradient(rgba(180,180,180,0.4), rgba(80,80,80,0.1))");
        });  
    </script> 
EOT;
}

////////////////////////////////////////////////////////////////////////////////
function ItemMenuGlossy($caption, $id, $link, $color = "linear-gradient(rgba(80,80,80,0.8), rgba(80,80,80,0.2))") {
    $buffer .= <<<EOT
    <script>

       function menuclick$id()
       {
          $("#smallmenufloat").show();
          $("#menusData").hide();
          window.location.href='$link';   
       }

    </script> 
EOT;
    $buffer .= divSemSombra("<div id=\"$id\" onclick=\"menuclick$id()\" class=\"glass\" style=\"width:180; height:;\" > <txbutton  > $caption </txbutton> <br>  </div>", $color, $opacity = "", $top = "", $left = "", $width = "", $height = "", $borderadius = "4px", $z_index = "2000", $position = "relative");
    return($buffer);
}

////////////////////////////////////////////////////////////////////////////////
function divSemSombra($container, $color = "#163f29", $opacity = "0.6", $top = "140px", $left = "50%", $width = "48%", $height = "78%", $borderradius = "1px", $z_index = "10", $position = "absolute", $id = "") {
    $buffer .= <<<EOT
    <div  id="$id" style="  background: $color; position:$position; z-index:$z_index; 
       top:$top; left:$left; width:$width; height:$height; opacity:$opacity; border-radius: $borderradius; -moz-border-radius: $borderradius; "  > 
    <script>
       // document.getElementById("demo").innerHTML = "Hello JavaScript!";
    
    </script>         
EOT;
    $buffer .= $container;
    // <p style=" color: rgb(255,255,255); margin-top:0px; margin-botton:0px; " >Track, monitor, remeber and interact. The definitive way to protect our loving.<p>
    $buffer .= "</div>";
    return($buffer);
}

////////////////////////////////////////////////////////////////////////////////
function divTransparente($container, $color = "#163f29", $opacity = "0.6", $top = "140px", $left = "50%", $width = "48%", $height = "78%", $borderradius = "1px", $z_index = "10", $position = "absolute", $id = "") {
    echo <<<EOT
    <div id="$id" style=" box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); -moz-box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); -webkit-box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); background-color:$color; position:$position; z-index:$z_index; 
       top:$top; left:$left; width:$width; height:$height; opacity:$opacity; border-radius: $borderradius; -moz-border-radius: selectDevice; "  > 
EOT;
    echo $container;
    // <p style=" color: rgb(255,255,255); margin-top:0px; margin-botton:0px; " >Track, monitor, remeber and interact. The definitive way to protect our loving.<p>
    echo "</div>";
}

////////////////////////////////////////////////////////////////////////////////
// TODO db_querySLI para atualização do acesso ao BD
function db_querySLI($query, $trim = 1) {
    // Aqui você se conecta ao banco
    $mysqli = new mysqli('localhost', 'root', '', 'mydb');
    // Executa uma consulta que pega cinco notícias
    $sql = "SELECT `id`, `titulo` FROM `noticias` LIMIT 5";
    $query = $mysqli->query($sql);
    while ($dados = $query->mysqli_fetch_array()) {
      echo 'ID: ' . $dados['id'] . '';
      echo 'Título: ' . $dados['titulo'] . '';
    }
    echo 'Registros encontrados: ' . $query->num_rows;
}
////////////////////////////////////////////////////////////////////////////////
function db_query($query, $trim = 1) {
    require 'GlobalVars.php';


    
    // $connect = "host={$host} dbname={$dbname} user={$user} password={$password}";
    // dbg($query);
    $link = mysql_connect($host, $user, $password);
    if (!$link)
        dbg("Erro mysql_connect");

    mysql_select_db($dbname);
    
    // dbg($query);

    $result = mysql_query($query, $link);
    // dbg($query);
    // if(!$result) dbg("Erro pg_exec");
    
    $numrows = mysql_num_rows($result);
    $numcols = mysql_num_fields($result);
    for ($i = 0; $i < $numcols; $i++) {
        $resultado[0][$i] = esptrim(mysql_field_name($result, $i), $trim);
    }
    $il = 1;
    while ($row = mysql_fetch_row($result)) {
        for ($i = 0; $i < $numcols; $i++) {
            $resultado[$il][$i] = esptrim($row[$i], $trim);
        }
        $il++;
    }
    /*
      dbg($resultado[0][0]);
      dbg($resultado[0][1]);

      dbg($resultado[1][0]);
      dbg($resultado[1][1]);
     */

    mysql_free_result($result);
    mysql_close($link);

    return $resultado;
}

////////////////////////////////////////////////////////////////////////////////
function db_query_log($query) {
    require 'GlobalVars.php';
    // $connect = "host={$host} dbname={$dbname} user={$user} password={$password}";
    // dbg($query);
    $linkop = mysql_connect($host, $user, $password);
    if (!$linkop)
        echo "mysql_connect error<br>";

    $result = mysql_select_db($dbname);
    mysql_query($query, $linkop);
    
    mysql_free_result($result);
    mysql_close($linkop);
}

////////////////////////////////////////////////////////////////////////////////
function esptrim($var, $trim = 1) {
    if ($trim == 0)
        return trim($var);
    if (trim($var) == "")
        return " ";
    else
        return(trim($var));
}

////////////////////////////////////////////////////////////////////////////////
/*
  function db_query_postgres($query)
  {
  require 'GlobalVars.php';

  $connect = "host={$host} dbname={$dbname} user={$user} password={$password}";
  $link = pg_connect($connect);
  // if(!$link) dbg("Erro pg_connect");
  $result = pg_exec($link,$query);
  // if(!$result) dbg("Erro pg_exec");
  $numrows = pg_numrows($result);
  $numcols = pg_numfields($result);

  for($i = 0; $i < $numcols; $i++)
  {
  $resultado[0][$i]=trim(pg_field_name($result,$i));
  }
  $il=1;
  while ($row = pg_fetch_row($result)) {
  for ($i = 0; $i < $numcols; $i++) {
  $resultado[$il][$i] = trim($row[$i]);
  }
  $il++;
  }
  //dbg($resultado[0][2]);
  //dbg($resultado[1][2]);
  //dbg($resultado[2][2]);
  //dbg($resultado[3][2]);
  //dbg($resultado[4][2]);
  return $resultado;
  }
 */
////////////////////////////////////////////////////////////////////////////////
/*
  function db_execquery($query) {
  global $e,$host,$port,$dbname,$user,$password;

  $connect="pgsql:host={$host};port={$port};dbname={$dbname}";
  $dbh = new PDO($connect, $user, $password);
  if(!$dbh) dbg("Erro PDO($connect, $user, $password)");
  // use the conne"ction here
  // echo "==== {$dbh}";
  $sth = $dbh->query($query);
  if(!$sth) dbg("Erro $dbh->query()");
  $numrows = $sth->rowCount();
  // $numcols = $sth->;
  $sth->setFetchMode(PDO::FETCH_ASSOC);
  while ($linha = $sth->fetch(PDO::FETCH_ASSOC))
  {

  // aqui eu mostro os valores de minha consulta
  $linha = $sth->fetch();
  echo "Nome: {$linha[0]} - Usuário: {$linha[1]}<br/>";
  }

  // and now we're done; close it
  $dbh = null;
  // echo $e->getMessage();
  }
 * 
 */
////////////////////////////////////////////////////////////////////////////////

/*
 * http://stackoverflow.com/questions/4163010/how-use-json-in-php-with-a-complex-structure
  $operations = json_decode($data);
  foreach ($operations as $op) {
  foreach ($op->bases as $base) {
  //Logic goes here
  }
  }
 */
////////////////////////////////////////////////////////////////////////////////
class PosInfo {

    public $longitude = 0.0;
    public $latitude = 0.0;
    public $bearing = 0.0;
    public $BatteryLevel = 0.0;
    public $mph = 0.0;
    public $email = "";
    public $currTime = "";
    public $idDevice = "";
    public $ProtocolVersion = 0;
    public $accuracy = 0.0;
}

////////////////////////////////////////////////////////////////////////////////
function SendGlobalPositionArray() {
    if ($_GET['JSON'] != null) {
        $sjson = $_GET['JSON'];
    } else {
        $sjson = $_POST['JSON'];
    }
    // insert into "MonitoredThings" (id, "Lat", "Long", "TimeStamp", "Velocity") values( 12,-12.34,-43.19,2000-11-11 13:23:02,3)
    // http://localhost/ChildMonitor/Util/SendGlobalPosition.php?id=12&Lat=-12.34&Long=-43.09&TimeStamp=%222000-11-11+13%3A23%3A02%22&Velocity=3


    $data = json_decode($sjson);
    // echo "Teste\n";
    // dbglog("Json");
    // dbglog($sjson);

    // var_dump($operations);
//    dbg($data[0]->longitude);
//    dbg($data[0]->latitude);
//    dbg($data[0]->bearing);
//    dbg($data[0]->BatteryLevel);
//    dbg($data[0]->mph);
//    dbg($data[0]->email);
//    dbg($data[0]->currTime);
//    dbg($data[0]->idDevice);
//    dbg($data[0]->ProtocolVersion);        
    $i = 0;
    while (1) {

        SendGlobalPositionPar($data[$i]);
        $i++;

        if ($data[$i]->currTime == "")
            break;
    }
}

////////////////////////////////////////////////////////////////////////////////
function SendGlobalPositionPar($dataPos) {
    $email = $dataPos->email;
    $idDevice = $dataPos->idDevice;
    $Lat = $dataPos->latitude;
    $Long = $dataPos->longitude;
    $TimeStamp = $dataPos->currTime;
    $Velocity = $dataPos->mph;
    $Bearing = $dataPos->bearing;
    $BatteryLevel = $dataPos->BatteryLevel;
    $Accuracy = $dataPos->accuracy;
    
    if($Accuracy=="")
        $Accuracy=0.0;


    $resultado = db_query("select id from User where email='$email'");
    if ($resultado[1][0] == "")
        return;
    $idUser = $resultado[1][0];

    $query = "insert into MonitoredThings (idDevice,idUser,Bearing,BatteryLevel,Lat, Longc, TimeStampc, Velocity,Accuracy,offLineData) values( '$idDevice',$idUser,$Bearing,$BatteryLevel,$Lat,$Long,'$TimeStamp',$Velocity,$Accuracy,1)";
    dbg($query);
    db_query($query);

    EventsManagerPar($idUser, $dataPos);
}    
////////////////////////////////////////////////////////////////////////////////
function EventsManagerPar($idUser, $dataPos) {
    $email = $dataPos->email;
    $idDevice = $dataPos->idDevice;
    $Lat = $dataPos->latitude;
    $Long = $dataPos->longitude;
    $TimeStamp = $dataPos->currTime;
    $Velocity = $dataPos->mph;
    $Bearing = $dataPos->bearing;
    $BatteryLevel = $dataPos->BatteryLevel;
    $Accuracy = $dataPos->accuracy;
    /*
      DELIMITER $$
      --
      -- Functions
      --
      CREATE DEFINER=`root`@`localhost` FUNCTION `haversine`(`lat1` FLOAT, `lon1` FLOAT, `lat2` FLOAT, `lon2` FLOAT) RETURNS float
      NO SQL
      return 6371000.00 * (ACOS(COS(RADIANS(lat1))
     * COS(RADIANS(lat2))
     * COS(RADIANS(lon1) - RADIANS(lon2))
      + SIN(RADIANS(lat1))
     * SIN(RADIANS(lat2))))$$

      DELIMITER ;


     * 
     * 
     * select haversine(-22.904748, -43.190300,-22.901081, -43.179150) = 1212.7547511759801 metros
     *   
     * SELECT `UserId`, `aLat`, `aLong`, `Radius`, `Address` FROM `MonitoredRegion` WHERE haversine(aLat,ALong,lat1,lon2)<Radius
     */

    $sql = "SELECT `UserId`, `aLat`, `aLong`, `Radius`, `Address` FROM `MonitoredRegion` WHERE UserId=$idUser and haversine(aLat,aLong,$Lat,$Long)<=Radius";
    // echo $sql;
    $resultado = db_query($sql);

    $ind = 1;
    while ($resultado[$ind][0] != "") {
        $Address = $resultado[$ind][4];
        if (!DeviceInsideRegion($idUser, $idDevice, $Address)) {
            // Insert event on table Events (User became inside a region)

            $sql = "INSERT INTO `Events`(`UserId`, `idDevice`, `Description`, `TimeStamp`) VALUES ($idUser,'$idDevice','User or Device Inside Region: $Address','$TimeStamp')";
            // echo $sql;
            db_query($sql);
            SetFlagDeviceInsideRegion($idUser, $idDevice, $Address, $TimeStamp, true);
        }
        $ind++;
    }
    // When we need to find outside region event?
    $sql = "SELECT `UserId`, `aLat`, `aLong`, `Radius`, `Address` FROM `MonitoredRegion` WHERE UserId=$idUser and haversine(aLat,aLong,$Lat,$Long)>Radius";
    // dbg($sql);
    $resultado = db_query($sql);
    $ind = 1;
    while ($resultado[$ind][0] != "") {
        $Address = $resultado[$ind][4];
        if (DeviceInsideRegion($idUser, $idDevice, $Address)) {
            // Insert event on table Events (User became outside a region)
            $sql = "INSERT INTO `Events`(`UserId`, `idDevice`, `Description`, `TimeStamp`) VALUES ($idUser,'$idDevice','User or Device Outside Region: $Address','$TimeStamp')";
            // echo $sql;
            db_query($sql);
            SetFlagDeviceInsideRegion($idUser, $idDevice, $Address, $TimeStamp, false);
        }
        $ind++;
    }
}

////////////////////////////////////////////////////////////////////////////////
function SendGlobalPosition() {
    if ($_GET['id'] != null) {
        $email = $_GET['email'];
        $idDevice = $_GET['idDevice'];
        $Lat = $_GET['Lat'];
        $Long = $_GET['Long'];
        $TimeStamp = $_GET['TimeStamp'];
        $Velocity = $_GET['Velocity'];
        $Bearing = $_GET['Bearing'];
        $BatteryLevel = $_GET['BatteryLevel'];
        $Accuracy = $_GET['accuracy'];
    } else {
        $email = $_POST['email'];
        $idDevice = $_POST['idDevice'];
        $Lat = $_POST['Lat'];
        $Long = $_POST['Long'];
        $TimeStamp = $_POST['TimeStamp'];
        $Velocity = $_POST['Velocity'];
        $Bearing = $_POST['Bearing'];
        $BatteryLevel = $_POST['BatteryLevel'];
        $Accuracy = $_POST['accuracy'];
    }

    // dbglog("Email antes ".$email );
    
    if($Accuracy=="")
        $Accuracy=0.0;

    
    
    // insert into "MonitoredThings" (id, "Lat", "Long", "TimeStamp", "Velocity") values( 12,-12.34,-43.19,2000-11-11 13:23:02,3)
    // http://localhost/ChildMonitor/Util/SendGlobalPosition.php?id=12&Lat=-12.34&Long=-43.09&TimeStamp=%222000-11-11+13%3A23%3A02%22&Velocity=3
    // $resultado=db_query("select id from \"User\" where email='$email'"); // postgres
    $resultado = db_query("select id from User where email='$email'");
    if ($resultado[1][0] == "")
        return;
    $idUser = $resultado[1][0];
  
    // dbglog("Accuracy ok".$Accuracy );
    $query = "insert into MonitoredThings (idDevice,idUser,Bearing,BatteryLevel,Lat, Longc, TimeStampc, Velocity,Accuracy,offLineData) values( '$idDevice',$idUser,$Bearing,$BatteryLevel,$Lat,$Long,'$TimeStamp',$Velocity,$Accuracy,0)";
    // echo $query;
    
    db_query($query);
    // dbglog($query );
    EventsManager($idUser);
    
    
    MessagesAndConfiguration2Devices($email,$idUser,$idDevice);
}
////////////////////////////////////////////////////////////////////////////////
class Msg2Devices
{
   public $Action=0; // 0 - Nothing; 1 - Update Gps timer; 2 - Update Calendar Actions; 3 - Msg
   public $Msg="";
   public $GpsTimer=40000; // 40 seconds
}
////////////////////////////////////////////////////////////////////////////////
function MessagesAndConfiguration2Devices($email,$idUser,$idDevice)
{
    $response= new Msg2Devices();
    $response->Action=0;
    $response->Msg="";
    echo json_encode($response);
}
////////////////////////////////////////////////////////////////////////////////
function EventsManager($idUser) {
    if ($_GET['id'] != null) {
        $email = $_GET['email'];
        $idDevice = $_GET['idDevice'];
        $Lat = $_GET['Lat'];
        $Long = $_GET['Long'];
        $TimeStamp = $_GET['TimeStamp'];
        $Velocity = $_GET['Velocity'];
        $Bearing = $_GET['Bearing'];
        $BatteryLevel = $_GET['BatteryLevel'];
    } else {
        $email = $_POST['email'];
        $idDevice = $_POST['idDevice'];
        $Lat = $_POST['Lat'];
        $Long = $_POST['Long'];
        $TimeStamp = $_POST['TimeStamp'];
        $Velocity = $_POST['Velocity'];
        $Bearing = $_POST['Bearing'];
        $BatteryLevel = $_POST['BatteryLevel'];
    }

    /*
      DELIMITER $$
      --
      -- Functions
      --
      CREATE DEFINER=`root`@`localhost` FUNCTION `haversine`(`lat1` FLOAT, `lon1` FLOAT, `lat2` FLOAT, `lon2` FLOAT) RETURNS float
      NO SQL
      return 6371000.00 * (ACOS(COS(RADIANS(lat1))
     * COS(RADIANS(lat2))
     * COS(RADIANS(lon1) - RADIANS(lon2))
      + SIN(RADIANS(lat1))
     * SIN(RADIANS(lat2))))$$

      DELIMITER ;


     * 
     * 
     * select haversine(-22.915439, -43.092436,-22.915276, -43.091138)
     * 
     * SELECT `UserId`, `aLat`, `aLong`, `Radius`, `Address` FROM `MonitoredRegion` WHERE haversine(aLat,ALong,lat1,lon2)<Radius
     */

    $sql = "SELECT `UserId`, `aLat`, `aLong`, `Radius`, `Address` FROM `MonitoredRegion` WHERE UserId=$idUser and haversine(aLat,aLong,$Lat,$Long)<=Radius";
    // echo $sql;
    $resultado = db_query($sql);

    $ind = 1;
    while ($resultado[$ind][0] != "") {
        $Address = $resultado[$ind][4];
        if (!DeviceInsideRegion($idUser, $idDevice, $Address)) {
            // Insert event on table Events (User became inside a region)

            $sql = "INSERT INTO `Events`(`UserId`, `idDevice`, `Description`, `TimeStamp`) VALUES ($idUser,'$idDevice','User or Device Inside Region: $Address','$TimeStamp')";
            // echo $sql;
            db_query($sql);
            SetFlagDeviceInsideRegion($idUser, $idDevice, $Address, $TimeStamp, true);
        }
        $ind++;
    }
    // When we need to find outside region event?
    $sql = "SELECT `UserId`, `aLat`, `aLong`, `Radius`, `Address` FROM `MonitoredRegion` WHERE UserId=$idUser and haversine(aLat,aLong,$Lat,$Long)>Radius";
    // dbg($sql);
    $resultado = db_query($sql);
    $ind = 1;
    while ($resultado[$ind][0] != "") {
        $Address = $resultado[$ind][4];
        if (DeviceInsideRegion($idUser, $idDevice, $Address)) {
            // Insert event on table Events (User became outside a region)
            $sql = "INSERT INTO `Events`(`UserId`, `idDevice`, `Description`, `TimeStamp`) VALUES ($idUser,'$idDevice','User or Device Outside Region: $Address','$TimeStamp')";
            // echo $sql;
            db_query($sql);
            SetFlagDeviceInsideRegion($idUser, $idDevice, $Address, $TimeStamp, false);
        }
        $ind++;
    }
}

////////////////////////////////////////////////////////////////////////////////
function DeviceInsideRegion($idUser, $idDevice, $idRegion) {
    $sql = "SELECT `UserId`, `idDevice`, `Event`, `TimeStamp` FROM `EventsFlagsDevice` WHERE UserId=$idUser and idDevice='$idDevice' and Event = '$idRegion'";
    // echo $sql;
    $resultado = db_query($sql);
    if ($resultado[1][0] != "")
        return true;
    else
        return false;
}

////////////////////////////////////////////////////////////////////////////////
function SetFlagDeviceInsideRegion($idUser, $idDevice, $idRegion, $TimeStamp, $bool) {
    if ($bool) {
        $sql = "INSERT INTO `EventsFlagsDevice`(`UserId`, `idDevice`, `Event`, `TimeStamp`) VALUES ($idUser,'$idDevice','$idRegion','$TimeStamp')";
        // echo $sql;
        db_query($sql);
    } else {
        $sql = "DELETE FROM `EventsFlagsDevice` WHERE UserId=$idUser and idDevice='$idDevice' and Event = '$idRegion'";
        // echo $sql;
        db_query($sql);
    }
}

////////////////////////////////////////////////////////////////////////////////
function DataInterchange() {
    if ($_GET['id'] != null) {
        $Email = $_GET['Email'];
        $Password = $_GET['Password'];
        $IdDevice = $_GET['IdDevice'];
        $TimeStamp = $_GET['TimeStamp'];
        $AppVersion = $_GET['AppVersion'];
    } else {
        $Email = $_POST['Email'];
        $Password = $_POST['Password'];
        $IdDevice = $_POST['IdDevice'];
        $TimeStamp = $_POST['TimeStamp'];
        $AppVersion = $_POST['AppVersion'];
    }
    // insert into "MonitoredThings" (id, "Lat", "Long", "TimeStamp", "Velocity") values( 12,-12.34,-43.19,2000-11-11 13:23:02,3)
    // http://localhost/ChildMonitor/Util/SendGlobalPosition.php?id=12&Lat=-12.34&Long=-43.09&TimeStamp=%222000-11-11+13%3A23%3A02%22&Velocity=3

    if ($AppVersion < 1.00) {
        // echo "Update App - Close";
        return;
    }

    // $sql="select email,id from \"User\" where email='$Email' and password='$Password'"; // postgres
    $sql = "select email,id from User where email='$Email'";
    $resultado = db_query($sql);
    if ($resultado[1][0] == "") {
        echo "Invalid email";
        return;
    }
    $id = $resultado[1][1];

    if ($Password != hash('sha256', $Email . "MobileChildMonitor")) {
        echo "Invalid password";
        return;
    }
    // $sql="select id from \"UserMonitThings\" where \"UserId\"=$id and id='$IdDevice'"; // postgres
    $sql = "select id from UserMonitThings where UserId=$id and id='$IdDevice'";
    $resultado = db_query($sql);
    if ($resultado[1][0] != "") {
        echo "Device already registred";
        return;
    }


    // $query="insert into \"UserMonitThings\" (\"UserId\", id, \"Label\", \"SysDescript\", \"TimeStamp\") values( $id,'$IdDevice','','','$TimeStamp')"; //postgres
    $query = "insert into UserMonitThings (UserId, id, Label, SysDescript, TimeStamp) values( $id,'$IdDevice','$IdDevice','','$TimeStamp')";
    // error_log($query.PHP_EOL, 3, "/Library/WebServer/Documents/ChildMonitor/php-errors.log");
    // dbg($query);
    $resultado = db_query($query);
    echo "Ok";
}

////////////////////////////////////////////////////////////////////////////////
function DeviceRegistration() {
    if ($_GET['id'] != null) {
        $Email = $_GET['Email'];
        $Password = $_GET['Password'];
        $IdDevice = $_GET['IdDevice'];
        $TimeStamp = $_GET['TimeStamp'];
        $AppFlavour = $_GET['AppFlavour'];
        $PhoneNumber = $_GET['PhoneNumber'];
    } else {
        $Email = $_POST['Email'];
        $Password = $_POST['Password'];
        $IdDevice = $_POST['IdDevice'];
        $TimeStamp = $_POST['TimeStamp'];
        $AppFlavour = $_POST['AppFlavour'];
        $PhoneNumber = $_POST['PhoneNumber'];
    }
    // insert into "MonitoredThings" (id, "Lat", "Long", "TimeStamp", "Velocity") values( 12,-12.34,-43.19,2000-11-11 13:23:02,3)
    // http://localhost/ChildMonitor/Util/SendGlobalPosition.php?id=12&Lat=-12.34&Long=-43.09&TimeStamp=%222000-11-11+13%3A23%3A02%22&Velocity=3
    // $sql="select email,id from \"User\" where email='$Email' and password='$Password'"; // postgres
    $sql = "select email,id from User where email='$Email'";
    $resultado = db_query($sql);
    if ($resultado[1][0] == "") {
        echo "Invalid email";
        return;
    }
    $id = $resultado[1][1];

    if ($Password != hash('sha256', $Email . "MobileChildMonitor")) {
        echo "Invalid password";
        return;
    }
    // $sql="select id from \"UserMonitThings\" where \"UserId\"=$id and id='$IdDevice'"; // postgres
    $sql = "select id from UserMonitThings where UserId=$id and id='$IdDevice'";
    $resultado = db_query($sql);
    if ($resultado[1][0] != "") {
        echo "Device already registred";
        return;
    }
    $label=$IdDevice;
    $img="";
    $truckImageDefault = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCABbAKADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD0CiiigAoopKAFopu4ZxmlyKAFopu4etZ13rllabjI7kK2xtqMefyoA06K56XxXFgm3srmUAZycL/XP6VX/wCEov3njjXSdpdtvzSn0z/doA6misIeI/KZheWU0IXqQQwH54q5FrmnS4zcCM+kgK/zoA0aKjjmjlUNG6up7qcin0ALRSUtABRRRQAUUUUAFFFFABSUHgVh+Itdj0qKLMc8jSH5REMfr6UAauAJXIPU0+uDk8aXe8mOw25P8Uo/limr431I8LZ22f8AgR/rTA73cOcEcVjXkafbWG0c5J469KpQa60U0El3GI2uBtxHypfcRxnmrV9ME1AJJwxXgeoPf9DTELgVE3F5a/7/APSpM1GxIubf/f8A6UDNtkVgQQCG6+9QS2NtKdzwRs3qVBq1SUgMhtBswVaNXRlGAVY5+v1oFlf26/6NqMucc+Z8+fzzitfFIRQBlfbNagwGhgnAAyRwSfz/AKVMuu7X23NlcRYzlgNy8e/FXSoPYUhQE570WAZFrWnyruF0iAf89Mp/Orscscq7o3V1PdTkVRktopDl40Y+6g1TfRrF2VjAMqMLgniiwG5S1kWlo9vfI4uZ3Rsjy3kLDp6VrUgFpKK5vxXrX9myQRGHzRIM43Ec0Ab80qIjDzFDYOAWArAuba4u9UikEAeKGIqv7xSCxPpn0rCfxQVjGy1jIxkj/wDXmut0aQT6dBc7VR5E3HaAOvaq2Fuc1ceEdTupGl3WceSSEDMAPb7tRQ+DNTikVnlsyoOcCRv/AImu6wTSMpApXCyOG1Lw9qU2rWPlBGht1Xc5bjdvLHjr3wOK0dVcvrUBwQUAQn17/wDs1bdkkRAmhVkWRFIUnoP8mud1+QG8ZhnDHgnvlFpDNEGo5G/fwf8AXT+hrmkyykSAcN2OfWrunFUvI9oAyeaYHcUVH58W4jzUz/vCl82M/wDLRP8AvoUAOopvmIejr+dKCD0INABRS4pKAEopaSgAT/XR/wC9/Q1bqqv+tj+v9DVmkBHcTCCB5WGQik4rgvFkk2p3ds0MDOEU52DIHNd5doXtZVUZJQgD8K4u9sUeIrbpEj/7a5GffGKLgc2NOvNq5t3A6c4/z0rs9L1mOy0uC3LRCWNApDN3rGgsLhHUu9ttB5Cx9R+dakUcSgAItFx2Nix1uGdSZp7dOmPnA/ma57WfFd4t0VsZYYo1bC7xkyD1Oegq3fyi3sJZfMRRGhIyep7D88VnaUtpcacY5Y1cyKckkHJzx6envjHGMnIgZq6d4iSTT4pp4CknKOEIChh9T6EYqpKLXUb6d7qa4jMrjyo4XGTgYyM8DIHI9qxtHv7eyiuIZ3xllCsQCON3P8q0jrdgCPLuY93b5Mf0pAZF1YyJqk9v9rnto8BoDNtOR33YPr6Zpj2mo26edFqcDFT1Vs4/8dpNX1wS3UTQlXKA5OeOccfpVX+0ftMkccqbIvMG/ae2R/8AXpoRKusakMK+oybywACQIwI78nHPXjFaWnz3t9FO0Wt4mhyxhlt0Tcg6tkZ6DnH/AOus/V7c6bPbhipkKZPOTjJxn3xitbTrCJ7uziE7RxXu9MoVLBdu4A5BH3l44yKBlzRbPVtQeYXGoiNYnKHbEpJIz7AY4qUDyVvJJtbt8QkiNSEzIAoOeD6kj8Petbw5ZSW1tcw3KSj98+0yH76noeO/vWfJ8PtHeRmE12gJ4VZFwPblc0COUXxVqKj/AJY9M/dP+NPbxhf/AMIi7DGG9Oe/rWRqdu+nalcWbJxE7IofuueD+IwajsoJLyeK0itlMkrqA4DZHP1x+lAHRweJdYkRpFSLauB/FySen3qeninVnaTZHGwTnO4jj86z/EFlcaJftZLK7wlQ6OVA3cc9B65FXYby3l1yGRI5nt2jVXxHyWx0/pTA6Pw5qepahcxm5tZUgILCXBKNx611NQWFrHZ2UVvEu1UXGMk89T+tWKQCGsTxFo7avaFYcw3CnKSg/ofUVt0UAec2/grXfOUXF4qxZ+Yxykkj2rs0sY4o1RNMhIUYBZgT+JxzWnRQBg6to7alp8lstnbwMcFJFYZUjv8Ad/D6E1z9n4R12A7BPaIufvhySoPXHy/4V31FAHMXvg63vbG2tmYRm3BAkT7zZ5Oc8deelVbf4fWEcgaaeaZR/AxAH6AGuxooA5L/AIV7pe7PnXGM527hj+VA+H2mhiftd5g9gyf/ABNddRQBzI8DaUQvmvdSlRjLSf4CtLTvD+naac21v8/952LEfTPT8K1KKAG7FH8I/KjavoPyp1FACYHpRS0UAJRilooASloooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/9k=";

    if($AppFlavour=="TTM" )
    {    
        $img = $truckImageDefault;
        $label = "Vehicle_".  substr($IdDevice, strlen($IdDevice)-4,4);
    }    
    // $query="insert into \"UserMonitThings\" (\"UserId\", id, \"Label\", \"SysDescript\", \"TimeStamp\") values( $id,'$IdDevice','','','$TimeStamp')"; //postgres
    $query = "insert into UserMonitThings (UserId, id, Label, SysDescript, TimeStamp,Image,PhoneNumber) values( $id,'$IdDevice','$label','','$TimeStamp','$img','$PhoneNumber')";
    // error_log($query.PHP_EOL, 3, "/Library/WebServer/Documents/ChildMonitor/php-errors.log");
    // dbg($query);
    $resultado = db_query($query);
    echo "Ok";
}

////////////////////////////////////////////////////////////////////////////////
function MenuPrincipal() {
    require 'GlobalVars.php';

    if ($AppTheme == "green")
        return MenuPrincipalGreen();
    if ($AppTheme == "myfather")
        return MenuPrincipalMyFather();
    if ($AppTheme == "listaescolar")
        return MenuPrincipalListaEscolar();
    if ( $AppName=="EstControl")
        return MenuPrincipalEstControl();
    if ($AppTheme == "truck")
        return MenuPrincipalTruck();
}

////////////////////////////////////////////////////////////////////////////////
function MenuPrincipalGreen() {
    require 'GlobalVars.php';

    // divGradBack();
    SetCookieTZO();
    VerifyMobileLogin();

    echo FaceBookImgInterface();

    if (ismobile()) {
        $MsgHeader = "Child Monitor";
        $showSubHeader = false;
    } else
        $MsgHeader = "Child Monitor - Don't Worry, We Help You";
    // MenuPrincipalDetail();
    echo <<<EOT
    <!DOCTYPE html>
    <html lang='' manifest="http://trucktrackweb.com/appcache.mf" >
    <head>
       <meta charset='utf-8'>
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="stylesheet" href="/ChildMonitor/Menus/cssmenu/styles.css">
       <link rel="stylesheet" href="/ChildMonitor/Util/Util.min.css">
       <script src="/ChildMonitor/Util/Jquery/jquery-latest.min.js" type="text/javascript"></script>
       <script type="text/javascript" src="/ChildMonitor/Util/Jquery/jquery.min.js"></script>  
       <script src="/ChildMonitor/Menus/cssmenu/script.js"></script>
       <title>$MsgHeader</title>
    </head>
    <body style="margin:0px; " >
            
EOT;
    FaceBookSuport();
    // GetFaceBookData();
    if ($showSubHeader) {
        $btnFB = FBLikeButton();
        echo <<<EOT
        <!-- <div style="float: left;" > <img src="Img/Logo.png"  height="60" width="60" alt="Logo"> </div> -->
        <div style=" font-size:16px; text-align:center; color:#FFF; font-weight:Light;  height:35px; padding-top:25px;"> $btnFB $MsgHeader  </div>
EOT;
    }
    MenuPrincipalDetail();
    
    echo <<<EOT
    </body>
    </html>    
EOT;
}

////////////////////////////////////////////////////////////////////////////////
function MenuPrincipalMyFather() {
    require 'GlobalVars.php';
    // divGradBack();
    SetCookieTZO();
    VerifyMobileLogin();

    echo FaceBookImgInterface();

    if (ismobile()) {
        $MsgHeader = "My Father";
        $showSubHeader = false;
    } else
        $MsgHeader = "My Father - Alzheimer Tracking and Monitoring System";
    // MenuPrincipalDetail();
    echo <<<EOT
    <!DOCTYPE html>
    <html lang='' manifest="http://trucktrackweb.com/appcache.mf" >
    <head>
       <meta charset='utf-8'>
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="stylesheet" href="Menus/cssmenu_myfather/styles.css">
       <link rel="stylesheet" href="/ChildMonitor/Util/Util.min.css">
       <script src="/ChildMonitor/Util/Jquery/jquery-latest.min.js" type="text/javascript"></script>
       <script type="text/javascript" src="/ChildMonitor/Util/Jquery/jquery.min.js"></script>  
       <script src="Menus/cssmenu_myfather/script.js"></script>
       <title>$MsgHeader</title>
    </head>
    <body style="margin:0px;" >
            
EOT;
    FaceBookSuport();
    // GetFaceBookData();
    if ($showSubHeader) {
        $btnFB = FBLikeButton();
        echo <<<EOT
        <!-- <div style="float: left;" > <img src="Img/Logo.png"  height="60" width="60" alt="Logo"> </div> -->
        <div style=" background-image:url(Img/BackClouds.png);no-repeat; background-size:cover;  font-size:16px; text-align:center; color:#FFF; font-weight:Light; height:30px; padding-top:25px;"> $btnFB $MsgHeader  </div>
EOT;
    }
    MenuPrincipalDetail();
    echo <<<EOT
    </body>
    </html>    
EOT;
}

////////////////////////////////////////////////////////////////////////////////
function MenuPrincipalListaEscolar() {
    require 'GlobalVars.php';
    // divGradBack();
    SetCookieTZO();
    VerifyMobileLogin();

    echo FaceBookImgInterface();

    if (ismobile()) {
        $MsgHeader = "My Father";
        $showSubHeader = false;
    } else
        $MsgHeader = "Lista Escolar - Compre seu material em um só lugar!";

    echo <<<EOT
    <!DOCTYPE html>
    <html lang=''>
    <head>
       <meta charset='utf-8'>
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="stylesheet" href="Menus/cssmenu_myfather/styles.css">
       <script src="/ChildMonitor/Util/Jquery/jquery-latest.min.js" type="text/javascript"></script>
       <script type="text/javascript" src="/ChildMonitor/Util/Jquery/jquery.min.js"></script>  
       <script src="Menus/cssmenu_myfather/script.js"></script>
       <title>$MsgHeader</title>
    </head>
    <body style="margin:0px;" >
            
EOT;
    FaceBookSuport();
    // GetFaceBookData();
    if ($showSubHeader) {
        $btnFB = FBLikeButton();
        echo <<<EOT
        <!-- <div style="float: left;" > <img src="Img/Logo.png"  height="60" width="60" alt="Logo"> </div> -->
        <div style=" background-image:url(Img/BackClouds.png);no-repeat; background-size:cover;  font-size:16px; text-align:center; color:#FFF; font-weight:Light; height:30px; padding-top:25px;"> $btnFB $MsgHeader  </div>
EOT;
    }
    MenuPrincipalDetailListaEscolar();
    echo <<<EOT
    </body>
    </html>    
EOT;
}

////////////////////////////////////////////////////////////////////////////////
function MenuPrincipalTruck() {
    require 'GlobalVars.php';
    // divGradBack();
    SetCookieTZO();
    VerifyMobileLogin();

    echo FaceBookImgInterface();

    if (ismobile()) {
        $MsgHeader = "Truck Track";
        $showSubHeader = false;
    } else
        $MsgHeader = "Truck Track - Tracking and Monitoring System";

    echo <<<EOT
    <!DOCTYPE html>
    <html lang=''manifest="/ChildMonitor/appcache.mf" >
    <head>
       <meta charset='utf-8'>
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="stylesheet" href="Menus/cssmenu_truck/styles.min.css">
       <link rel="stylesheet" href="/ChildMonitor/Util/Util.min.css">
       <script src="/ChildMonitor/Util/Jquery/jquery-latest.min.js" type="text/javascript"></script>
       <script type="text/javascript" src="/ChildMonitor/Util/Jquery/jquery.min.js"></script>  
       <script src="Menus/cssmenu_truck/script.min.js"></script>
       <title>$MsgHeader</title>
    </head>
    <body style=" position: fixed; margin:0px;  top:0px; left:0px; " >
            
EOT;
    // FaceBookSuport();
    // GetFaceBookData();
    if ($showSubHeader) {
        // $btnFB = FBLikeButton();
        echo <<<EOT
        <!-- <div style="float: left;" > <img src="Img/Logo.png"  height="60" width="60" alt="Logo"> </div> -->
        <div style=" background-image:url(Img/BackMetalLow.jpg);no-repeat; background-size:100%;  font-size:16px; text-align:center; color:#FFF; font-weight:Light; height:30px; padding-top:25px;"> $btnFB $MsgHeader  </div>
EOT;
    }
    MenuPrincipalDetail();
    echo <<<EOT
    </body>
    </html>    
EOT;
}
////////////////////////////////////////////////////////////////////////////////
function MenuPrincipalEstControl() {
    require 'GlobalVars.php';
    // divGradBack();
    SetCookieTZO();
    VerifyMobileLogin();

    // echo FaceBookImgInterface();


    $MsgHeader = "ParkingFit";
    
//      <script src="Menus/cssmenu/jquery-latest.min.js" type="text/javascript"></script>
//      <script type="text/javascript" src="Menus/cssmenu/jquery.min.js"></script>  

    echo <<<EOT
    <!DOCTYPE html>
    <html lang=''manifest="/ChildMonitor/appcache.mf" >
    <head>
       <meta charset='utf-8'>
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="stylesheet" href="Menus/cssmenu_truck/styles.min.css">
       <link rel="stylesheet" href="/ChildMonitor/Util/Util.min.css">
       
       
       <script type="text/javascript" src="/ChildMonitor/Util/Jquery/jquery.min.js"></script>

       <script src="Menus/cssmenu_truck/script.min.js"></script>

       <title>$MsgHeader</title>
    </head>
     <body style=" position: fixed; margin:0px;  top:0px; left:0px; " >   
EOT;
   //    <script src="/ChildMonitor/Util/Jquery/jquery-latest.min.js" type="text/javascript"></script> 
    MenuPrincipalDetailParkingFit();
    

//       $.ajax({
//        async: false,
//        url: "/ChildMonitor/Util/fingerprint2.min.js",
//        dataType: "script"
//    });
    echo HeadersMap();
    echo <<<EOT
    <link rel="stylesheet" href="/ChildMonitor/Menus/jquery-ui_$AppTheme/jquery-ui.min.css">
    <script src="/ChildMonitor/Menus/jquery-ui_$AppTheme/jquery-ui.min.js"></script>   
    <link type="text/css" href="/ChildMonitor/Util/toastr.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="/ChildMonitor/Util/toastr.min.js"></script>   
    <script src="/ChildMonitor/Util/Util.js"> </script>
    <script>
    
    function CloseMenu()
    {
        if ($(window).width() <= 768) 
        {
            $("#cssmenu").find('ul').hide().removeClass('open');
        }
    }
    
    function SetupParkingFitMenu()
    {
        // alert(window.location);
        $( document ).ready(function() 
        { 
            // "http://localhost/ChildMonitor/index.php?stme=VAG"
            $('body').css("margin", "0");
            
            $('#Msg_Detail_Menu').html("Parking Fit"); 
            $('#Icon_Detail_Menu').attr("src", "/ChildMonitor/Img/ParkingFitLogoLow.png");
            // Javascrip menus control
            $('#idmenuprovedor').click(function() {  DialogProvider(); CloseMenu();  });
            $('#idmenuhome').click(function() { ParkingFitHomePage(); CloseMenu(); });
    

        });

        var style_div = "font-family: Montserrat, sans-serif; background: #333333;  font-size: 12px; letter-spacing: 1px; text-decoration: none; color: #dddddd; font-weight: 700;";
        document.write("<div  id='id_logodecor'  style=' z-index:20; position: absolute; top: 11px; left: 100px; "+style_div+"  '> <div style='float: left; padding: 0px; '> <img  src=\"/ChildMonitor/Img/ParkingFitLogoLow.png\" alt=\"\" height=\"25\" >  </div> <div style='float: left; padding-left:10px;  padding-top:6px; ' > Parking Fit </div> </div>  ");
        // document.write("<div  id='id_logodecor'  style=' z-index:7; position: absolute; top:35px; left: 100px; "+style_div+"  '> <div style='float: left; padding: 0px; '> <img  src=\"/ChildMonitor/Img/ParkingFitLogoLow.png\" alt=\"\" height=\"25\" >  </div> <div style='float: left; padding-left:10px;  padding-top:6px; ' > Parking Fit </div> </div>  ");
        
        $('#id_logodecor').css("left", $( window ).width()-140);
        if ($( window ).width() <= 768)
           $('#id_logodecor').hide(); 
    
        
         $(window).on('resize', function()
         {

             if ($( window ).width() > 768)
             {
                $('#id_logodecor').css("left", $( window ).width()-140);
                $('#id_logodecor').show();
             }
         });
    }
    
    SetupParkingFitMenu();
    </script>

 
  </body>
    </html>  
     
EOT;
    //   
}

////////////////////////////////////////////////////////////////////////////////
function GetFaceBookData() {
    session_start();
    if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['code'])) {

        // Informe o seu App ID abaixo
        $appId = '1617888258447434';

        // Digite o App Secret do seu aplicativo abaixo:
        $appSecret = '6e75c7aa5c3b80d8764329c1b18235d0';

        // Url informada no campo "Site URL"
        $redirectUri = urlencode('http://childmonitor1.ngrok.com/');
        // Obtém o código da query string'

        $code = $_GET['code'];

        // Monta a url para obter o token de acesso e assim obter os dados do usuário
        $token_url = "https://graph.facebook.com/oauth/access_token?"
                . "client_id=" . $appId . "&redirect_uri=" . $redirectUri
                . "&client_secret=" . $appSecret . "&code=" . $code;

        //pega os dados
        $response = @file_get_contents($token_url);
        if ($response) {
            $params = null;
            parse_str($response, $params);
            if (isset($params['access_token']) && $params['access_token']) {
                $graph_url = "https://graph.facebook.com/me?access_token="
                        . $params['access_token'];
                $user = json_decode(file_get_contents($graph_url));

                // nesse IF icamos se veio os dados corretamente
                if (isset($user->email) && $user->email) {

                    /*
                     * Apartir daqui, você já tem acesso aos dados usuario, podendo armazená-los
                     * em sessão, cookie ou já pode inserir em seu banco de dados para efetuar
                     * autenticação.
                     * No meu caso, solicitei todos os dados abaixo e guardei em sessões.
                     */

                    $_SESSION['email'] = $user->email;
                    $_SESSION['nome'] = $user->name;
                    $_SESSION['localizacao'] = $user->location->name;
                    $_SESSION['uid_facebook'] = $user->id;
                    $_SESSION['user_facebook'] = $user->username;
                    $_SESSION['link_facebook'] = $user->link;
                    dbg($_SESSION['email']);
                }
            } else {
                echo "Erro de conexão com Facebook";
                exit(0);
            }
        } else {
            echo "Erro de conexão com Facebook";
            exit(0);
        }
    } else
    if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['error'])) {
        echo 'Permissão não concedida';
    }
}

////////////////////////////////////////////////////////////////////////////////
function GetFB_Img($username_id) {
    if (trim($username_id) == '')
        return '';
    return("http://graph.facebook.com/729549303831705/picture?type=large");
}

////////////////////////////////////////////////////////////////////////////////

function FaceBookSuport() {
    if($_SESSION['stme']=="TTM" )
       $appid="483337128496389";
    else
       $appid="1617888258447434"; 
    
    echo <<<EOT
    <script type="text/javascript" src="https://connect.facebook.net/en_us/all.js"></script>
    <script>
    //////////////////////////////////////////////////////////////////////////
      // This is called with the results from from FB.getLoginStatus().
      function statusChangeCallback(response) 
      {
        console.log('statusChangeCallback');
        console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') 
        {
          // Logged into your app and Facebook.
          // $('#EMailFB').val(response.email);
          // $('#fbId').val(response.id);
          // SaveUserDataFB();
          testAPI();
        } 
        else 
        if (response.status === 'not_authorized') 
        {
          // The person is logged into Facebook, but not your app.
          document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
        } else 
        {
          // The person is not logged into Facebook, so we're not sure if
          // they are logged into this app or not.
          document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';
        }
      }
    //////////////////////////////////////////////////////////////////////////
      // This function is called when someone finishes with the Login
      // Button.  See the onlogin handler attached to it in the sample
      // code below.
      function checkLoginState() 
    {
        FB.getLoginStatus(function(response) 
        {
          statusChangeCallback(response);
        });
      }
    //////////////////////////////////////////////////////////////////////////
      window.fbAsyncInit = function() {
      FB.init({
        appId      : '$appid',
        cookie     : true,  // enable cookies to allow the server to access 
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.2' // use version 2.2
      });
    //////////////////////////////////////////////////////////////////////////
      // Now that we've initialized the JavaScript SDK, we call 
      // FB.getLoginStatus().  This function gets the state of the
      // person visiting this page and can return one of three states to
      // the callback you provide.  They can be:
      //
      // 1. Logged into your app ('connected')
      // 2. Logged into Facebook, but not your app ('not_authorized')
      // 3. Not logged into Facebook and can't tell if they are logged into
      //    your app or not.
      //
      // These three cases are handled in the callback function.
      //////////////////////////////////////////////////////////////////////////
      FB.getLoginStatus(function(response) 
      {
        statusChangeCallback(response);
      });

      };
      //////////////////////////////////////////////////////////////////////////
      // Load the SDK asynchronously
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
      //////////////////////////////////////////////////////////////////////////
      // Here we run a very simple test of the Graph API after login is
      // successful.  See statusChangeCallback() for when this call is made.
      function testAPI() 
      {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function(response) 
        {
          console.log('Successful login for: ' + response.name);
          console.log('Successful login for: ' + response.email);
          
          // alert($('#emailFBHid').val ())
          // alert(response.email);
          // document.getElementById('status').innerHTML =
          //  'Thanks for logging in, ' + response.name + '!';
        });
      }

    //////////////////////////////////////////////////////////////////////////
    </script>


EOT;
}

////////////////////////////////////////////////////////////////////////////////
function FBButton() {
    $buffer = <<<EOT
    <!--
      Below we include the Login Button social plugin. This button uses
      the JavaScript SDK to present a graphical Login button that triggers
      the FB.login() function when clicked.
    -->

    <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
    </fb:login-button><br>
    <!--
     <div id="status">
     </div>
    -->        
EOT;
    return($buffer);
}

////////////////////////////////////////////////////////////////////////////////
function FBLikeButton() {
    if($_SESSION['stme']=="TTM" )
    {    
       $appid="483337128496389";
       $ssite="http://trucktrackweb.com";
    }
    else
    {    
       $appid="1617888258447434"; 
       $ssite="http://childmonitorweb.com";
    }
    $buffer = <<<EOT
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=$appid&version=v2.3";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <div class="fb-like" data-href="$ssite" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>            
EOT;
    return($buffer);
}

////////////////////////////////////////////////////////////////////////////////
function isLoggedParkingFit()
{
    session_start();
    ini_set('session.use_cookies', 1);
    ini_set('session.use_only_cookies', 1);    
    
    if ($_COOKIE['userEmailProviderParkFit'] != "")
        return true;
    if ($_COOKIE['userEmailStaffParkFit'] != "")
        return true;
    if ($_COOKIE['userEmailParkFit'] != "")
        return true;              
}
////////////////////////////////////////////////////////////////////////////////
function isLogged() {
    session_start();
    ini_set('session.use_cookies', 1);
    ini_set('session.use_only_cookies', 1);
    
    if(isLoggedParkingFit())
        return true;
  
    if ($_SESSION['userEmail'] != "")
        return true;
    else
        return false;
}

////////////////////////////////////////////////////////////////////////////////
function MenuPrincipalDetail() 
{
    // if(isMobile())
    // {    
    //    MenuFloating();
    //    return;
    // }  
    // position:absolute;
    echo <<<EOT
    <div id='cssmenu' style="z-index:20; ">
    <ul class="" style="display: none;">    
    <li class='active'><a href='index.php'>Home</a></li>
EOT;
    if (isLogged()) {
        $useremail = $_SESSION['userEmail'];
        echo <<<EOT
        <li><a href='mapaRT.php'>Real Time Find</a></li>     
        <li><a href='mapa.php'>Locate</a></li>   
        <li><a href='userPlaces.php'>Places</a></li> 
        <li><a href='userReport.php'>Report</a></li>
        <li><a href='userProfile.php'>Profile</a></li>
EOT;
    if ($_SESSION['AppLogin'] != 1) {
        echo <<<EOT
        <li><a  href='formLogout.php'>Logout [$useremail]</a></li>
EOT;
        }
        echo <<<EOT
    </ul>
    </div>
EOT;
        return;
    }
    echo <<<EOT
    <li><a href='formSignin.php'>Sign In</a></li>
    <li><a href='formLogin.php'>Login</a></li>    
EOT;

    echo <<<EOT
    </ul>
    </div>
EOT;
    // echo GoogleAd();
}
////////////////////////////////////////////////////////////////////////////////
function MenuPrincipalDetailParkingFit() {
    // if(isMobile())
    // {    
    //    MenuFloating();
    //    return;
    // }  
    // position:absolute;
    $bufParkMsg = i2l("Estaciona");
    $bufProviderMsg = i2l("Provedor");
    $bufStaffMsg = i2l("Fiscal");
    echo <<<EOT

    <div id='cssmenu' style="z-index:20; ">
    <ul class="" style="display: none;">    
    <li class='active'><a id='idmenuhome' >Home</a></li>
    <li class='active'><a href='user.php?stme=VAG'>$bufParkMsg</a></li>
    <li class='active'><a id='idmenuprovedor' >$bufProviderMsg</a></li>
    <li class='active'><a href='provider.php?stme=VAG'>$bufStaffMsg</a></li>
EOT;
    if (isLogged()) {
        if($_COOKIE['userEmailProviderParkFit']!="")
        {    
             $useremail = $_COOKIE['userEmailProviderParkFit'];
        }     
        if($_COOKIE['userEmailStaffParkFit']!="")
        {    
             $useremail = $_COOKIE['userEmailStaffParkFit'];
        }  
        if($_COOKIE['userEmailParkFit']!="")
        {    
             $useremail = $_COOKIE['userEmailParkFit'];
        }     
        echo <<<EOT
EOT;
  
    if ($_COOKIE['userEmailProviderParkFit'] != "" || $_COOKIE['userEmailStaffParkFit'] != "" || $_COOKIE['userEmailParkFit'] != "") {
        echo <<<EOT
        <li><a  href='formLogout.php'>Logout [$useremail]</a></li>
EOT;
        }
        echo <<<EOT
    </ul>
    </div>
EOT;
        return;
    }
    echo <<<EOT
    </ul>
    </div>
EOT;
    // echo GoogleAd();
}

////////////////////////////////////////////////////////////////////////////////
function MenuPrincipalDetailListaEscolar() {
    // if(isMobile())
    // {    
    //    MenuFloating();
    //    return;
    // }  
    // position:absolute;
    echo <<<EOT
    <div id='cssmenu' style="z-index:20;">
    <ul>    
    <li class='active'><a href='index.php'>Home</a></li>
    <li class='active'><a href='index.php'>Compras</a></li>
EOT;
    if (isLogged()) 
    {
        $useremail = $_SESSION['userEmail'];
        echo <<<EOT

EOT;
        if ($_SESSION['AppLogin'] != 1) {
            echo <<<EOT
        <li><a href='formLogout.php'>Logout [$useremail]</a></li>
EOT;
        }
        echo <<<EOT
    </ul>
    </div>
EOT;
        return;
    }
    echo <<<EOT
  
EOT;

    echo <<<EOT
    </ul>
    </div>
EOT;
}

////////////////////////////////////////////////////////////////////////////////
function StdBackGround() {
    DivImgComSombra("Img/Back_" . rand(1, 3) . ".jpg");
    divTransparente("<p style=\" color: rgb(255,255,255); margin-top:0px; margin-botton:0px; \" >Track, monitor, remeber and interact. The definitive way to protect our loving.<p>");
    // echo "</body>";  
    // echo "</html>";
}

////////////////////////////////////////////////////////////////////////////////
function StdBackGroundPlain() {
    require 'GlobalVars.php';

    if ($AppTheme == "myfather") {
        DivImgComSombra("Img/BackMyFather_1.jpg");
        return;
    }

    if ($AppTheme == "listaescolar") {
        DivImgComSombra("Img/BackListaEscolar_2.jpg");
        return;
    }

    if ($_SESSION['stme']=="TTM") {
        DivImgComSombra("Img/BackTruckLow.jpg");
        return;
    }
    
    if ($_SESSION['stme']=="VAG") {
        DivImgComSombra("");
        return;
    }


    if (ismobile())
        DivImgComSombra("Img/Back_" . rand(1, 1) . "_Mobile" . ".jpg");
    else
        DivImgComSombra("Img/Back_" . rand(1, 1) . "_1440x900" . ".jpg");


    // echo "</body>";  
    // echo "</html>";
    /*

      background: rgba(200,235,211,1);
      background: -moz-linear-gradient(left, rgba(200,235,211,1) 0%, rgba(83,198,123,1) 100%);
      background: -webkit-gradient(left top, right top, color-stop(0%, rgba(200,235,211,1)), color-stop(100%, rgba(83,198,123,1)));
      background: -webkit-linear-gradient(left, rgba(200,235,211,1) 0%, rgba(83,198,123,1) 100%);
      background: -o-linear-gradient(left, rgba(200,235,211,1) 0%, rgba(83,198,123,1) 100%);
      background: -ms-linear-gradient(left, rgba(200,235,211,1) 0%, rgba(83,198,123,1) 100%);
      background: linear-gradient(to right, rgba(200,235,211,1) 0%, rgba(83,198,123,1) 100%);
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c8ebd3', endColorstr='#53c67b', GradientType=1 );

     */
}

////////////////////////////////////////////////////////////////////////////////

function DivImgComSombra($img) {
    // Terminar usando o tutorial http://www.infowester.com/tut_imagens_sombreadas.php
    global $AppTheme;
    echo <<<EOT
    <html lang="en">
    <head>
    <link rel="stylesheet" href="/ChildMonitor/Menus/jquery-ui_$AppTheme/jquery-ui.css"> 
    <script src="/ChildMonitor/Menus/jquery-ui_$AppTheme/jquery-ui.js"></script>  

<style>
            body.bodyForm{
                    font: 75.5% "Trebuchet MS", sans-serif;
                    margin: 0px;
            }
            .demoHeaders {
                    margin-top: 2em;
            }
            #dialog-link {
                    padding: .4em 1em .4em 20px;
                    text-decoration: none;
                    position: relative;
            }

            #icons 
            {
                    margin: 0;
                    padding: 0;
            }
            #icons li {
                    margin: 2px;
                    position: relative;
                    padding: 4px 0;
                    cursor: pointer;
                    float: left;
                    list-style: none;
            }
            #icons span.ui-icon {
                    float: left;
                    margin: 0 4px;
            }
            .fakewindowcontain .ui-widget-overlay {
                    position: absolute;
            }
            select {
                    width: 234px;
            }
    </style>

    <style>
        .ui-menu { width: 250px; }
    </style>
    
    <script type="text/javascript">
        $(document).ready(function(){
        var altura_tela = $(window).height(); /*cria variável com valor do altura da janela*/
        var largura_tela = $(window).width();    
        $("#bloco").height(altura_tela-0); /* aplica a variável a altura da div*/ 
        $("#bloco").width(largura_tela);   
        

            
        $( window ).resize(function() 
            { /*quando redimensionar a janela faz a mesma coisa */ 
               var body = document.body,
               html = document.documentElement;

               var height = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
                // var altura_tela = $(document).height();
                var altura_tela = height;
                var largura_tela = $(window).width();
                $("#bloco").height(altura_tela-0);
                $("#bloco").width(largura_tela);
            });
        }); 
    </script>     
    <body class="bodyForm" > 
     <div id='bloco' style=" background-image:url($img);  repeat; width=98%;  background-size:cover; "> 
        <!--- <img src="$img" width="100%" height="30%"> --->
     </div> 
    </body>  
    </html>        
EOT;
}

////////////////////////////////////////////////////////////////////////////////
function GetIconDevice($idDevice, $idUser) {
    $query = "select Image from UserMonitThings where UserId=$idUser and id='$idDevice'";
    // echo $query;
    $resultado = db_query($query);
    return(trim($resultado[1][0]));
}

////////////////////////////////////////////////////////////////////////////////
function GetFirstDevice($userId) {
    $query = "select id from UserMonitThings where UserId=$idUser order by id asc";
    $resultado = db_query($query);
    return($resultado[1][0]);
}
////////////////////////////////////////////////////////////////////////////////
function GetValue($query) 
{
    // $query = "select id from UserMonitThings where UserId=$idUser order by id asc";
    $resultado = db_query($query);
    return($resultado[1][0]);
}
////////////////////////////////////////////////////////////////////////////////
function MenuRegions($idSelect, $idUser) {
    $query = "SELECT `Address` FROM `MonitoredRegion` where UserId=$idUser order by `Address` asc";
    // echo $query;
    $resultado = db_query($query);

    /*
      <ul id=\"menu\">
     *   <li>Item 1</li> 
     *   <li>Item 2</li>  
     * </ul>
     */
    $tmp = "<ul id=\\\"$idSelect\\\"> ";
    $ind = 1;
    while ($resultado[$ind][0] != "") {
        $restmp = $resultado[$ind][0];
        $tmp = $tmp . "<li>$restmp</li>";
        $ind++;
    }
    $tmp = $tmp . "</ul>";
    return($tmp);
}

////////////////////////////////////////////////////////////////////////////////
function SelectDevice($idSelect, $idUser, $valSelected, $width="98%") {
    $query = "select id,Label from UserMonitThings where UserId=$idUser order by id asc";
    $resultado = db_query($query);
    $tmp = "<select style=\"width:$width; height:25px;  font-size:11px \" id=\"$idSelect\">\n";
    $ind = 1;
    while ($resultado[$ind][0] != "") {
        if (trim($resultado[$ind][1]) == "")
            $label = $resultado[$ind][0];
        else
            $label = $resultado[$ind][1];
        $restmp = $resultado[$ind][0];
        if ($restmp != $valSelected)
            $tmp = $tmp . "<option value=\"$restmp\">$label</option>\n";
        else
            $tmp = $tmp . "<option selected=\"selected\" value=\"$restmp\">$label</option>\n";


        $ind++;
    }
    $tmp = $tmp . "</select>";
    return($tmp);
}

////////////////////////////////////////////////////////////////////////////////
class ItemRegion {

    public $aLat = 0.0;
    public $aLong = 0.0;
    public $Radius = 0.0;
    public $Address = "";

}

class vetItensR {

    public $vet;
    public $numItens = 0;
    public $response = "";
    public $sql = "";
}

////////////////////////////////////////////////////////////////////////////////
function GetMonitoredRegions() {
    /*

     */

    $UserId = $_GET['UserId'];
    // $idDevice=$_GET['idDevice'];

    $query = "select * from `MonitoredRegion` where UserId=$UserId ";
    // dbg($query);
    $resultado = db_query($query);

    // print_r($resultado);

    $r = new vetItensR();
    $r->vet[] = new ItemRegion();


    $i = 0;
    while ($resultado[$i + 1][0] != "") {
        $r->vet[$i]->aLat = $resultado[$i + 1][1];
        $r->vet[$i]->aLong = $resultado[$i + 1][2];
        $r->vet[$i]->Radius = $resultado[$i + 1][3];
        $r->vet[$i]->Address = $resultado[$i + 1][4];
        $i++;
    }
    $r->numItens = $i;


    // Uso
    // http://localhost/ChildMonitor/Util/GetGlobalPosition.php?id=12
    // Retorno
    // {"id":"12","Lat":"23.123212","Long":"12.129877","TimeStamp":"2000-11-11 13:23:02","Velocity":"3"} 
    // Decodificação em javascript
    // var json = '{"result":true,"count":1}',
    // obj = JSON && JSON.parse(json) || $.parseJSON(json);
    // alert(obj.count);

    echo json_encode($r);
}

////////////////////////////////////////////////////////////////////////////////
function GetLabelDevice($idUser, $idDevice) {
    $query = "SELECT `Label` FROM `UserMonitThings` WHERE UserId=$idUser and id = '$idDevice'";
    $resultado_tmp = db_query($query);
    return($resultado_tmp[1][0]);
}

////////////////////////////////////////////////////////////////////////////////
// XXXXXXXXXXXX
function GetGlobalPosition() {
    /*
      CREATE TABLE "MonitoredThings"
      (
      id integer NOT NULL,
      "Lat" double precision NOT NULL,
      "Long" double precision NOT NULL,
      "TimeStamp" timestamp without time zone NOT NULL,
      "Velocity" double precision,
      CONSTRAINT id PRIMARY KEY (id, "Lat", "Long", "TimeStamp")
      )
      WITH (
      OIDS=FALSE);
     */

    $idUser = $_GET['idUser'];
    $idDevice = $_GET['idDevice'];
    $FilterDate = $_GET['FilterDate'];
    $RealTime = $_GET['RealTime'];

    // select * from MonitoredThings where idUser=1 and idDevice='SAMSUNG_GT-I9500_4d0023003073400f' and datediff(`TimeStampc`,'2015-03-17')=0 order by TimeStampc desc
    // SELECT `idDevice`, `idUser`, `Lat`, `Longc`, `TimeStampc`, dateiff(`TimeStampc`,'2015-03-18') FROM `MonitoredThings` WHERE 1
    // select `idDevice`, `idUser`, `Lat`, `Longc`, max(`TimeStampc`), `Velocity`, `Bearing`, `BatteryLevel` from MonitoredThings where idUser=1 group by idDevice;


    if ($RealTime == 0)
        $query = "select `idDevice`, `idUser`, `Lat`, `Longc`, `TimeStampc`, `Velocity`, `Bearing`, `BatteryLevel`, `offLineData`, `Accuracy` from MonitoredThings where idUser=$idUser and idDevice='$idDevice' and datediff(`TimeStampc`,'$FilterDate')=0 and Accuracy<50.0 order by TimeStampc desc";
    else
        $query = "select `idDevice`, `idUser`, `Lat`, `Longc`, `TimeStampc`, `Velocity`, `Bearing`, `BatteryLevel`, `offLineData`, `Accuracy` from MonitoredThings where idUser=$idUser and TimeStampc in (select max(`TimeStampc`) from MonitoredThings where idUser=$idUser group by idDevice)";

    // echo $query;
    // select `idDevice`, max(`TimeStampc`) from MonitoredThings where idUser=1 group by idDevice
    //  
    // select * from MonitoredThings where idUser=1 and TimeStampc in (select max(`TimeStampc`) from MonitoredThings where idUser=1 group by idDevice)
    // 
    // dbg($query);
    $resultado = db_query($query);
    $idDevice = $resultado[1][0];


    $label = GetLabelDevice($idUser, $idDevice);

    $r = new vetItens();
    $r->vet[] = new ItemPosition();


    $i = 0;
    while ($resultado[$i + 1][0] != "") {
        $r->vet[$i]->Label = GetLabelDevice($resultado[$i + 1][1], $resultado[$i + 1][0]);
        $r->vet[$i]->idDevice = $resultado[$i + 1][0];
        $r->vet[$i]->idUser = $resultado[$i + 1][1];
        $r->vet[$i]->Lat = $resultado[$i + 1][2];
        $r->vet[$i]->Long = $resultado[$i + 1][3];
        $r->vet[$i]->TimeStamp = $resultado[$i + 1][4];
        $r->vet[$i]->Velocity = $resultado[$i + 1][5];
        $r->vet[$i]->Bearing = $resultado[$i + 1][6];
        $r->vet[$i]->BatteryLevel = $resultado[$i + 1][7];
        $r->vet[$i]->icon = GetIconDevice($resultado[$i + 1][0], $resultado[$i + 1][1]);
        $r->vet[$i]->Accuracy = $resultado[$i + 1][9];
        $i++;
    }
    $r->numItens = $i;

    /*
      $r->vet[0]->idDevice =     $resultado[1][0];
      $r->vet[0]->idUser =       $resultado[1][1];
      $r->vet[0]->Lat  =         $resultado[1][2];
      $r->vet[0]->Long =         $resultado[1][3];
      $r->vet[0]->TimeStamp =    $resultado[1][4];
      $r->vet[0]->Velocity =     $resultado[1][5];
      $r->vet[0]->Bearing =      $resultado[1][6];
      $r->vet[0]->BatteryLevel = $resultado[1][7];
      // $r->icon = GetIconDevice($resultado[1][0],$resultado[1][1]);

      $r->vet[1]->idDevice =     $resultado[1][0];
      $r->vet[1]->idUser =       $resultado[1][1];
      $r->vet[1]->Lat  =         $resultado[1][2];
      $r->vet[1]->Long =         $resultado[1][3];
      $r->vet[1]->TimeStamp =    $resultado[1][4];
      $r->vet[1]->Velocity =     $resultado[1][5];
      $r->vet[1]->Bearing =      $resultado[1][6];
      $r->vet[1]->BatteryLevel = $resultado[1][7];
      // $r->icon = GetIconDevice($resultado[1][0],$resultado[1][1]);
     */

    // Uso
    // http://localhost/ChildMonitor/Util/GetGlobalPosition.php?id=12
    // Retorno
    // {"id":"12","Lat":"23.123212","Long":"12.129877","TimeStamp":"2000-11-11 13:23:02","Velocity":"3"} 
    // Decodificação em javascript
    // var json = '{"result":true,"count":1}',
    // obj = JSON && JSON.parse(json) || $.parseJSON(json);
    // alert(obj.count);

    echo json_encode($r);
}

////////////////////////////////////////////////////////////////////////////////
class ItemPosition {

    public $idDevice = "";
    public $idUser = null;
    public $Lat = 0.0;
    public $Long = 0.0;
    public $TimeStamp = "";
    public $Velocity = 0.0;
    public $Bearing = 0.0;
    public $BatteryLevel = 0.0;
    public $icon = "";
    public $Label = "";
    public $Accuracy = "";
}

class vetItens {

    public $vet;
    public $numItens = 0;

}

////////////////////////////////////////////////////////////////////////////////
function GetDevicesInRegion() {
    /*
      CREATE TABLE "MonitoredThings"
      (
      id integer NOT NULL,
      "Lat" double precision NOT NULL,
      "Long" double precision NOT NULL,
      "TimeStamp" timestamp without time zone NOT NULL,
      "Velocity" double precision,
      CONSTRAINT id PRIMARY KEY (id, "Lat", "Long", "TimeStamp")
      )
      WITH (
      OIDS=FALSE);
     */

    $idUser = $_GET['idUser'];
    $idDevice = $_GET['idDevice'];
    $FilterDate = $_GET['FilterDate'];
    $RealTime = $_GET['RealTime'];

    // select * from MonitoredThings where idUser=1 and idDevice='SAMSUNG_GT-I9500_4d0023003073400f' and datediff(`TimeStampc`,'2015-03-17')=0 order by TimeStampc desc
    // SELECT `idDevice`, `idUser`, `Lat`, `Longc`, `TimeStampc`, dateiff(`TimeStampc`,'2015-03-18') FROM `MonitoredThings` WHERE 1
    // select `idDevice`, `idUser`, `Lat`, `Longc`, max(`TimeStampc`), `Velocity`, `Bearing`, `BatteryLevel` from MonitoredThings where idUser=1 group by idDevice;


    if ($RealTime == 0)
        $query = "select `idDevice`, `idUser`, `Lat`, `Longc`, `TimeStampc`, `Velocity`, `Bearing`, `BatteryLevel`, `offLineData`, `Accuracy` from MonitoredThings where idUser=$idUser and idDevice='$idDevice' and datediff(`TimeStampc`,'$FilterDate')=0 and Accuracy<50.0 order by TimeStampc desc";
    else
        $query = "select `idDevice`, `idUser`, `Lat`, `Longc`, `TimeStampc`, `Velocity`, `Bearing`, `BatteryLevel`, `offLineData`, `Accuracy` from MonitoredThings where idUser=$idUser and TimeStampc in (select max(`TimeStampc`) from MonitoredThings where idUser=$idUser group by idDevice)";

    // echo $query;
    // select `idDevice`, max(`TimeStampc`) from MonitoredThings where idUser=1 group by idDevice
    //  
    // select * from MonitoredThings where idUser=1 and TimeStampc in (select max(`TimeStampc`) from MonitoredThings where idUser=1 group by idDevice)
    // 
    // dbg($query);
    $resultado = db_query($query);
    $idDevice = $resultado[1][0];


    $label = GetLabelDevice($idUser, $idDevice);

    $r = new vetItens();
    $r->vet[] = new ItemPosition();


    $i = 0;
    while ($resultado[$i + 1][0] != "") {
        $r->vet[$i]->Label = GetLabelDevice($resultado[$i + 1][1], $resultado[$i + 1][0]);
        $r->vet[$i]->idDevice = $resultado[$i + 1][0];
        $r->vet[$i]->idUser = $resultado[$i + 1][1];
        $r->vet[$i]->Lat = $resultado[$i + 1][2];
        $r->vet[$i]->Long = $resultado[$i + 1][3];
        $r->vet[$i]->TimeStamp = $resultado[$i + 1][4];
        $r->vet[$i]->Velocity = $resultado[$i + 1][5];
        $r->vet[$i]->Bearing = $resultado[$i + 1][6];
        $r->vet[$i]->BatteryLevel = $resultado[$i + 1][7];
        $r->vet[$i]->icon = GetIconDevice($resultado[$i + 1][0], $resultado[$i + 1][1]);
        $r->vet[$i]->Accuracy = $resultado[$i + 1][9];
        $i++;
    }
    $r->numItens = $i;

    /*
      $r->vet[0]->idDevice =     $resultado[1][0];
      $r->vet[0]->idUser =       $resultado[1][1];
      $r->vet[0]->Lat  =         $resultado[1][2];
      $r->vet[0]->Long =         $resultado[1][3];
      $r->vet[0]->TimeStamp =    $resultado[1][4];
      $r->vet[0]->Velocity =     $resultado[1][5];
      $r->vet[0]->Bearing =      $resultado[1][6];
      $r->vet[0]->BatteryLevel = $resultado[1][7];
      // $r->icon = GetIconDevice($resultado[1][0],$resultado[1][1]);

      $r->vet[1]->idDevice =     $resultado[1][0];
      $r->vet[1]->idUser =       $resultado[1][1];
      $r->vet[1]->Lat  =         $resultado[1][2];
      $r->vet[1]->Long =         $resultado[1][3];
      $r->vet[1]->TimeStamp =    $resultado[1][4];
      $r->vet[1]->Velocity =     $resultado[1][5];
      $r->vet[1]->Bearing =      $resultado[1][6];
      $r->vet[1]->BatteryLevel = $resultado[1][7];
      // $r->icon = GetIconDevice($resultado[1][0],$resultado[1][1]);
     */

    // Uso
    // http://localhost/ChildMonitor/Util/GetGlobalPosition.php?id=12
    // Retorno
    // {"id":"12","Lat":"23.123212","Long":"12.129877","TimeStamp":"2000-11-11 13:23:02","Velocity":"3"} 
    // Decodificação em javascript
    // var json = '{"result":true,"count":1}',
    // obj = JSON && JSON.parse(json) || $.parseJSON(json);
    // alert(obj.count);

    echo json_encode($r);
}

////////////////////////////////////////////////////////////////////////////////
function SelectAddressAutoComp($id,$default,$lat,$lon) 
{
    // http://photon.komoot.de:2322/api?q=moreira%20cesar&lat=-22.802671&lon=-43.0722389
    
    $idHid = $_GET[$id."Hid"];
    $id_div = $id."DivDat";
    $id_img = $id."ImgDivDatPre";
    $id_imgclean = $id."ImgClean";
    
    $id_imgGlb = $id."ImgDivDatPreGlb";
    $id_imgCit = $id."ImgDivDatPreCit";

    $id_lat = $id."latGlb";
    $id_lon = $id."lonGlb";

    $imgRange = "<img id=\"$id_img\" onclick=\"onclickbmpdat_$id()\" src=\"/ChildMonitor/Img/icon_city.png\" alt=\"\" style=\"width:28px;height:28px;\">";
    $imgClean = "<img id=\"$id_imgclean\" onclick=\"onclickclean_$id()\" src=\"/ChildMonitor/Img/CancelBlk.png\" alt=\"\" style=\"width:20px;height:20px;\">";
    // esse cai dentro de um javascript
    // $imgCloseAutocomplete = "<img  onclick=\\\"onclickcloseautocomplete_$id()\\\" src=\\\"/ChildMonitor/Img/Cancel.png\\\" alt=\\\"\\\" style=\\\"width:20px;height:20px;\\\">";
    $imgCloseAutocomplete = "";
    $id_tableinput = $id."TableInputDivDat";
    $id_table = $id."TableDivDat";
    $divStyle= " opacity: 1.5;   border-radius: 5px; box-shadow:    0 0px 0 0 #444; color:  #fff; display:       inline-block; padding: 0px 3px 7px 3px; text-align:left; box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); -moz-box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); -webkit-box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5);  background-color:rgba(0,0,0,0.8); "; 
    
    $tdTableStyle = " style=\\\"border-top:1px solid #3E3C3C;\\\" ";
    $hooverStyle=  " onMouseOut=\\\"this.style.background=#999\\\" onMouseOver=\\\"this.style.background=#000\\\" ";
    $inputBox = "<input  placeholder=\"$default\" class=\"form-control\" onblur=\"onblur_$id()\"  onclick=\"onclick_$id()\"   onchange=\"onchange_$id()\" onkeyup=\"onkeyup_$id()\" style=\" \" type=\"text\" name=\"$id\" id=\"$id\" value=\"$idHid\" />";
     
    $buffer =  <<<EOT
                                            <!--  Range (city or global) button commented    -->
       <table id="$id_tableinput" style="width:100%"> <tr> <th> $inputBox  </th> <th> $imgClean </th> </tr> </table>
       <div  style="  overflow-x: hidden; overflow-y: auto; position:absolute; z-index:47; $divStyle top:0; left:10px;   "  id="$id_div" /> <p style="font-size:10px" id="idAddssdess" >  </p> </div>
    
        <script type="text/javascript"> 
            
        // $('<img id=\\"$id_imgCit\\"  />')[0].src = "/ChildMonitor/Img/icon_city.png";
        // $('<img id=\\"$id_imgGlb\\"  />')[0].src = "/ChildMonitor/Img/icon_globe.png";
            
        var largura_tela = $(window).width(); 
        largura = largura_tela-40-12;
        $("#$id").css('width', largura);
            
            
        $("#$id_div").hide();    
         
        var callJson$id=null;
        var scopeGlobal$id=0;
            
        $id_lat = 0.0; 
        $id_lon = 0.0;   
  
        function cHide_$id()  
        {   
           $("#$id_tableinput").hide(); 
        }   
        function cShow_$id()  
        {   
           $("#$id_tableinput").show(); 
        }   
  
        function onclick_$id()  
        {  
            // alert("blur");
            
            if(callJson$id!=null)
               callJson$id.abort(); 
            $('#$id_div').slideUp();
            
        }   

 
        function onblur_$id()  
        {  
            // alert("blur");
            if(callJson$id!=null)
               callJson$id.abort(); 
            $('#$id_div').slideUp();
  
        }   
            
        function onclickclean_$id()  
        {   
           $id_lat = 0.0; 
           $id_lon = 0.0;  
           $("#$id").val('');
        }   
        function onclickcloseautocomplete_$id()  
        {   
           $("#$id_div").slideUp();   
        }       
        function onclickbmpdat_$id()  
        {
            
            if(scopeGlobal$id==0)
            {
                scopeGlobal$id=1;
                $("#$id_img").attr("src","/ChildMonitor/Img/icon_globe.png");
            }
            else            
            {
                scopeGlobal$id=0;
                $("#$id_img").attr("src","/ChildMonitor/Img/icon_city.png");
            }
            
        }
        //////////////////////////////////////////////////////////////////////// 
        function noUndef(str)
        {
            if(str===undefined)
               return("");
            else
               return(str+",");
            
        }
        ////////////////////////////////////////////////////////////////////////
        function GetTypeData(data,type)
        {
            if(data===undefined)
               return("");
            else
            {
               // alert(data.address_components[0].long_name+","+data.address_components[0].types[0]);
               indp=0;
               
               while(data.address_components[indp]!==undefined )
               {
                  // alert(data.address_components[indp].long_name);
                  console.log(data.address_components[indp].long_name);
                  if(data.address_components[indp].types[0]==type)
                     return(","+data.address_components[indp].long_name);
                  indp++;
                   
               }
               
            }
            return("");
        }           
        ////////////////////////////////////////////////////////////////////////   
        function ProcessGoogleJsonData_$id(data)
        {
            
           // alert(data.predictions[0].description);
           ind=0;
           saida = "";
           while(!(data.predictions[ind]===undefined))
            {
                // alert(data.results[ind].formatted_address);
                console.log(data.predictions[ind].description);
                saida = saida + "<tr>  <td $tdTableStyle onclick=\"Click$id_table"+ind+"()\" class='$id_table' id=\"$id_table\""+ind+" align=\"left\" >" + 
                                data.predictions[ind].description+",<br> <p style=\"font-size:10px\"  >"+" </p></td></tr>\\n";
                saida = saida + "<script> function Click$id_table"+ind+"() \{   \\n"+
                                "      pos="+ind+" ; \\n" +
                              //  "      glbLngNow = \'"+data.results[ind].geometry.location.lng+"\' ; \\n" +
                              //  "      glbLatNow = \'"+data.results[ind].geometry.location.lat+"\' ; \\n" +  
                              //  "      $id_lat = glbLatNow;  $id_lon = glbLngNow; \\n" + 
                              //  "      SetMarkerOnNewLocation(glbLatNow,glbLngNow); \\n" + 
                                "      $(\"#$id\").val(\'"+data.predictions[ind].description+"\'); \\n"+
                                "      fGetLatLonFromAdress_$id(\'"+data.predictions[ind].description+"\');  \\n"+
                                "      $(\"#$id_div\").slideUp(); \\n"+
                                " \} <\/script>\\n";


               ind++;  
            }
            return saida;
        }
        ////////////////////////////////////////////////////////////////////////            
        function  fGetLatLonFromAdress_$id(str)
        {
            // alert("oi");
            updateFunction = function( lat, lon ) {
               // alert(str);
               $id_lat = lat; 
               $id_lon = lon;   
            }
            
            GetLatLonFromAdressGoogle(str,updateFunction);        
        }   
        ////////////////////////////////////////////////////////////////////////    
        function onchange_$id()
        {
           // fGetLatLonFromAdress_$id($('#$id').val());
        }
        ////////////////////////////////////////////////////////////////////////    
        function onkeyup_$id()
        {
           AutoCompleteGeneration$id();
        } 
        ////////////////////////////////////////////////////////////////////////
        function AutoCompleteGeneration$id()
        {
            query = $('#$id').val();
            
            if(scopeGlobal$id==0)
            {
               // query = query+" "+glbCity+" "+glbCountry;
            }
            for (i = 0; i < 30; i++)
               query = query.replace(" ", "%20");
            console.log(query);
            
            // Geocomplete Google com chave de API
            //   documentação https://developers.google.com/places/web-service/search?hl=pt-br
            //   https://maps.googleapis.com/maps/api/place/autocomplete/json?input=sacada&location=-22.57217017791925,-43.916473388671875&radius=500&key=AIzaSyBS6rSkiRWc6hoSB5E3PJZk9rms4-86ayM
            // Geocode --- Transforma de endereço para lat long
            //    https://maps.googleapis.com/maps/api/geocode/json?address=41+rua+os+goitacases,Niteroi&location=51.503186,-0.126446&key=AIzaSyBS6rSkiRWc6hoSB5E3PJZk9rms4-86ayM
            //              location=51.503186,-0.126446
            
            // Geocomplete Photon
            // sSite = "http://photon.komoot.de:2322/api?q="+query+"&lat="+glbLatNow+"&lon="+glbLngNow+"&limit=25";

            // Geocode GoogleApi
            // sSite = "https://maps.googleapis.com/maps/api/geocode/json?address="+query+"&location="+glbLatNow+","+glbLngNow+"&key=AIzaSyBS6rSkiRWc6hoSB5E3PJZk9rms4-86ayM";
            
            // Geoautocomplete Google
            bufSite = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input="+query+"&location="+glbLatNow+","+glbLngNow+"&radius=500&key=AIzaSyBS6rSkiRWc6hoSB5E3PJZk9rms4-86ayM";
            
            sSite = "/ChildMonitor/Util/dataproxy.php?site="+encodeURIComponent(bufSite);
            console.log(sSite);
            
            
            
            // alert(sSite);
            if(callJson$id!=null)
               callJson$id.abort();
            
            console.log("Chamei getJSON");
            $.ajaxSetup({"error":function(event, jqxhr, settings, exception) {
                               if (exception == 'abort') {return false;}
                               if (exception === '') {exception = 'user aborted process';}
                               // alert('An AJAX error [' + exception + '] occurred in script: ' + settings.url + '<br \><br \>Please report this to the development team!');
                               console.log('Erro no Ajax [' + exception + '] ocorrido no script: ' + settings.url + '!');
                         }});
            
      
   
            
            //////////////////////////////////
            function countItens(data)
            {
               ind=0;
               if(data===undefined)
                 return ind;
               while(!(data.predictions[ind]===undefined))
                  ind++;
            
               return ind;
            }
            //////////////////////////////////
            // Function ajax proc declaration
            functProcessJson = function (data) 
            {
                    // alert(xinspect(data));
                    // alert(data.features[0].properties.name);
                    var list = [];
                    var saida="";  
                    var ind=0;     

                    console.log("getJSON PROCESSOU");

                    saida = " $imgCloseAutocomplete <table id=\"$id_table\"  border: 1px solid black; style=\"width:100%\">  ";

                    $(".$id_table").hover(function(){
                       $(this).css("opacity", "0.5");
                    }, function(){
                       $(this).css("opacity", "1.5");
                    });   

                    //  
                    // list=[];
                    // saida="";

                    // alert(data.results[0].address_components[0].long_name);
                    // alert("22222222222");
                    var googleSite=true;
                    if(googleSite)
                    {  
                        saida = saida + ProcessGoogleJsonData_$id(data);
                    }
                    else
                    { 
                        while(!(data.features[ind]===undefined))
                        {
                            // alert(data.features[ind].properties.name);
                            console.log(data.features[ind].properties.name+","+data.features[ind].properties.city+","+data.features[ind].properties.country);
                            list[ind]=data.features[ind].properties.name+","+data.features[ind].properties.postcode+","+data.features[ind].properties.city+","+data.features[ind].properties.country;
                            saida = saida + "<tr><td onclick=\"Click$id_table"+ind+"()\" class='$id_table' id=\"$id_table\""+ind+" align=\"left\" >" + 
                                            data.features[ind].properties.name+",<br> <p style=\"font-size:10px\"  >"+ noUndef(data.features[ind].properties.postcode)+noUndef(data.features[ind].properties.city)+
                                            noUndef(data.features[ind].properties.country)+noUndef(data.features[ind].geometry.coordinates[1])+
                                            noUndef(data.features[ind].geometry.coordinates[0])+" </p></td></tr>\\n";
                            saida = saida + "<script> function Click$id_table"+ind+"() \{   \\n"+
                                            "      pos="+ind+" ; \\n" +
                                            "      glbLngNow = \'"+data.features[ind].geometry.coordinates[0]+"\' ; \\n" +
                                            "      glbLatNow = \'"+data.features[ind].geometry.coordinates[1]+"\' ; \\n" +  
                                            "      $id_lat = glbLatNow;  $id_lon = glbLngNow; \\n" + 
                                            "      SetMarkerOnNewLocation(glbLatNow,glbLngNow); \\n" + 
                                            "      $(\"#$id\").val(\'"+data.features[ind].properties.name+"\'); \\n"+
                                            "      $(\"#$id_div\").slideUp(); \\n"+
                                            " \} <\/script>\\n";


                           ind++;  
                        }
                    }
                    saida = saida + "</table>";

                    $('.$id_table').on({
                        mouseenter: function () {
                            $(this).attr('opacity', "0.5");
                            // alert("sdsdfs");
                        },
                        mouseleave: function () {
                            $(this).attr('opacity', "1.5");

                        }    
                    }); 
             
                    // $("#$id_div").append(saida);
                    $("#$id_div").html(saida);

                   var position = $('#$id').position();
                   // $( "p:last" ).text( "left: " + position.left + ", top: " + position.top );
                   $("#$id_div").css('width', $('#$id').width());
                   
   
                   // $("#$id_div").css('height', "10vh"); 
            
                   
                   // alert(countItens(data));
            
                   $("#$id_div").css('height', 34*countItens(data) );
            
                   $("#$id_div").css('left', position.left);  
                   $("#$id_div").css('top', position.top + $('#$id').height()+10);  
                   // $("#$id_div").show(0); 
                   // alert(saida);

                   $("#$id_div").slideDown();
                   setTimeout(function() 
                   {
                      $("#$id_div").slideUp();  
                   }, 40000);   

            };
            // End function ajax proc declaration
            //////////////////////////////////
            
            $.support.cors = true;
            // callJson = $.getJSON(sSite , functProcessJson);
           // alert(list[0]); opacity: 0.9;
            
            callJson$id = $.ajax
            ({ 
                url: sSite,
                data: {   },
                type: "GET",
                async: true,
                crossDomain: true,
                dataType : "json",
                success: functProcessJson
            }); 
        }
        // Fim AutoCompleteGeneration$id Function
        ////////////////////////////////////////////////////////////////////////
        </script>    
	<style>

	</style>

            
EOT;
     return $buffer;
}
////////////////////////////////////////////////////////////////////////////////

function MapaGoogle($RealTime = 1) {
    // $_SESSION['userId']=$resultado[1][0];
    // $_SESSION['userEmail']=$emailLog;
    // $email=$_POST['EMailHid'];


    if (!isLogged())
        return;

    $idUser = $_SESSION['userId'];
    $idDevice = $_POST['SelectedDeviceHid'];
    $menuRegions = MenuRegions("menuRegions", $idUser);

    $waitDialog = jsFormWait("1");

    echo <<<EOT
      <html>
        <head>
            <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
            <meta charset="utf-8">
            <title>Simple markers</title>
            <style>
              html, body, #map-canvas {
                height: 100%;
                margin: 0px;
                padding: 0px
              }
            </style>
             <style type="text/css">
               .labels {
                 color: white;
                 background-color: white;
                 // font-family: "Lucida Grande", "Arial", sans-serif;
                 font-family: "WinterthurCondensed", sans-serif;
                 font-size: 5px;
                 text-align: center;
                 width: 40px;   
                 white-space: nowrap;
               }
             </style>
            <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=drawing"></script> 
             <script src="/ChildMonitor/Util/markerwithlabel.js"></script>
            <script>
            var map;
            var lLat=0.0;
            var lLong=0.0;
            var idDevice='';
            var dTimeStamp='';
            var marker=null;
            var iTimer=null;
            var iTimerRegions=null;
            var listener;
            var mapOptions;
            var myLatlng;
            // var image = 'Img/icons-green/nursery.png';
            var image = 'Img/marker-icon.png';
            var imageMin = 'Img/MapMarker_Ball_Right_Chartreuse.png';
            var infoWindow=null;
            var pathDevice = [];
            var firstTime=0;
            var RealTime=$RealTime; 
            var bJsonReady=false; 

            ////////////////////////////////////
            function initialize() 
            {
                  if(firstTime==1)
                    return;
                  firstTime=1; 
                      GetAjaxData();
                  
                  myLatlng = new google.maps.LatLng(lLat,lLong);
                  mapOptions = {zoom: 3,center: myLatlng, mapTypeId: google.maps.MapTypeId.HYBRID};
                  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                  map.setMapTypeId(google.maps.MapTypeId.ROADMAP); 
                  iTimer=setInterval(function () {myTimer()}, 5000); // 5 segundos
                  myTimer(); 
            }
            ////////////////////////////////////
            var editMode=0;
            function myTimer() 
            {
                    console.log("Timer -------- ");
                    if(RealTime==1)
                       GetAjaxData();
                    // map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                    console.log("lLat,lLong = "+lLat+","+lLong);;
                    myLatlng = new google.maps.LatLng(parseFloat(lLat),parseFloat(lLong));
                    mapOptions = {zoom: 16,center: myLatlng};
                    if (map.getZoom() < 4.5 && pathDevice.length!=0) 
                        map.setOptions(mapOptions); 
 
                    drawIconPositions(map);
            
                    if(editMode==1)
                       EditInterestAreas(map);
            
                    if(RealTime==0 )
                    {  
                       // if(bJsonReady==true)  
                          clearInterval(iTimer);
                       // setTimeout(function () {drawPath(map);}, 1000);  
                       // setTimeout(function () {drawRegions(map);}, 2000); 
                       drawPath(map);
                       drawRegions(map);
                       return;
                    }
                    clearInterval(iTimer);
                    iTimer=setInterval(function () {myTimer()}, 15000);
             } 
            ////////////////////////////////////////////////////////////////////
             /**
             * Data for the markers consisting of a name, a LatLng and a zIndex for
             * the order in which these markers should display on top of each
             * other.
             */
            ////////////////////////////////////////////////////////////////////
            var PathDevice=null;
            function drawPath(map)
            {
                if(PathDevice!=null)
                   PathDevice.setMap(null);
                PathDevice = new google.maps.Polyline({
                    path: pathDevice,
                    geodesic: true,
                    strokeColor: '#00FF00',
                    strokeOpacity: 1.0,
                    strokeWeight: 0.8
                });
                PathDevice.setMap(map);
            }
            //////////////////////////////////// 
            function SetMarkers(markers,map)
            {
                for (var i = 0; i < markers.length; i++) 
                {
                    markers[i].setMap(map);  
                }
            }
            ////////////////////////////////////
            var marker = [];
            var infoWindow = [];
                array_of_functions = [ 
                    function() { ShowInfoWindow(0); },
                    function() { ShowInfoWindow(1); },
                    function() { ShowInfoWindow(2); },
                    function() { ShowInfoWindow(3); },
                    function() { ShowInfoWindow(4); },
                    function() { ShowInfoWindow(5); },
                    function() { ShowInfoWindow(6); },
                    function() { ShowInfoWindow(7); },
                    function() { ShowInfoWindow(8); },
                    function() { ShowInfoWindow(9); },
                    function() { ShowInfoWindow(10); },
                    function() { ShowInfoWindow(11); },
                    function() { ShowInfoWindow(12); },
                    function() { ShowInfoWindow(13); },
                    function() { ShowInfoWindow(14); },
                    function() { ShowInfoWindow(15); },
                    function() { ShowInfoWindow(16); },
                    function() { ShowInfoWindow(17); },
                    function() { ShowInfoWindow(18); },
                    function() { ShowInfoWindow(19); },
                    function() { ShowInfoWindow(20); },
                    function() { ShowInfoWindow(21); },
                    function() { ShowInfoWindow(22); },
                    function() { ShowInfoWindow(23); },
                    function() { ShowInfoWindow(24); },
                    function() { ShowInfoWindow(25); },
                    function() { ShowInfoWindow(26); },
                    function() { ShowInfoWindow(27); },
                    function() { ShowInfoWindow(28); },
                    function() { ShowInfoWindow(29); },
                    function() { ShowInfoWindow(30); },
                    function() { ShowInfoWindow(31); },
                    function() { ShowInfoWindow(32); },
                    function() { ShowInfoWindow(33); },
                    function() { ShowInfoWindow(34); },
                    function() { ShowInfoWindow(35); },
                    function() { ShowInfoWindow(36); },
                    function() { ShowInfoWindow(37); },
                    function() { ShowInfoWindow(38); },
                    function() { ShowInfoWindow(39); },
                    function() { ShowInfoWindow(40); },
                    function() { ShowInfoWindow(41); },
                    function() { ShowInfoWindow(42); },
                    function() { ShowInfoWindow(43); },
                    function() { ShowInfoWindow(44); },
                    function() { ShowInfoWindow(45); },
                    function() { ShowInfoWindow(46); },
                    function() { ShowInfoWindow(47); },
                    function() { ShowInfoWindow(48); },
                    function() { ShowInfoWindow(49); },
                    function() { ShowInfoWindow(50); },
                    function() { ShowInfoWindow(51); },
                    function() { ShowInfoWindow(52); },
                    function() { ShowInfoWindow(53); },
                    function() { ShowInfoWindow(54); },
                    function() { ShowInfoWindow(55); },
                    function() { ShowInfoWindow(56); },
                    function() { ShowInfoWindow(57); },
                    function() { ShowInfoWindow(58); },
                    function() { ShowInfoWindow(59); },
                    function() { ShowInfoWindow(60); },
                    function() { ShowInfoWindow(61); },
                    function() { ShowInfoWindow(62); },
                    function() { ShowInfoWindow(62); },
                    function() { ShowInfoWindow(64); },
                    function() { ShowInfoWindow(65); },
                    function() { ShowInfoWindow(66); },
                    function() { ShowInfoWindow(67); },
                    function() { ShowInfoWindow(68); },
                    function() { ShowInfoWindow(69); },
                    function() { ShowInfoWindow(70); },
                    function() { ShowInfoWindow(71); },
                    function() { ShowInfoWindow(72); },
                    function() { ShowInfoWindow(73); },
                    function() { ShowInfoWindow(74); },
                    function() { ShowInfoWindow(75); },
                    function() { ShowInfoWindow(76); },
                    function() { ShowInfoWindow(77); },
                    function() { ShowInfoWindow(78); },
                    function() { ShowInfoWindow(79); },
                    function() { ShowInfoWindow(80); },
                    function() { ShowInfoWindow(81); },
                    function() { ShowInfoWindow(82); },
                    function() { ShowInfoWindow(83); },
                    function() { ShowInfoWindow(84); },                         
                    function() { ShowInfoWindow(85); }
                ];             
            var deviceAnt="";
            function drawIconPositions(map)
            {
                var i=0;
                if(pathDevice.length==0)
                {
                  return;
                }

            
                SetMarkers(marker,null);
                infoWindow = [];
                // array_of_functions = [];

                var Title = infoPoint[i].Label+'\\n'+infoPoint[i].idDevice+'\\n'+infoPoint[i].TimeStamp+'\\nVelocity:'+infoPoint[i].Velocity+
                           '\\nBearing:'+infoPoint[i].Bearing +'\\nBatteryLevel:'+infoPoint[i].BatteryLevel+'\\nAccuracy:'+infoPoint[i].Accuracy;
                var Info = infoPoint[i].Label+'<br>'+infoPoint[i].idDevice+'<br>'+infoPoint[i].TimeStamp+'<br>Velocity:'+infoPoint[i].Velocity+
                           '<br>Bearing:'+infoPoint[i].Bearing +'<br>BatteryLevel:'+infoPoint[i].BatteryLevel+'<br>Accuracy:'+infoPoint[i].Accuracy;
                
                var pictureLabel = "";
                var labelClass = "";
                if(deviceAnt!=infoPoint[i].idDevice)
                {
                    if(infoPoint[i].icon!="")
                    {
                       // alert(infoPoint[i].icon);
                       pictureLabel = document.createElement("img");
                       pictureLabel.style.width = '40px'; // pictureLabel.style.height = '40px';
                       pictureLabel.src = infoPoint[i].icon;
                       labelClass="labels"
                    }            
                }
                deviceAnt=infoPoint[i].idDevice;
            
                marker[i] = new MarkerWithLabel({
                    position: pathDevice[i],
                    map: map,
                    icon: image,
                    title: Title,
                    labelContent: pictureLabel,
                    labelAnchor: new google.maps.Point(22, 0),
                    labelClass: labelClass, // the CSS class for the label
                    labelStyle: {opacity: 0.70}, 
                    zIndex: 0
                });   
                infoWindow[i] = new google.maps.InfoWindow({ content: Info });
                google.maps.event.addListener(marker[i], "click", array_of_functions[i]);
 
            
                for (var i = 1; i < pathDevice.length; i++) 
                {
                    var Title = infoPoint[i].Label+'\\n'+infoPoint[i].idDevice+'\\n'+infoPoint[i].TimeStamp+'\\nVelocity:'+infoPoint[i].Velocity+
                                '\\nBearing:'+infoPoint[i].Bearing +'\\nBatteryLevel:'+infoPoint[i].BatteryLevel+'\\nAccuracy:'+infoPoint[i].Accuracy;
                    var Info = infoPoint[i].Label+'<br>'+infoPoint[i].idDevice+'<br>'+infoPoint[i].TimeStamp+'<br>Velocity:'+infoPoint[i].Velocity+
                           '<br>Bearing:'+infoPoint[i].Bearing +'<br>BatteryLevel:'+infoPoint[i].BatteryLevel+'<br>Accuracy:'+infoPoint[i].Accuracy;
            
                    
                    var icon;
                    if(RealTime==1)
                       icon = image;
                    else
                       icon = imageMin; 
            
                    var pictureLabel = "";
                    var labelClass = "";
                    if(deviceAnt!=infoPoint[i].idDevice)
                    {
                        if(infoPoint[i].icon!="")
                        {
                           // alert(infoPoint[i].icon);  
                           pictureLabel = document.createElement("img");
                           pictureLabel.style.width = '40px'; // pictureLabel.style.height = '40px';
                           pictureLabel.src = infoPoint[i].icon;
                           labelClass="labels"
                        }            
                    }
                    deviceAnt=infoPoint[i].idDevice;
            
                    marker[i] = new MarkerWithLabel({
                        position: pathDevice[i],
                        map: map,
                        icon: icon,
                        title: Title,
                        labelContent: pictureLabel,
                        labelAnchor: new google.maps.Point(22, 0),
                        labelClass: labelClass, // the CSS class for the label
                        labelStyle: {opacity: 0.50},
                        zIndex: -10
                    });
                   if(i<62)
                   {
                       infoWindow[i] = new google.maps.InfoWindow({ content: Info });
                       // array_of_functions[i]= eval("ShowInfoWindow("+i+")"); 
                       google.maps.event.addListener(marker[i], "click", array_of_functions[i]);
                   }
                   
                }
            }            
            ////////////////////////////////////
            function ShowInfoWindow(i)
            {
               infoWindow[i].open(map, marker[i]);
               
            }
            ////////////////////////////////////
            var infoPoint = []; 
            function processJsonData(json)
            {
                // console.log("Depurando JSON");
                // console.log(json.numItens); 
                pathDevice = [];
                for(i = 0; i < json.numItens; i++) 
                {
                   pathDevice[i]=new google.maps.LatLng(parseFloat(json.vet[i].Lat), parseFloat(json.vet[i].Long));
                }
                infoPoint = [];
                var Item = makeStruct("Label idDevice TimeStamp Velocity Bearing BatteryLevel Address icon Accuracy");
                 
                GetAddress(json.vet[0].Lat, json.vet[0].Long,0);     
                for(i = 0; i < json.numItens; i++) 
                {
                   infoPoint[i]=new Item(json.vet[i].Label, json.vet[i].idDevice, json.vet[i].TimeStamp, json.vet[i].Velocity,json.vet[i].Bearing, json.vet[i].BatteryLevel, "", json.vet[i].icon, json.vet[i].Accuracy);
                }
 
            }
            ///////////////////////////////////
            var drawingManager=null;
            var circles = [];
            function EditInterestAreas(map)
            {
            
                drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.MARKER,
                drawingControl: true,
                drawingControlOptions: {
                  position: google.maps.ControlPosition.LEFT_CENTER,
                  drawingModes: [ google.maps.drawing.OverlayType.CIRCLE ]
                },
                circleOptions: {
                  fillColor: '#00ff00',
                  fillOpacity: 0.25,
                  strokeColor: '#00ff00',
                  strokeOpacity: 0.45,
                  strokeWeight: 1,
                  clickable: false,
                  editable: false,
                  zIndex: -10
                }
                });
                google.maps.event.addDomListener(drawingManager, 'circlecomplete', function(circle) { SaveCircle(circle);});
                drawingManager.setMap(map);
            }
            //////////////////////////////////
            function SaveCircle(circle)
            { 
               circles.push(circle); 
               SaveRegionOnServer();
               editMode=0;
               drawingManager.setMap(null);
               // firstTime=0;
               // initialize(); 
               $("form:first").submit(); 
            } 
            ////////////////////////////////////
            function GetAddress(lat,lng,i) 
            {
                // var lat = parseFloat(document.getElementById("txtLatitude").value);
                // var lng = parseFloat(document.getElementById("txtLongitude").value);
                var latlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                var geocoder = geocoder = new google.maps.Geocoder();
                var buf="";
                geocoder.geocode({ 'latLng': latlng }, function (results, status) 
                {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            // alert("Location: " + results[1].formatted_address);
                            infoPoint[i].Address = results[1].formatted_address;
                        }
                    }
                });
             }
            //////////////////////////////////
            function SetEditMode()
            {
                if(editMode==1)
                {
                   editMode=0;
                   trashMode=0; 
                   google.maps.event.removeListener(trashListener);
                   drawingManager.setMap(null); 
                }  
                else
                   editMode=1;    
                myTimer(); 
            }  
            //////////////////////////////////
            var trashMode=0;
            var trashListener;
            function SetTrashMode()
            {
                CreateDialogDeleteRegion(); 
            }  
            ///////////////////////////////////
            function CreateDialogDeleteRegion()
            { 
                $( "$menuRegions" ).dialog({title: "Delete Region", buttons: [ {  text: "Close", click: function() { $( this ).dialog( "destroy" ); return; } }]});
                $( "#menuRegions" ).menu({ select: function( event, ui ) {SelectedMenuRegion( event, ui );} });
            }
            ///////////////////////////////////
            function SelectedMenuRegion( event, ui )
            {
               var menuId = ui.item.context.innerHTML;
               RemoveRegionFromServer(ui.item.context.innerHTML);
               $("form:first").submit(); 
               // firstTime=0;
               // initialize(); 
            }
            ///////////////////////////////////
            function GetMonitoredRegions()
            {
                console.log("GetMonitoredRegions");
                $.ajax
                ({
                    url: "Util/GetMonitoredRegions.php",
                   data: { UserId: $idUser },
                   type: "GET",
                   async: false,
                   dataType : "json",
                    success: function( json ) 
                    {
                        console.log("Depurando JSON processRegionsJsonData");
                        processRegionsJsonData(json);           
                    },
                    error: function( xhr, status, errorThrown ) 
                    {
                        console.log( "Erro json :" + errorThrown );
                        console.dir( xhr );
                    },
                    complete: function( xhr, status ) { }
                });    
                // drawRegions(map)
            }
            ///////////////////////////////////
            var infoRegion = [];
            var RadiusRegion = [];
            function processRegionsJsonData(json)
            {
                console.log("processRegionsJsonData");
                console.log(json.numItens); 
                var rCenter; 
                var rRadius;
                infoRegion = [];
                RadiusRegion = [];
                for(i = 0; i < json.numItens; i++) 
                {
                    console.log("Radius:" + json.vet[i].Radius);
                    rCenter = new google.maps.LatLng(json.vet[i].aLat, json.vet[i].aLong); 
                    infoRegion[i] = {
                      fillColor: '#ff0000',
                      fillOpacity: 0.25,
                      strokeColor: '#ff0000',
                      strokeOpacity: 0.45,
                      strokeWeight: 1,
                      zIndex: -10,                     
                      center: rCenter,
                      radius: parseFloat(json.vet[i].Radius)
                    };
                    // RadiusRegion[i]=json.vet[i].Radius;
               };
     
            
            }
            ///////////////////////////////////
            var circleRegion = [];
            function drawRegions(map)
            {
                  /*
                  var searchArea = new google.maps.LatLng(25.435833800555567, -80.44189453125);
                  // Draw a circle around the radius
                  var circle = new google.maps.Circle({
                    center: searchArea,
                    radius: 4000000, 
                    strokeColor: "#0000FF",
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: "#0000FF",
                    fillOpacity: 0.4
                   });          
                   circle.setMap(map);  
                  */
   

                console.log("Drawing circles");
                circleRegion = [];
                for(i = 0; i < infoRegion.length; i++) 
                {
                   console.log(infoRegion[i]);
                   circleRegion[i] = new google.maps.Circle(infoRegion[i]);
                   // circleRegion[i].setRadius(parseFloat(RadiusRegion[i]));  
                   circleRegion[i].setMap(map);
                   google.maps.event.addListener(circleRegion[i], 'click', function(ev){ CircleClick(ev); }); 
                }
                // cityCircle = new google.maps.Circle(infoRegion[i]);
                // clearInterval(iTimerRegions); 
            }
            ///////////////////////////////////
            function CircleClick(ev)
            {
                // if(infoWindow!=null)
                //   infoWindow.close();
                
                
                if(trashMode==1)
                { 
                    alert(ev.latLng.lat()+" "+ev.latLng.lng());
                     // google.maps.geometry.spherical.computeDistanceBetween (latLngA, latLngB);
                    // var circ = FindRightCircle(ev.latLng.lat(),ev.latLng.lng());  
                    // RemoveRegionFromServer(ev.latLng.lat(),ev.latLng.lng());
                    trashMode=0; 
                    editMode=0;
                    initialize();
                }
                // infoWindow = new google.maps.InfoWindow({ content: "sasas" });
            
                // infoWindow.open(map, ev.latLng);
            
            }
            ///////////////////////////////////
            function RemoveRegionFromServer(address)
            {
                var SendData = { idUser: $idUser, Address: address };
                
                $.ajax
                ({
                       url: "Util/RemoveMonitoredRegion.php",
                       data: SendData,
                       type: "POST",
                       async: false,
                       dataType : "json",
                       success: function( json ) 
                       {
                           // console.log("Depurando JSON");
                           // lLat = json.vet[0].Lat;
                           // alert("Region saved");  
                       },
                       error: function( xhr, status, errorThrown ) 
                       {
                           // console.log( "Erro json :" + errorThrown );
                           // console.dir( xhr );
                           // alert("Error saving region: " + errorThrown);     
                       },
                       complete: function( xhr, status ) 
                       {
                           // alert("Region saved");
                       }
                });    
            
            }
            ///////////////////////////////////
            function SaveRegionOnServer()
            {
                for (var i = 0; i < circles.length; i++) 
                {
                  var circleCenter = circles[i].getCenter();
                  var circleRadius = circles[i].getRadius();
                  console.log("circle(("+circleCenter.lat().toFixed(3) + "," + circleCenter.lng().toFixed(3)+"), "+circleRadius.toFixed(3) + ")");
                }   
                // $('#SelectedDeviceHid').val (); 
                var SendData = { idUser: $idUser, Lat: circleCenter.lat(), Long: circleCenter.lng(), Radius: circleRadius };
                // alert(circleCenter.lat());
                $.ajax
                ({
                       url: "Util/SendMonitoredRegion.php",
                       data: SendData,
                       async: false,
                       type: "POST",
                       dataType : "json",
                       success: function( json ) 
                       {
                           // console.log("Depurando JSON");
                           // lLat = json.vet[0].Lat;
                           // alert("Region saved");  
                       },
                       error: function( xhr, status, errorThrown ) 
                       {
                           // console.log( "Erro json :" + errorThrown );
                           // console.dir( xhr );
                           // alert("Error saving region: " + errorThrown);     
                       },
                       complete: function( xhr, status ) 
                       {
                           // alert("Region saved");
                       }
                });    
    
            }
            ///////////////////////////////////
            // var Item = makeStruct("id speaker country");
            // var row = new Item(1, 'john', 'au');
            function makeStruct(names) 
            {
              var names = names.split(' ');
              var count = names.length;
              function constructor() {
                for (var i = 0; i < count; i++) {
                  this[names[i]] = arguments[i];
                }
              }
              return constructor;
            }
            ////////////////////////////////////
            function GetAjaxData()
            {
                    // $('#SelectedDeviceHid').val (); 
                    // alert($( "#datepicker" ).val());
                    //   alert($('#selDevice').val ()+"  -  "+$( "#datepicker" ).val());
                    if(RealTime==0)
                    { 
                       $waitDialog;
                    }
                    // XXXXXXXXXX
                    console.log("Data:"+$( "#datepicker" ).val());
                    $.ajax
                    ({
                        url: "Util/GetGlobalPosition.php",
                       data: { idUser: $idUser, idDevice: $('#selDevice').val (), FilterDate: $( "#datepicker" ).val(),RealTime: RealTime  },
                       type: "GET",
                       async: false,
                       dataType : "json",
                        success: function( json ) {
                            console.log("Depurando JSON"+json.vet[0].idDevice);
                            
                            lLat = parseFloat(json.vet[0].Lat);
                            lLong = parseFloat(json.vet[0].Long);
                            idDevice = json.vet[0].idDevice;
                            dTimeStamp = json.vet[0].TimeStamp;
             
 
                            processJsonData(json);           
                            if(RealTime==0)
                               dlgWait1.dialog("close");    
                            // console.log(lLat);
                            // console.log(lLong);
                            // console.log(dTimeStamp);                       
                                },
                        error: function( xhr, status, errorThrown ) {

                            bJsonReady=true; 
                            if(RealTime==0)
                               dlgWait1.dialog("close");   
                            // console.log( "Erro json :" + errorThrown );
                            // console.dir( xhr );
                            // alert("Erro ajax");
                        },
                        complete: function( xhr, status ) 
                        { 
                             bJsonReady=true; 
                            if(RealTime==0)
                               dlgWait1.dialog("close");   
                             // alert("Ajax OK");
                        }
                    }); 
                    if(RealTime==0)
                       GetMonitoredRegions();
            }
            ////////////////////////////////////
            google.maps.event.addDomListener(window, 'load', initialize);

            </script>
        </head>
        <body>
            <div id="map-canvas" >
            <!--  <div style="float: left;" >Painel Lateral</div> -->
             <!-- <button id="savebutton">SAVE</button> -->
            <input type="hidden" id="editModeHid" name="editModeHid" >
EOT;
    // formChooseDevice();
    echo <<<EOT
            </div>
        </body>
    </html>  
EOT;
    if ($RealTime == 0)
        formChooseDevice();
    else
        formChoosePeople();
}

////////////////////////////////////////////////////////////////////////////////
function SendMonitoredRegion() {
    if ($_GET['id'] != null) {
        $idUser = $_GET['idUser'];
        $Lat = $_GET['Lat'];
        $Long = $_GET['Long'];
        $Radius = $_GET['Radius'];
    } else {
        $idUser = $_POST['idUser'];
        $Lat = $_POST['Lat'];
        $Long = $_POST['Long'];
        $Radius = $_POST['Radius'];
    }

 

    $address = trim(GetAddress($Lat, $Long));
    if (trim($address) == "")
        $address = "World";

    $address = $address . ": " . sprintf('%0.2f', $Lat) . "," . sprintf('%0.2f', $Long) . "  Radius: " . sprintf('%0.0f', $Radius) . "m";

    $query = "INSERT INTO `MonitoredRegion`(`UserId`, `aLat`, `aLong`, `Radius`, `Address`) VALUES ($idUser,$Lat,$Long,$Radius,'$address')";
    // echo $query;
    // error_log($query.PHP_EOL, 3, "/Library/WebServer/Documents/ChildMonitor/php-errors.log");
    // dbg($query);
    $resultado = db_query($query);
}

////////////////////////////////////////////////////////////////////////////////
function GetDateTime() {
    date_default_timezone_set($_COOKIE['tzo']);
    $date = date('Y-m-d H:i:s', time());
    return($date);
}

////////////////////////////////////////////////////////////////////////////////
function GetDateTimeNTZO() {
    $date = date('Y-m-d H:i:s', time());
    return($date);
}

////////////////////////////////////////////////////////////////////////////////
function GetOnlyDate() {
    date_default_timezone_set($_COOKIE['tzo']);
    $date = date('Y-m-d', time());
    return($date);
}

////////////////////////////////////////////////////////////////////////////////
function RemoveMonitoredRegion() {
    if ($_GET['id'] != null) {
        $idUser = $_GET['idUser'];
        $Address = $_GET['Address'];
    } else {
        $idUser = $_POST['idUser'];
        $Address = $_POST['Address'];
    }

    /*
      CREATE DEFINER=`root`@`localhost` FUNCTION `haversine`(`lat1` FLOAT, `lon1` FLOAT, `lat2` FLOAT, `lon2` FLOAT) RETURNS float
      NO SQL
      DETERMINISTIC
      COMMENT 'Returns the distance in degrees (now meters) on the Earth between two known points of latitude and longitude'
      BEGIN
      RETURN 6378137.0 * DEGREES(ACOS(
      COS(RADIANS(lat1)) *
      COS(RADIANS(lat2)) *
      COS(RADIANS(lon2) - RADIANS(lon1)) +
      SIN(RADIANS(lat1)) * SIN(RADIANS(lat2))
      ))
      END
     */

    // SELECT `UserId`, `aLat`, `aLong`, `Radius`, `Address`,(`Radius` - haversine(aLat,aLong,-22.913456225316892,-43.08988094329834)) FROM `MonitoredRegion` WHERE `UserId`=0 and ((`Radius` - haversine(aLat,aLong,-22.913456225316892,-43.08988094329834)) > 0.0)
    // DELETE FROM `MonitoredRegion` WHERE `UserId`=0 and ((`Radius` - haversine(aLat,aLong,-22.913456225316892,-43.08988094329834)) > 0.0)
    // $query="DELETE FROM `MonitoredRegion` WHERE `UserId`=$idUser and ((`Radius` - haversine(aLat,aLong,-22.913456225316892,-43.08988094329834)) > 0.0)";


    $query = "DELETE FROM `MonitoredRegion` WHERE `UserId`=$idUser and `Address` = '$Address' ";
    // echo $query;
    $resultado = db_query($query);
}

////////////////////////////////////////////////////////////////////////////////
function GetAddress($lat, $lng) {
    $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($lng) . '&sensor=false';
    $json = @file_get_contents($url);
    $data = json_decode($json);
    $status = $data->status;

    // echo mb_detect_encoding( "Rua Goitacases, 20 - SÃ£o Francisco, NiterÃ³i - RJ...", "auto" );

    if ($status == "OK")
        return $data->results[0]->formatted_address;
    else
        return false;
}

////////////////////////////////////////////////////////////////////////////////
function ActivationEmailSimple($email, $passwordLog,$app="Nope") 
{
    require 'GlobalVars.php';
    // $link = "http://ec2-54-200-152-177.us-west-2.compute.amazonaws.com/ChildMonitor/Util/UserActivation.php?Email=$email&Password=$passwordLog";
    // https://serverapps.tk/ChildMonitor/
    
    $link = "https://singularitystorm.com/ChildMonitor/Util/UserActivation.php?Email=$email&Password=$passwordLog&App=$app";
    // dbg($link);
    if($app==="VAGPROVRESET")
    {
        mail($email, "ParkingFit Provider Reset Password", "Click on link below to change user password:\n\n $link");
        // error_log($link);
    }
    else  
    if($app==="ParkingFitUserReset")
    {
        mail($email, "ParkingFit User Reset Password", "Click on link below to change user password:\n\n $link");
        // error_log($link);
    }
    else
    {   
        mail($email, "$app Confirm Signin", "Click on link below to activate user:\n\n $link");
        mail("andrerezende_br@yahoo.com.br", "User $email Registred", "$User $email Registred\n\n $link");
    }
}

////////////////////////////////////////////////////////////////////////////////
function ActivateParkingFit($Email,$Password) 
{
    $sql = "select email,password from `ParkingProvider` where email='$Email'";
    // dbg($sql);
    $resultado = db_query($sql);
    if ($resultado[1][0] == "") {
        echo "Invalid email";
        return;
    }
    $passwordDb = $resultado[1][1];
    
    if (hash('sha256', $passwordDb . "ParkingFit") != $Password) {
        echo "Invalid password";
        return;
    }
    // $sql="select id from \"UserMonitThings\" where \"UserId\"=$id and id='$IdDevice'"; // postgres

    $sql = "UPDATE `ParkingProvider` SET `Active`=1 WHERE `email`='$Email'";
    $resultado = db_query($sql);

    echo "Activation Ok";    
}
////////////////////////////////////////////////////////////////////////////////
function ActivateParkingFitUser($Email,$Password)
{
    $sql = "select email,password from `ParkingFitUser` where email='$Email'";
    // dbg($sql);
    $resultado = db_query($sql);
    if ($resultado[1][0] == "") {
        echo "Invalid email";
        return;
    }
    $passwordDb = $resultado[1][1];
    
    if (hash('sha256', $passwordDb . "ParkingFit") != $Password) {
        echo "Invalid password";
        return;
    }
    // $sql="select id from \"UserMonitThings\" where \"UserId\"=$id and id='$IdDevice'"; // postgres

    $sql = "UPDATE `ParkingFitUser` SET `Active`=1 WHERE `email`='$Email'";
    $resultado = db_query($sql);

    echo "Activation Ok";    
}
//////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
function ResetPasswordParkingFit($Email,$context="provider")
{
    $passwordLog = hash('sha256', $Email . "ParkingFit");
    echo "<script> window.location.href = \"https://singularitystorm.com/ChildMonitor/index.php?Email=$Email&pwdreset=$passwordLog&context=$context&stme=VAG\"; </script>";
}
////////////////////////////////////////////////////////////////////////////////
function ParkingFitHomePage()
{
   // error_log("ParkingFitHomePage");
   $Email = $_GET['Email'];
   if($_GET['context']=="provider" )
   {
      $table = "ParkingProvider";
      $cmdchangepasswd = "<script>  DialogChgPassword('$Email','ParkingFit'); </script>";
   }
   if($_GET['context']=="User" )
   {
      $table = "ParkingFitUser"; 
      $cmdchangepasswd = "<script>  DialogChgPassword('$Email','ParkingFitUser'); </script>";
   }
   if($_GET['pwdreset']!="" )
   {    
       $Email = $_GET['Email'];
//       error_log("pwdreset - ".$_GET['pwdreset']);
//       error_log("email - ".$Email);
//       error_log("hash - ".hash('sha256', $Email . "ParkingFit"));
       if(hash('sha256', $Email . "ParkingFit")==$_GET['pwdreset'])
       {
          $sql = "UPDATE `$table` SET `Active`=1 WHERE `email`='$Email'";
          $resultado = db_query($sql);
          echo $cmdchangepasswd;
       }
       else
          echo "<script> ParkingFitHomePage(); </script>";  
   }    
   else
      echo "<script> ParkingFitHomePage(); </script>";  
}
////////////////////////////////////////////////////////////////////////////////
function UserActivation() {
    if ($_GET['Email'] != null) {
        $Email = $_GET['Email'];
        $Password = $_GET['Password'];
        $ProtocolVersion = $_GET['ProtocolVersion'];
        $app = $_GET['App'];
    } else {
        $Email = $_POST['Email'];
        $Password = $_POST['Password'];
        $ProtocolVersion = $_POST['ProtocolVersion'];
        $app = $_GET['App'];
    }
    $Email=  strtolower($Email);
    // insert into "MonitoredThings" (id, "Lat", "Long", "TimeStamp", "Velocity") values( 12,-12.34,-43.19,2000-11-11 13:23:02,3)
    // http://localhost/ChildMonitor/Util/SendGlobalPosition.php?id=12&Lat=-12.34&Long=-43.09&TimeStamp=%222000-11-11+13%3A23%3A02%22&Velocity=3
    // $sql="select email,id from \"User\" where email='$Email' and password='$Password'"; // postgres
    if($app=="VAG")
    {
       return ActivateParkingFit($Email,$Password);
    }
    if($app=="VAGPROVRESET")
    {
       $sql = "UPDATE `ParkingProvider` SET `Active`=1 WHERE `email`='$Email'";
       $resultado = db_query($sql);
       return ResetPasswordParkingFit($Email);
    }
    if($app=="ParkingFitUser")
    {
       return ActivateParkingFitUser($Email,$Password);
    }
    if($app=="ParkingFitUserReset")
    {
       $sql = "UPDATE `ParkingFitUser` SET `Active`=1 WHERE `email`='$Email'";
       $resultado = db_query($sql);
       return ResetPasswordParkingFit($Email,"User");
    }    
    
    $sql = "select email,fbId,password from User where email='$Email'";
    // dbg($sql);
    $resultado = db_query($sql);
    if ($resultado[1][0] == "") {
        echo "Invalid email";
        return;
    }
    $passwordDb = $resultado[1][2];

    if ($passwordDb != $Password) {
        echo "Invalid password";
        return;
    }
    // $sql="select id from \"UserMonitThings\" where \"UserId\"=$id and id='$IdDevice'"; // postgres

    $sql = "UPDATE `User` SET `Active`=1 WHERE `email`='$Email'";
    $resultado = db_query($sql);

    echo "Activation Ok";
}

////////////////////////////////////////////////////////////////////////////////        
function ActivationEmail() {
    require_once('PHPMailer_5.2.4/class.phpmailer.php');
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

    $mail = new PHPMailer();

    //  $body = file_get_contents('contents.html');
    // $body = preg_replace('/[\]/', '', $body);
    // $mail->Debugoutput = 'html';
    $mail->IsSMTP(); // telling the class to use SMTP
    // $mail->Host = "mail.yourdomain.com"; // SMTP server
    $mail->SMTPDebug = 2;                     // enables SMTP debug information (for testing)
    // 1 = errors and messages
    // 2 = messages only
    $mail->SMTPAuth = true;                  // enable SMTP authentication
    $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
    $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
    $mail->Port = 465;                   // set the SMTP port for the GMAIL server
    $mail->Username = "ageservidorbackup@gmail.com";  // GMAIL username
    $mail->Password = "******";            // GMAIL password

    $mail->SetFrom('ageservidorbackup@gmail.com', 'First Last');

    $mail->AddReplyTo("ageservidorbackup@gmail.com", "First Last");

    $mail->Subject = "PHPMailer Test Subject via smtp (Gmail), basic";

    $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

    $body = "oi";
    $mail->MsgHTML($body);

    $address = "andrerezende_br@yahoo.com.br";
    $mail->AddAddress($address, "User Monitor");

    // $mail->AddAttachment("images/phpmailer.gif");      // attachment
    // $mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

    if (!$mail->Send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Message sent!";
    }
}

////////////////////////////////////////////////////////////////////////////////
function PayPallButtonFrame() {
    //<iframe src=$siteENUS
    //        frameborder="0px" width="95%" height="95%"
    //        marginwidth="0px" marginheight="0px" style="margin-top: 2%; margin-left: 2%;" >
    //</iframe>
    $buffer = <<<EOT
    <iframe src="Util/payPallButton.php" frameBorder="0" width="95%" height="140px"
            marginwidth="0px" marginheight="0px" style="margin-top: 0%; margin-left: 0%;" >
    </iframe>   
EOT;
    return $buffer;
}

////////////////////////////////////////////////////////////////////////////////
function PayPallButton() {
    // Conta
    // ageservidorbackup@gmail.com
    $buffer = <<<EOT
        

    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
    <input type="hidden" name="cmd" value="_s-xclick">
    <input type="hidden" name="hosted_button_id" value="9PDBDNKNK2BT2">
    <table>
    <tr><td><input type="hidden" name="on0" value="Subscription"></td></tr><tr><td><select name="os0">
            <option value="1 month">1 month $1,99 USD</option>
            <option value="6 months">6 months $10,99 USD</option>
            <option value="1 year">1 year $22,99 USD</option>
    </select> </td></tr>
    </table>
    <input type="hidden" name="currency_code" value="USD">
    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
    <img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1">
    </form>


             
EOT;
    return $buffer;
}

////////////////////////////////////////////////////////////////////////////////
function PayPallButtonSandBox() {
    // PayPall PDT protocol
    // You have successfully saved your preferences. Please use the following 
    // identity token when setting up Payment Data Transfer on your website. 
    // E5x9SYXzIWJCoodNBErIN4gr4ciZTsqsrTlE7QwP_81utflPeNyL29b36kW
    //
    // First response from PayPall
    // http://childmonitorweb.com/PayPallPaymentOk.php?tx=4J027428F4842115X&st=Completed&amt=4.99&cc=USD&cm=&item_number=001
    //amt	["4.99"]
    //cc	["USD"]
    //cm	[""]
    //item_number	["001"]
    //st	["Completed"]
    //tx        ["4J027428F4842115X"]
    //
    //
    //How PDT Works
    //
    //When a customer pays you, PayPal sends the transaction ID of the payment to you by appending the transaction ID to a URL you specify in your account Profile. This information is sent via a HTTP GET as this name/value pair:
    //
    //    tx=transactionID 
    //
    //After you get the transaction ID, you post a FORM to PayPal that includes the transaction ID and your identity token, a string value that identifies your account to PayPal. There are instructions below that describe how to get your identity token. The form looks like this:
    //
    //    <form method=post action="https://www.paypal.com/cgi-bin/webscr">
    //    <input type="hidden" name="cmd" value="_notify-synch">
    //    <input type="hidden" name="tx" value="TransactionID">
    //    <input type="hidden" name="at" value="YourIdentityToken">
    //    <input type="submit" value="PDT">
    //    </form> 
    //
    //In PayPal's reply to your post, the first line will be SUCCESS or FAIL. An example successful response looks like this (HTTP Header has been omitted):
    //
    //    SUCCESS
    //    first_name=Jane+Doe
    //    last_name=Smith
    //    payment_status=Completed
    //    payer_email=janedoesmith%40hotmail.com
    //    payment_gross=3.99
    //    mc_currency=USD
    //    custom=For+the+purchase+of+the+rare+book+Green+Eggs+%26+Ham
    //    ... 
    //
    //If the response contains the word FAIL, make sure that the transaction ID token and identity token on your post are correct. PayPal was not able to validate the transaction.
    //
    //For a list of all possible variables and values that PayPal can return to you, see the IPN/PDT Variable Reference.
    //
    //This diagram shows the basic flow of a PDT transaction.
    //    
    $buffer = <<<EOT
        
    <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
    <input type="hidden" name="cmd" value="_s-xclick">
    <input type="hidden" name="hosted_button_id" value="V63TWJXQPYELG">
    <table>
    <tr><td><input type="hidden" name="on0" value="Subscription"></td></tr><tr><td><select name="os0">
            <option value="1 month">1 month $4.99 USD</option>
            <option value="6 months">6 months $28.99 USD</option>
            <option value="1 year">1 year $50.00 USD</option>
    </select> </td></tr>
    </table>
    <input type="hidden" name="currency_code" value="USD">        
    <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
    <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
    </form>




             
EOT;
    return $buffer;
}

////////////////////////////////////////////////////////////////////////////////
// Ver este http://www.html-form-guide.com/php-form/php-form-submit.html
// muito bom
function PhpPostToPayPall($status, $transaction_id, $command, $IdentityToken) {
    //After you get the transaction ID, you post a FORM to PayPal that includes the transaction ID and your identity token, a string value that identifies your account to PayPal. There are instructions below that describe how to get your identity token. The form looks like this:
    //
    //    <form method=post action="https://www.paypal.com/cgi-bin/webscr">
    //    <input type="hidden" name="cmd" value="_notify-synch">
    //    <input type="hidden" name="tx" value="TransactionID">
    //    <input type="hidden" name="at" value="YourIdentityToken">
    //    <input type="submit" value="PDT">
    //    </form> 
    //    
    //            
    //extract data from the post
    // extract($_POST);
    //set POST variables
    //
    // $url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
    $url = 'https://www.paypal.com/cgi-bin/webscr';
    $fields = array(
        'cmd' => urlencode($command),
        'tx' => urlencode($transaction_id),
        'at' => urlencode($IdentityToken),
        'submit' => urlencode("PDT")
    );

    $result = PhpPost($url, $fields);
    return($result);
}

////////////////////////////////////////////////////////////////////////////////
function PhpPost($url, $fields) {
    //extract data from the post
    // extract($_POST);
    //url-ify the data for the POST 
    // USAR http_build_query($data);??????? 
    // dbg($fields);
    foreach ($fields as $key => $value) {
        $fields_string .= $key . '=' . $value . '&';
    }
    rtrim($fields_string, '&');

    // dbg($fields_string);
    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

    // use output buffering instead of returntransfer -itmaybebuggy
    ob_start();
    //execute post
    $result = curl_exec($ch);
    if (!$result)
        return("Post Error");
    //close connection
    curl_close($ch);
    $retrievedhtml = ob_get_contents();
    ob_end_clean();
    return($retrievedhtml);
}

////////////////////////////////////////////////////////////////////////////////
function ParsePayPallPDTResponse($response) {

    // parse the data
    $lines = explode("\n", $response);
    $keyarray = array();
    if (strcmp($lines[0], "SUCCESS") == 0) {
        for ($i = 1; $i < count($lines); $i++) {
            list($key, $val) = explode("=", $lines[$i]);
            $keyarray[urldecode($key)] = urldecode($val);
        }
        // check the payment_status is Completed
        // check that txn_id has not been previously processed
        // check that receiver_email is your Primary PayPal email
        // check that payment_amount/payment_currency are correct
        // process payment
        $firstname = $keyarray['first_name'];
        $lastname = $keyarray['last_name'];
        $itemname = $keyarray['item_name'];
        $amount = $keyarray['payment_gross'];
        return($keyarray);
    } else if (strcmp($lines[0], "FAIL") == 0) {
        // log for manual investigation
        echo "fail";
    } else {
        echo "fail2";
    }
}

////////////////////////////////////////////////////////////////////////////////
function ProcessPayPallPayment() {
    //dbg($_GET['st']);
    //dbg($_GET['tx']);
    //dbg($_GET['cm']);
    //dbg($_GET['item_number']);
    if (!isLogged())
        return;
    formHeaders();
    $status = $_GET['st'];
    $transaction_id = $_GET['tx'];
    $item_number = $_GET['item_number'];
    $command_received = $_GET['cm'];

    $useremail = $_SESSION['userEmail'];
    $userId = $_SESSION['userId'];
    $DatePaymentLoc = GetDateTime();

    if ($_GET['st'] = "Completed") {
        //dbg("Sending response to paypall");
        $command = "_notify-synch";
        $IdentityToken = "E5x9SYXzIWJCoodNBErIN4gr4ciZTsqsrTlE7QwP_81utflPeNyL29b36kW";
        $return = PhpPostToPayPall($status, $transaction_id, $command, $IdentityToken);
    } else {
        $laststatus = $_GET['st'] . " Tid - " . $_GET['tx'];
        //dbg("Paypall error - ".$laststatus);

        $query = "INSERT INTO `PaymentInfo`(`UserId`, `email`, `Instituition`, `DatePaymentLoc`, `LastPayPallStatus`) VALUES ($userId,'$useremail','PayPall','$DatePaymentLoc','$laststatus','','')";
        //echo $query;
        $resultado = db_query($query);
        formBuyingStatus($error = true, $keyarray);
        return;
    }
    //dbg("Paypall Response");
    //dbg($return);
    $keyarray = ParsePayPallPDTResponse($return);

    $last_name = $keyarray['last_name'];
    $first_name = $keyarray['first_name'];
    $email = $keyarray['payer_email'];
    $first_name = $keyarray['first_name'];
    $last_name = $keyarray['last_name'];

    $payer_id = $keyarray['payer_id'];
    $payment_date = $keyarray['payment_date'];
    $address_city = $keyarray['address_city'];
    $address_country = $keyarray['address_country'];

    $planselection = $keyarray['option_selection1'];
    $valuepaid = $keyarray['payment_gross'];

    $RawPayPallData = $return;

    //dbg("Paypall Response Parse");
    //dbg($planselection);
    //dbg($valuepaid);

    $DatePaymentLoc = GetDateTime();
    ////////////////////////////////////////////////////////////////////////////
    // Save this payment on system database
    // INSERT INTO `PaymentInfo`(`UserId`, `email`, `Instituition`, `payer_email`, `first_name`, `last_name`, `DatePaymentLoc`, `LastPayPallStatus`, `payer_id`, `payment_date`, ` address_city`, `address_country`, `RawPayPallData`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9],[value-10],[value-11],[value-12],[value-13])

    $query = "INSERT INTO `PaymentInfo`"
            . "       (`UserId`, `email`, `Instituition`, `payer_email`, `first_name`, `last_name`, `DatePaymentLoc`, `LastPayPallStatus`, `payer_id`, `payment_date`, ` address_city`, `address_country`, `PlanSelection`,`ValuePaid`,`RawPayPallData`) "
            . "VALUES ($userId,'$useremail','PayPall','$email','$first_name', '$last_name','$DatePaymentLoc','', '$payer_id', '$payment_date', '$address_city', '$address_country','$planselection','$valuepaid','$RawPayPallData')";
    //echo $query;
    $resultado = db_query($query);
    ////////////////////////////////////////////////////////////////////////////
    // Update user plan
    $query = "SELECT  `PlanEndDate` FROM `User` WHERE id=$userId";
    $resultado = db_query($query);
    $enddate = $resultado[1][0];
    // echo date(GetDateTime(), strtotime("+30 days"));

    if (strtotime($enddate) < strtotime("now"))
        $enddate = date('Y-m-d', strtotime("now"));

    $planselection = trim($keyarray['option_selection1']);
    if ($planselection == "1 month")
        $enddate = date('Y-m-d', strtotime("+31 days", strtotime($enddate)));
    if ($planselection == "6 months")
        $enddate = date('Y-m-d', strtotime("+186 days", strtotime($enddate)));
    if ($planselection == "1 year")
        $enddate = date('Y-m-d', strtotime("+368 days", strtotime($enddate)));

    $query = "UPDATE `User` SET `Plan`='$planselection',`PlanEndDate`='$enddate' WHERE id=$userId";
    $resultado = db_query($query);

    // Show status to user
    formBuyingStatus($error = false, $keyarray);
    formUserProfile();
    formUserPlan();
}

////////////////////////////////////////////////////////////////////////////////
function SetCookieTZO() {
    echo <<<EOE
    <script src="/ChildMonitor/Util/jstz-1.0.4.min.js"></script>
    <script type="text/javascript">
     if (navigator.cookieEnabled)
     {
       // var cmtimezone = new Date().getTimezoneOffset();
       // document.cookie = "tzo="+ (- cmtimezone);
        var cmtimezone = getTimezoneName();
        document.cookie = "tzo="+ cmtimezone;
        var scwidth = screen.width;
        var scheight = screen.height;
        document.cookie = "scr_width="+scwidth;
        document.cookie = "scr_height="+scheight; 
    
     }

     function getTimezoneName() 
     {
            timezone = jstz.determine()
            return timezone.name();
      }
   </script>
EOE;
    if (!isset($_COOKIE['tzo'])) {
        echo <<<EOE
      <script type="text/javascript">
          if (navigator.cookieEnabled) window.location.reload(true); 
          else alert("Cookies must be enabled!");
      </script>
EOE;
        die();
    }
    // $ts = new DateTime('now', new DateTimeZone('GMT'));
    // $ts->add(DateInterval::createFromDateString($_COOKIE['tzo'].' minutes'));
    // dbg("______________________________".$_COOKIE['tzo']);
}

////////////////////////////////////////////////////////////////////////////////
function formBuyingStatus($error = true, $keyarray) {
    if (!$error) {
        $last_name = $keyarray['last_name'];
        $first_name = $keyarray['first_name'];
        $email = $keyarray['payer_email'];
        $msg = "Name: $first_name $last_name<br>Email: $email";
    } else {
        $msg = $keyarray;
    }
    echo <<<EOT
    <script>
    $( 
        "<div>PayPall Payment Sucessfull.<br><br>$msg</div>"
    ).dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"});
    </script>
EOT;
}

////////////////////////////////////////////////////////////////////////////////
function isMobile() {
    // dbg(strtolower($_SERVER['HTTP_USER_AGENT']));
    // dbg($_COOKIE['scr_width']);
    // dbg($_COOKIE['scr_height']);
    
    $witdh = $_COOKIE['scr_width'];
    $height = $_COOKIE['scr_height'];
    
    // return true;
    if($witdh<$height)
        return true;
    return check_user_agent('mobile');
}
////////////////////////////////////////////////////////////////////////////////
function CheckAndroidChrome()
{
    $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);

    if(strpos($user_agent, "android") && strpos($user_agent, "chrome") && $_SESSION['AppLogin']==0)
       return true;
    else
       return false; 
}
////////////////////////////////////////////////////////////////////////////////
function check_user_agent($type = NULL) {
    $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    if ($type == 'bot') {
        // matches popular bots
        if (preg_match("/googlebot|adsbot|yahooseeker|yahoobot|msnbot|watchmouse|pingdom\.com|feedfetcher-google/", $user_agent)) {
            return true;
            // watchmouse|pingdom\.com are "uptime services"
        }
    } else if ($type == 'browser') {
        // matches core browser types
        if (preg_match("/mozilla\/|opera\//", $user_agent)) {
            return true;
        }
    } else if ($type == 'mobile') {
        // matches popular mobile devices that have small screens and/or touch inputs
        // mobile devices have regional trends; some of these will have varying popularity in Europe, Asia, and America
        // detailed demographics are unknown, and South America, the Pacific Islands, and Africa trends might not be represented, here
        if (preg_match("/phone|iphone|itouch|ipod|symbian|android|htc_|htc-|palmos|blackberry|opera mini|iemobile|windows ce|nokia|fennec|hiptop|kindle|mot |mot-|webos\/|samsung|sonyericsson|^sie-|nintendo/", $user_agent)) {
            // these are the most common
            return true;
        } else if (preg_match("/mobile|pda;|avantgo|eudoraweb|minimo|netfront|brew|teleca|lg;|lge |wap;| wap /", $user_agent)) {
            // these are less common, and might not be worth checking
            return true;
        }
    }
    return false;
}

////////////////////////////////////////////////////////////////////////////////
function MenuMobile() {
    $buffer = <<<EOT
     <img src="Img/mobile_menu.png"  height="50" width="50" alt="Menu">    
EOT;
    return divTransparente($buffer, $color = "#f0f0f0", $opacity = "0.5", $top = "30%", $left = "5px", $width = "50px", $height = "50px", $borderradius = "30px");
}

////////////////////////////////////////////////////////////////////////////////
function NewsFeedWidget() {
    $sitePT = "http://www.bbc.co.uk/portuguese/search?q=Educação%20Infantil";
    $siteEN = "http://www.bbc.co.uk/search?q=Children%20Education";
    $siteENUS = "http://www.nytimes.com/pages/education/index.html";
    $buffer = <<<EOT
    <iframe src=$siteENUS
            frameborder="0px" width="95%" height="95%"
            marginwidth="0px" marginheight="0px" style="margin-top: 2%; margin-left: 2%;" >
    </iframe>
EOT;
    // return $buffer;
    // $color="#163f29"
    return divTransparente($buffer, $color = "#ffffff", $opacity = "0.988", $top = "140px", $left = "140px", $width = "70%", $height = "78%");
}
////////////////////////////////////////////////////////////////////////////////
function MainInfoChildMonitor(){
    if (ismobile()) {
        $font = "130%";
    } else {
        $font = "150%";
    }
    $buffer .= <<<EOT
        <style>
        //@font-face {
        //    font-family: 'DistProTh';
        //    src: url("/ChildMonitor/Fonts/DistProTh.otf") format("opentype");
        //}

        //tx 
        //{
        //    font: 100% 'DistProTh', sans-serif;  
        //    font-weight: normal;    
        //    font-size: $font;    
        //    text-align: center;   
        //    margin: 5px 5px;
        //    color: #ffffff;
        //    line-height: 200%;    
        //    text-shadow: 0 4px 4px rgba(255,255,255,.35);
        //}        
        </style>
EOT;

    if (!ismobile()) {
        $buffer .= <<<EOT
            <tx class="tx"> ChildMonitor </tx> <br> <br>
            <table style="width:100%">
              <tr>
                <td><tx class="tx"> &bull; Robust tracking to parents take care of your infants </tx> <br></td>
                <td><tx class="tx"> &bull; Reability on slow networks </tx></td>
                <td><tx class="tx"> &bull; Low price </tx></td>
              </tr>
              <tr>
                <td><tx class="tx"> &bull; Ready to track vital signals </tx></td>
                <td><tx class="tx"> &bull; Easy day by day places monitoring </tx></td>
                <td>  </td>
              </tr>
            </table>     
                
EOT;
    } else {
        $buffer .= <<<EOT
            <tx class="tx"> ChildMonitor </tx> <br> <br>
            <table style="width:100%">
              <tr>
                <td><tx class="tx"> &bull; Robust tracking to parents take care of your infants </tx> <br></td>
              </tr>
              <tr>      
                <td><tx class="tx"> &bull; Reability on slow networks </tx></td>
              </tr>
              <tr>      
                <td><tx class="tx"> &bull; Low price </tx></td>
              <tr>
                <td><tx class="tx"> &bull; Ready to track vital signals </tx></td>
              </tr>
              <tr>      
                <td><tx class="tx"> &bull; Easy day by day places monitoring </tx></td>
              </tr>
            </table>     
                
EOT;
    }

    // return $buffer;
    // $color="#163f29"
    if (ismobile()) {
        $top = "80px";
        $width = "95%";
    } else {
        $top = "150px";
        $width = "99%";
    }
    return divTransparente($buffer, $color = "rgba(0, 0, 0, 0.4)", $opacity = "", $top, $left = "10px", $width, $height = "", $borderadius = "1px");
}

////////////////////////////////////////////////////////////////////////////////
function MainInfoTruckTrack()
{
    if (ismobile()) {
        $font = "130%";
    } else {
        $font = "150%";
    }
    $buffer .= <<<EOT
        <style>
        //@font-face {
        //    font-family: 'DistProTh';
        //    src: url("/ChildMonitor/Fonts/DistProTh.otf") format("opentype");
        //}
        //@font-face {
        //    font-family: 'Montserrat';
        //    src: url("/ChildMonitor/Fonts/Montserrat-Light.otf") format("opentype");    
        //}
            
        //tx 
        //{
        //    font: 100% 'Montserrat', sans-serif;  
        //    font-weight: light;    
        //    font-size: $font;    
        //    text-align: center;   
        //    margin: 5px 5px;
        //    color: #ffffff;
        //    line-height: 200%;    
        //    text-shadow: 0 4px 4px rgba(255,255,255,.35);
        //}        
        </style>
EOT;

    if (!ismobile()) {
        $buffer .= <<<EOT
            <tx class="tx"> Truck Track </tx> <br> <br>
            <table style="width:100%">
              <tr>
                <td><tx class="tx"> &bull; Professional Tracking for Trucks or Vehicles </tx> <br></td>
                <td><tx class="tx"> &bull; Tracking Even Without Network</tx></td>
                <td><tx class="tx"> &bull; Pretrip and Post-Trip Inspections and Maintenance </tx></td>
              </tr>
              <tr>
                <td><tx class="tx"> &bull; Fuel, Distance and Maintenance Costs Prediction </tx></td>
                <td><tx class="tx"> &bull; Artificial Intelligence Logistics Analysis </tx></td>
                 <td><tx class="tx"> &bull; Bad Behaviour Alarms and Geofencing </tx></td>
                <td>  </td>
              </tr>
            </table>     
                
EOT;
    } else {
        $buffer .= <<<EOT
            <tx class="tx"> Truck Track </tx> <br> <br>
            <table style="width:100%">
              <tr>
                <td><tx class="tx"> &bull; Professional Tracking for Trucks or Vehicles </tx> <br></td>
              </tr>
              <tr>      
                <td><tx class="tx"> &bull; Tracking Even Without Network </tx></td>
              </tr>
              <tr>      
                <td><tx class="tx"> &bull; Pretrip and Post-Trip Inspections and Maintenance </tx></td>
              <tr>
                <td><tx class="tx"> &bull; Fuel, Distance and Maintenance Costs Prediction </tx></td>
              </tr>
              <tr>      
                <td><tx class="tx"> &bull; Artificial Intelligence Logistics Analysis </tx></td>
              </tr>
              <tr>      
                <td><tx class="tx"> &bull; Bad Behaviour Alarms and Geofencing  </tx></td>
              </tr>                
            </table>     
                
EOT;
    }

    // return $buffer;
    // $color="#163f29"
    if (ismobile()) {
        $top = "180px";
        $width = "95%";
    } else {
        $top = "230px";
        $width = "99%";
    }
    return divTransparente($buffer, $color = "rgba(0, 0, 0, 0.4)", $opacity = "", $top, $left = "10px", $width, $height = "", $borderadius = "1px");
}

////////////////////////////////////////////////////////////////////////////////
function MainInfo() {
    require 'GlobalVars.php';

    if ($AppName == "ChildMonitor") {
        MainInfoChildMonitor();
        return;
    }
    /*if ($AppName == "TruckMonitor") {
        MainInfoTruckTrack();
        return;
    } */   
    
}

////////////////////////////////////////////////////////////////////////////////
function GooglePlayButton() {
    // com.singularityage.trucktrack.app
    if($_SESSION['stme']=="TTM")
    {    
        // 
        // $shref = "https://play.google.com/store/apps/details?id=com.singularityage.trucktrack.app";
        $shref = "https://play.google.com/store/apps/details?id=com.singularityage.trucktrackfree.app";
    
    }  
    if($_SESSION['stme']=="MMF")
    {    
        // com.singularityage.myfather.app
        $shref = "https://play.google.com/store/apps/details?id=com.singularityage.myfather.app";
    
    }     
    else
        $shref = "https://play.google.com/store/apps/details?id=com.childmonitor.track.map";
    $buffer = <<<EOT
    <a href="$shref">
      <img  width="150px" alt="Android app on Google Play"
           src="/ChildMonitor/Img/BotGooglePlay.png" />
    </a>
EOT;
    return divTransparente($buffer, $color = "#000000", $opacity = "1.0", $top = "80%", $left = "5px", $width = "", $height = "");
}

////////////////////////////////////////////////////////////////////////////////
function FaceBookImgInterface() {
    
    session_start();
    if (trim($_SESSION['fbId']) == "") {
        return "";
    }
    $img = GetFB_Img($_SESSION['fbId']);

    $buffer = <<<EOT
    <img src="$img" width="60px" height="60px">
EOT;
    if (!ismobile())
        $left = "96%";
    else
        $left = "82%";

    $posTop = "5px";
    return divTransparente($buffer, $color = "#f0f0f0", $opacity = "0.9", $top = $posTop, $left, $width = "60px", $height = "60px", $z_index = "1");
}

////////////////////////////////////////////////////////////////////////////////
function divGradBack($container = "", $opacity = "0.9", $top = "0px", $left = "0px", $width = "100%", $height = "100%", $borderradius = "0px") {
    echo <<<EOT
    <div style="background: rgba(94,146,189,1);
                background: -moz-linear-gradient(left, rgba(94,146,189,1) 0%, rgba(19,59,92,1) 50%, rgba(7,58,100,1) 100%);
                background: -webkit-gradient(left top, right top, color-stop(0%, rgba(94,146,189,1)), color-stop(50%, rgba(19,59,92,1)), color-stop(100%, rgba(7,58,100,1)));
                background: -webkit-linear-gradient(left, rgba(94,146,189,1) 0%, rgba(19,59,92,1) 50%, rgba(7,58,100,1) 100%);
                background: -o-linear-gradient(left, rgba(94,146,189,1) 0%, rgba(19,59,92,1) 50%, rgba(7,58,100,1) 100%);
                background: -ms-linear-gradient(left, rgba(94,146,189,1) 0%, rgba(19,59,92,1) 50%, rgba(7,58,100,1) 100%);
                background: linear-gradient(to right, rgba(94,146,189,1) 0%, rgba(19,59,92,1) 50%, rgba(7,58,100,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#5e92bd', endColorstr='#073a64', GradientType=1 );
                position:absolute; z-index:-50; 
       top:$top; left:$left; width:$width; height:$height; opacity:$opacity; border-radius: $borderradius; -moz-border-radius: $borderradius; "  > 
EOT;
    echo $container;
    // <p style=" color: rgb(255,255,255); margin-top:0px; margin-botton:0px; " >Track, monitor, remeber and interact. The definitive way to protect our loving.<p>
    echo "</div>";
}

////////////////////////////////////////////////////////////////////////////////
function divRelative($container, $color = "#163f29", $opacity = "0.6", $top = "140px", $left = "50%", $width = "48%", $height = "78%", $borderradius = "1px") {
    echo <<<EOT
    <div style="  box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); -moz-box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); -webkit-box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); background-color:$color; position:relative; z-index:1; 
       top:$top; left:$left; width:$width; height:$height; opacity:$opacity; border-radius: $borderradius; -moz-border-radius: 6px; "  > 
EOT;
    echo $container;
    // <p style=" color: rgb(255,255,255); margin-top:0px; margin-botton:0px; " >Track, monitor, remeber and interact. The definitive way to protect our loving.<p>
    echo "</div>";
}

// estilos de div
// background-color:Black; z-index: 0; opacity:0.5; position:absolute;
// 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function GoogleAd() {
//    $saida = <<<EOT
//    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
//    <!-- TruckTrack -->
//    <ins class="adsbygoogle"
//         style="display:block"
//         data-ad-client="ca-pub-5485141617906447"
//         data-ad-slot="5704274219"
//         data-ad-format="auto"></ins>
//    <script>
//    (adsbygoogle = window.adsbygoogle || []).push({});
//    </script>
//EOT;
  

    
   $saida = <<<EOT
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- TruckTrack -->
    <ins class="adsbygoogle"
         style="display:block"
         data-ad-client="ca-pub-5485141617906447"
         data-ad-slot="5704274219"
         data-ad-format="auto"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
EOT;
   return($saida);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function TermsOfService() {
    /*
      Terms of Service

      Welcome to Life360.com, the website and online service of Life360, Inc. (“Life360,” “we,” or “us”). This page explains the terms by which you may use our online and/or mobile services, web site, and software provided on or in connection with the service (collectively the “Service”). By accessing or using the Service, you signify that you have read, understood, and agree to be bound by this Terms of Use Agreement (“Agreement”) and to the collection and use of your information as set forth in the Life360 Privacy Policy, whether or not you are a registered user of our Service. This Agreement applies to all visitors, users, and others who access the Service (“Users”).

      PLEASE READ THIS AGREEMENT CAREFULLY TO ENSURE THAT YOU UNDERSTAND EACH PROVISION. THIS AGREEMENT CONTAINS A MANDATORY ARBITRATION OF DISPUTES PROVISION THAT REQUIRES THE USE OF ARBITRATION ON AN INDIVIDUAL BASIS TO RESOLVE DISPUTES, RATHER THAN JURY TRIALS OR CLASS ACTIONS.
      1. Use of Our Service

      Life360 is a free communication and location app for smartphones. With Life360, you can see your entire family on a private map, check in with them to say everything’s okay, or get help in an emergency. Subscribers to Life360 Premium also enjoy 24/7 access to a live advisor who can help in any emergency, unlimited locations in Life360 Places, expanded location history, location lookups for non-smartphones, and more.
      A. Eligibility

      You may use the Service only if you can form a binding contract with Life360, and only in compliance with this Agreement and all applicable local, state, national, and international laws, rules and regulations. Anyone under 13 is strictly prohibited from creating an account for the Service. In addition, anyone under 13 may only accept invitations from parents / legal guardians to join their account. The Service is not available to any Users previously removed from the Service by Life360.
      B. Life360 Service

      Subject to the terms and conditions of this Agreement, you are hereby granted a non-exclusive, limited, non-transferable, freely revocable license to use the Service for your personal, noncommercial use only and as permitted by the features of the Service. Life360 reserves all rights not expressly granted herein in the Service and the Life360 Content (as defined below). Life360 may terminate this license at any time for any reason or no reason.
      C. Life360 Accounts

      Your Life360 account gives you access to the services and functionality that we may establish and maintain from time to time and in our sole discretion.

      You may never use another User’s account without permission. When creating your account, you must provide accurate and complete information. You are solely responsible for the activity that occurs on your account, and you must keep your account password secure. We encourage you to use “strong” passwords (passwords that use a combination of upper and lower case letters, numbers and symbols) with your account. You must notify Life360 immediately of any breach of security or unauthorized use of your account. Life360 will not be liable for any losses caused by any unauthorized use of your account.

      You may control your User profile and how you interact with the Service by changing the Settings in the Life360 app. By providing Life360 your email address you consent to our using the email address to send you Service-related notices, including any notices required by law, in lieu of communication by postal mail. We may also use your email address to send you other messages, such as changes to features of the Service and special offers. If you do not want to receive promotional email messages, you may opt out by unsubscribing from such email communications from Life360. Opting out may prevent you from receiving email messages regarding updates, improvements, or offers.
      D. Service Rules

      You agree not to engage in any of the following prohibited activities: (i) copying, distributing, or disclosing any part of the Service in any medium, including without limitation by any automated or non-automated “scraping”; (ii) using any automated system, including without limitation “robots,” “spiders,” “offline readers,” etc., to access the Service in a manner that sends more request messages to the Life360 servers than a human can reasonably produce in the same period of time by using a conventional on-line web browser; (iii) transmitting spam, chain letters, or other unsolicited email; (iv) attempting to interfere with, compromise the system integrity or security or decipher any transmissions to or from the servers running the Service; (v) taking any action that imposes, or may impose at our sole discretion an unreasonable or disproportionately large load on our infrastructure; (vi) uploading invalid data, viruses, worms, or other software agents through the Service; (vii) collecting or harvesting any personally identifiable information, including account names, from the Service; (viii) using the Service for any commercial solicitation purposes; (ix) impersonating another person or otherwise misrepresenting your affiliation with a person or entity, conducting fraud, hiding or attempting to hide your identity; (x) interfering with the proper working of the Service; (xi) accessing any content on the Service through any technology or means other than those provided or authorized by the Service; or (xii) bypassing the measures we may use to prevent or restrict access to the Service, including without limitation features that prevent or restrict use or copying of any content or enforce limitations on use of the Service or the content therein.

      We may, without prior notice, change the Service; stop providing the Service or features of the Service, to you or to users generally; or create usage limits for the Service. We may permanently or temporarily terminate or suspend your access to the Service without notice and liability for any reason, including if in our sole determination you violate any provision of this Agreement, or for no reason. Upon termination for any reason or no reason, you continue to be bound by this Agreement. You are responsible for all the mobile data usage resulting from the use of Service. Consult your mobile operator concerning your plan, data rate charges and limits. You are solely responsible for your interactions with other Life360 Users. We reserve the right, but have no obligation, to monitor disputes between you and other Users. Life360 shall have no liability for your interactions with other Users, or for any User’s action or inaction.
      2. User Content

      Some areas of the Service allow Users to post content such as profile information, comments, questions, and other content or information (any such materials a User submits, posts, displays, or otherwise makes available on the Service “User Content”). We claim no ownership rights over User Content created by you. The User Content you create remains yours; however, by sharing User Content through the Service, you agree to allow others to view, edit, and/or share your User Content in accordance with your settings and this Agreement. Life360 has the right (but not the obligation) in its sole discretion to remove any User Content that is shared via the Service.

      You agree not to post or transmit User Content that: (i) may create a risk of harm, loss, physical or mental injury, emotional distress, death, disability, disfigurement, or physical or mental illness to you, to any other person, or to any animal; (ii) may create a risk of any other loss or damage to any person or property; (iii) seeks to harm or exploit children by exposing them to inappropriate content, asking for personally identifiable details or otherwise; (iv) may constitute or contribute to a crime or tort; (v) contains any information or content that we deem to be unlawful, harmful, abusive, racially or ethnically offensive, defamatory, infringing, invasive of personal privacy or publicity rights, harassing, humiliating to other people (publicly or otherwise), libelous, threatening, profane, or otherwise objectionable; (vi) contains any information or content that is illegal (including, without limitation, the disclosure of insider information under securities law or of another party’s trade secrets); (vii) contains any information or content that you do not have a right to make available under any law or under contractual or fiduciary relationships; (viii) contains any information or content that you know is not correct and current; (ix) violates any school or other applicable policy, including those related to cheating or ethics; (x) interferes with other Users of the Service including, without limitation, disrupting the normal flow of dialogue in an interactive area of the Service and deleting or revising any content posted by another person or entity; (xi) except where expressly permitted, post or transmit charity requests, petitions for signatures, franchise arrangements, distributorship arrangements, sales representative agency arrangements or other business opportunities (including offers of employment or contracting arrangements), club memberships, chain letters or letters relating to pyramid schemes, any advertising or promotional materials or any other solicitation of other users to use goods or services except in those areas (e.g., a classified bulletin board) that are designated for such purpose. You agree that any employment or other relationship you form or attempt to form with an employer, employee, or contractor whom you contact through areas of the Service that may be designated for that purpose is between you and that employer, employee, or contractor alone, and not with us. You may not copy or use personal identifying or business contact information about other Users without their permission. You agree that any User Content that you post does not and will not violate third-party rights of any kind, including without limitation any Intellectual Property Rights (as defined below) or rights of privacy. Life360 reserves the right, but is not obligated, to reject and/or remove any User Content that Life360 believes, in its sole discretion, violates these provisions.

      For the purposes of this Agreement, “Intellectual Property Rights” means all patent rights, copyright rights, mask work rights, moral rights, rights of publicity, trademark, trade dress and service mark rights, goodwill, trade secret rights and other intellectual property rights as may now exist or hereafter come into existence, and all applications therefore and registrations, renewals and extensions thereof, under the laws of any state, country, territory or other jurisdiction.

      In connection with your User Content, you affirm, represent and warrant the following:

      A. You have the consent of each and every identifiable natural person in the User Content to use such person’s name or likeness in the manner contemplated by the Service and this Agreement, and each such person has released you from any liability that may arise in relation to such use.

      B. Your User Content and Life360’s use thereof as contemplated by this Agreement and the Service will not violate any law or infringe any rights of any third party, including but not limited to any Intellectual Property Rights and privacy rights. Life360 takes no responsibility and assumes no liability for any User Content that you or any other User or third party posts or sends over the Service. You shall be solely responsible for your User Content and the consequences of posting or publishing it, and you agree that we are only acting as a passive conduit for your online distribution and publication of your User Content. You understand and agree that you may be exposed to User Content that is inaccurate, objectionable, inappropriate for children, or otherwise unsuited to your purpose, and you agree that Life360 shall not be liable for any damages you allege to incur as a result of User Content.
      3. User Content License Grant

      By posting any User Content on the Service, you expressly grant, and you represent and warrant that you have all rights necessary to grant, to Company a royalty-free, sublicensable, transferable, perpetual, irrevocable, non-exclusive, worldwide license to use, reproduce, modify, publish, list information regarding, edit, translate, distribute, syndicate, publicly perform, publicly display, and make derivative works of all such User Content and your name, voice, and/or likeness as contained in your User Content, in whole or in part, and in any form, media or technology, whether now known or hereafter developed, for use in connection with the Service
      4. Mobile Software
      A. Mobile Software.

      We make available software to access the Service via a mobile device (“Mobile Software”). To use the Mobile Software you must have a mobile device that is compatible with the Mobile Service. Life360 does not warrant that the Mobile Software will be compatible with your mobile device. Life360 hereby grants you a non-exclusive, non-transferable, revocable license to use a compiled code copy of the Mobile Software for one Life360 account owned or leased solely by you, for your personal use. You may not: (i) modify, disassemble, decompile or reverse engineer the Mobile Software, except to the extent that such restriction is expressly prohibited by law; (ii) rent, lease, loan, resell, sublicense, distribute or otherwise transfer the Mobile Software to any third party or use the Mobile Software to provide time sharing or similar services for any third party; (iii) make any copies of the Mobile Software; (iv) remove, circumvent, disable, damage or otherwise interfere with security-related features of the Mobile Software, features that prevent or restrict use or copying of any content accessible through the Mobile Software, or features that enforce limitations on use of the Mobile Software; or (v) delete the copyright and other proprietary rights notices on the Mobile Software. You acknowledge that Life360 may from time to time issue upgraded versions of the Mobile Software, and may automatically electronically upgrade the version of the Mobile Software that you are using on your mobile device. You consent to such automatic upgrading on your mobile device, and agree that the terms and conditions of this Agreement will apply to all such upgrades. Any third-party code that may be incorporated in the Mobile Software is covered by the applicable open source or third-party license EULA, if any, authorizing use of such code. The foregoing license grant is not a sale of the Mobile Software or any copy thereof, and Life360 or its third party partners or suppliers retain all right, title, and interest in the Mobile Software (and any copy thereof). Any attempt by you to transfer any of the rights, duties or obligations hereunder, except as expressly provided for in this Agreement, is void. Life360 reserves all rights not expressly granted under this Agreement. If the Mobile Software is being acquired on behalf of the United States Government, then the following provision applies. Use, duplication, or disclosure of the Mobile Software by the U.S. Government is subject to restrictions set forth in this Agreement and as provided in DFARS 227.7202-1(a) and 227.7202-3(a) (1995), DFARS 252.227-7013(c)(1)(ii) (OCT 1988), FAR 12.212(a) (1995), FAR 52.227-19, or FAR 52.227-14 (ALT III), as applicable. The Mobile Software originates in the United States, and is subject to United States export laws and regulations. The Mobile Software may not be exported or re-exported to certain countries or those persons or entities prohibited from receiving exports from the United States. In addition, the Mobile Software may be subject to the import and export laws of other countries. You agree to comply with all United States and foreign laws related to use of the Mobile Software and the Life360 Service. (1) Mobile Software from iTunes. The following applies to any Mobile Software you acquire from the iTunes Store (“iTunes-Sourced Software”): You acknowledge and agree that this Agreement is solely between you and Life360, not Apple, and that Apple has no responsibility for the iTunes-Sourced Software or content thereof. Your use of the iTunes-Sourced Software must comply with the App Store Terms of Service. You acknowledge that Apple has no obligation whatsoever to furnish any maintenance and support services with respect to the iTunes-Sourced Software. In the event of any failure of the iTunes-Sourced Software to conform to any applicable warranty, you may notify Apple, and Apple will refund the purchase price for the iTunes-Sourced Software to you; to the maximum extent permitted by applicable law, Apple will have no other warranty obligation whatsoever with respect to the iTunes-Sourced Software, and any other claims, losses, liabilities, damages, costs or expenses attributable to any failure to conform to any warranty will be solely governed by this Agreement and any law applicable to Life360 as provider of the software. You acknowledge that Apple is not responsible for addressing any claims of you or any third party relating to the iTunes-Sourced Software or your possession and/or use of the iTunes-Sourced Software, including, but not limited to: (i) product liability claims; (ii) any claim that the iTunes-Sourced Software fails to conform to any applicable legal or regulatory requirement; and (iii) claims arising under consumer protection or similar legislation; and all such claims are governed solely by this Agreement and any law applicable to Life360 as provider of the software. You acknowledge that, in the event of any third party claim that the iTunes-Sourced Software or your possession and use of that iTunes-Sourced Software infringes that third party’s intellectual property rights, Life360, not Apple, will be solely responsible for the investigation, defense, settlement and discharge of any such intellectual property infringement claim to the extent required by this Agreement. You and Life360 acknowledge and agree that Apple, and Apple’s subsidiaries, are third party beneficiaries of this Agreement as relates to your license of the iTunes-Sourced Software, and that, upon your acceptance of the terms and conditions of this Agreement, Apple will have the right (and will be deemed to have accepted the right) to enforce this Agreement as relates to your license of the iTunes-Sourced Software against you as a third party beneficiary thereof.
      5. Our Proprietary Rights

      Except for your User Content, the Service and all materials therein or transferred thereby, including, without limitation, software, images, text, graphics, illustrations, logos, patents, trademarks, service marks, copyrights, photographs, audio, videos, music, and User Content belonging to other Users (the “Life360 Content”), and all Intellectual Property Rights related thereto, are the exclusive property of Life360 and its licensors (including other Users who post User Content to the Service). Except as explicitly provided herein, nothing in this Agreement shall be deemed to create a license in or under any such Intellectual Property Rights, and you agree not to sell, license, rent, modify, distribute, copy, reproduce, transmit, publicly display, publicly perform, publish, adapt, edit or create derivative works from any Life360 Content. Use of the Life360 Content for any purpose not expressly permitted by this Agreement is strictly prohibited.

      You may choose to or we may invite you to submit comments or ideas about the Service, including without limitation about how to improve the Service or our products (“Ideas”). By submitting any Idea, you agree that your disclosure is gratuitous, unsolicited and without restriction and will not place Life360 under any fiduciary or other obligation, and that we are free to use the Idea without any additional compensation to you, and/or to disclose the Idea on a non-confidential basis or otherwise to anyone. You further acknowledge that, by acceptance of your submission, Life360 does not waive any rights to use similar or related ideas previously known to Life360, or developed by its employees, or obtained from sources other than you.
      6. Life360 Premium
      A. Life360 Premium.

      Life360 Premium is a paid subscription service from Life360 that includes (where available): 24/7 Live Advisor; Stolen Phone Protection; unlimited Location Look-Ups; unlimited use of Places; expanded History Data. Up to six (6) devices can be used per Life360 Premium subscription.
      B. 24/7 Live Advisor.

      24/7 Live Advisor provides twenty-four hour a day, seven days a week call support for non-critical emergencies to help users find, for example, roadside assistance or the nearest hospital, urgent care facility, pharmacy or natural disaster emergency shelter. 24/7 Live Advisor is NOT A REPLACEMENT FOR 911. In the event of a critical emergency, always dial 911 immediately. Calls may be monitored for quality assurance purposes. 24/7 Live Advisor is only available to current Life360 Premium subscribers.
      C. Stolen Phone Protection.

      Stolen Phone Protection provides $USD 100 for Life360 Premium subscribers in the event of a stolen phone. Limit one claim per year per Circle. Applies to phones with cellular voice and data capabilities. Tablets are expressly excluded. Stolen Phone Protection is available to current Life360 Premium subscribers in the United States after the first subscription month for subscribers that have at least two (2) actively linked family members on the account, including the account holder, who have updated for the past thirty (30) days. To be eligible for Stolen Phone Protection, the stolen phone must have registered location in the vicinity of the subscriber’s physical address (no P.O. boxes) associated with the Life360 Premium account in the last thirty (30) days prior to any claim; the account must be in good standing (e.g. no past due amount, collections, or defaults on the account); the account must have history for the stolen phone for the past thirty (30) days; a police report must be filed for the stolen phone; claim forms must be submitted, along with the police report, within fifteen (15) days of the phone being stolen. To make a claim, send an email to stolenphone@life360.com from the Life360 Premium administrator’s account. We will send you a claim form for which you will need to attach the police report filed for the stolen phone. Life360 reserves the right to deny any claims that it deems fraudulent, in its sole discretion.
      D. Geographic Coverage.

      Life360 Premium is a service that is designed for residents of the contiguous U.S., Alaska, and Hawaii. International users may subscribe to Life360 Premium but will not be eligible for: 24/7 Live Advisor or Stolen Phone Protection, and phone location tracking may be limited.
      E. Billing Policies.

      If you elect to use Life360 Premium, you agree to the pricing and payment terms, as we may update them from time to time. Life360 may add new services for additional fees and charges, or amend fees and charges for existing services, at any time in its sole discretion. Any change to our pricing or payment terms shall become effective in the billing cycle following notice of such change to you as provided in this Agreement.
      F. Pricing and Payment Terms
      1. Life360 Premium subscription/access fees are payable in advance.

      All subscription and access charges for the Service are payable in advance. Life360 Premium subscriptions can be purchased annually or monthly. Life360 is not responsible for any charges or expenses you incur resulting from charges billed by Life360 in accordance with the Terms of Service (e.g. overdrawn accounts, exceeding credit card limit, etc.). By providing a credit card number or other payment method with advance authorization features (e.g. some PayPal accounts), you authorize Life360 to continue charging the payment method for all charges due Life360 until your Life360 Premium account is settled and is terminated by either you or Life360. Life360 reserves the right to limit the number of accounts that may be charged to a credit card or other payment or identification method per unique user.
      2. Life360 Premium subscription accounts have a trial period.

      After initial registration of a Life360 Premium account, you will be given a trial period beginning with your first login to Life360 Premium. You may cancel your account at any time during the trial. If you want to change your account type, you may do so at any time (either before or after the trial period). You are limited to one trial per person (credit card or other unique payment or identification method) for any 12-month period. If you do not cancel your account during the trial, you will be charged based on the account type you selected during registration. To cancel your Life360 Premium subscription at any time, send an email to support@life360.com.
      3. Payment methods.

      Life360 accepts credit and debit cards issued through VISA, MasterCard, American Express, and Discover. Life360 requires that you provide the security code for your debit or credit card to protect against the unauthorized use of your card by other persons. The security code is an individual three- or four-digit number specific to your card that may be printed on the face of your card above the embossed account number (if American Express), or on the back of your card, on the signature panel (if Visa, or MasterCard). In the event that Life360 is unable to charge the card you have provided (i.e. expired credit card), Life360 will send you a notice to update your card information. You will have a 14-day grace period to update your billing information. If the account is not updated within the 14-day grace period, Life360 will terminate your Premium subscription.
      G. No Refunds.

      You may cancel your Life360 account at any time; however, there are no refunds for any unused time on a subscription, any license or subscription fees for any portion of the Service, any content or data associated with your account, or for anything else. Upon cancelling your Life360 Premium service, your subscription will be valid until your paid period is completed. H. Payment Information; Taxes. All information that you provide in connection with a purchase or transaction or other monetary transaction interaction with the Service must be accurate, complete, and current. You agree to pay all charges incurred by users of your credit card, debit card, or other payment method used in connection with a purchase or transaction or other monetary transaction interaction with the Service at the prices in effect when such charges are incurred. You will pay any applicable taxes, if any, relating to any such purchases, transactions or other monetary transaction interactions.
      7. Closing Your Account

      You may cancel your Life360 account at any time. To cancel your account, email support@life360.com or call (866)277-2233. If you send an email, include your name, the email address you registered with, and a phone number where you can be reached. Your account will be canceled within 48 hours of your cancelation request.
      8. No Professional Advice

      If the Service provides professional information (e.g. medical or legal), such information is for informational purposes only and should not be construed as professional advice. No action should be taken based upon any information contained in the Service. You should seek independent professional advice from a person who is licensed and/or qualified in the applicable area.
      9. Privacy

      We care about the privacy of our Users. You understand that by using the Services you consent to the collection, use and disclosure of your personally identifiable information and aggregate data as set forth in our Privacy Policy, and to have your personally identifiable information collected, used, transferred to and processed in the United States.
      10. Security

      Life360 cares about the integrity and security of your personal information. However, we cannot guarantee that unauthorized third parties will never be able to defeat our security measures or use your personal information for improper purposes. You acknowledge that you provide your personal information at your own risk.
      11. Third-Party Links

      The Service may contain links to third-party websites, advertisers, services, special offers, or other events or activities that are not owned or controlled by Life360. Life360 does not endorse or assume any responsibility for any such third-party sites, information, materials, products, or services. If you access a third party website from the Service, you do so at your own risk, and you understand that this Agreement and Life360’s Privacy Policy do not apply to your use of such sites. You expressly relieve Life360 from any and all liability arising from your use of any third-party website, service, or content. Additionally, your dealings with or participation in promotions of advertisers found on the Service, including payment and delivery of goods, and any other terms (such as warranties) are solely between you and such advertisers. You agree that Life360 shall not be responsible for any loss or damage of any sort relating to your dealings with such advertisers.
      12. Indemnity

      You agree to defend, indemnify and hold harmless Life360 and its subsidiaries, agents, licensors, managers, and other affiliated companies, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney’s fees) arising from: (i) your use of and access to the Service, including any data or content transmitted or received by you; (ii) your violation of any term of this Agreement, including without limitation your breach of any of the representations and warranties above; (iii) your violation of any third-party right, including without limitation any right of privacy or Intellectual Property Rights; (iv) your violation of any applicable law, rule or regulation; (v) any claim or damages that arise as a result of any of your User Content or any that is submitted via your account; or (vi) any other party’s access and use of the Service with your unique username, password or other appropriate security code.
      13. No Warranty

      THE SERVICE IS PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS. USE OF THE SERVICE IS AT YOUR OWN RISK. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE SERVICE IS PROVIDED WITHOUT WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM LIFE360 OR THROUGH THE SERVICE WILL CREATE ANY WARRANTY NOT EXPRESSLY STATED HEREIN. WITHOUT LIMITING THE FOREGOING, LIFE360, ITS SUBSIDIARIES, ITS AFFILIATES, AND ITS LICENSORS DO NOT WARRANT THAT THE CONTENT IS ACCURATE, RELIABLE OR CORRECT; THAT THE SERVICE WILL MEET YOUR REQUIREMENTS; THAT THE SERVICE WILL BE AVAILABLE AT ANY PARTICULAR TIME OR LOCATION, UNINTERRUPTED OR SECURE; THAT ANY DEFECTS OR ERRORS WILL BE CORRECTED; OR THAT THE SERVICE IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. ANY CONTENT DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SERVICE IS DOWNLOADED AT YOUR OWN RISK AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR MOBILE DEVICE OR LOSS OF DATA THAT RESULTS FROM SUCH DOWNLOAD OR YOUR USE OF THE SERVICE.

      LIFE360 DOES NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE LIFE360 SERVICE OR ANY HYPERLINKED WEBSITE OR SERVICE, AND LIFE360 WILL NOT BE A PARTY TO OR IN ANY WAY MONITOR ANY TRANSACTION BETWEEN YOU AND THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES.
      14. Limitation of Liability

      TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL LIFE360, ITS AFFILIATES, AGENTS, DIRECTORS, EMPLOYEES, SUPPLIERS OR LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING WITHOUT LIMITATION DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES, THAT RESULT FROM THE USE OF, OR INABILITY TO USE, THIS SERVICE. UNDER NO CIRCUMSTANCES WILL LIFE360 BE RESPONSIBLE FOR ANY DAMAGE, LOSS OR INJURY RESULTING FROM HACKING, TAMPERING OR OTHER UNAUTHORIZED ACCESS OR USE OF THE SERVICE OR YOUR ACCOUNT OR THE INFORMATION CONTAINED THEREIN. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, LIFE360 ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT; (II) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO OR USE OF OUR SERVICE; (III) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION STORED THEREIN; (IV) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM THE SERVICE; (V) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE THAT MAY BE TRANSMITTED TO OR THROUGH OUR SERVICE BY ANY THIRD PARTY; (VI) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE THROUGH THE SERVICE; AND/OR (VII) USER CONTENT OR THE DEFAMATORY, OFFENSIVE, OR ILLEGAL CONDUCT OF ANY THIRD PARTY. IN NO EVENT SHALL LIFE360, ITS AFFILIATES, AGENTS, DIRECTORS, EMPLOYEES, SUPPLIERS, OR LICENSORS BE LIABLE TO YOU FOR ANY CLAIMS, PROCEEDINGS, LIABILITIES, OBLIGATIONS, DAMAGES, LOSSES OR COSTS IN AN AMOUNT EXCEEDING THE AMOUNT YOU PAID TO LIFE360 HEREUNDER OR $100.00, WHICHEVER IS GREATER.

      THIS LIMITATION OF LIABILITY SECTION APPLIES WHETHER THE ALLEGED LIABILITY IS BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY, OR ANY OTHER BASIS, EVEN IF LIFE360 HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION.

      SOME STATES DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATIONS OR EXCLUSIONS MAY NOT APPLY TO YOU. THIS AGREEMENT GIVES YOU SPECIFIC LEGAL RIGHTS, AND YOU MAY ALSO HAVE OTHER RIGHTS WHICH VARY FROM STATE TO STATE. THE DISCLAIMERS, EXCLUSIONS, AND LIMITATIONS OF LIABILITY UNDER THIS AGREEMENT WILL NOT APPLY TO THE EXTENT PROHIBITED BY APPLICABLE LAW.

      The Service is controlled and operated from facilities in the United States. Life360 makes no representations that the Service is appropriate or available for use in other locations. Those who access or use the Service from other jurisdictions do so at their own volition and are entirely responsible for compliance with all applicable United States and local laws and regulations, including but not limited to export and import regulations. You may not use the Service if you are a resident of a country embargoed by the United States, or are a foreign person or entity blocked or denied by the United States government. Unless otherwise explicitly stated, all materials found on the Service are solely directed to individuals, companies, or other entities located in the United States.
      15. Governing Law and Arbitration
      A. Governing Law.

      You agree that: (i) the Service shall be deemed solely based in California; and (ii) the Service shall be deemed a passive one that does not give rise to personal jurisdiction over Life360, either specific or general, in jurisdictions other than California. This Agreement shall be governed by the internal substantive laws of the State of California, without respect to its conflict of laws principles. The parties acknowledge that this Agreement evidences a transaction involving interstate commerce. Notwithstanding the preceding sentences with respect to the substantive law, any arbitration conducted pursuant to the terms of this Agreement shall be governed by the Federal Arbitration Act (9 U.S.C. §§ 1-16). The application of the United Nations Convention on Contracts for the International Sale of Goods is expressly excluded. You agree to submit to the personal jurisdiction of the federal and state courts located in Santa Clara County, California for any actions for which we retain the right to seek injunctive or other equitable relief in a court of competent jurisdiction to prevent the actual or threatened infringement, misappropriation or violation of a our copyrights, trademarks, trade secrets, patents, or other intellectual property or proprietary rights, as set forth in the Arbitration provision below.
      B. Arbitration.

      READ THIS SECTION CAREFULLY BECAUSE IT REQUIRES THE PARTIES TO ARBITRATE THEIR DISPUTES AND LIMITS THE MANNER IN WHICH YOU CAN SEEK RELIEF FROM LIFE360. In the unlikely event that Life360 has not been able to resolve a dispute it has with you after 60 days, we each agree to resolve any claim, dispute, or controversy (excluding any Life360 claims for injunctive or other equitable relief) arising out of or in connection with or relating to these Terms of Use, or the breach or alleged breach thereof (collectively, “Claims”), by binding arbitration by the Judicial Mediation and Arbitration Services (“JAMS”) under the Optional Expedited Arbitration Procedures then in effect for JAMS, except as provided herein. The arbitration will be conducted in Santa Clara County, California, unless you and Life360 agree otherwise. Each party will be responsible for paying any JAMS filing, administrative and arbitrator fees in accordance with JAMS rules. The award rendered by the arbitrator shall include costs of arbitration, reasonable attorneys’ fees and reasonable costs for expert and other witnesses, and any judgment on the award rendered by the arbitrator may be entered in any court of competent jurisdiction. Nothing in this Section shall be deemed as preventing Life360 from seeking injunctive or other equitable relief from the courts as necessary to protect any of Life360’s proprietary interests. ALL CLAIMS MUST BE BROUGHT IN THE PARTIES’ INDIVIDUAL CAPACITY, AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PURPORTED CLASS ACTION, COLLECTIVE ACTION, PRIVATE ATTORNEY GENERAL ACTION OR OTHER REPRESENTATIVE PROCEEDING. THIS WAIVER APPLIES TO CLASS ARBITRATION, AND, UNLESS WE AGREE OTHERWISE, THE ARBITRATOR MAY NOT CONSOLIDATE MORE THAN ONE PERSON’S CLAIMS. YOU AGREE THAT, BY ENTERING INTO THIS TERMS OF SERVICE, YOU AND LIFE360 ARE EACH WAIVING THE RIGHT TO A TRIAL BY JURY OR TO PARTICIPATE IN A CLASS ACTION, .COLLECTIVE ACTION, PRIVATE ATTORNEY GENERAL ACTION, OR OTHER REPRESENTATIVE PROCEEDING OF ANY KIND.
      16. General
      A. Assignment.

      This Agreement, and any rights and licenses granted hereunder, may not be transferred or assigned by you, but may be assigned by Life360 without restriction. Any attempted transfer or assignment in violation hereof shall be null and void.
      B. Notification Procedures and Changes to the Agreement.

      Life360 may provide notifications, whether such notifications are required by law or are for marketing or other business related purposes, to you via email notice, written or hard copy notice, or through posting of such notice on our website, as determined by Life360 in our sole discretion. Life360 reserves the right to determine the form and means of providing notifications to our Users, provided that you may opt out of certain means of notification as described in this Agreement. Life360 is not responsible for any automatic filtering you or your network provider may apply to email notifications we send to the email address you provide us. Life360 may, in its sole discretion, modify or update this Agreement from time to time, and so you should review this page periodically. When we change the Agreement in a material manner, we will update the ‘last modified’ date at the bottom of this page. Your continued use of the Service after any such change constitutes your acceptance of the new Terms of Use. If you do not agree to any of these terms or any future Terms of Use, do not use or access (or continue to access) the Service.
      C. Entire Agreement/Severability.

      This Agreement, together with any amendments and any additional agreements you may enter into with Life360 in connection with the Service, shall constitute the entire agreement between you and Life360 concerning the Service. If any provision of this Agreement is deemed invalid by a court of competent jurisdiction, the invalidity of such provision shall not affect the validity of the remaining provisions of this Agreement, which shall remain in full force and effect.
      D. No Waiver.

      No waiver of any term of this Agreement shall be deemed a further or continuing waiver of such term or any other term, and Life360’s failure to assert any right or provision under this Agreement shall not constitute a waiver of such right or provision.
      E. Contact.

      Please contact us admin@life360.com with any questions regarding this Agreement.

      This Agreement was last modified on September 10th, 2014.

     */
}
