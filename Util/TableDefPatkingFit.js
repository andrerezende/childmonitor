
//////////////////////////////////////////////////////////////////////////////// 
//drop table IF EXISTS `ParkingDataPenaltyHistory`;
//CREATE TABLE `ParkingDataPenaltyHistory`
//  AS (SELECT *
//      FROM `ParkingData` WHERE 1=2);
//
//drop table IF EXISTS `ParkingDataHistory`;
//CREATE TABLE `ParkingDataHistory`
//  AS (SELECT *
//      FROM `ParkingData` WHERE 1=2);
///////////////////////////////////////////////////////////////////////////////
/*
     drop table IF EXISTS `ParkingFitUser`;
     CREATE TABLE `ParkingFitUser` (
            `email` VARCHAR(255) NOT NULL,
            `password` VARCHAR(255) NULL DEFAULT '',
            `Name` VARCHAR(255) NULL DEFAULT '',
            `Telephone` VARCHAR(255) NULL DEFAULT '',
            `DateInclusion` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
            `Currency` VARCHAR(10) NULL DEFAULT 'BRL', 
            `Language` VARCHAR(10) NULL DEFAULT 'PT-BR', 
            `Active` INT(1) NULL DEFAULT 0,
            INDEX `Index 1` (`email`, `DateInclusion`),
            PRIMARY KEY (`email`)
    )
    COLLATE='utf8_general_ci'
    ENGINE=InnoDB;

////////////////////////////////////////////////////////////////////////////////    
       
    drop table IF EXISTS `PaymentLogs`;
    CREATE TABLE `PaymentLogs` (
            `Date` DATETIME not NULL DEFAULT CURRENT_TIMESTAMP,
            `emailUser` VARCHAR(255) not NULL DEFAULT '',
            `Application` VARCHAR(255) not NULL DEFAULT '',
            `AppContext` VARCHAR(255) not NULL DEFAULT '',
            `PaymentGateway` VARCHAR(255) not NULL DEFAULT '',
            `RawPayment` VARCHAR(15120) not null DEFAULT '',
            INDEX `Index 1` (`Date`, `emailUser`, `Application`, `AppContext`)
    )
    COLLATE='latin1_swedish_ci'
    ENGINE=InnoDB; 
 */
/*
    drop table IF EXISTS `PaymentCardsStored`;
    CREATE TABLE `PaymentCardsStored` (
            `emailUser` VARCHAR(255) NULL DEFAULT NULL,
            `Application` VARCHAR(255) NULL DEFAULT NULL,
            `AppContext` VARCHAR(255) NULL DEFAULT NULL,
            `CardNumber` VARCHAR(100) NULL DEFAULT NULL,
            `Holder` VARCHAR(100) NULL DEFAULT NULL,
            `ExpirationDate` VARCHAR(100) NULL DEFAULT NULL,
            `CardToken` VARCHAR(100) NULL DEFAULT NULL,
            `Brand` VARCHAR(25) NULL DEFAULT NULL,
            INDEX `Index 1` (`emailUser`, `Application`, `AppContext`)
    )
    COLLATE='latin1_swedish_ci'
    ENGINE=InnoDB
    ;
    ALTER TABLE `PaymentCardsStored` ADD COLUMN `cvc` VARCHAR(25) NULL AFTER `Brand`;
    ALTER TABLE `PaymentCardsStored` ADD COLUMN `Data` DATETIME NULL DEFAULT NULL AFTER `cvc`;   
    ALTER TABLE `PaymentCardsStored` ADD COLUMN `Default` INT NULL DEFAULT '0' AFTER `Data`;
    ALTER TABLE `PaymentCardsStored` CHANGE COLUMN `Default` `DefaultSel` INT(11) NULL DEFAULT '0' AFTER `Data`;

*/

/*
drop table IF EXISTS `ParkingFitProviderPaidFee`;
CREATE TABLE `ParkingFitProviderPaidFee` (
	`emailProvider` VARCHAR(255) NULL,
        `RefDate` VARCHAR(50) NULL,
        `Status` VARCHAR(50) NULL DEFAULT 'Not paid',
        `Date` DATETIME not NULL DEFAULT CURRENT_TIMESTAMP,
        INDEX `Index 1` (`Date`, `emailProvider`, `RefDate`)
)
*/
////////////////////////////////////////////////////////////////////////////////
/* 
 drop table IF EXISTS `ParkingProvider`;
 CREATE TABLE `ParkingProvider` (
	`email` VARCHAR(255) NOT NULL,
	`password` VARCHAR(255) NULL DEFAULT NULL,
        `NomeProvider` VARCHAR(255) NULL DEFAULT NULL,
        `NomeResponsavel` VARCHAR(255) NULL DEFAULT NULL,
        `TelefoneResponsavel` VARCHAR(255) NULL DEFAULT NULL,
        `IdResponsavel` VARCHAR(255) NULL DEFAULT NULL,
	`city` VARCHAR(255) NULL DEFAULT NULL,
	`address` VARCHAR(255) NULL DEFAULT NULL,
	`country` VARCHAR(40) NULL DEFAULT NULL,
        `countryCode` VARCHAR(2) NULL DEFAULT NULL,
	`AutorizationLegalDocuments` MEDIUMTEXT NULL,
        `Observacoes` MEDIUMTEXT NULL,
	`DateInclusion` DATETIME NULL DEFAULT NULL,
        `HourValue` float NULL DEFAULT 5.0, 
        `StaffPercHour` float NULL DEFAULT 5.0, 
        `Currency` VARCHAR(10) NULL DEFAULT 'BRL', 
        `Language` VARCHAR(10) NULL DEFAULT 'PT-BR', 
	`Active` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`email`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
  
ALTER TABLE `ParkingProvider`
  ADD `countryOperation` VARCHAR(2) NULL DEFAULT NULL,
  ADD `cityOperation` VARCHAR(255) NULL DEFAULT NULL,
  ADD `useRangeOperation` INT(11) NULL DEFAULT 0,
  ADD `latOperation` double NULL DEFAULT NULL, 
  ADD `lonOperation` double NULL DEFAULT NULL,
  ADD `rangeOperation` INT(11) NULL DEFAULT 0;  
  
 ALTER TABLE `ParkingProvider`
 ADD `cielomerid` VARCHAR(255) NULL DEFAULT NULL,
 ADD `cielomerkey` VARCHAR(255) NULL DEFAULT NULL;

 ALTER TABLE `ParkingProvider` ADD COLUMN `ParkingFitFee` DECIMAL(10,2) NULL DEFAULT '1' AFTER `cielomerkey`; 
  
ALTER TABLE `parkingprovider`
	ADD COLUMN `stripeid` VARCHAR(255) NULL DEFAULT NULL AFTER `cielomerkey`;

ALTER TABLE `parkingprovider`
	ADD COLUMN `paymentGateway` VARCHAR(50) NULL DEFAULT '0' AFTER `rangeOperation`;

*/
////////////////////////////////////////////////////////////////////////////////
/*
drop table IF EXISTS `ParkingDataPenalty`;
CREATE TABLE `ParkingDataPenalty`
  AS (SELECT *
      FROM `ParkingData` WHERE 1=2);
 ALTER TABLE `ParkingDataPenalty`
 ADD `emailstaff` VARCHAR(128) NULL;
 ALTER TABLE `ParkingDataPenalty`
 ADD `dateinfraction` DATETIME NULL DEFAULT CURRENT_TIMESTAMP;
 ALTER TABLE `ParkingDataPenalty`
 ADD `emailProvider` VARCHAR(128) NULL;
 ALTER TABLE `ParkingDataPenalty`
 ADD  `staffMoney` DECIMAL(10,2) NULL DEFAULT NULL;
 * 
 */

////////////////////////////////////////////////////////////////////////////////

/*
    drop table IF EXISTS `ParkingData`;
    CREATE TABLE `ParkingData` (
            `idUser` BIGINT(20) NOT NULL,
            `idCar`   VARCHAR(25) NULL,
  	    `Lat` DOUBLE NOT NULL,
	    `Longc` DOUBLE NOT NULL,
            `Address`    VARCHAR(255) NULL,
            `Street`    VARCHAR(128) NULL,
            `City`    VARCHAR(128) NULL,
            `State`   VARCHAR(128) NULL,
            `Country` VARCHAR(128) NULL,
            `Period`   INT(2) NULL,
            `DateTimePark` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
            `ParkPaid` TINYINT NULL DEFAULT '0',
            `JsonDat`     MEDIUMTEXT NULL,
            INDEX `iidParkingData` (`idUser`, `idCar`,`Country`,`City`, `Street`,`Lat`,`Longc` )
    )
    COLLATE='latin1_swedish_ci'
    ENGINE=InnoDB;
 
    ALTER TABLE `ParkingData`
      ADD `idProvider` VARCHAR(255) NULL DEFAULT NULL,
      ADD `cityOperation` VARCHAR(255) NULL DEFAULT NULL;  
 
     ALTER TABLE `ParkingData`
      ADD `valuePaid` decimal(10,2) NULL DEFAULT 0.0; 
   
    ALTER TABLE `ParkingProvider` CHANGE COLUMN `HourValue` `HourValue` DECIMAL(10,2) NULL DEFAULT '5' AFTER `DateInclusion`;

    ALTER TABLE `ParkingData`
            CHANGE COLUMN `idUser` `idUser` VARCHAR(255) NOT NULL DEFAULT '\'\'' FIRST,
            CHANGE COLUMN `idProvider` `idProvider` VARCHAR(255) NULL DEFAULT '\'\'' AFTER `JsonDat`;
    ALTER TABLE `ParkingData` ADD INDEX `Index 2` (`DateTimePark`,`idUser`,`idCar`);


 */   


///////////////////////////////////////////////////////////////////////////////
/*
    drop table IF EXISTS `ParkingDataPayment`;
    CREATE TABLE `ParkingDataPayment` (
            `idUser` BIGINT(20) NOT NULL,
            `idCar`   VARCHAR(25) NULL,
            `ParkPaid` TINYINT NULL DEFAULT '0',
            `Address`    VARCHAR(255) NULL,
            `City`    VARCHAR(128) NULL,
            `State`   VARCHAR(128) NULL,
            `Country` VARCHAR(128) NULL,
            `DateTimePark` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
            `JsonDatPark`     MEDIUMTEXT NULL, 
            `JsonDatPayment`     MEDIUMTEXT NULL,
            INDEX `iidParkingData` (`idUser`, `idCar`,`Country`,`City`,`Address` )
    )
    COLLATE='latin1_swedish_ci'
    ENGINE=InnoDB;
 */ 

////////////////////////////////////////////////////////////////////////////////
/*
 drop table IF EXISTS `ParkingProviderStaff`;
 CREATE TABLE `ParkingProviderStaff` (
	`emailProvider` VARCHAR(255) NOT NULL,
        `email` VARCHAR(255) NOT NULL,
	`password` VARCHAR(255) NULL DEFAULT NULL,
        `Phone` VARCHAR(255) NULL DEFAULT NULL,
        `Card_id` VARCHAR(255) NULL DEFAULT NULL,
        `Bank` VARCHAR(255) NULL DEFAULT NULL,
        `Agency` VARCHAR(255) NULL DEFAULT NULL,
        `Account` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`email`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
*/      

////////////////////////////////////////////////////////////////////////////////
