<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Multilanguage
 *
 * @author andre
 */
class Multilanguage {
    //put your code here
}
////////////////////////////////////////////////////////////////////////////////
LoadMultilanguageTable();
//////////////////////////////////////////////////////////////////////////////// 
// Set path to CSV file
function LoadMultilanguageTable()
{
    global $LangTable;
    $csvFile = $_SERVER["DOCUMENT_ROOT"]."/ChildMonitor/Util/Multilanguage.csv";
    // echo $csvFile ."<br><br>";
    $LangTable = readCSV($csvFile);
    // echo  "Ok<br><br>";
}

////////////////////////////////////////////////////////////////////////////////
// $docroot = realpath($_SERVER["DOCUMENT_ROOT"]);
// $pathfile = realpath(".");

// echo '<pre>';
// print_r($csv);
// echo  i2l("Mensal")."<br><br>";
// echo $docroot."<br><br>";
// echo $pathfile."<br><br>";
// echo findRoot() ."<br><br>";
// echo $_SERVER["DOCUMENT_ROOT"] ."<br><br>";


//echo '</pre>';

////////////////////////////////////////////////////////////////////////////////
function findRoot() 
{ 
    return(substr($_SERVER["SCRIPT_FILENAME"], 0, (stripos($_SERVER["SCRIPT_FILENAME"], $_SERVER["SCRIPT_NAME"])+1)));
}
////////////////////////////////////////////////////////////////////////////////
function readCSV($csvFile){
    $file_handle = fopen($csvFile, 'r');
    while (!feof($file_handle) ) {
        $line_of_text[] = fgetcsv($file_handle, 1024);
    }
    fclose($file_handle);
    return $line_of_text;
}

////////////////////////////////////////////////////////////////////////////////
function readLang()
{
    if(!isset($_COOKIE["language"])) {
        return "EN";
    } 
    else 
    {
        return $_COOKIE["language"];
    }
}
////////////////////////////////////////////////////////////////////////////////
// data - message on standard language PT-BR
function i2l($data)
{
    global $LangTable;
    // PT-BR - col 0      US - col 1
    if(readLang()==="")
    {
        //setCookie("language", DefLanguage, 1000);
        return $data;
    }  
    if(readLang()==="PT-BR")
    {
        return $data;
    }
    $language = readLang();
    if($language==="EN")
       $colLang = 1;
    else
    {   // Future default language US
        $colLang = 1;
    }
    $lin = 1; $col=0;
    //alert(LangTable.length);
    while( $LangTable[$lin][0]!="" )
    {
        if($LangTable[$lin][0]===$data || substr($LangTable[$lin][0],0, 20)===substr($data,0, 20) )
        {
           return  $LangTable[$lin][$colLang];
        }    
        $lin++;
    }
    // console.log("["+data+"] -> "+language+"!")
    return " [".$data."] -> ".$language."!  " ;
}
////////////////////////////////////////////////////////////////////////////////

