/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////// 
function CenterObjectTop(id)
{
    $(id).css('position', 'absolute');
    $(id).css('top', 0);
    $(id).css('left', ($(window).height() - $(id).height()) / 2);
}
////////////////////////////////////////////////////////////////////////////////
function JqueryDlgAcceptEnter(id, functexec)
{
    $(id).keypress(function (e)
    {
        if (e.keyCode == $.ui.keyCode.ENTER)
        {
            // $(id).find('.ui-button').first().click()
            functexec(id);
        }
    });
}
////////////////////////////////////////////////////////////////////////////////
function SelectPayment()
{
    /*
     <div id = "buttons">
     <label for="b1">button1</label>
     <label for="b2">button2</label>
     <label for="b3">button3</label>

     <input type="radio" name = "groupa" id = "b1" value="b1">
     <input type="radio" name = "groupa" id = "b2" value="b2">
     <input type="radio" name = "groupa" id = "b3" value="b3">
     </div>

     <script>
       $('#buttons input').button();     
       $('#b3').prop('checked', true).button("refresh");
     </script>
    */
    
    
    
    buffer = "<div id = \"buttons\">\
        <legend>Select payment gateway: </legend>\
        <label for=\"radio-1\">Stripe - World</label>\
        <input type=\"radio\" name=\"radio-1\" id=\"selStripe\">\
        <label for=\"radio-2\">Cielo - Brazil</label>\
        <input type=\"radio\" name=\"radio-1\" id=\"selCielo\">\
        </fieldset>";
    

    
//    $( document ).ready(function() 
//    {
//       // $('#buttons input').button();  
//       // $('#buttons input' ).buttonset();
//        $( "#selStripe" ).click(function() {
//           alert( "selStripe." );
//        });
//
//        $( "#selCielo" ).click(function() {
//           alert( "selCielo." );
//        });
//    
//    });
    
    return buffer;
}
////////////////////////////////////////////////////////////////////////////////
function trim(str)
{
    return str.replace(/^\s+|\s+$/g, "");
}
////////////////////////////////////////////////////////////////////////////////
// $data= new ColumnData();
function SqlInsert(table, data) {

    conlumnsnames = "";
    i = 0;
    while (typeof data[i] !== 'undefined')
    {
        if (i == 0)
            virg = "";
        else
            virg = ",";
        buffer = data[i].Name;
        conlumnsnames += virg + "`" + buffer + "` ";
        i++;
    }

    conlumnsvalues = "";
    i = 0;
    while (typeof data[i] !== 'undefined')
    {
        if (i === 0)
            virg = "";
        else
            virg = ",";
        buffer = data[i].Value;

        if (data[i].Type === "Num")
            conlumnsvalues += virg + buffer + " ";
        if (data[i].Type === "Char" || data[i].Type === "Date" || trim(data[i].Type) === "")
            conlumnsvalues += virg + "'" + buffer + "' ";
        i++;
    }

    sql = "INSERT INTO `" + table + "` (" + conlumnsnames + ") VALUES (" + conlumnsvalues + ")";
    return(sql);
}
////////////////////////////////////////////////////////////////////////////////
function SqlUpdate(table, data, condition)
{
    conlumnsvalues = "";
    i = 0;

    while (typeof data[i] !== 'undefined')
    {
        if (i === 0)
            virg = "";
        else
            virg = ",";
        bufferName = data[i].Name;
        bufferData = data[i].Value;


        if (data[i].Type === "Num")
            conlumnsvalues += virg + "`" + bufferName + "` = " + bufferData + " ";
        if (data[i].Type === "Char" || data[i].Type === "Date" || trim(data[i].Type) === "")
            conlumnsvalues += virg + "`" + bufferName + "` = '" + bufferData + "' ";
        i++;
    }
    sql = "UPDATE `" + table + "` SET " + conlumnsvalues + " WHERE (" + condition + ")";

    return(sql);
}

////////////////////////////////////////////////////////////////////////////////
function PostExecSql(sql, fdatProcess)
{
    var dataSQL = {};
    url = window.location.protocol + '//' + location.host + '/ChildMonitor/Util/PostGeoRequestService.php';
    // TODO: isLogged verifica apenas parking provider
    dataSQL.code = "DSCLAD8724FN4KJF4NKF145243524K2RFK345JK423KFN23K5J4K2352MF3K4J534RE243K5245";
    dataSQL.sql = sql;
    json_str = JSON.stringify(dataSQL);
    // alert("Size json: "+json_str.length);

    execAjax(url, {type: "ExecuteSQL", json: json_str}, false, fdatProcess);
}
////////////////////////////////////////////////////////////////////////
function CreateSelectFromSql(id, sql,opt)
{
    //     <select name="speed" id="speed">
    //      <option>Slower</option>
    //      <option>Slow</option>
    //      <option selected="selected">Medium</option>
    //      <option>Fast</option>
    //      <option>Faster</option>
    //    </select>

    // $("#staff").selectmenu();
    $('.ui-dialog').css('overflow', 'visible');  // Solves menu hidden on overflow
    output="";
    fdatProcess = function (data)  
    {
       if (data.response === "SqlResult")
        {
            output = "<select "+opt+" name="+id+" id="+id+">";
            il = 0;
            sel = "selected=\"selected\" ";
            while (typeof data.sql[il] !== 'undefined')
            {
                output += "<option value=\"" + data.sql[il][data.sqlrows[0]] + "\" "+sel+">" + data.sql[il][data.sqlrows[1]] + "</option>";
                sel="";
                il++;
            }

            output += " </select>";
            
        }
    };

    PostExecSql(sql, fdatProcess);

    // $("#staff").selectmenu();
    return output;
}

//////////////////////////////////////////////////////////////////////////////////
// var SqlToTableClickOnDataFunc = function(data){ alert(data); };
var SqlToTableClickOnDataFunc = null;
function SqlToTableClickOnData(data)
{   
    // Data is the first column of clicked row
    if(SqlToTableClickOnDataFunc!==null)
        SqlToTableClickOnDataFunc(data);
}
////////////////////////////////////////////////////////////////////////////////
function SqlToTable(sql,size)
{
    SqlToTableClickOnDataFunc = null;
    divStyle = "  opacity: 1.5;   border-radius: 2px; box-shadow:    0 0px 0 0 #444; color:  #fff; display:       inline-block; padding: 0px 0px 0px 0px; text-align:left; box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); -moz-box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); -webkit-box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5);  background-color:rgba(255,255,255,1); ";
    tdTableStyle = "   style=' border-bottom:1px solid #999999;  border-left:1px solid #EEEEEE; border-right:1px solid #EEEEEE; border-top:1px solid #EEEEEE; ' ";
    pStyle =  " white-space: nowrap; margin: 1px; height:20px; ";
    // alert("sdsdsds"); 
    if(typeof size==="undefined" || size===null)
        size = " width:98%; height:300px; "

    output = "";
    // alert(sql);
    fdatProcess = function (data)
    {
        // alert(data.response); 
        // alert(data.period); 
        if (data.response === "SqlResult")
        {
            mouseover = " onmouseover=\"this.style.backgroundColor ='#616f87';\" onmouseout=\"this.style.backgroundColor ='#FFFF';\" ";
            output = "<div  id='sql_table' style=' "+size+" overflow-x: auto; overflow-y: auto;  " + divStyle + "  '   > ";
            output += "<table  > <tr "+mouseover+ " " + tdTableStyle + " >";
            i = 0;
            while (typeof data.sqlrows[i] !== 'undefined')
            {
                output += "<th " + tdTableStyle + " ><p style=\" "+pStyle+"  color:black;\"' >" + data.sqlrows[i] + "</p></th>";

                i++;
            }
            output += "</tr>";
            console.log(output);
            il = 0;
            while (typeof data.sql[il] !== 'undefined')
            {
                i = 0;
                output += "<tr onclick=\"SqlToTableClickOnData('"+data.sql[il][data.sqlrows[0]]+"')\" "+mouseover+ " " + tdTableStyle + " >";
                while (typeof data.sqlrows[i] !== 'undefined')
                {
                    output += "<td " + tdTableStyle + " ><p style=\" "+pStyle+"  color:black;\"' >" + data.sql[il][data.sqlrows[i]] + "</p></td>";
                    i++;
                }
                output += "</tr>";

                il++;
            }

            output += " </table> </div> ";
            
            
        }

    };

    PostExecSql(sql, fdatProcess);
    return output;
}
////////////////////////////////////////////////////////////////////////////////
function SqlJsonResult2Vet(data)
{
    // Max 50 coluns
    var vet = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]];
    i = 0;

    while (typeof data.sqlrows[i] !== 'undefined')
    {
        vet[0][i] = data.sqlrows[i];
        i++;
    }
    il = 0;
    while (typeof data.sql[il] !== 'undefined')
    {
        i = 0;
        while (typeof data.sqlrows[i] !== 'undefined')
        {
            vet[il + 1][i] = data.sql[il][data.sqlrows[i]];
            i++;
        }
        il++;
    }
    return vet;
}
////////////////////////////////////////////////////////////////////////////////
function SqlToVet(sql)
{
    var vet = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]];
    fdatProcess = function (data)
    {
        // alert(data.response); 
        // alert(data.period); 
        if (data.response === "SqlResult")
        {
            vet = SqlJsonResult2Vet(data); 
        }

    };

    PostExecSql(sql, fdatProcess);
    return vet;
}
////////////////////////////////////////////////////////////////////////////////
function ConvertDate2DB(date)
{
    return AjustaData_dd_MM_yyyy_2_yyyy_MM_dd(date);
}
////////////////////////////////////////////////////////////////////////////
function AjustaData_dd_MM_yyyy_2_yyyy_MM_dd(sData)
{
   // "01/12/2010"
   // "2010-12-01"

   // String c = "abc".substring(2,3);
   // String d = cde.substring(1, 2);
   if(typeof sData==='undefined')
      return(null);
   sData = sData.substring(6, 10)+"-"+sData.substring(3, 5)+"-"+sData.substring(0, 2);
   return(sData);
}
////////////////////////////////////////////////////////////////////////////////
function CheckExistence(sql)
{
   bexist = false;
   fdatProcess = function(data) 
   {   
        // alert(data.response); 
        // alert(data.period); 
        if(data.response==="SqlResult")
        {
            i=0;
            if(typeof data.sql[i]==='undefined')
                return;
            else
                bexist=true;


        }
  }; 
  PostExecSql(sql,fdatProcess);
  return bexist;
}
////////////////////////////////////////////////////////////////////////////////
function GetSqlValue(sql)
{
   bexist = "";
   fdatProcess = function(data) 
   {   
        // alert(data.response); 
        // alert(data.period); 
        if(data.response==="SqlResult")
        {
            i=0;
            if(typeof data.sql[i]==='undefined')
                return;
            else
                bexist= data.sql[i][data.sqlrows[0]];  
        }
  }; 
  PostExecSql(sql,fdatProcess);
  return bexist;
}
////////////////////////////////////////////////////////////////////////////////
// var SqlToTableClickOnDataFunc = function(data){ alert(data); };
// Transformar esta porra em um vetor
var VetToTableIntClickOnDataFunc = [];
function VetToTableIntClickOnData(id,data,data2)
{   
    // Data is the first column of clicked row
    if(VetToTableIntClickOnDataFunc[id]!==null)
        VetToTableIntClickOnDataFunc[id](data,data2);
}
////////////////////////////////////////////////////////////////////////////////
// Array to Table Interface
function VetToTableInt(id,vet,size)
{
    VetToTableIntClickOnDataFunc[id] = null;
    
    divStyle = "  opacity: 1.5;   border-radius: 2px; box-shadow:    0 0px 0 0 #444; color:  #fff; display:       inline-block; padding: 0px 0px 0px 0px; text-align:left; box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); -moz-box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); -webkit-box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5);  background-color:rgba(255,255,255,1); ";
    tdTableStyle = "   style=' border-bottom:1px solid #999999;  border-left:1px solid #EEEEEE; border-right:1px solid #EEEEEE; border-top:1px solid #EEEEEE; ' ";
    pStyle =  " white-space: nowrap; margin: 1px; height:20px; ";
    // alert("sdsdsds"); 
    if(typeof size==="undefined")
        size = " width:98%; height:300px; "
    
    mouseover = " onmouseover=\"this.style.backgroundColor ='#616f87';\" onmouseout=\"this.style.backgroundColor ='#FFFF';\" ";
    output = "<div  id='vet_table' style=' "+size+" overflow-x: auto; overflow-y: auto;  " + divStyle + "  '   > ";

    output += "<table  > ";
    il = 0;
    ic = 0;
    while (typeof vet[il][ic] !== 'undefined')
    {
        output += "<tr onclick=\"VetToTableIntClickOnData('"+id+"','"+vet[il][0]+"','"+vet[il][1]+"')\" "+mouseover+ " " + tdTableStyle + " valign=\"middle\" >";
        while (typeof vet[il][ic] !== 'undefined')
        {
            if(il===0)
               output += "<th " + tdTableStyle + " ><p style=\" "+pStyle+"  color:black;\"' >" + vet[il][ic] + "</p></th>";
            else
               output += "<td " + tdTableStyle + " ><p style=\" "+pStyle+"  color:black;\"' >" + vet[il][ic] + "</p></td>";
            ic++;
        }
        output += "</tr>";
        ic = 0;
        il++;
    }
    output += " </table> </div> ";
    return output;
}
////////////////////////////////////////////////////////////////////////////////
function VetToTable(vet)
{
    output = "<table  > ";
    il = 0;
    ic = 0;
    while (typeof vet[il][ic] !== 'undefined')
    {
        output += "<tr valign=\"middle\" >";
        while (typeof vet[il][ic] !== 'undefined')
        {
            output += "<td >" + vet[il][ic] + "</td>";
            ic++;
        }
        output += "</tr>";
        ic = 0;
        il++;
    }
    output += " </table>";
    return output;
}
////////////////////////////////////////////////////////////////////////////////
function CreateJqTabs(id, vetTabs, vetTabsData)
{
    // $(id).tabs();
    output = "<div id=\"" + id + "\"> ";
    il = 0;
    output += "<ul> ";
    while (typeof vetTabs[il] !== 'undefined')
    {
        output += "<li><a href=\"#" + id + "-" + il + 1 + "\">" + vetTabs[il] + "</a></li>";
        il++;
    }
    output += "</ul> ";
    il = 0;
    while (typeof vetTabs[il] !== 'undefined')
    {

        output += "<div id=\"" + id + "-" + il + 1 + "\">" + vetTabsData[il] + "</div>";
        il++;
    }
    output += "</div>";
    return output;
}

////////////////////////////////////////////////////////////////////////////////   
function ShowErrorToast(msg, fasthide)
{
    /*
     *   toastr["info"]("Are you the six fingered man?")
     *   toastr["success"]("Inconceivable!")
     toastr["warning"]("<div><input class="input-small" value="textbox"/>&nbsp;<a href="http://johnpapa.net" target="_blank">This is a hyperlink</a></div><div><button type="button" id="okBtn" class="btn btn-primary">Close me</button><button type="button" id="surpriseBtn" class="btn" style="margin: 0 8px 0 8px">Surprise me</button></div>")
     */
    itimeOut = "5000";
    if (fasthide === 0)
        itimeOut = "0";
    else
    {
        itimeOut = "5000";
        // botclose = " <button type=\"button\" class=\"ui-button\">Ok</button>"
    }
    // opções de posição
    // toast-top-right  toast-bottom-right
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-center-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": itimeOut,
        "extendedTimeOut": itimeOut,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    toastr["error"](i2l(msg));
}
//////////////////////////////////////////////////////////////////////////////// 
function ShowInfoToast(msg, fasthide)
{
    /*
     *   toastr["info"]("Are you the six fingered man?")
     *   toastr["success"]("Inconceivable!")
     toastr["warning"]("<div><input class="input-small" value="textbox"/>&nbsp;<a href="http://johnpapa.net" target="_blank">This is a hyperlink</a></div><div><button type="button" id="okBtn" class="btn btn-primary">Close me</button><button type="button" id="surpriseBtn" class="btn" style="margin: 0 8px 0 8px">Surprise me</button></div>")
     */
    botclose = "";
    itimeOut = "5000";
    if (fasthide === 0)
        itimeOut = "0";
    else
    {
        itimeOut = "5000";
        // botclose = " <button type=\"button\" class=\"ui-button\">Ok</button>"
    }
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-center-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": itimeOut,
        "extendedTimeOut": itimeOut,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    toastr["success"](i2l(msg) + botclose);
}

////////////////////////////////////////////////////////////////////////////////
function SetUITransparent()
{
//    .ui-widget-content {
//	border: 1px solid #555555;
//        /* Background das janelas */
///*	background: #000000 url("/ChildMonitor/Menus/jquery-ui_truck/images/ui-bg_loop_25_000000_21x21.png") 50% 50% repeat;*/
//        background: rgba(0, 0, 0, 0.80);
///*        background: #093028;  fallback for old browsers 
//        background: -webkit-linear-gradient(to left, #093028 , #237A57);  Chrome 10-25, Safari 5.1-6 
//        background: linear-gradient(to left, #093028 , #237A57);  W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
//        
//
//	color: #ffffff;
//   }
    // alert('tentei iiiiiiiiiiii'); 
    // $('#dlglogin').css('background','#FFFFFF');
    // $('.ui-widget-content').css('background',rgba(255, 0, 0, 0.80));
    
    background_color  = 'transparent';
    
    
    $('.ui-widget-content').css('background-color', background_color);
    $('.ui-button').css('background', '#dddddd');
    $('.ui-button').css('background-color', background_color);
    $('.ui-dialog-titlebar').css('border', '#555555');


    $('#tabs').css('background', '#dddddd');
    $('#tabs').css('background-color', background_color);



    $("li[role='tab']").css('background', '#dddddd');
    $("li[role='tab']").css('background-color', background_color);

    $("ul[role='tablist']").css('border', '1px solid #555');


    $('.ui-dialog').css('z-index', '19');
    $('.ui-dialog').css('top', '49px');
    $('.ui-dialog').css('overflow', 'visible');

    $('body').css('overflow-y', 'auto');
    // Close the close button
    $("button[title='Close']").hide();
    
    // background escurecido dos diálogos modais
    $('.ui-widget-overlay').css('opacity', '0.1');
    
    $(LastActiveDlg).css('padding', '0 0');
}
////////////////////////////////////////////////////////////////////////////////
function SetUI_Black()
{
 
     background_color  = 'rgba(0, 0, 0, 0.80)';
    
   
    
    
    $('.ui-widget-content').css('background-color', background_color);
    $('.ui-button').css('background', '#dddddd');
    $('.ui-button').css('background-color', background_color);
    $('.ui-dialog-titlebar').css('border', '#555555');


    $('#tabs').css('background', '#dddddd');
    $('#tabs').css('background-color', background_color);



    $("li[role='tab']").css('background', '#dddddd');
    $("li[role='tab']").css('background-color', background_color);

    $("ul[role='tablist']").css('border', '1px solid #555');


    $('.ui-dialog').css('z-index', '19');
    $('.ui-dialog').css('top', '49px');
    $('.ui-dialog').css('overflow', 'visible');

    $('body').css('overflow-y', 'auto');
    // Close the close button
    $("button[title='Close']").hide();
    
    // background escurecido dos diálogos modais
    $('.ui-widget-overlay').css('opacity', '0.1');
    
    $(LastActiveDlg).css('padding', '0 0');
}
////////////////////////////////////////////////////////////////////////////////
var vetCallBackCreateMenuDiv = [];
function CreateMenuDivFunc(id,il)
{
    if(vetCallBackCreateMenuDiv!==null)
    {
        if (typeof vetCallBackCreateMenuDiv[id][il] !== 'undefined')
        {    
           vetCallBackCreateMenuDiv[id][il]();
        }   
    }

}
////////////////////////////////////////////////////////////////////////////////
function CreateMenuDivUser(UserName,id,vet,vetCallBack)
{
    if (typeof vetCallBack !== 'undefined' && vetCallBack !== null )
         vetCallBackCreateMenuDiv[id] = vetCallBack;
     
    output =  "<style> .divMenuZtp:hover{ color:rgb(240,240,240); } </style>";
    output += "<div  onclick=\"\"; id=\""+id+"\"  style=\" z-index:30; display:none; top:46px; font: 80.0% \'Montserrat\', Arial, sans-serif; letter-spacing: 1px; color:rgb(200,200,200);  position:absolute; background-color:rgb(50,50,50); \"  > ";
    // Get loggin Name
    output += "<div class=\"divMenuZtp\" style=\" background-color:rgb(0,0,0); padding:25px; border-bottom:1pt solid rgb(155,155,155); \"  >" + UserName + "</div>";
    il = 0;
    ic = 0;
    while (typeof vet[il][ic] !== 'undefined')
    {
        output += "<div  id=\""+id+"-"+il+"\"  >";
        while (typeof vet[il][ic] !== 'undefined')
        {
            output += "<div onclick=\"CreateMenuDivFunc('"+id+"',"+ic+");\"; id=\""+id+"-"+il+"-"+ic+"\"  class=\"divMenuZtp\" style=\" padding:10px; border-bottom:1pt solid rgb(55,55,55); \"  >" + vet[il][ic] + "</div>";
            ic++;
        }
        output += "</div>\n";
        ic = 0;
        il++;
    }
    output += " </div>";
    return output;    
}
////////////////////////////////////////////////////////////////////////////////
function CreateMenuDiv(id,top,left,vet,vetCallBack)
{
    if (typeof vetCallBack !== 'undefined' && vetCallBack !== null )
         vetCallBackCreateMenuDiv[id] = vetCallBack;
     
    output =  "<style> .divMenuZtp:hover{ color:rgb(240,240,240); } </style>";
    output += "<div  onclick=\"\"; id=\""+id+"\"  style=\" z-index:30; display:none; top:"+top+"px; left:"+left+"px; font: 80.0% \'Montserrat\', Arial, sans-serif; letter-spacing: 1px; color:rgb(200,200,200);  position:absolute; background-color:rgb(50,50,50); \"  > ";
    il = 0;
    ic = 0;
    while (typeof vet[il][ic] !== 'undefined')
    {
        output += "<div  id=\""+id+"-"+il+"\"  >";
        while (typeof vet[il][ic] !== 'undefined')
        {
            output += "<div onclick=\"CreateMenuDivFunc('"+id+"',"+ic+");\"; id=\""+id+"-"+il+"-"+ic+"\"  class=\"divMenuZtp\" style=\" padding:10px; border-bottom:1pt solid rgb(55,55,55); \"  >" + vet[il][ic] + "</div>";
            ic++;
        }
        output += "</div>\n";
        ic = 0;
        il++;
    }
    output += " </div>";
    return output;    
}
////////////////////////////////////////////////////////////////////////
var handleTimeOut=null;
function CreateFloatingMenuMobile(User,vetMenu,vetCallback)
{
   // vet = [["1","2","3"],[]]
   
   szBuffer = "<div style=\" position:absolute; z-index:2; top:100px; left:10px;\" width:30px; height:30px; id=\"id_FloatMenuMobile\"> <img src=\"Img/mobile_menuBLK.png\" alt=\"\" height=\"35\" width=\"35\"> </div> ";
   szBuffer += CreateMenuDivUser(User,"id_FloatMenuMobileMenu",vetMenu,vetCallback);
   $( document ).ready(function() 
   {
      SetupClicks();

   });
   function SetupClicks()
   {
       $("#id_FloatMenuMobileMenu").hide();
       $("#id_FloatMenuMobileMenu").click(function(){
            $("#id_FloatMenuMobileMenu").toggle("slow");
            clearTimeout(handleTimeOut);

       });
       $( "#id_FloatMenuMobile" ).click(function() 
       {
            $("#id_FloatMenuMobileMenu").toggle("slow");
            handleTimeOut=setTimeout(function(){  $("#id_FloatMenuMobileMenu").hide( "slow"); }, 4000);
       }); 
   }
   SetupClicks();
   return szBuffer;
}
////////////////////////////////////////////////////////////////////////
function SetCheckBox(checkbox, bval)
{
    // $('#checkRegion').prop('checked', false); $('#checkRegion').button( "refresh" )
    $(checkbox).prop('checked', bval); // $(checkbox).button( "refresh" );
}
////////////////////////////////////////////////////////////////////////
function GetCheckBoxState(checkbox)
{
    return $(checkbox).is(':checked');
}
////////////////////////////////////////////////////////////////////////
function CreateCheckBox(id, label, onclick)
{
    return " <p>" + label + " <input onclick='" + onclick + "' type='checkbox' name='" + id + "' id='" + id + "'> </p>";
}
////////////////////////////////////////////////////////////////////////////////
function ConfirmDialog(message, funcexecOK)
{
    dlgdata = "<div id=\"dlgconf1111\" title=\"Confirma\">";
    dlgdata += message + "<br>";
    dlgdata += "</div>";

    $(dlgdata).dialog({
        position: dposition,
        resizable: false,
        height: "auto",
        width: "auto",
        modal: true,
        open: function () {
            SetupDialog();
        },
        buttons:
                {
                    "Sim": function () 
                    {
                        funcexecOK();
                        $("#dlgconf1111").dialog("destroy");
                    },
                    "Não": function () 
                    {
                        $("#dlgconf1111").dialog("destroy");

                    }
                }
    });

}
////////////////////////////////////////////////////////////////////////