<?php ob_start("ob_gzhandler"); ?>
<?php session_start(); ?>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
////////////////////////////////////////////////////////////////////////////////
function MapServicesUser() 
{
    $top = "45px";
    $heightz = "45";
    
    // Mapzen key search-EzrHuLH
    $idUser = 0;
    if(isMobile()) $isMobile = 1; else $isMobile = 0;
    $headersM = HeadersMap();
    $selectAddr = SelectAddressAutoComp("idOri","End:",$lat,$lon);
    $selectAddrDst = SelectAddressAutoComp("idDst","To:",$lat,$lon);
    $divStyle= "  z-index: 1; opacity: 0.5; background:    #000; background:    -webkit-linear-gradient(#000, #011629); background:    linear-gradient(#000, #011629);  border-radius: 5px; box-shadow:    0 0px 0 0 #444; color:  #fff; display:       inline-block; padding: 0px 3px 7px 3px; text-align:    center; text-shadow:   1px 1px 0 #000;";
      
    $buttonGetService = "<table  style=\"width:100%\" >  <tr> <td align=\"center\" > <p style=\"font-size:10px\">  </p> </td> <td align=\"center\" id=\"LabelFromHere\" > Registra Estacionamento  </td> <td align=\"right\" >  </td>  <tr> </table>";
    $divGetService = "<div onclick=\"click_fromhere()\" style=\" position:absolute; z-index:-49; top:50vh; left:25vw; width:200px; min-height:40px; $divStyle \" id=\"start\" /> $buttonGetService  </div>";
    
    
    $dataSearch = <<<EOT
    $headersM 
  
    <script src="/ChildMonitor/Util/Util.min.js"></script> 
    <script type="text/javascript">  
        
    vetCallBack = [];        
    vetCallBack[0] = funcUserParkingHistory;        
    vetCallBack[1] = funcUserParkingPayment;  
    if(isLogged('ParkingFitUser')==="")
    {
       vetMenu = [[i2l("Estacionamentos"),i2l("Pagamentos")],[]];
    }
    else
    {
       vetMenu = [[i2l("Estacionamentos"),i2l("Pagamentos"),"Logout"],[]];
       vetCallBack[2] = funcUserParkingLogout; 
    }
   
    </script>        
    <body  class="bodyForm"   >   
    <div id="idbodymapuser" style=" position:absolute; z-index:1; top:0; left:0; width:100%; height:100%; "  >
        <div style=" position:absolute; z-index:1; top:$top; left:0; width:100%; height:100; "  id="map1"> </div> <br> 
        $divGetService 

        <div style=" position:absolute; z-index:2; top:$top; left:10px; width:98vw; $divStyle "  id="divAddress" /> <p style="font-size:10px" id="idAddress" >    </div>
        <div style=" position:absolute; z-index:2; top:$top; left:10px; width:98vw;  "  id="divOriDst" />  $selectAddr  $selectAddrDst </div>
        <div style=" position:absolute; z-index:2; top:$top; left:0;" width:10px; height:10px; id="gpsLoc"> <img src="Img/GPS.png" alt="" height="30" width="30"> </div> <br> 
        <div id="idMenuMobileParkUser" > <script type="text/javascript"> document.write(CreateFloatingMenuMobile(isLogged('ParkingFitUser'),vetMenu,vetCallBack)); </script> </div>  
    </div>    
    </body>      
    <script type="text/javascript">        
 
    //////////////////////////////////// 
    ActiveFormWindow = "#idbodymapuser"; 
            
    var markerGlb=null;  
    var markerMe=null; 
    var markerOri=null;  
    var markerDst=null;  
    var controlRoute=null; 
    var idOrilatGlbOld=null;
    var idOrilonGlbOld=null;
    var idDstlatGlbOld=null;
    var idDstlonGlbOld=null;
    var isMobile=$isMobile;
    var dtHoraPedido=null;
    var UserId=1;         
    ///////////////////////////////////// 
            
    WaitBmp(0);
    var altura_tela = $(window).height(); /*cria variável com valor do altura da janela*/
    var largura_tela = $(window).width(); 
     
            
    largura = largura_tela-50;
    $("#divAddress").css('width', largura);
    $("#divAddress").css('left', (largura_tela/2)-(largura/2));    
            
    largura = 200;
    $("#start").css('width', largura);
    $("#start").css('left', (largura_tela/2)-(largura/2));        
            
    $("#gpsLoc").css('top',altura_tela-55);
    $("#gpsLoc").css('left', largura_tela-50);
    $('#gpsLoc').on( "click", function() {
        SetMarkerOnMyLocation();
    });
  
    // avoid showing scrollbars on main window
    $(".bodyForm").css('overflow','hidden');
        
    $("#map1").height(altura_tela-$heightz); /* aplica a variável a altura da div*/ 
    $('#divAddress').hide();
    $('#divOriDst').hide();        
 
    $('#divOriDst').slideToggle();
            
    CleanRoute();
           
    var bTimerLoc = 0; 
   
    glbLatNow = 0.0;    
    glbLngNow = 0.0;          
    setTimeout(function() 
    {
       SetMarkerOnMyLocation();
       SetMapDragActions();     
    }, 5000);
     
    initialize();
    GetLastUserStatus();

   
    ////////////////////////////////////////////////////////////////////////////
    function timeDiff(dFinal, dInicial) 
    {
        var d1 = dFinal.getTime();
        var d2 = dInicial.getTime();
        var df = (d1 - d2);
        if(df<0)
           return "NegativeTime";
        var td = {
            h: Math.trunc(df / (60 * 60 * 1000)), //horas
            m: Math.trunc((df / (60 * 1000))%60), //minutos
            s: Math.trunc((df / 1000)%60)
        };
        // console.log("-------------");
        // console.log(df);    
        // console.log(td.h);
        // console.log(td.m);
        // console.log(td.s);
            
        var result = sprintf("%02d:%02d:%02d",td.h,td.m,td.s);
            
        return result;
    }
    ////////////////////////////////////////////////////////////////////////////
    function click_fromhere()
    {
        //  "<a href=\"waze://?ll="+idDstlatGlb+","+idDstlonGlb+"&z=10\">Go!</a>"  
            
        // $('#idOri').val($('#idAddress').text().trim());
        
        // Close autocompletes
        onclick_idOri();
        onclick_idDst(); 
            
        if($('#LabelFromHere').html().trim()== "Registra Estacionamento")
        {    
           
            
           if($('#idOri').val()=="")
           {
              ShowErrorToast("Endereço Inválido",1);            
              return; 
           } 
           idOrilatGlb = glbLatNow;    
           idOrilonGlb = glbLngNow; 
           DialogConfirmParking();
           
           SaveUserLastStatus(CollectUserData());
           return;

        }
        if($('#LabelFromHere').html().trim()== "Waiting Transport...Cancel?")
        {    

        }           
        ////////////////////////////////////////////////////////////////////////      
        // Chammar Waze http://waze.to/?ll=40.758895,-73.985131 
        // waze://?ll=37.331689,-122.030731&z=10
    }  

    ///////////////////////////////////////////////////////////////////////////       
    function CleanRoute()
    { 
        cShow_idOri();
        cHide_idDst();                
        // $('#idOri').val("");
        // $('#idDst').val("");

        if( controlRoute != null)
           controlRoute.setWaypoints([]); 
        markerOri = RemoveMarker(markerOri);
        markerDst = RemoveMarker(markerDst);
        if(map!=null)
        {
            map.setZoom(16);
            SetMapDragActions();
        }
    }

    //////////////////////////////////// 
    function SetMapDragActions() 
    {
        map.on('move', function () {
           // StopMapTimer();
            if(markerMe!=null) 
               markerMe.setLatLng(map.getCenter());
            
            pos = map.getCenter()
            
            if(pos.lat!=undefined && pos.lng!=undefined )
            {

               if($('#LabelFromHere').html().trim()== "Registra Estacionamento")
               {
                    glbLatNow = pos.lat;    
                    glbLngNow = pos.lng; 
                    
                    glbLatUserInterface = pos.lat;    
                    glbLngUserInterface = pos.lng; 
            
                    idOrilatGlb = pos.lat;    
                    idOrilonGlb = pos.lng;   
               }           
 
            }
            
           //  CreateMapTimer();
	});
        
	//Dragstart event of map for update marker position
	map.on('dragstart', function(e) {  
            StopMapTimer();
            ActionOnControls(["#start","#divOriDst","#id_FloatMenuMobile","#gpsLoc"],"hide");          
        });
            
            
	//Dragend event of map for update marker position
	map.on('dragend', function(e) {
            CreateMapTimer();
            ActionOnControls(["#start","#divOriDst","#id_FloatMenuMobile","#gpsLoc"],"show");
            
            var cnt = map.getCenter();
            if(markerMe!=null) 
               var position = markerMe.getLatLng();
            lat = Number(position['lat']).toFixed(5);
            lng = Number(position['lng']).toFixed(5);
            //console.log(position);
            // setLeafLatLong(lat, lng);
            WriteAddressOnDestiny(); 
            
	});
            
            
    }
    //////////////////////////////////// 
    function CleanMapDragActions() 
    {
        map.off('move', function (e) {
            
	});
           
	//Dragend event of map for update marker position
	map.off('dragend', function(e) {
            
	});          
    }
    //////////////////////////////////// 
    function initialize() 
    {
        map = ShowOpenMap('map1',glbLatNow,glbLngNow);     
        // map.on('click', onMapClick);     
    
        GetDeviceId();
            
        CreateMapTimer();
        myTimer(); 
    }
    ////////////////////////////////////   
    function CreateMapTimer()
    { 
       iTimer=setInterval(function () {myTimer()}, 3000); // 3 segundos 
    }
    ////////////////////////////////////   
    function StopMapTimer()
    {
        clearInterval(iTimer);    
    }
    ////////////////////////////////////         
    function onMapClick(e) 
    {
       alert("You clicked the map at " + e.latlng);
    }
    //////////////////////////////////// 
    var SetMarkerOnMyLocationTimes=0;        
    function SetMarkerOnMyLocation()
    {
        // getLocation();      
        // alert(GetOpenMapAddress(glbLatNow,glbLngNow));  
            
        getLocation();
        glbLatNow =   glbLat;    
        glbLngNow =   glbLng;    
            
        map.setView(new L.LatLng(glbLatNow, glbLngNow)); 
            
        // markerMe = PutMarker(markerMe, map,"Img/Black_Marker.png",20,33,glbLatNow,glbLngNow);
        markerMe = PutMarkerLocalization(markerMe, map,glbLatNow,glbLngNow);    
    }        
    //////////////////////////////////// 
    function SetMarkerOnNewLocation(glbLatNow,glbLngNow)
    {
        // getLocation();      
        // alert(GetOpenMapAddress(glbLatNow,glbLngNow));   
            
        map.setView(new L.LatLng(glbLatNow, glbLngNow)); 
            
        // markerMe = PutMarker(markerMe, map,"Img/Black_Marker.png",20,33,glbLatNow,glbLngNow);
        markerMe = PutMarkerLocalization(markerMe, map,glbLatNow,glbLngNow);
    }        

    ///////////////////////////////////////////////////////////////////////////
    function ChangedRoute()
    {
        if( idOrilatGlb!=0.0 && idOrilonGlb!=0.0 && idDstlatGlb!=0.0 && idDstlonGlb!=0.0 )
        {    
           if( idOrilatGlb!=idOrilatGlbOld || idOrilonGlb!=idOrilonGlbOld || idDstlatGlb!=idDstlatGlbOld || idDstlonGlb!=idDstlonGlbOld ) 
           { 
                idOrilatGlbOld = idOrilatGlb;
                idOrilonGlbOld = idOrilonGlb;
                idDstlatGlbOld = idDstlatGlb; 
                idDstlonGlbOld = idDstlonGlb;             
                return true;
           }
        }       
        idOrilatGlbOld = idOrilatGlb;
        idOrilonGlbOld = idOrilonGlb;
        idDstlatGlbOld = idDstlatGlb; 
        idDstlonGlbOld = idDstlonGlb; 
        return false;
    }        
    /////////////////////////////////////////////////////////////////////////// 
    function SetupRoute(latOri,lonOri,latDest,lonDest)
    { 
        // alert(latOri+"-"+lonOri+","+latDest+"-"+lonDest);
        WaitBmp(1);
        error=false;
        if( controlRoute != null)
           controlRoute.setWaypoints([]); 

        controlRoute = L.Routing.control({
               fitSelectedRoutes: true,
               createMarker: function() { return null; },
               lineOptions: { styles: [{color: 'green', opacity: 1, weight: 5}] },
               waypoints: [ L.latLng(latOri,lonOri), L.latLng(latDest,lonDest)]}
        ).addTo(map)
        .on('routingerror', function (e) 
        { 
            ShowErrorToast("Routing error"); 
            error=true;
            if(map!=null)
            {
               map.fitBounds([ [latOri,lonOri],[latDest,lonDest]]);             
            }
            WaitBmp(0);
        });
         
        if(error==false)
           setTimeout(function(){ map.setZoom(map.getZoom()-1);  }, 3000); 
            
        markerOri = PutMarker(markerOri, map,"Img/MapMarker_Marker_Outside_Chartreuse.png",16,16,latOri,lonOri);
        markerDst = PutMarker(markerDst, map,"Img/MapMarker_Marker_Outside_Pink.png",16,16,latDest,lonDest); 
        $('.leaflet-routing-alternatives-container').hide();
        $('.leaflet-top').hide();
        $('.leaflet-left').hide();
        WaitBmp(0);
     }   

    ///////////////////////////////////////////////////////////////////////////
    function WriteAddressOnDestiny()
    {
        var updateAddress = function(strbuf,country,city)
        {
            glbCity = city; 
            glbCountry = country; 
  
            if (strbuf === undefined || strbuf == "") 
            {
                // $('#divAddress').slideUp();  
                //$('#idAddress').text("");
            }    
            else
            {    
                $('#idOri').val(strbuf.substring(0, 400));     
            }    
        };        
        
        var updateAddressGoogle = function(strbuf,country,estado,city,street)
        {
            // if($('#divOriDst').is(':visible'))
            //   return;
            
            if (strbuf === undefined || strbuf == "") 
            {
                // $('#divAddress').slideUp();  
                // $('#idAddress').text("");
            }    
            else
            {    
                // $('#divAddress').show();  
                // $('#divAddress').slideUp(); 
                // $('#idAddress').text(strbuf.substring(0, 400));  
            
                $('#idOri').val(strbuf.substring(0, 400)); 
                glbEstado = retirarAcento(estado); 
                glbCity = retirarAcento(city); 
                glbCountry = retirarAcento(country); 
                glbStreet = retirarAcento(street); 
                glbAddress = strbuf.substring(0, 400); 
            }    
        };   
            
        // strbuf = GetOpenMapAddress(glbLatNow,glbLngNow,updateAddress);   
            
        strbuf = GetGoogleAddress(glbLatNow,glbLngNow,updateAddressGoogle);    
 
 
    }
    ///////////////////////////////////////////////////////////////////////////
    function myTimer()
    { 
        console.log("Timer - "+bTimerLoc);
        if(glbLatNow==0.0)
        {
            SetMarkerOnMyLocation();
            
        }
        if(bTimerLoc==1)
           return;   
        bTimerLoc=1;     
            
         // map.removeLayer(osmBuildingsLayer);
            
        if($('#idOri').val()=="" && glbLatNow!=0.0)
            WriteAddressOnDestiny();
 
        ////////////////////////////////////////////////////////////////////////
        if( (idOrilatGlb==0.0 || idOrilonGlb==0.0 || idDstlatGlb==0.0 || idDstlonGlb==0.0))
        {
           if( controlRoute != null)
              controlRoute.setWaypoints([]);     
        }
        ///////////////////////////////////////////////////////////////////////////
        if( ChangedRoute() && idOrilatGlb!=0.0 && idOrilonGlb!=0.0 && idDstlatGlb!=0.0 && idDstlonGlb!=0.0)
        {
           // SetupRoute(idOrilatGlb,idOrilonGlb,idDstlatGlb,idDstlonGlb);
        }
    
        // bufnada = "Rota = ("+idOrilatGlb+","+idOrilonGlb+"),("+idDstlatGlb+","+idDstlonGlb+")";
        // console.log(bufnada);            
        
            
        if($('#LabelFromHere').html().trim()== "From Here")
            PostGetProvidersInRegion($idUser,glbLat,glbLng,"CarTransport",PlotCars);   
            
        if($('#LabelFromHere').html().trim()== "Waiting Transport...Cancel?")
            PostGetProviderStatus($idUser,glbLat,glbLng,"CarTransport",funcdados);
            
        // ShowRandomCar();

        bTimerLoc=0;            
    }
    ////////////////////////////////////
    var markerVet = [];
    var oldNumItens = 0;
    function PlotCars(data)
    {
        // alert(data.vet[0].aLat);
        // alert(data.numItens);
        // alert(data.numItens);
        for(i = 0; i < oldNumItens; i++) 
            markerVet[i] = RemoveMarker(markerVet[i]);
            
        oldNumItens = data.numItens;    
        for(i = 0; i < data.numItens; i++) 
            markerVet[i] = PutCarMarker(markerVet[i], map,data.vet[i].aLat,data.vet[i].aLng,data.vet[i].Heading);
    }
    ////////////////////////////////////
    function ShowRandomCar()
    {
        glbLatTmp = glbLatNow + (Math.random() * 0.001); 
        glbLngTmp = glbLngNow + (Math.random() * 0.001);     
        angle = 360 * Math.random();
        if(markerGlb!=null)    
           map.removeLayer(markerGlb)    
        markerGlb = L.marker([glbLatTmp, glbLngTmp]);
          
        var bufimg = RotateImage('Img/iconcar0.png',angle,0.27);
        var carIcon = L.icon({
            iconUrl: bufimg
        });
        
        /*
        var carIcon = L.icon({
            iconUrl: '$img',
            iconSize:     [25, 25] // size of the icon
        });
        */
            
        markerGlb.setIcon(carIcon);    
        markerGlb.addTo(map);  
        map.addLayer(markerGlb);            
    }        
    ////////////////////////////////////
    function GetAjaxData()
    {
        // XXXXXXXXXX
        console.log("Data:"+$( "#datepicker" ).val());
        $.ajax
        ({
            url: "Util/GetGlobalPosition.php",
           data: { idUser: $idUser, idDevice: $('#selDevice').val (), FilterDate: $( "#datepicker" ).val(),RealTime: RealTime  },
           type: "GET",
           async: false,
           dataType : "json",
            success: function( json ) {
                console.log("Depurando JSON"+json.vet[0].idDevice);

                lLat = parseFloat(json.vet[0].Lat);
                lLong = parseFloat(json.vet[0].Long);
                idDevice = json.vet[0].idDevice;
                dTimeStamp = json.vet[0].TimeStamp;


                processJsonData(json);           
                if(RealTime==0)
                   dlgWait1.dialog("close");    
                // console.log(lLat);
                // console.log(lLong);
                // console.log(dTimeStamp);                       
                    },
            error: function( xhr, status, errorThrown ) {

                bJsonReady=true; 
                if(RealTime==0)
                   dlgWait1.dialog("close");   
                // console.log( "Erro json :" + errorThrown );
                // console.dir( xhr );
                // alert("Erro ajax");
            },
            complete: function( xhr, status ) 
            { 
                 bJsonReady=true; 
                if(RealTime==0)
                   dlgWait1.dialog("close");   
                 // alert("Ajax OK");
            }
        });           

    }
    ////////////////////////////////////
                
    </script>  
    
EOT;
    echo $dataSearch;
}
////////////////////////////////////////////////////////////////////////////////
