
$.ajax({
    async: false,
    url: "/ChildMonitor/Util/jquery.csv.min.js",
    dataType: "script"
});

$.ajax({
    type: "GET",
    url: "/ChildMonitor/Util/Multilanguage.csv",
    dataType: "text",
    success: function(data) { processDataCSV(data); }
});
////////////////////////////////////////////////////////////////////////////////
// https://github.com/evanplaice/jquery-csv/
var LangTable = null;
var DefLanguage = "EN"; // "EN"; "PT-BR"; 
////////////////////////////////////////////////////////////////////////////////
function processDataCSV(data)
{
    LangTable = $.csv.toArrays(data);   
}
////////////////////////////////////////////////////////////////////////////////
function setLang(lang)
{
    setCookie("language", lang, 1000); 
}
////////////////////////////////////////////////////////////////////////////////
function readLang()
{
    return getCookie("language"); 
}
////////////////////////////////////////////////////////////////////////////////
// data - message on standard language PT-BR
function i2l(data)
{
    // PT-BR - col 0      US - col 1
    if(readLang()==="")
    {
        setCookie("language", DefLanguage, 1000);
        return data;
    }  
    if(readLang()==="PT-BR")
    {
        return data;
    }
    language = readLang();
    if(language==="EN")
       colLang = 1;
    else
    {   // Future default language US
        colLang = 1;
    }
    lin = 1; col=0;
    //alert(LangTable.length);
    while( lin < LangTable.length )
    {
        if(LangTable[lin][0]===data || LangTable[lin][0].substring(0, 20)===data.substring(0, 20) )
        {
           return  LangTable[lin][colLang];
        }    
        lin++;
    }
    console.log("["+data+"] -> "+language+"!")
    return " ["+data+"] -> "+language+"!  " ;
}
////////////////////////////////////////////////////////////////////////////////
function LanguageChange()
{
   setLang($("#id_languagetot").val());
   window.location.reload(false); 
}
////////////////////////////////////////////////////////////////////////////////
function CreateSelectLanguage()
{
    // https://www.webcis.com.br/personalizando-campos-select-com-css-e-javascript.html
    // Verificar link acima
    // Muito com https://harvesthq.github.io/chosen/
    cssstyle = "<style>\
                .id_languagetot { \
                   background:url('/ChildMonitor/Img/ico-seta-a-ppearance.gif') no-repeat rgb(51, 51, 51);  /* Imagem de fundo (Seta) */ \
                   background-position:30px center;  /*Posição da imagem do background*/ \
                   width:110px; /* Tamanho do select, maior que o tamanho da div \"div-select\" */ \
                   height:25px; /* Altura do select, importante para que tenha a mesma altura em todo os navegadores */ \
                   font-size:11px; letter-spacing: 1px; text-decoration: none; \
                   border:1px solid #222;\
                   border-radius:4px;\
                   -webkit-border-radius:4px; \
                   background-color:rgb(51, 51, 51);\
                   -webkit-background-color:rgb(51, 51, 51);\
                   -moz-background-color:rgb(51, 51, 51); \
                   color:rgb(200, 200, 200);\
                   option {\
                      color: rgb(221, 221, 221); // color of all the other options\
                    }\
                }</style>";
    
    
    output = cssstyle+"<div id=\"id_languagediv\" style =\" width:50px;  \
                                    font-family: Montserrat,sans-serif; \
                                    background: rgb(51, 51, 51) none repeat scroll 0% 0%; \
                                    font-size: 12px; letter-spacing: 1px; text-decoration: none; \\n\
                                    border-radius: 4px;\
                                    color: rgb(221, 221, 221); font-weight: 700;\" >";
    output += "<select class=\"id_languagetot\" onchange='LanguageChange()' name=\"id_languagetot\" id=\"id_languagetot\"> \
    <option value=\"EN\"  selected=\"selected\" >English</option> \
    <option value=\"PT-BR\"    >Portuguese</option> \
    </select>"+"</div>";
    // $("#staff").selectmenu();    VetToTable( [["Lat", "Lon", "Raio (m)"],
    $(document).ready(function ()
    {
        $('#id_languagediv').css("left", $( window ).width()-115);
        // $('#id_languagediv').css("top", $( "#id_languagediv" ).parent().height()-45);
        $("#id_languagetot").val(readLang());
    });
        
    $(window).on('resize', function ()
    {
        $('#id_languagediv').css("left", $( window ).width()-115);
        // $('#id_languagediv').css("top", $( "#id_languagediv" ).parent().height()-45);
    });
    
    
    
    return output;
}
////////////////////////////////////////////////////////////////////////////////