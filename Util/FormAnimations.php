<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    $backrainbowhelix = "	html {
                    height: 100%;
                    font-size: 1em;
                    background-color: hsl(60,0%,20%);
                    background-image:
                            /* 1 */
                            radial-gradient(
                                    white 10%,
                                    hsla(0,0%,100%,0) 11%
                                    ),
                            /* 2 */
                            radial-gradient(
                                    white 10%,
                                    hsla(0,0%,100%,0) 11%
                                    ),
                            /* 3 */
                            radial-gradient(
                                    white 8%,
                                    hsla(0,0%,100%,0) 9%
                                    ),
                            /* 4 */
                            radial-gradient(
                                    white 6%,
                                    hsla(0,0%,100%,0) 7%
                                    ),
                            /* 5 */
                            radial-gradient(
                                    white 6%,
                                    hsla(0,0%,100%,0) 7%
                                    ),
                            /* 6 */
                            radial-gradient(
                                    white 8%,
                                    hsla(0,0%,100%,0) 9%
                                    ),  
                            /* 7 */
                            radial-gradient(
                                    white 10%,
                                    hsla(0,0%,100%,0) 11%
                                    ),
                            /* 8 */
                            radial-gradient(
                                    white 10%,
                                    hsla(0,0%,100%,0) 11%
                                    ),
                            /* bg */
                            radial-gradient(
                                    hsla(105,80%,70%,1), 
                                    hsla(0,0%,100%,0)
                                    ), 
                            linear-gradient(
                                    60deg, 
                                    hsl(224,80%,50%), 
                                    hsl(10,80%,50%)
                                    );
                    background-position: 
                            calc(50% - 5em) 50%,/* 1 */
                            calc(50% - 2.5em) calc(50% - 1.5em),/* 2 */
                            50% calc(50% - 2em),/* 3 */ 
                            calc(50% + 2.5em) calc(50% - 1.5em),/* 4 */

                            calc(50% - 5em) 50%,/* 5 */
                            calc(50% - 2.5em) calc(50% + 1.5em),/* 6 */
                            50% calc(50% + 2em),/* 7 */ 
                            calc(50% + 2.5em) calc(50% + 1.5em),/* 8 */

                            0 50%,    
                            0 50%;    
                            background-size: 
                                    10em 10em,
                                    10em 10em,
                                    10em 10em,
                                    10em 10em,

                                    10em 10em,
                                    10em 10em,
                                    10em 10em,
                                    10em 10em,

                                    100% 100%,      
                                    100% 100%;
                    background-repeat: repeat-x;
                    animation: .25s ring linear infinite;
                    }


            @keyframes ring {
                    0% {
                            background-position: 
                                    calc(50% - 5em) 50%,/* 1 */
                                    calc(50% - 2.5em) calc(50% - 1.5em),/* 2 */
                                    50% calc(50% - 2em),/* 3 */ 
                                    calc(50% + 2.5em) calc(50% - 1.5em),/* 4 */

                                    calc(50% - 5em) 50%,/* 5 */
                                    calc(50% - 2.5em) calc(50% + 1.5em),/* 6 */
                                    50% calc(50% + 2em),/* 7 */ 
                                    calc(50% + 2.5em) calc(50% + 1.5em),/* 8 */

                                    0 50%,      
                                    0 50%;  
                            }
                    100% { 
                            background-position: 
                                    calc(50% - 2.5em) calc(50% - 1.5em),/* 1 */
                                    50% calc(50% - 2em),/* 2 */
                                    calc(50% + 2.5em) calc(50% - 1.5em),/* 3 */ 
                                    calc(50% + 5em) 50%,/* 4 */

                                    calc(50% - 2.5em) calc(50% + 1.5em),/* 5 */
                                    50% calc(50% + 2em),/* 6 */
                                    calc(50% + 2.5em) calc(50% + 1.5em),/* 7 */ 
                                    calc(50% + 5em) 50%,/* 8 */

                                    0 50%,      
                                    0 50%;  
                            }
                    }



    ";
    
    $backnet = "  html {
    height: 100%;

    background-color: #222;

    background-image:
      linear-gradient(
        155deg,
        transparent 13%,
        hsla(200,40%,60%,1) 15%,
        transparent 0
        ),
      linear-gradient(
        90deg,
        transparent 49%,
        hsla(200,40%,60%,1) 49%,
        hsla(200,40%,60%,1) 50%,
        transparent 0
        ),
      linear-gradient(
        65deg,
        transparent 31%,
        hsla(200,40%,60%,1) 33%,
        transparent 0
        ),
      linear-gradient(
        45deg,
        transparent 47%,
        hsla(200,40%,60%,1) 49%,
        transparent 0
        ),
      linear-gradient(
        25deg,
        transparent 32%,
        hsla(200,40%,60%,1) 34%,
        transparent 0
        ),
      linear-gradient(
        -25deg,
        transparent 60%,
        hsla(200,40%,60%,1) 62%,
        transparent 0
        ),
      linear-gradient(
        -65deg,
        transparent 31%,
        hsla(200,40%,60%,1) 33%,
        transparent 0
        ),
      linear-gradient(
        180deg,
        transparent 53%,
        hsla(200,40%,60%,1) 55%,
        transparent 0
        ),
      linear-gradient(
        -25deg,
        transparent 32%,
        hsla(200,40%,60%,1) 34%,
        transparent 0
        ),
      linear-gradient(
        -65deg,
        transparent 65%,
        hsla(200,40%,60%,1) 67%,
        transparent 0
        ),
      linear-gradient(
        -45deg,
        transparent 47%,
        hsla(200,40%,60%,1) 49%,
        transparent 0
        );

    background-size: 
      80px 100px, 
      100px 100px, 
      75px 100px, 
      75px 100px, 
      80px 100px, 
      80px 100px, 
      80px 100px, 
      80px 100px, 
      80px 100px, 
      75px 100px, 
      80px 100px;

    background-position: 50% 50%;
    background-repeat: no-repeat;

    animation: sloppy 2s linear infinite alternate;
    }

  @keyframes sloppy {
    0% {
      background-size:  
        0 100px, 
        100px 0, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px;
      }
    10% {
      background-size: 
        0 100px, 
        100px 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px; 
      }
    20% {
      background-size: 
        0 100px, 
        100px 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        80px 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px; 
      }
    30% {
      background-size: 
        80px 100px, 
        100px 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        80px 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px; 
      }
    40% {
      background-size: 
        80px 100px, 
        100px 100px, 
        0 100px, 
        75px 100px, 
        0 100px, 
        80px 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px, 
        0 100px; 
      }
    50% {
      background-size: 
        80px 100px, 
        100px 100px, 
        0 100px, 
        75px 100px, 
        0 100px, 
        0 100px, 
        80px 100px, 
        80px 100px, 
        0 100px, 
        0 100px, 
        0 100px; 
      }
    60% {
      background-size: 
        80px 100px, 
        100px 100px, 
        75px 100px, 
        75px 100px, 
        0 100px, 
        0 100px, 
        80px 100px, 
        80px 100px, 
        0 100px, 
        0 100px, 
        0 100px; 
      }
    70% {
      background-size: 
        80px 100px, 
        100px 100px, 
        75px 100px, 
        75px 100px, 
        80px 100px, 
        0 100px, 
        80px 100px, 
        80px 100px, 
        0 100px, 
        0 100px, 
        80px 100px; 
      }
    80% {
      background-size: 
        80px 100px, 
        100px 100px, 
        75px 100px, 
        75px 100px, 
        80px 100px, 
        80px 100px, 
        80px 100px, 
        80px 100px, 
        0 100px, 
        75px 100px, 
        80px 100px; 
      }
    90% {
      background-size: 
        80px 100px, 
        100px 100px, 
        75px 100px, 
        75px 100px, 
        80px 100px, 
        80px 100px, 
        80px 100px, 
        80px 100px, 
        80px 100px, 
        75px 100px, 
        80px 100px; 
      }
    100% {
      background-size: 
        80px 100px, 
        100px 100px, 
        75px 100px, 
        75px 100px, 
        80px 100px, 
        80px 100px, 
        80px 100px, 
        80px 100px, 
        80px 100px, 
        75px 100px, 
        80px 100px;    
      }
    }";
    
    $backanimblue = " html {
          height: 100%;
          font-size: 1em;
          background-color: #222;
          background-image:
            radial-gradient(
              hsla(0,0%,100%,.6) 40%,
              hsla(0,0%,100%,0) 60%
              ),
            radial-gradient(
              hsla(0,0%,100%,.6) 40%,
              hsla(0,0%,100%,0) 60%
              ),
            radial-gradient(
              hsla(0,0%,100%,.6) 40%,
              hsla(0,0%,100%,0) 60%
              ),
            radial-gradient(
              white 50%,
              hsla(0,0%,100%,0) 53%
              ),
            radial-gradient(
              white 50%,
              hsla(0,0%,100%,0) 53%
              ),
            radial-gradient(
              white 50%,
              hsla(0,0%,100%,0) 53%
              ),
            radial-gradient(
              white 50%,
              hsla(0,0%,100%,0) 53%
              ),
            radial-gradient(
              hsla(0,0%,100%,.6) 40%,      
              hsla(0,0%,100%,0) 60%
              ),

            radial-gradient(
              hsl(200,80%,60%), 
              hsl(60,0%,20%)
              );
          background-position: 
            50% 50%,
            calc(50% + 3em) calc(50% - .5em),
            calc(50% + 4em) calc(50% - 1.5em),
            calc(50% + 3em) calc(50% - 2.5em),
            50% calc(50% - 3em),
            calc(50% - 3em) calc(50% - 2.5em),
            calc(50% - 4em) calc(50% - 1.5em),
            calc(50% - 3em) calc(50% - .5em),

            0 0;
          background-size: 
            3em 3em,
            2.5em 3em,
            1em 3em,
            2.5em 3em,
            3em 3em,
            2.5em 3em,
            1em 3em,
            2.5em 3em,

            100% 100%; 
          background-repeat: no-repeat;
          animation: .3s ring linear infinite reverse;
          }


        @keyframes ring {
          0% {
            background-position: 
              50% 50%,
              calc(50% + 3em) calc(50% - .5em),
              calc(50% + 4em) calc(50% - 1.5em),
              calc(50% + 3em) calc(50% - 2.5em),
              50% calc(50% - 3em),
              calc(50% - 3em) calc(50% - 2.5em),
              calc(50% - 4em) calc(50% - 1.5em),
              calc(50% - 3em) calc(50% - .5em),

              0 0;
            background-size: 
              3em 3em,
              2.5em 3em,
              1em 3em,
              2.5em 3em,
              3em 3em,
              2.5em 3em,
              1em 3em,
              2.5em 3em,

              100% 100%;    
            }
          100% {
            background-position: 
              calc(50% - 3em) calc(50% - .5em),
              50% 50%,
              calc(50% + 3em) calc(50% - .5em),
              calc(50% + 4em) calc(50% - 1.5em),
              calc(50% + 3em) calc(50% - 2.5em),
              50% calc(50% - 3em),
              calc(50% - 3em) calc(50% - 2.5em),
              calc(50% - 4em) calc(50% - 1.5em),

              0 0;
            background-size:
              2.5em 3em,
              3em 3em,
              2.5em 3em,
              1em 3em,
              2.5em 3em,
              3em 3em,
              2.5em 3em,
              1em 3em,

              100% 100%;    
            }
          }";
    $backcarbonfiber = "background:
                        radial-gradient(black 15%, transparent 16%) 0 0,
                        radial-gradient(black 15%, transparent 16%) 8px 8px,
                        radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 0 1px,
                        radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 8px 9px;
                        background-color:#282828;
                        background-size:16px 16px;";
    $backblack = "background:
                linear-gradient(27deg, #151515 5px, transparent 5px) 0 5px,
                linear-gradient(207deg, #151515 5px, transparent 5px) 10px 0px,
                linear-gradient(27deg, #222 5px, transparent 5px) 0px 10px,
                linear-gradient(207deg, #222 5px, transparent 5px) 10px 5px,
                linear-gradient(90deg, #1b1b1b 10px, transparent 10px),
                linear-gradient(#1d1d1d 25%, #1a1a1a 25%, #1a1a1a 50%, transparent 50%, transparent 75%, #242424 75%, #242424);
                background-color: #131313;
                background-size: 20px 20px;";
    
    $backbluedeg_old = "background: rgba(94,146,189,1);
                    background: -moz-linear-gradient(left, rgba(94,146,189,1) 0%, rgba(19,59,92,1) 50%, rgba(7,58,100,1) 100%);
                    background: -webkit-gradient(left top, right top, color-stop(0%, rgba(94,146,189,1)), color-stop(50%, rgba(19,59,92,1)), color-stop(100%, rgba(7,58,100,1)));
                    background: -webkit-linear-gradient(left, rgba(94,146,189,1) 0%, rgba(19,59,92,1) 50%, rgba(7,58,100,1) 100%);
                    background: -o-linear-gradient(left, rgba(94,146,189,1) 0%, rgba(19,59,92,1) 50%, rgba(7,58,100,1) 100%);
                    background: -ms-linear-gradient(left, rgba(94,146,189,1) 0%, rgba(19,59,92,1) 50%, rgba(7,58,100,1) 100%);
                    background: linear-gradient(to right, rgba(94,146,189,1) 0%, rgba(19,59,92,1) 50%, rgba(7,58,100,1) 100%);
                    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#5e92bd', endColorstr='#073a64', GradientType=1 );";

   $backbluedeg = "background: rgba(20,63,92,1);background: -moz-linear-gradient(left, rgba(20,63,92,1) 0%, rgba(23,54,87,1) 53%, rgba(43,91,143,1) 100%);";
   
   $backchocweave =  "background: #948E99; /* fallback for old browsers */
                      background: -webkit-linear-gradient(to left, #948E99 , #2E1437); /* Chrome 10-25, Safari 5.1-6 */
                      background: linear-gradient(to left, #948E99 , #2E1437); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */";
   
   $backParkingFit = " background-image: url(/ChildMonitor/Img/BackBlurPFitMobile.jpg); background-size:cover; background-repeat:no-repeat; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; ";
   
   $backParkingFitNew = " background-image: url(/ChildMonitor/Img/BackBlurPFit.jpg); background-size: cover; background-repeat:no-repeat; ";