/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/////////////////////////////////////////////////////////////////////////
function GenerateInputCardData()
{
    // alert(VMasker.toPattern(1099911111, "(99) 9999-9999"));
    vardatacard = "<table align=\"left\" > \
                    <tr><td> <input autocomplete=\"off\" title=\"Cartão\" class=\"formdlg\" placeholder=\"Cartão\" style=\"width:150px;\" id=\"id_card\"></td><td><img style=\"p\" height =\"21\" id=\"idflag\" ></td> </tr> \
                    </table> <br>\
                    <table align=\"left\" > \
                    <tr><td> <input autocomplete=\"off\" title=\"CVC\" class=\"formdlg\" placeholder=\"CVC\" maxlength=\"3\" style=\"width:110px;\" id=\"id_cvc\"> </td><td> \n\
                    <input autocomplete=\"off\" title=\"Data expiração do cartão MM/AAAA\" class=\"formdlg\"  placeholder=\"DatExp MM/AAAA\" style=\"width:110px;\" id=\"id_dtexp\"> </td> </tr> \
                    </table> <br><br>  <input title=\"Nome Cartão\" class=\"formdlg\" placeholder=\"Nome Cartão\" style=\"width:100%\" id=\"id_nome\"> ";
    return vardatacard;
}
/////////////////////////////////////////////////////////////////////////
function cc_flag(data)
{
    data = data.toLowerCase();
    $('#idflag').attr('alt', '');
    $('#idflag').attr('src', '');
    if (data === "-")
        return $('#idflag').attr('src', '');
    
    $('#idflag').attr('alt', data);
    if (data === "amex")
        return $('#idflag').attr('src', 'Img/ccamerican.png');
    if (data === "visa")
        return $('#idflag').attr('src', 'Img/ccvisa.png');
    if (data === "mastercard")
        return $('#idflag').attr('src', 'Img/ccmaster.png');
    if (data === "maestro")
        return $('#idflag').attr('src', 'Img/ccmaestro.png');
    if (data === "diners")
        return $('#idflag').attr('src', 'Img/ccdiners.png');
    if (data === "diners_club_carte_blanche")
        return $('#idflag').attr('src', 'Img/ccdiners.png');
    $('#idflag').attr('alt', data);
    $('#idflag').attr('src', '');
}
/////////////////////////////////////////////////////////////////////////
function InputCardDataFlag()
{
    PutInputMask("#id_dtexp", "99/9999");
    PutInputMask("#id_cvc", "999");
    PutInputMask("#id_card", "9999.9999.9999.9999");
     
    // moip.creditCard.cardType($('#id_card').val()); 
    $( '#id_card').keyup(function() 
    {
       if($('#id_card').val()==="0000000000000001" || $('#id_card').val()==="0000.0000.0000.0001" ||
          $('#id_card').val()==="0000000000000002" || $('#id_card').val()==="0000.0000.0000.0002" )
       {
           $('#idflag').attr('alt','Visa');
           return;
       } 
       cc_flag("");
       card = moip.creditCard.cardType($('#id_card').val());
       $('#idflag').attr('alt',card.brand );
       cc_flag(card.brand);
       

    });


}
/////////////////////////////////////////////////////////////////////////
function DialogConfirmParking()
{
    varselect = "<select class=\"formdlg backBlack\" style=\" width:55px;\" name=\"periodo\" id=\"periodo\"> \
              <option>1h</option> \
              <option>2h</option> \
              <option selected=\"selected\">3h</option> \\n\
              <option >4h</option> \\n\
              <option >5h</option> \\n\
              <option >6h</option> \\n\
              <option >7h</option> \\n\
              <option >8h</option> \\n\
              <option >9h</option> \
            </select><br>";  
    
    //     $query = "select idCar,Lat,Longc,Period,DateTimePark,haversine(Lat,Longc,$lat,$lon) as harvin from `ParkingData` where idCar='$datadec->placa' order by harvin asc limit 30";
    glb_context = "ParkingFitUserRequest";
    sqlProvList = "select email,NomeProvider,haversine(latOperation,lonOperation,"+idOrilatGlb+","+idOrilonGlb+") as harvin from ParkingProvider order by harvin asc limit 15;";
    varselectprov = ""+CreateSelectFromSql("id_providersel",sqlProvList ,"style=\" width:190px;\"");
    
    vardatacard = GenerateInputCardData();

    datHowToPay= "";
    if(isLogged('ParkingFitUser')==="") // Is not logged 
    {
       datHowToPay = VetToTable([[CreateCheckBox("id_extradat","Details","$(\"#tabsPark\").toggle();"), $('#idOri').val()],[]]) + 
                   "<div style=\"border-style:none; background-color:transparent;\" id=\"tabsPark\" > "+vardatacard+"</div>";
    }
    else
    {
        // Pagamento com token de cartão já cadastrado
//        sql = "select `CardNumber` from `PaymentCardsStored` where `DefaultSel`=1 and emailUser='"+userEmail+"' and Application='"+Application+"' and AppContext='"+AppContext+"';";
//        sqlDate = "select `ExpirationDate` from `PaymentCardsStored` where `DefaultSel`=1 and emailUser='"+userEmail+"' and Application='"+Application+"' and AppContext='"+AppContext+"';";
//        selectedCard = "<div id=\"id_selcard\" style=\" background-color:rgb(25, 0, 0);\"  >Pagar o valor <label id='id_valpay'>"+val2pay+"</label> com o cartão:<br><b><label id='id_cardpay'>"+GetSqlValue(sql)+"</label>   -   <label id='id_expdt'>"+GetSqlValue(sqlDate)+"</label></b>?<br><br> <button id=\"id_pag\">Ok</button> </div><br>";
        datHowToPay = $('#idOri').val();
    }
    
    dlgdata = "<div id=\"dialog-confirm\" title=\""+i2l("Confirma Estacionamento?")+"\"> "+
                   VetToTable([["<input autocomplete=\"off\" title=\""+i2l("Placa")+"\" class=\"formdlg\" style=\"text-transform:uppercase; width:80px;\" placeholder=\""+i2l("Placa")+"\" id=\"id_placa\">", 
                                varselect,varselectprov],[]])+datHowToPay+
             " </div>";
    
//    SetCheckBox("#id_extradat",true);
//    GetCheckBoxState(checkbox)
//    CreateCheckBox(id,label,onclick)
    


    var execSim = function (id)
    {
        UserId = isLogged('ParkingFitUser');
        if(UserId!=="")
        {
            PayParkingWithToken();
            return;
        }
        
        var controls = {};
        controls[0] = {id: "#id_placa", valtype: ["notnull", "size"], name: "Placa automóvel", size: 8};
        controls[1] = {id: "#id_card", valtype: ["notnull"], name: "Número cartão"};
        controls[2] = {id: "#id_cvc", valtype: ["notnull", "int"], name: "Codigo cartão"};
        controls[3] = {id: "#id_dtexp", valtype: ["notnull"], name: "Data expiração cartão"};
        controls[4] = {id: "#id_nome", valtype: ["notnull"], name: "Nome no cartão"};

        if (!ValidateControls(controls))
            return;

//        setCookie("id_nome", $('#id_nome').val(), 1000);
         setCookie("id_placa", $('#id_placa').val().toUpperCase(), 1000);
//        setCookie("id_dtexp", $('#id_dtexp').val(), 1000);
//        setCookie("id_cvc", $('#id_cvc').val(), 1000);
//        setCookie("id_card", $('#id_card').val(), 1000);
        setCookie("glbEstado", glbEstado, 1000);
        setCookie("glbCity", glbCity, 1000);
        setCookie("glbCountry", glbCountry, 1000);
        setCookie("glbSelectedProvider", $('#id_providersel').val(), 1000);

        if(GetLastUserStatus())
        {
            ShowInfoToast("Automovel com tempo restante, pagamento não realizado", 1);
            $(id).dialog("destroy");
        }


        dtHoraPedido = new Date();

        
        var datPark = {};

        datPark.userid = UserId;
        datPark.namecard = $('#id_nome').val();
        // alert($('#id_nome').val());
        datPark.periodo = $('#periodo').val();
        datPark.cardnumber = $('#id_card').val();
        datPark.cvc = $('#id_cvc').val();
        datPark.dtexp = $('#id_dtexp').val();
        
        datPark.brand = $('#idflag').attr('alt');
     
        datPark.placa = $('#id_placa').val().toUpperCase();
        datPark.lat = idOrilatGlb;
        datPark.lon = idOrilonGlb;
        datPark.heading = glbHeading;
        datPark.accuracy = glbAccuracy;
        datPark.endereco = $('#idOri').val();
        datPark.estado = glbEstado;
        datPark.cidade = glbCity;
        datPark.pais = glbCountry;
        datPark.street = glbStreet;
        datPark.dthorapedido = dtHoraPedido;
        datPark.id_providersel = $('#id_providersel').val();
        datPark.PaidByToken="";
        datPark.PaymentMerchantUserId = GetCieloMerchantId("ParkingFitProvider",datPark.id_providersel);
        datPark.PaymentMerchantUserKey = GetCieloMerchantKey("ParkingFitProvider",datPark.id_providersel);
        
        if (PostGeoServiceParkingPay(datPark))
        {
            ShowDialogTimerPark(datPark.placa, datPark.periodo, idOrilatGlb, idOrilonGlb);
            $(id).dialog("destroy");
        } else
        {
            // $( this ).dialog("destroy");
        }
    }
    ////////////////////////////////////////////////////////////////////////////////
    function PayUserDebtAndSendParking(provider,date,amount)
    {
        paymentData = {};
        paymentData.val2pay = amount;
        paymentData.date = date;    
        paymentData.orderid = GetPaymentOrderId('ParkingFitUser');
        paymentData.namecustomer=isLogged('ParkingFitUser');
        paymentData.SoftDescriptor = DescOnCardPayment();
        paymentData.PayForWho="Provider";
        paymentData.MerchantToPay = provider;
    
        paymentData.CallBackFunc = function(data)
        {
            if(data.response === "ErrorPayingValueWithCardToken")
            {
                ShowErrorToast("Erro pagamento");
            }   
            else
            {  
                ShowInfoToast("Pagamento autorizado");
                $( "#dlgprovpagmethod" ).dialog("destroy");
                RegisterParkingBuyed();
            }
        };
        DialogManagePaymentMethods('ParkingFitUser',paymentData);
    }
    /////////////////////////////////////////////
    function RegisterParkingBuyed()
    {
        var datPark = {};
        datPark.periodo = $('#periodo').val();
        dtHoraPedido = new Date();     
        UserId = isLogged('ParkingFitUser');

        datPark.userid = UserId;
        datPark.periodo = $('#periodo').val();
        datPark.placa = $('#id_placa').val().toUpperCase();
        datPark.lat = idOrilatGlb;
        datPark.lon = idOrilonGlb;
        datPark.heading = glbHeading;
        datPark.accuracy = glbAccuracy;
        datPark.endereco = $('#idOri').val();
        datPark.estado = glbEstado;
        datPark.cidade = glbCity;
        datPark.pais = glbCountry;
        datPark.street = glbStreet;
        datPark.dthorapedido = dtHoraPedido;
        datPark.id_providersel = $('#id_providersel').val();
        datPark.PaidByToken="Ok";
        
        if (PostGeoServiceParkingPay(datPark))
        {
            ShowDialogTimerPark(datPark.placa, datPark.periodo, idOrilatGlb, idOrilonGlb);
            $(id).dialog("destroy");
        } else
        {
            // $( this ).dialog("destroy");
        }    
    }
    /////////////////////////////////////////////
    function PayParkingWithToken()
    {
        UserId = isLogged('ParkingFitUser');
        
        var controls = {};
        controls[0] = {id: "#id_placa", valtype: ["notnull", "size"], name: "Placa automóvel", size: 8};

        if (!ValidateControls(controls))
            return;

        setCookie("id_placa", $('#id_placa').val().toUpperCase(), 1000);
        setCookie("glbEstado", glbEstado, 1000);
        setCookie("glbCity", glbCity, 1000);
        setCookie("glbCountry", glbCountry, 1000);
        setCookie("glbSelectedProvider", $('#id_providersel').val(), 1000);

        if(GetLastUserStatus())
        {
            ShowInfoToast("Automovel com tempo restante, pagamento não realizado", 1);
            $("#dialog-confirm").dialog("destroy");
            return;
        }
        
        provider = $('#id_providersel').val();
        valuePerHour = GetSqlValue("select HourValue from ParkingProvider where  email='"+provider+"';");
        
        period = $('#periodo').val();
        period = period.replace("h","");
        amount = period*valuePerHour;
        PayUserDebtAndSendParking(provider,GetDateTime(),amount);
    }
    /////////////////////////////////////////////
    function SetupDialog()
    {
        $('#id_placa').val(getCookie("id_placa"));
        $('#id_dtexp').val(getCookie("id_dtexp"));
        $('#id_cvc').val(getCookie("id_cvc"));
        $('#id_card').val(getCookie("id_card"));
        $('#id_nome').val(getCookie("id_nome"));
        
        $( "#tabsPark" ).tabs();
        $("#id_botlogin").button();
        $("#id_botlogin").click(function() 
        {   
            DialogLogin("ParkingFitUser"); 
            
            // getCookie('userEmailParkFit')
        });

        $('.ui-dialog').css('overflow', 'visible');  // Solves menu hidden on overflow of selectmenu
        $("#periodo").selectmenu();
        
        PutInputMask("#id_placa", "AAA-9999");

        JqueryDlgAcceptEnter("#dialog-confirm", execSim);
        $("#id_providersel").selectmenu();
        if(getCookie("id_card")!=="")
        {
           SetCheckBox("#id_extradat",false);
           $("#tabsPark").hide();
        }
        else
        {
           SetCheckBox("#id_extradat",true);
           $("#tabsPark").show();    
        }
        InputCardDataFlag();
    }

    dwidth = "400px";
    // i
    //if(1)
    if(isTestMobile())
    {
        dwidth = "90%";
        dposition = {my: 'top', at: 'top+100'};
    } else
        dposition = {my: "center", at: "center", of: window};


    $(dlgdata).dialog({
        position: dposition,
        resizable: true,
        height: "auto",
        width: dwidth,
        modal: true,
        open: function () {
            SetupDialog()
        },
        buttons:
                {
                    "Sim": function () {
                        execSim("#dialog-confirm");
                    },
                    "Não": function () {
                        $("#dialog-confirm").dialog("destroy");
                    }
                }
    });
}
////////////////////////////////////////////////////////////////////////////
var GetLastUserStatusRet=false;
function GetLastUserStatus()
{     
    var datPark = {};
    datPark.placa = getCookie("id_placa");
    datPark.estado = getCookie("glbEstado");
    datPark.cidade = getCookie("glbCity");
    datPark.pais = getCookie("glbCountry");


    GetLastUserStatusRet=false;
    fdatProcess = function(data) 
    {   
        // alert(data.response);
        console.log("GetLastUserStatus - "+data.response);
        if(data.response=="RemainingPeriod")
        {
            GetLastUserStatusRet=true;
            glbLat = data.aLat;  
            glbLng = data.aLng; 
            // SetMarkerOnNewLocation(data.aLat,data.aLng);
            // alert(data.RemainingTimeHs+"h");
            ShowDialogTimerPark(data.idCar,data.RemainingTimeHs+"h",glbLat, glbLng);
        }
    };

    PostGetPlateLastStatus(datPark,fdatProcess);   
    return GetLastUserStatusRet;
}
////////////////////////////////////////////////////////////////////////////
function ShowDialogTimerPark(plate, period, lat, lon)
{
    $("#idDlgTimerPark").dialog("destroy");
    var dlgHandle = null;
    period = period.replace('h', '');
    var loctimer = 0;
    console.log("ShowDialogTimerPark("+plate+", "+period+", "+lat+", "+lon+")");
    dlgdata = "<div id=\"idDlgTimerPark\" title=\"Timer: " + plate + " \"> ";

    dtsecline = "<table align=\"center\" ><tr><td> </td></tr><tr><td><p align=\"center\" id=\"iddistance\"  \" > </p></td></tr></table>";

    dlgdata += "<p align=\"center\" id=\"idtimerSDTP\"  style=\"font-size:600%;\" > </p> <br> " + dtsecline + " </div>";



    function SetupDialog()
    {
        loctimer = setInterval(function () {
            timerDlg()
        }, 1000); // 3 segundos 
        timerDlg();

        // clearInterval(loctimer);   
    }
    var destData = new Date();

    destData.setTime(destData.getTime() + (period * 60 * 60 * 1000));
    // destData.setTime(destData.getTime()+(10000));  // Teste de 10 segundos
    // alert(destData.getHours()+":"+destData.getMinutes());
    function timerDlg()
    {
        // Usar para calcular a distancia do carro.... harvesine...
        // glbLat  
        // glbLng    
        // lat,lon 
        getLocation();
        $("#iddistance").html(haversineDistance([glbLat, glbLng], [lat, lon]) + " metros");

        // alert(period);
        // var time = new Date('2014-03-14T23:54:00');

        horaatual = new Date();
        stRemainTime = timeDiff(destData, horaatual);
        if (stRemainTime == "NegativeTime")
        {
            clearInterval(loctimer);

            ShowInfoToast("Tempo terminado", 0);
            $("#idDlgTimerPark").dialog("destroy");
        }
        $("#idtimerSDTP").html(stRemainTime);
    }


    dwidth = "auto";
    if (isTestMobile())
    {

        // dwidth = $(window).width()+"px";
        dwidth = "100%";
        dposition = {my: 'top', at: 'top+48'};
    } else
        dposition = {my: "center", at: "center", of: window};

    dlgHandle = $(dlgdata).dialog({
        position: dposition,
        resizable: false,
        height: "auto",
        width: dwidth,
        modal: true,
        open: function () {
            SetupDialog();
        },
        buttons: {}
    });

}
////////////////////////////////////////////////////////////////////////////
function DialogShowParkingHistory()
{
   id="dlgShowParkingHistory";
   LastActiveDlg="#"+id;
   
   user = isLogged('ParkingFitUser');
   
   // "select  `idCar`, `Address`,`Period` ,`DateTimePark`,`valuePaid` from `ParkingData` where `idUser`='"+user+"' order by `DateTimePark` desc; ";
   dataTableParking = SqlToTable("select  `idCar`, `Address`,`Period` ,`DateTimePark`,`valuePaid` from `ParkingData` where `idUser`='"+user+"' order by `DateTimePark` desc; ",null)
   
   sqldate = "select distinct  DATE_FORMAT(DateTimePark,'%Y') as Data,DATE_FORMAT(DateTimePark,'%Y') from ParkingData where   idProvider='"+user+"' order by Data desc;";
   
   datParkHist =  VetToTable([[i2l("Selecione ano:"),CreateSelectFromSql("id_year", sqldate," class=\"formdlg backBlack\"  style=\"width:60px; \" ")],[]])+
                      "<div id=\"divProfit\">"+ dataTableParking + "</div>";
               

   title = i2l("Estacionamentos");
   dlgdata  = "<div id=\""+id+"\" title=\""+title+"\">";
   dlgdata +=  datParkHist;    
   dlgdata += " </div>"; // end dlg
   

   
   var execSim = function()
   {
       

   }
   
   function SetupDialog()
   { 
        SetUI_Black(); 

        $('#id_year').on('change', function() 
        {
            dataTableParking = i2l("Lista de estacionamentos");        
            $("#divProfit").html(dataTableParking);
        })  
        
        JqueryDlgAcceptEnter("#dlgShowParkingHistory",execSim);     
       
   }
    
   dwidth = "550px";
   if(isTestMobile())
   {
       
       // dwidth = $(window).width()+"px";
       dwidth = "100%";
       dposition = { my: 'top', at: 'top+48' }; 
   }
   else     
       dposition = { my: "center", at: "center", of: window };    

   $( dlgdata ).dialog({
      position: dposition,   
      resizable: true,
      height: "auto",
      width: dwidth,
      modal: false,
      open: function() { SetupDialog() },  
      buttons: 
      {
        "Fecha": function() 
        {
            $("#dlgShowParkingHistory").dialog("destroy");
        }
      }
  });
    
}
////////////////////////////////////////////////////////////////////////////
funcUserParkingHistory = function ()
{ 
    user = isLogged('ParkingFitUser');
    if(user==="")
    {
        DialogLogin('ParkingFitUser');
        return;
    } 
    DialogShowParkingHistory();
};  
////////////////////////////////////////////////////////////////////////////
funcUserParkingPayment = function () 
{
    user = isLogged('ParkingFitUser');
    if(user==="")
    {
        DialogLogin('ParkingFitUser');
        return;
    }   
    DialogManagePaymentMethods("ParkingFitUser",null);
};  
////////////////////////////////////////////////////////////////////////////
funcUserParkingLogout = function () 
{
    ShowInfoToast("Logout: "+isLogged('ParkingFitUser'), 1);
    deleteCookie('userEmailParkFit'); 
    
    MontaMenuUserParking();
    
}; 
////////////////////////////////////////////////////////////////////////////
funcUserParkingLogout = function () 
{
    ShowInfoToast("Logout: "+isLogged('ParkingFitUser'), 1);
    deleteCookie('userEmailParkFit'); 
    
    MontaMenuUserParking();
    
}; 
////////////////////////////////////////////////////////////////////////////
funcStaffParkingLogout = function () 
{
    ShowInfoToast("Staff logout: "+isLogged('ParkingFitStaff'), 1);
    deleteCookie('userEmailStaffParkFit'); 
    window.location.pathname = "/ChildMonitor/index.php";
        
}; 
////////////////////////////////////////////////////////////////////////////
funcStaffDat = function () 
{
   DialogProviderStaff('ParkingFitStaff');

    
}; 
////////////////////////////////////////////////////////////////////////////
function MontaMenuUserParking()
{
    vetCallBack = [];        
    vetCallBack[0] = funcUserParkingHistory;        
    vetCallBack[1] = funcUserParkingPayment;  
    if(isLogged('ParkingFitUser')==="")
    {
       vetMenu = [[i2l("Estacionamentos"),i2l("Pagamentos")],[]];
    }
    else
    {
       vetMenu = [[i2l("Estacionamentos"),i2l("Pagamentos"),"Logout"],[]];
       vetCallBack[2] = funcUserParkingLogout; 
    }
    
    output = CreateFloatingMenuMobile(isLogged('ParkingFitUser'),vetMenu,vetCallBack);
    $("#idMenuMobileParkUser").html(output);
    
    $("#id_FloatMenuMobileMenu").hide();
    $("#id_FloatMenuMobileMenu").click(function () {
        $("#id_FloatMenuMobileMenu").toggle("slow");
        clearTimeout(handleTimeOut);

    });
    $("#id_FloatMenuMobile").click(function ()
    {
        $("#id_FloatMenuMobileMenu").toggle("slow");
        handleTimeOut = setTimeout(function () {
            $("#id_FloatMenuMobileMenu").hide("slow");
        }, 4000);
    }); 
}
////////////////////////////////////////////////////////////////////////////