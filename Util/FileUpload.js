/*
Para upload de grandes blobs

Changes on php.ini of server

    post_max_size=100M
    memory_limit=128M

ou

    ini_set('post_max_size', '100M');
    ini_set('upload_max_filesize', '100M');
    ini_set('memory_limit', '128M');
    ini_set('session.gc_maxlifetime','50800');
    ini_set('max_input_time','50800');
    ini_set('max_execution_time','50800');

No MySql my.ini

    max_allowed_packet = 200M

    [mysqldump]
    quick
    max_allowed_packet = 200M

    innodb_log_file_size = 700M
    innodb_log_buffer_size = 750M

No Apache httpd.conf (100M)
   LimitRequestBody 102400000 

*/
////////////////////////////////////////////////////////////////////////////////
// Function to download a javascript var to a file
// Download.save("data to be on a file","FileName.txt");
var Download = 
{
    click : function(node) {
        var ev = document.createEvent("MouseEvents");
        ev.initMouseEvent("click", true, false, self, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        return node.dispatchEvent(ev);
    },
    encode : function(data) {
            return 'data:application/octet-stream;base64,' + btoa( data );
    },
    link : function(data, name){
        var a = document.createElement('a');
        a.download = name || self.location.pathname.slice(self.location.pathname.lastIndexOf('/')+1);
        a.href = data || self.location.href;
        return a;
    }
};
Download.save = function(data, name)
{
    this.click(this.link(this.encode( data ),name));
};
////////////////////////////////////////////////////////////////////////////////
function SetupInputFilePdf(id, dataFile)
{
    dataFile.readSingleFile = function (e)
    {

        var file = e.target.files[0];
        if (!file)
        {
            return;
        }

        // alert(e.target.files[0].name);


        var reader = new FileReader();

        reader.onprogress = function (data) {
            if (data.lengthComputable) {
                var progress = parseInt(((data.loaded / data.total) * 100), 10);
                // console.log(progress);
                $("#" + id + "Progress").html(progress+"%");
            }
        }


        reader.onloadend = function (e) {

            var contents = e.target.result;
            dataFile.contents = contents;
            $("#" + id + "Down").show();
            $("#" + id + "Progress").html("");
            // console.log(JSON.stringify(dataFile1));
        };


        // reader.readAsDataURL(file);
        dataFile.name = e.target.files[0].name;
        $("#" + id + "FileName").html(e.target.files[0].name);
        // reader.readAsBinaryString(file);
        reader.readAsDataURL(file);

    }


    $("#" + id + "Progress").html("");
    $("#" + id + "Down").hide();
    $("#" + id + "Del").on("click", function ()
    {
        // document.getElementById("idImg1Show").src = "/ChildMonitor/Img/pdficon.png";
        dataFile.contents = "";
        dataFile.name = "";
        $("#" + id + "Down").hide();
        $("#" + id + "FileName").html("");
    });
    $("#" + id + "Down").on("click", function ()
    {
        $("#" + id + "Progress").html("Downloading...");
        $("#" + id + "Progress").toggle().toggle(); // just for refesh
        SaveFileDown();
        // SaveFileBinary(dataFile1); 
    });
    
    function SaveFileDown()
    {
        binnaryDtFile = _base64ToArrayBuffer(dataFile.contents);
        Download.save(binnaryDtFile, dataFile.name);
        $("#" + id + "Progress").html("");    
    }
    
    document.getElementById(id).addEventListener('change', dataFile.readSingleFile, false);
    if (dataFile.name !== "")
    {
        $("#" + id + "FileName").html(dataFile.name);
        $("#" + id + "Down").show();
    }
}
////////////////////////////////////////////////////////////////////////////////
// Converts a base64 text to binary data (Example: for use in a PDF database stored)
// TODO: Problemas com UTF8 ?
function _base64ToArrayBuffer(base64) 
{
    databuffer = base64.split("data:application/pdf;base64,");
    var binary_string =  window.atob(databuffer[1]);
    return binary_string;
//    var len = binary_string.length;
//    alert(databuffer[1]);
//    alert(binary_string);
//    var bytes = new Uint8Array( len );
//    for (var i = 0; i < len; i++)        {
//        bytes[i] = binary_string.charCodeAt(i);
//    }
//    return bytes.buffer;
}
////////////////////////////////////////////////////////////////////////////////
function InputFilePdf(id,dataFile)
{  
    var bufInputFile = " <input type='file' name='"+id+"' id='"+id+"' accept=\"application/pdf\" style='display: none;'  > \
    <img id='"+id+"Show' onclick=\"document.getElementById('"+id+"').click();\" src='/ChildMonitor/Img/pdficon.png' alt='Load photo' style='width:20px;'>  \
    <img id='"+id+"Down' src='Img/downloadWhite.png' alt='Download' style='width:13px;'> <img id='"+id+"Del' src='Img/deleteWhite.png' alt='Delete' style='width:10px;'>  \
    <span style='width:150px; display:inline-block;' id=\""+id+"FileName\"></span>\
    <span style='width:30px; display:inline-block;' id=\""+id+"Progress\"></span> ";
  
    return bufInputFile;
}
////////////////////////////////////////////////////////////////////////////////