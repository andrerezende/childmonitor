/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
////////////////////////////////////////////////////////////////////////////////
function DlgVerifyCardId()
{

    StopMapTimer();  
    varselect = "";
    dlgdata ="<div id=\"dlgcheckplate\" style=\" z-index:20; \" title=\""+i2l("Verifica veículo")+"\"> ";

    dlgdata += "<table align=\"left\" > <tr><td><input class=\"formdlg\" style=\"text-transform:uppercase; width:100px;\" placeholder=\"Placa\" id=\"id_placa\"></td><td> "+varselect+"</td></tr> </table> </div>";


   var execSim = function(id)
   {
         var controls = {};
         controls[0] = {id:"#id_placa", valtype:["notnull","size"], name:"Placa automóvel" , size:8};
         toastr.clear();
         if(!ValidateControls(controls))
           return;

         var datPark = { };

         datPark.placa = $('#id_placa').val().toUpperCase();
         datPark.lat = glbLatNow; 
         datPark.lon = glbLngNow;
         datPark.userid = UserId;
         datPark.estado = glbEstado;
         datPark.cidade = glbCity;
         datPark.pais = glbCountry;
         datPark.street = glbStreet;
         datPark.endereco = glbAddress;
         datPark.dtHora = GetDateTime();
         datPark.emailstaff = isLogged('ParkingFitStaff');
         // alert("Provider deste fiscal - "+GetStaffProvider(datPark.emailstaff));
         datPark.emailprovider = GetStaffProvider(datPark.emailstaff);

         // alert(Date());
         // alert(datPark.dtHora);
         if(PostGeoServiceCheckPlateParking(datPark))
         {

           //  $( id ).dialog("destroy");
         }
         else
         {
         // $( this ).dialog("destroy");
         }
   }

   function SetupDialog()
   {

        PutInputMask("#id_placa","AAA-9999");      
        JqueryDlgAcceptEnter("#dlgcheckplate",execSim);
   }

   dwidth = "auto";
   if(isTestMobile())
   {
       // dwidth = "100%";
       dposition = { my: 'top', at: 'top+48' };
   }
   else     
       dposition = { my: "center", at: "center", of: window };    



   $( dlgdata ).dialog({
      position: dposition,   
      resizable: false,
      height: "auto",
      width: "auto",
      modal: true,
      open: function() { SetupDialog() },  
      buttons: 
      {
        "Verifica": function() {  
            execSim("#dlgcheckplate");

        },
        "Fecha": function() {
            CreateMapTimer();
            $("#dlgcheckplate").dialog("destroy");
        }
      }
  });
}

////////////////////////////////////////////////////////////////////////////////
var tempData=null;
////////////////////////////////////////////////////////////////////////////////               
function PostGeoServiceCheckPlateParking(datPark)
{   
    tempData = datPark;  
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    

    json_str = JSON.stringify(datPark);
    ok_pay=false;
    fdatProcess = function(data) 
    {   // alert(data.response); 
        
        if(data.response==="PlacaNaoEncontrada")
        {
           ShowErrorToast("Placa não encontrada: "+tempData.placa,0);  
           ConfirmDialog(i2l("Registra infração? ")+tempData.placa,function() { RegisterPlateEndTime(tempData);});
           return;
        }
         if(data.response==="PlacaEncontrada")
        {
           dAtual = new Date();
           dPark = new Date(data.vet[0].DateTimePark);

           periodmilissec = data.vet[0].period*60*60*1000;
           // alert(periodmilissec - (timeDiffMilissec(dAtual, dPark)));
           tempodecorrido = periodmilissec - (timeDiffMilissec(dAtual, dPark));
           if(tempodecorrido<0) 
           {
               tempHoras = (-1) * (tempodecorrido*(1/1000)*(1/60)*(1/60));
               if(tempHoras<24.0)
                  ShowErrorToast("Periodo terminado:<br>Placa: "+tempData.placa,0); 
               else
                  ShowErrorToast("Automóvel não registrado hoje:<br>Placa: "+tempData.placa,0);  
               // tempData.DateTimePark = dAtual; 
               ConfirmDialog(i2l("Registra infração? ")+tempData.placa,function() { RegisterPlateEndTime(tempData);});
           }
           else
           {    
               // tempodecorrido = tempodecorrido;
               ShowInfoToast("Placa dentro do periodo :<br>Placa: "+tempData.placa,1); 
           }    
           ok_pay=true;
 
           return;
        }       
        
    };
    
    execAjax(url,{ type:"CheckPlateParking",json:json_str},false,fdatProcess);  
    return ok_pay;
}
////////////////////////////////////////////////////////////////////////////////
