<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

////////////////////////////////////////////////////////////////////////////////
function MapProvider() 
{
    
//    if (ismobile()) {
//        $top = "45px";
//        $heightz = "45";
//
//    } else {
//        $top = "100px";
//        $heightz = "85";
//    }
    
    $top = "45px";
    $heightz = "45";
        
    // Mapzen key search-EzrHuLH
    $idUser = 0;
    if(isMobile()) $isMobile = 1; else $isMobile = 0;
    $headersM = HeadersMap();
    $divStyle= "  z-index: 1; opacity: 0.5; background:    #000; background:    -webkit-linear-gradient(#000, #011629); background:    linear-gradient(#000, #011629);  border-radius: 5px; box-shadow:    0 0px 0 0 #444; color:  #fff; display:       inline-block; padding: 0px 3px 7px 3px; text-align:    center; text-shadow:   1px 1px 0 #000;";
      
    $dataSearch = <<<EOT
    $headersM
    
    <script src="/ChildMonitor/Util/Util.min.js"></script> 
    <script type="text/javascript">  
    vetCallBack = [];        
    vetCallBack[0] = funcStaffDat;        
    if(isLogged('ParkingFitStaff')==="")
    {
       vetMenu = [["Meus Dados"],[]];
    }
    else
    {
       vetMenu = [["Meus Dados","Logout"],[]];
       vetCallBack[1] = funcStaffParkingLogout; 
    }
    </script>     
    <body class="bodyForm" style="overflow:hidden;"  >   
    <div id="idbodymapprovider" style=" position:absolute; z-index:1; top:0; left:0; width:100%; height:100%; "  >
        <div style=" position:absolute; z-index:1; top:$top; left:0; width:100%; height:100; "  id="map1"> </div> <br> 
        <div style=" position:absolute; z-index:2; top:$top; left:10px; width:99vw; $divStyle "  id="divAddress" /> <p style="font-size:10px" id="idAddress" > <br> <p style="font-size:10px" id="idCarrosSt" >  </div>
        <div style=" position:absolute; z-index:2; top:$top; left:0;" width:10px; height:10px; id="gpsLoc"> <img src="Img/GPS.png" alt="" height="30" width="30"> </div> <br> 
        <div style=" position:absolute; z-index:2; top:$top; left:0;" width:10px; height:10px; id="idCarCheck"> <img src="Img/idcar.png" alt="" height="30" width=""> </div> <br> 
            <div id="idMenuMobileParkStaff" > <script type="text/javascript"> document.write(CreateFloatingMenuMobile(isLogged('ParkingFitStaff'),vetMenu,vetCallBack)); </script> </div>    
    </div>       
    </body>        
    <script type="text/javascript">        
 
    //////////////////////////////////// 
    ActiveFormWindow = "#idbodymapprovider"; 
         
    var markerGlb=null;  
    var markerMe=null; 
    var markerOri=null;  
    var markerDst=null;  
    var controlRoute=null; 
    var idOrilatGlbOld=null;
    var idOrilonGlbOld=null;
    var idDstlatGlbOld=null;
    var idDstlonGlbOld=null;
    var isMobile=$isMobile;
    var UserId=1;         
    /////////////////////////////////////
            
    WaitBmp(0);
    var altura_tela = $(window).height(); /*cria variável com valor do altura da janela*/
    var largura_tela = $(window).width(); 
            
            
    largura = largura_tela-5;
    $("#divAddress").css('width', largura);
    $("#divAddress").css('left', 0);              
            
    $("#gpsLoc").css('top',altura_tela-55);
    $("#gpsLoc").css('left', largura_tela-50);
    $('#gpsLoc').on( "click", function() {
        SetMarkerOnMyLocation();
    });

    $("#idCarCheck").css('top',altura_tela-(55*1.8));
    $("#idCarCheck").css('left', largura_tela-50);
    $('#idCarCheck').on( "click", function() {
        DlgVerifyCardId();
    });
        
    // avoid showing scrollbars on main window
    $(".bodyForm").css('overflow','hidden');
    
       
    $("#map1").height(altura_tela-$heightz); /* aplica a variável a altura da div*/ 
    $('#divAddress').show();


            
    ////////////////////////////////////////////////////////////////////////////      
    var bTimerLoc = 0; 
   
    glbLatNow = 0.0;    
    glbLngNow = 0.0;          
    setTimeout(function() 
    {
       SetMarkerOnMyLocation();
       SetMapDragActions();     
    }, 5000);
      
    
    // alert(glbLat);
     
    initialize();

    VerifyLoginOnLoad("ParkingFitStaff");
        
    $.ajax({
    async: false,
    url: "/ChildMonitor/Util/MapProviderPark.min.js",
    dataType: "script"
    });
            
    ////////////////////////////////////////////////////////////////////////////

    function SetMapDragActions() 
    {
        map.on('move', function () {
           // StopMapTimer();
            if(markerMe!=null) 
               markerMe.setLatLng(map.getCenter());
            
            pos = map.getCenter()
         
	});
        
	//Dragstart event of map for update marker position
	map.on('dragstart', function(e) {  
            StopMapTimer();
            ActionOnControls(["#divAddress","#divOriDst"],"hide");          
        });
            
            
	//Dragend event of map for update marker position
	map.on('dragend', function(e) {
            CreateMapTimer();
            ActionOnControls(["#divAddress","#divOriDst"],"show");
            
            var cnt = map.getCenter();
            if(markerMe!=null) 
               var position = markerMe.getLatLng();
            glbLatNow = Number(position['lat']).toFixed(5);
            glbLngNow = Number(position['lng']).toFixed(5);
            
            //console.log(position);
            // setLeafLatLong(lat, lng);
            WriteAddressOnDestiny(); 
            RemoveParkedCarsMakers();
            
	});
            
            
    }
    //////////////////////////////////// 
    function CleanMapDragActions() 
    {
        map.off('move', function (e) {
            
	});
           
	//Dragend event of map for update marker position
	map.off('dragend', function(e) {
            
	});          
    }
    //////////////////////////////////// 
    function initialize() 
    {
        GetDeviceId();
        map = ShowOpenMap('map1',glbLatNow,glbLngNow);     
        // map.on('click', onMapClick);     
            
        CreateMapTimer();
        myTimer(); 
        SetMarkerOnMyLocation();
    }
    ////////////////////////////////////   
    function CreateMapTimer()
    { 
       iTimer=setInterval(function () {myTimer()}, 5000); // 5 segundos 
    }
    ////////////////////////////////////   
    function StopMapTimer()
    {
        clearInterval(iTimer);    
    }
    ////////////////////////////////////         
    function onMapClick(e) 
    {
       alert("You clicked the map at " + e.latlng);
    }
    //////////////////////////////////// 
    function SetMarkerOnMyLocation()
    {
        // getLocation();      
        // alert(GetOpenMapAddress(glbLatNow,glbLngNow));  
            
        getLocation();
        glbLatNow =   glbLat;    
        glbLngNow =   glbLng;    
            
        map.setView(new L.LatLng(glbLatNow, glbLngNow)); 
            
        markerMe = PutMarker(markerMe, map,"Img/Black_Marker.png",20,66,glbLatNow,glbLngNow); 
            
    }        
    //////////////////////////////////// 
    function SetMarkerOnNewLocation(glbLatNow,glbLngNow)
    {
        // getLocation();      
        // alert(GetOpenMapAddress(glbLatNow,glbLngNow));   
            
        map.setView(new L.LatLng(glbLatNow, glbLngNow)); 
            
        markerMe = PutMarker(markerMe, map,"Img/Black_Marker.png",20,66,glbLatNow,glbLngNow);
      
    }        

    ///////////////////////////////////////////////////////////////////////////
    function WriteAddressOnDestiny()
    {
        var updateAddress = function(strbuf,country,city)
        {
            glbCity = city; 
            glbCountry = country; 
  
            if (strbuf === undefined || strbuf == "") 
            {
                // $('#divAddress').slideUp();  
                //$('#idAddress').text("");
            }    
            else
            {    
                $('#idOri').val(strbuf.substring(0, 400));     
            }    
        };        
        
        var updateAddressGoogle = function(strbuf,country,estado,city,street)
        {
            if (strbuf === undefined || strbuf == "") 
            {
                // $('#divAddress').slideUp();  
                // $('#idAddress').text("");
            }    
            else
            {    
                // $('#divAddress').show();  
                // $('#divAddress').slideUp(); 
                // $('#idAddress').text(strbuf.substring(0, 400));  
            
                $('#idAddress').html(strbuf.substring(0, 400)); 
                glbEstado = retirarAcento(estado); 
                glbCity = retirarAcento(city); 
                glbCountry = retirarAcento(country); 
                glbStreet = retirarAcento(street); 
                glbAddress = strbuf.substring(0, 400); 
            }    
        };   
            
        // strbuf = GetOpenMapAddress(glbLatNow,glbLngNow,updateAddress);   
            
        strbuf = GetGoogleAddress(glbLatNow,glbLngNow,updateAddressGoogle);    
    }
    ///////////////////////////////////////////////////////////////////////////
    var markerVet = [];  
    var oldNumItens = 0;
    ///////////////////////////////////////////////////////////////////////////
    function RemoveParkedCarsMakers() 
    {
        $("#idCarrosSt").html("Sem carros nessa visão"); 
        for(i = 0; i < oldNumItens; i++) 
        {
            markerVet[i] = RemoveMarker(markerVet[i]);
        }   
        oldNumItens=0;
    }
    ///////////////////////////////////////////////////////////////////////////
    function PlotParkedCars(data)
    {
        // alert(data.vet[0].aLat);
        // alert(data.numItens);
        // alert(data.numItens);
        RemoveParkedCarsMakers();
        $("#idCarrosSt").html("Buscando carros");     
        oldNumItens = data.numItens;
        strBufCar = "";
        for(i = 0; i < data.numItens; i++) 
        {    
            if(!PeriodoExpirado(GetDateTime(),data.vet[i].DateTimePark,data.vet[i].period))
                markerVet[i] = PutMarker(markerVet[i], map,"Img/MapMarker_Flag4_Left_Chartreuse.png",20,20,data.vet[i].aLat,data.vet[i].aLng);
            else
                markerVet[i] = PutMarker(markerVet[i], map,"Img/MapMarker_Flag4_Left_Pink.png",20,20,data.vet[i].aLat,data.vet[i].aLng);
            
            markerVet[i].bindPopup("<p style=\"font-size:9px\"  > "+data.vet[i].idCar+"<br>Hora: "+data.vet[i].DateTimePark+"<br>Período: "+data.vet[i].period+"<\p>");
            strBufCar+=" "+data.vet[i].idCar;
            markerVet[i].on('click', function(e){ this.openPopup(); }); 
        } 
        strBufCar+="";
        $("#idCarrosSt").html("Carros nessa visão:"+data.numItens+"<br>"+strBufCar);
    }
    function onClick(i) {alert(e.latlng);}
    ///////////////////////////////////////////////////////////////////////////   
    function myTimer()
    { 
        // GetDeviceId();
        console.log("Timer "+glbStreet);
            
        // SetMarkerOnMyLocation();  
        WriteAddressOnDestiny();
            
        fdatProcess = function(data) 
        {   // alert(data.response); 
            // alert(data.period); 
            if(data.response=="CarList")
            {
               // ShowErrorToast("Exibindo carros"); 
               
               PlotParkedCars(data);
            }
            else
              $("#idCarrosSt").html("Sem carros nessa visão"); 
        };
        
        PostGetCarsInThisStreet(glbLatNow,glbLngNow,glbCountry,glbCity,glbStreet,GetDateTime(),fdatProcess);
        
    }
              
    WaitBmp(0);
                
    </script>    
  
EOT;
    echo $dataSearch;
}
////////////////////////////////////////////////////////////////////////////////
