<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once '../../CreditCard.php';
// 
// MELHOR
// http://developercielo.github.io/Webservice-3.0/english.html?shell#creating-a-simple-transaction
// MerchantId: 684b2bb4-be71-4477-a54b-a2222123e529
// MerchantKey: LUUSGHMKUNIBZTIQFKSDPQZGEPTTUIXXTLUAOHJF
// 
// MerchantId: 2f52f4af-3e6e-49ff-bcce-9751dcd55474
// MerchantKey: YAPIOVLATGELKQEYAMLGZAGIACWSRVYGYFEEAJMX
//
// Credit Card issuer (Visa / Master / Amex / link / Aura / JCB / Diners / Discover).
// 
// 
////////////////////////////////////////////////////////////////////////////////
// https://en.wikipedia.org/wiki/Payment_card_number
//
//Card Type	                Card Number Prefix
//American Express	        34, 37
//China UnionPay	        62, 88
//Diners ClubCarte Blanche	300-305
//Diners Club International	300-305, 309, 36, 38-39
//Diners Club US & Canada	54, 55
//Discover Card	                6011, 622126-622925, 644-649, 65
//JCB	                        3528-3589
//Laser	6304, 6706, 6771, 6709
//Maestro	5018, 5020, 5038, 5612, 5893, 6304, 6759, 6761, 6762, 6763, 0604, 6390
//Dankort	5019
//MasterCard	50-55
//Visa	4
//Visa Electron	4026, 417500, 4405, 4508, 4844, 4913, 4917

////////////////////////////////////////////////////////////////////////////////
class DataTrasactionCielo
{
   public $sandbox=0;  // 1 - USE CIELO SANDBOX
   public $orderid="121221121212";
   public $namecustomer="Somebody Name"; 
   public $type="CreditCard"; 
   public $amount=100; 
   public $capture="true"; 
   public $instalments=1; // parcelamento
   public $cardnumber="0000000000000001"; 
   public $cardtoken=""; 
   public $SoftDescriptor= "123456789ABCD";
   // public $cardnumber="545400000000003";
   public $holder="Test Holder"; 
   public $expdate="12/2021"; 
   public $cvc="123"; 
   public $savecard="true"; 
   public $brand="visa"; 
   public $requestid="343343233"; 
   public $datetime=""; 
   // Cielo payment information 
   public $merchantidSB="684b2bb4-be71-4477-a54b-a2222123e529";
   public $merchantkeySB="LUUSGHMKUNIBZTIQFKSDPQZGEPTTUIXXTLUAOHJF";
   public $merchantid="7de82818-1601-4b12-aa61-d662e4fa9e2e";
   public $merchantkey="5uNMRYPLKbQTVIV0TgnzhjdIC15mgv1rjoGk6MMb";
   // Stripe payment information 
   public $stripeApiKeySB= "sk_test_BQokikJOvBiI2HlWgH4olfQ2";

}
////////////////////////////////////////////////////////////////////////////////
function GenerateTransactionJSON($type,$dtatrans)
{ 
    ////////////////////////////////////////////////////////////////////////////
    // CIELO ACEITA "master" e não "mastercard" ... pqp..... recebí esta resposta
    //               após vários emails sem resposta
    ////////////////////////////////////////////////////////////////////////////
    if(strtolower ($dtatrans->brand) === "mastercard")
    {    
       $dtatrans->brand = "master";
    }
    $dtatrans->capture=true;
    ////////////////////////////////////////////////////////////////////////////
    if($type=="CardPayment")
    {    
        $string = <<<init
        {  
           "MerchantOrderId":"$dtatrans->orderid",
           "Customer":{  
              "Name":"$dtatrans->namecustomer"
           },
           "Payment":{  
             "Type":"$dtatrans->type",
             "Amount":$dtatrans->amount,
             "Installments":$dtatrans->instalments,
             "Capture":$dtatrans->capture,
             "CreditCard":{  
                 "CardNumber":"$dtatrans->cardnumber",
                 "Holder":"$dtatrans->holder",
                 "ExpirationDate":"$dtatrans->expdate",
                 "SecurityCode":"$dtatrans->cvc",
                 "SaveCard":"$dtatrans->savecard",
                 "Brand":"$dtatrans->brand"
             }
           }
        }
init;
    }
    /////////////////////////////////////////
    if($type=="GetCardToken")
    {    
        $string = <<<init
         {
            "CustomerName": "$dtatrans->namecustomer",
            "CardNumber":"$dtatrans->cardnumber",
            "Holder":"$dtatrans->holder",
            "ExpirationDate":"$dtatrans->expdate",
            "Brand":"$dtatrans->brand"
        }      
init;
    }    
    /////////////////////////////////////////
    if($type=="PayWithCardToken")
    {    
        $string = <<<init
        {  
           "MerchantOrderId":"$dtatrans->orderid",
           "Customer":{  
              "Name":"$dtatrans->namecustomer"
           },
           "Payment":{  
             "Type":"$dtatrans->type",
             "Amount":$dtatrans->amount,
             "Installments":$dtatrans->instalments,
             "Capture":$dtatrans->capture,
             "SoftDescriptor":"$dtatrans->SoftDescriptor",
             "CreditCard":{  
                 "CardToken":"$dtatrans->cardtoken",
                 "SecurityCode":"$dtatrans->cvc",
                 "Brand":"$dtatrans->brand"
             }
           }
        }    
init;
    }       
    /////////////////////////////////////////
    return $string;
}
////////////////////////////////////////////////////////////////////////////////
function CieloTransaction($type,$dtatrans) 
{
    $dtatrans->cardnumber = str_replace(".", "", $dtatrans->cardnumber);
    if($dtatrans->sandbox==1)
    {    
       $merchantid = $dtatrans->merchantid;  
       $merchantkey = $dtatrans->merchantkey;       
       $url = 'https://apisandbox.cieloecommerce.cielo.com.br/1/sales/';
       if($type=="GetCardToken") { $url = "https://apisandbox.cieloecommerce.cielo.com.br/1/card/"; }
    }       
    else   
    {   
       $merchantid = $dtatrans->merchantid;  
       $merchantkey = $dtatrans->merchantkey;      
       $url = 'https://api.cieloecommerce.cielo.com.br/1/sales/'; // https://api.cieloecommerce.cielo.com.br/1/sales/
       if($type=="GetCardToken") { $url = "https://api.cieloecommerce.cielo.com.br/1/card/"; }
       // $card = CreditCard::validCreditCard($dtatrans->cardnumber);
       // $dtatrans->brand = $card['type'];    
    }  
    
    $string = GenerateTransactionJSON($type,$dtatrans);
        
    $dbgmsg = "Tipo da tansação - $type \n";
    $dbgmsg .= "URL \n";
    $dbgmsg .= $url; $dbgmsg .= "\n\n";
    $dbgmsg .= "\n";

    
    $ch = curl_init();
    flush();
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt( $ch, CURLOPT_SSLVERSION, 'CURL_SSLVERSION_SSLv3' );
    
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    
    curl_setopt($ch, CURLOPT_FAILONERROR, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 40);
          
    $headers = [
    "Content-Type: application/json",
    "MerchantId: $merchantid",
    "MerchantKey: $merchantkey",
    "RequestId: $dtatrans->requestid"
    ];
     
    $dbgmsg .= "Headers da função curl_setopt(\$ch, CURLOPT_HTTPHEADER, \$headers); \n"; 
    $dbgmsg .= $headers[0]."<br>\n";  $dbgmsg .= $headers[1]."\n";  
    $dbgmsg .= $headers[2]."<br>\n";  $dbgmsg .= $headers[3]."\n";  
    $dbgmsg .= "\n"; 
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
    $dbgmsg .= "Solicitação: curl_setopt(\$ch, CURLOPT_POSTFIELDS, \$string);\n\n";
    $dbgmsg .= $string; $dbgmsg .= "\n";
    $dbgmsg .= "\n";    

    curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
    
    $data = curl_exec($ch);
    
    $dbgmsg .= "\n";
    $dbgmsg .= "Resposta: curl_exec(\$ch)\n";
    $dbgmsg .= "[$data]"; $dbgmsg .= "\n";
    $dbgmsg .= "\n";
    
    $msg_curl_error="";
    
    if (curl_errno($ch)) {
        $msg_curl_error = curl_error($ch); 
        $dbgmsg .= "curl_error: " . curl_error($ch);
    }
    
    curl_close($ch);
    
    // $xml = simplexml_load_string($data);
    
    $xml = json_decode($data); 
    $xml->msg_curl_error = $msg_curl_error;
    
//    $dbgmsg .= 'Tid: '.$xml->Payment->Tid.'<br>\n';
//    $dbgmsg .= 'Provider: '.$xml->Payment->Provider.'<br>\n';
//    $dbgmsg .= 'ReturnMessage: '.$xml->Payment->ReturnMessage.'<br>\n'; 
//    $dbgmsg .= 'ReturnCode: '.$xml->Payment->ReturnCode.'<br>\n'; 

    $xml->dbg =$dbgmsg; 
    
    // error_log($dbgmsg);
    
    // echo $xml->dbg;
    // $xml->Payment->ReturnCode == 6 OK

    // $xml->Payment->Status == 2 OK - pago e capturado
    // 
    //    Status returned by the API
    //    Code 	Payment Status 	Payment Method 	Description
    //    0 	NotFinished 	All 	Failed to process the payment
    //    1 	Authorized 	All 	Suitable payment method to be captured or paid (Boleto)
    //    2 	PaymentConfirmed 	All 	Payment Confirmed and concluded
    //    3 	Denied 	Credit Card and Debt/Eletronic transfer 	
    //    10 	Voided 	All 	Canceled Payment
    //    11 	Refunded 	Credit Card and Debit 	Payment Cancelled/Reversed
    //    12 	Pending 	Credit Card and Debit/Eletronicll 	Payment canceled for failure processing
    //    13 	Aborted 	All 	Payment canceled for failure processing
    //    20 	Scheduled 	Credit card 	Scheduled Recurrence
    
    return $xml;
    
}

//Resposta tipica ok:
//{
//	"MerchantOrderId": "2014111703",
//	"Customer": {
//		"Name": "Somebody Name"
//	},
//	"Payment": {
//		"ServiceTaxAmount": 0,
//		"Installments": 1,
//		"Interest": 0,
//		"Capture": true,
//		"Authenticate": false,
//		"Recurrent": false,
//		"CreditCard": {
//			"CardNumber": "000000******0001",
//			"Holder": "Test Holder",
//			"ExpirationDate": "12/2021",
//			"SaveCard": false,
//			"Brand": "Visa"
//		},
//		"Tid": "0211031432134",
//		"ProofOfSale": "20170211031432134",
//		"AuthorizationCode": "731234",
//		"Provider": "Simulado",
//		"PaymentId": "c41724ca-ad7e-4722-90fe-864e04510bc4",
//		"Type": "CreditCard",
//		"Amount": 100,
//		"ReceivedDate": "2017-02-11 15:14:32",
//		"CapturedAmount": 100,
//		"CapturedDate": "2017-02-11 15:14:32",
//		"Currency": "BRL",
//		"Country": "BRA",
//		"ReturnCode": "6",
//		"ReturnMessage": "Operation Successful",
//		"Status": 2,
//		"Links": [{
//			"Method": "GET",
//			"Rel": "self",
//			"Href": "https://apiquerysandbox.cieloecommerce.cielo.com.br/1/sales/c41724ca-ad7e-4722-90fe-864e04510bc4"
//		}, {
//			"Method": "PUT",
//			"Rel": "void",
//			"Href": "https://apisandbox.cieloecommerce.cielo.com.br/1/sales/c41724ca-ad7e-4722-90fe-864e04510bc4/void"
//		}]
//	}
//}
////////////////////////////////////////////////////////////////////////////////
// Operação de pegar o token de um cartão
        /*
                Criando um Cartão Tokenizado

                Para salvar um cartão sem autoriza-lo, basta realizar um posto com os dados do cartão.
                Requisição
                POST /1/card/

                {
                    "CustomerName": "Comprador Teste Cielo",
                    "CardNumber":"4532117080573700",
                    "Holder":"Comprador T Cielo",
                    "ExpirationDate":"12/2030",
                    "Brand":"Visa"
                }

                curl
                --request POST "https://apisandbox.cieloecommerce.cielo.com.br/1/card/"
                --header "Content-Type: application/json"
                --header "MerchantId: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
                --header "MerchantKey: 0123456789012345678901234567890123456789"
                --header "RequestId: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
                --data-binary
                {
                    "CustomerName": "Comprador Teste Cielo",
                    "CardNumber":"4532117080573700",
                    "Holder":"Comprador T Cielo",
                    "ExpirationDate":"12/2030",
                    "Brand":"Visa"
                }
                --verbose

                Propriedade 	Tipo 	Tamanho 	Obrigatório 	Descrição
                Name 	Texto 	255 	Sim 	Nome do Comprador.
                CardNumber 	Texto 	16 	Sim 	Número do Cartão do Comprador.
                Holder 	Texto 	25 	Sim 	Nome do Comprador impresso no cartão.
                ExpirationDate 	Texto 	7 	Sim 	Data de validade impresso no cartão.
                Brand 	Texto 	10 	Sim 	Bandeira do cartão (Visa / Master / Amex / Elo / Aura / JCB / Diners / Discover).
                Resposta

                {
                  "CardToken": "db62dc71-d07b-4745-9969-42697b988ccb",
                  "Links": {
                    "Method": "GET",
                    "Rel": "self",
                    "Href": "https://apiquerydev.cieloecommerce.cielo.com.br/1/card/db62dc71-d07b-4745-9969-42697b988ccb"}
                }

                --header "Content-Type: application/json"
                --header "RequestId: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
                --data-binary
                {
                  "CardToken": "db62dc71-d07b-4745-9969-42697b988ccb",
                  "Links": {
                    "Method": "GET",
                    "Rel": "self",
                    "Href": "https://apiquerydev.cieloecommerce.cielo.com.br/1/card/db62dc71-d07b-4745-9969-42697b988ccb"}
                }

         */
// Informações sobre problemas com a Cielo e só aceitar valores cheios        
// https://groups.google.com/forum/#!topic/cielo-magento/iX0jHsex7W4

// Dicas sobre a homologação na Cielo   
// https://oldwolf1602.wordpress.com/2013/07/27/formulario-de-homologacao-da-plataforma-cielo-e-commerce/
// https://groups.google.com/forum/#!topic/magentobr/NQBzQ3kyiZs   
////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////
function CieloGetCardToken($datadec)
{
    $dtatrans = new DataTrasactionCielo();
    
    $dtatrans->namecustomer =    $datadec->userEmail;
    $dtatrans->cardnumber =    $datadec->cardnumber;
    $dtatrans->holder = $datadec->namecard;
    $dtatrans->expdate =    $datadec->dtexp;
    // $card = CreditCard::validCreditCard($dtatrans->cardnumber);
    $dtatrans->brand = $datadec->brand;    

    $dtatrans->merchantid=$datadec->PaymentMerchantUserId;
    $dtatrans->merchantkey=$datadec->PaymentMerchantUserKey;
    
    $resp = CieloTransaction("GetCardToken",$dtatrans);
    
//    
//    if($resp->CardToken=="")
//    {
//        $token="invalid";
//    }
//    else
//        $token=$resp->CardToken;
    return($resp);
}
////////////////////////////////////////////////////////////////////////////////
// Pay with a cardtoken
// http://developercielo.github.io/Webservice-3.0/english.html?shell#creating-a-sale-with-card-token
//
/*
        {  
           "MerchantOrderId":"2014111706",
           "Customer":{  
              "Name":"Comprador CardToken"
           },
           "Payment":{  
             "Type":"CreditCard",
             "Amount":100,
             "Installments":1,
             "SoftDescriptor":"123456789ABCD",
             "CreditCard":{  
                 "CardToken":"6e1bf77a-b28b-4660-b14f-455e2a1c95e9",
                 "SecurityCode":"262",
                 "Brand":"Visa"
             }
           }
        }
 * 
             * curl
            --request POST "https://apisandbox.cieloecommerce.cielo.com.br/1/sales/"
            --header "Content-Type: application/json"
            --header "MerchantId: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
            --header "MerchantKey: 0123456789012345678901234567890123456789"
            --header "RequestId: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
            --data-binary
            {  
               "MerchantOrderId":"2014111706",
               "Customer":{  
                  "Name":"Comprador CardToken"
               },
               "Payment":{  
                 "Type":"CreditCard",
                 "Amount":100,
                 "Installments":1,
                 "SoftDescriptor":"123456789ABCD",
                 "CreditCard":{  
                     "CardToken":"6e1bf77a-b28b-4660-b14f-455e2a1c95e9",
                     "SecurityCode":"262",
                     "Brand":"Visa"
                 }
               }
            }
            --verbose
 * 
 * 
 * 
 * */

////////////////////////////////////////////////////////////////////////////////
//    datSend.amount = paymentData.val2pay;
//    datSend.date = GetDateTime();
function CieloPayWithCardToken($datadec)
{
    $dtatrans = new DataTrasactionCielo();
    
    $dtatrans->orderid = $datadec->orderid;
    $dtatrans->namecustomer = $datadec->namecustomer;
    $dtatrans->type  = $datadec->type;
    $dtatrans->amount  = $datadec->amount;
    $dtatrans->SoftDescriptor = $datadec->SoftDescriptor;
    $dtatrans->cardtoken = $datadec->cardtoken;
    $dtatrans->cvc =$datadec->cvc;
    $dtatrans->brand = $datadec->brand;    
    $dtatrans->capture = "true";
    
    $dtatrans->merchantid=$datadec->PaymentMerchantUserId;
    $dtatrans->merchantkey=$datadec->PaymentMerchantUserKey;
                     
    //  $dtatrans->amount  = "10";
    // $dtatrans->orderid = "12322232";
     // $dtatrans->brand = "Visa";
    
    $resp = CieloTransaction("PayWithCardToken",$dtatrans);
    
    // error_log($resp->dbg);

    return($resp);    
}
////////////////////////////////////////////////////////////////////////////////
?>