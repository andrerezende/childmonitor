/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
////////////////////////////////////////////////////////////////////////////////
if(BackFormDegPink===undefined)
{
    var BackFormDegPink = "background: #948E99; /* fallback for old browsers */ background: -webkit-linear-gradient(to left, #948E99 , #2E1437); /* Chrome 10-25, Safari 5.1-6 */ background: linear-gradient(to left, #948E99 , #2E1437); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */";
    var backbluedeg = "background: rgba(20,63,92,1);background: \n\
     -moz-linear-gradient(left, rgba(20,63,92,1) 0%, rgba(23,54,87,1) 53%, rgba(43,91,143,1) 100%);";

   // document.write("<div id='idBackFormProvider' style=' position:absolute; background-image: url(/ChildMonitor/Img/BackBlurPFit.jpg); opacity : 0.1  no-repeat center center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; top:0; left:0; width:100%; height:100%; '  > </div>");
}
////////////////////////////////////////////////////////////////////////////////
var msg_OK = i2l("Ok");
var msg_Cancel = i2l("Cancela");
////////////////////////////////////////////////////////////////////////////////
window.onload = function() { 
   // ShowInfoToast("Versão Beta", 0);
};
////////////////////////////////////////////////////////////////////////////////
var LastActiveDlg=null;
////////////////////////////////////////////////////////////////////////////////
function DestroyLastDlg()
{
   console.log("Destroying dlg = "+LastActiveDlg);
   if(LastActiveDlg!==null)
      $(LastActiveDlg).dialog("destroy");
   LastActiveDlg=null;
}
////////////////////////////////////////////////////////////////////////////////
function PostValidadeLogin(datPark)
{
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    
    json_str = JSON.stringify(datPark);
    ok_pay=false;
    fdatProcess = function(data) 
    {   // alert(data.response); 
        // alert(data.period); 
        if(data.response==="UserCreated")
        {
            ShowErrorToast("Usuário criado, clicar no link de ativação no seu email",1);  
            if(datPark.context==="ParkingFitUser")
            {
               setCookie('userEmailParkFit',datPark.email,1);
               $("#dlgloginprov").dialog("destroy");
            }
        }
        if(data.response==="InvalidLogin")
        {
            ShowErrorToast("Login inválido",1);  
        }
        if(data.response==="Provider Inactive")
        {
            ShowErrorToast("Provider inativo clique no email de ativação",1);  
        }
        if(data.response==="User Inactive")
        {
            ShowErrorToast("Usuário inativo clique no email de ativação",1);  
            $("#dlgloginprov").dialog("destroy");
        }        
        if(data.response==="LoginOk")
        {
            ShowInfoToast("Login ok",1); 
            ok_pay=true;
        }
    };
    
    execAjax(url,{ type:"CheckLogin",json:json_str},false,fdatProcess);
    return ok_pay;  
}
////////////////////////////////////////////////////////////////////////////////
function DialogChgPassword(email, context)
{
    CreateFormBackground();
    id = "dlgchangepasswd";
    
    $(LastActiveDlg).dialog("destroy");
    LastActiveDlg = "#" + id;
    

    dlgtitle = i2l("Nova senha para ") + email;
    dlgdata = "<div id=\"" + id + "\" title=\"" + dlgtitle + "\">";
    dlgdata += "<p>"+i2l("Nova senha:") +" <input type=\"password\" class=\"formdlg\" style=\" width:300px;\"  id=\"id_passwd\"> </p> \n\
               <p>"+i2l("Repita:") +" <input type=\"password\" class=\"formdlg\" style=\" width:300px;\" id=\"id_passwdrep\"> </p>\n\
               </div>";
    var execSim = function (id)
    {
        var controls = {};
        controls[1] = {id: "#id_passwd", valtype: ["notnull"], name: "Password"};

        if (!ValidateControls(controls))
            return;
   
        if ( trim($('#id_passwd').val()) === "")
        {
            ShowErrorToast("Nova senha inválida", true);
            return;            
        }
        
        if ($('#id_passwd').val() !== $('#id_passwdrep').val())
        {
            ShowErrorToast("Senhas informadas diferentes", true);
            return;
        }

        dtHoraPedido = new Date();

        var datPark = {};

        datPark.email = email;
        datPark.passwd = $('#id_passwd').val();
        datPark.context = context;
        PostGenericAction("SavePassword", datPark, null);
        $("#dlgchangepasswd").dialog("destroy");
        ParkingFitHomePage();
    }

   function SetupDialog()
   {
       $( document ).tooltip();
       JqueryDlgAcceptEnter("#dlgchangepasswd",execSim);
       SetUITransparent(); 
        
        
   }

   dwidth = "auto";
   if(isTestMobile())
   {
       
       // dwidth = $(window).width()+"px";
       dwidth = "100%";
       dposition = { my: 'top', at: 'top+48' }; 
   }
   else     
       dposition = { my: "center", at: "center", of: window };    

   
   
   
   $( dlgdata ).dialog({
      position: dposition,   
      resizable: false,
      height: "auto",
      width: dwidth,
      modal: false,
      open: function() { SetupDialog() },  
      buttons: 
      {
        "Ok": function() {  
            execSim("#dlgchangepasswd");
        }
      }
  });

}
////////////////////////////////////////////////////////////////////////////////
function DialogLogin(context,action)
{
    if (action === 'pwdreset' && (context === ''|| context==='ParkingFitUser' || context==='ParkingFitProvider'))
    {
        var datPark = {};
        datPark.email = trim($('#id_login').val());
        datPark.email = datPark.email.toLowerCase();
        if (datPark.email === "")
        {
            ShowErrorToast("Digite o email a ter a senha reconfigurada", true);
            return;
        }
        datPark.context = context;

        PostGenericAction("ResetPassword",datPark,null);
        ShowInfoToast("Mensagem com link de nova senha enviado para seu endereço de email.", true); 
        return;
    }
   
   if(context!=="ParkingFitUser")  
       CreateFormBackground();
   
   id="dlgloginprov";
   LastActiveDlg="#"+id;
   $(LastActiveDlg).dialog("destroy"); // Apaga das instancia anterior deste dialogo
   
   if(typeof context === 'undefined' ||  context === '')
   {
       context="";
       dlgtitle = i2l("Login Provider");
   }    
   if(context==="ParkingFitStaff") 
   {
       dlgtitle = i2l("Login Fiscal");
   }
   if(context==="ParkingFitUser") 
   {
       dlgtitle = i2l("Login de Usuário");
   }       
    
   dlgdata  = "<div id=\""+id+"\" title=\""+dlgtitle+"\">";
   dlgdata += "<input class=\"formdlg\" style=\" width:300px;\" placeholder=\"Email\" id=\"id_login\"> <br>";
   dlgdata += "<input type=\"password\" class=\"formdlg\" style=\" width:300px;\" placeholder=\"Password\" id=\"id_passwd\"> ";
   if(context==="ParkingFitUser" || context==="ParkingFitProvider" || context==="") 
   {
       dlgdata += "<a href='javascript:DialogLogin(\""+context+"\",\"pwdreset\"); '>"+i2l("Reset password") +"</a> </div>";
   }
   
   var execSim = function(id)
   {
         var controls = {};
         controls[0] = {id:"#id_login", valtype:["email"], name:"Email"};
         controls[1] = {id:"#id_passwd", valtype:["notnull"], name:"Password"};
         
         if(!ValidateControls(controls))
           return;
        
         dtHoraPedido = new Date();

         var datPark = { };

         datPark.email = $('#id_login').val();
         datPark.email = datPark.email.toLowerCase();
         datPark.passwd = $('#id_passwd').val();
         datPark.context = context;
         if(PostValidadeLogin(datPark))
         {
            setCookie('context',context,360);
            if(context==="") 
            {
               setCookie('userEmailProviderParkFit',datPark.email,360);
               url = window.location.protocol+'//'+location.host+'/ChildMonitor/index.php?&stme=VAG';
               window.location = url;
            }
            if(context==="ParkingFitStaff") 
            {
               setCookie('userEmailStaffParkFit',datPark.email,360);
               // GetStaffProvider(datPark.email);
               url = window.location.protocol+'//'+location.host+'/ChildMonitor/provider.php?&stme=VAG';
               window.location = url;
            }
            if(context==="ParkingFitUser") 
            { 
               setCookie('userEmailParkFit',datPark.email,360);
               $( "#dlgloginprov" ).dialog("destroy");
               // GetStaffProvider(datPark.email);
               // url = window.location.protocol+'//'+location.host+'/ChildMonitor/provider.php?&stme=VAG';
            }

            // alert(getCookie('userEmailJS'));
            // alert(document.cookie);
            // $( LastActiveDlg ).dialog("destroy");

            // ParkingFitHomePage();
         }
         else
         {

         }
   }

   function SetupDialog()
   {
       $( document ).tooltip();
       JqueryDlgAcceptEnter("#dlgloginprov",execSim);
       
       
       if(context==="ParkingFitUser") 
       {
          SetUI_Black(); 
       }       
       else
       {
          SetUITransparent();  
       }
       
       
        
        
   }

   dwidth = "auto";
   if(isTestMobile())
   {
       
       // dwidth = $(window).width()+"px";
       dwidth = "100%";
       dposition = { my: 'top', at: 'top+48' }; 
   }
   else     
       dposition = { my: "center", at: "center", of: window };    

   if(context==="ParkingFitUser") 
   {
       dlg_buttons_data = {
                            "Ok": function() {  
                                execSim("#dlgloginprov");
                            },
                            "Fecha": function() {  
                                $("#dlgloginprov").dialog("destroy"); 
                            }                            
                          };   
   }
   else
   {
       dlg_buttons_data = {
                            "Ok": function() {  
                                execSim("#dlgloginprov");
                            }
                          };
   }
   $( dlgdata ).dialog({
      position: dposition,   
      resizable: false,
      height: "auto",
      width: dwidth,
      modal: false,
      open: function() { SetupDialog() },  
      buttons: dlg_buttons_data
  });
}
////////////////////////////////////////////////////////////////////////////////
function GetStaffProvider(email)
{
    // setCookie('userEmailStaffParkFit',datPark.email,360);
    sql = "select emailProvider from `ParkingProviderStaff` where email='"+email+"'";
    return GetSqlValue(sql);            
}
////////////////////////////////////////////////////////////////////////////////
function MsgDialogProvider()
{
    var idMsgDialogProvider= i2l("Crie seu provedor de estacionamentos, definindo sua cidade ou região de abrangência.<br>Informe o embasamento legal municipal ou seus documentos de autorização de operação do serviço ou estabelecimento. Após a verificação da documentação e meios de pagamento, seu serviço será ativado.<br>Informe também seu MerchandId no Gateway de pagamentos Cielo e receba diretamente em sua conta os pagamentos de estacionamento.<br>Cadastre os fiscais e funcionários que farão a verificação do automóveis estacionados e calcule automaticamente o valor pelos serviços prestados.<br><br>");
    if(isLogged()=="")
       links = i2l("Faça") +" <a href=\"javascript:DialogLogin(); \">Login</a> "+i2l("e gerencie sua conta, funcionários e estacionamentos ou ") +"<a href=\"javascript:DialogCreateProvider(); \">"+i2l("Crie sua conta de provedor") +"</a>.";
    else
       links = i2l("Atualize os dados de sua ") +"<a href=\"javascript:DialogCreateProvider(); \">"+i2l("conta") +"</a>,  <a href=\"javascript:DialogProviderProfit(); \">"+i2l("faturamento") +"</a>, <a href=\"javascript:DialogProviderStaff(); \">"+i2l("funcionários") +"</a>"+i2l("e estacionamentos.") +" ";
    return (idMsgDialogProvider+links);    
}
////////////////////////////////////////////////////////////////////////////////
var idMsgDialogUser  = i2l("Apenas estacione e faça o ")+"<a href=\"/ChildMonitor/user.php?stme=VAG\">"+i2l("pagamento") +"</a>"+i2l(" comodamente pela internet ou aplicativo") +".<br><br>";
idMsgDialogUser += "<a style='padding:10px 0px 10px 10px; ' href='https://play.google.com/store/apps/details?id=app.singularityage.parkingfit&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img width=100px alt='Get it on Google Play' src='/ChildMonitor/Img/GooglePlay.png'/></a>";
idMsgDialogUser += "<a  style='padding:10px 0px 10px 10px; ' href=''><img width=100px alt='Apple Store' src='/ChildMonitor/Img/AppleStore.png'/></a><br><br>";

var idImgInfo=" <img src=\"/ChildMonitor/Img/selos_cielo_seguranca.png\" height=\"25\"  /> \n\
<img src=\"/ChildMonitor/Img/selos_cielo_logos_credito.png\"  height=\"50\" />";
var idImgInfoRotterdam=" <img src=\"/ChildMonitor/Img/selos_cielo_seguranca.png\" height=\"25\"  /> \n\
<img src=\"/ChildMonitor/Img/selos_cielo_logos_credito.png\"  height=\"50\" />";
function DialogProvider()
{
   console.log('DialogProvider');
   CreateFormBackground();
   id="dlgprov";
   LastActiveDlg="#"+id;
   $(LastActiveDlg).dialog("destroy");
   console.log('$(LastActiveDlg).dialog("destroy");');
   
   dlgdata  = "<div id=\""+id+"\" title=\""+i2l("Provedores de Estacionamento") +"\">";
   dlgdata += MsgDialogProvider();
   dlgdata += " </div>";
   var execSim = function(id)
   {
       
   }

   function SetupDialog()
   {
        $( document ).tooltip();
        JqueryDlgAcceptEnter("#dlgprov",execSim);
        SetUITransparent(); 
   }

   dwidth = "98%";
   if(isTestMobile())
   {
       
       // dwidth = $(window).width()+"px";
       dwidth = "99%";
       dposition = { my: 'top', at: 'top+48' }; 
   }
   else     
       dposition = { my: "center", at: "top+48", of: window };    

   
   dposition = { my: 'top', at: 'top+48' }; 
   
   $( dlgdata ).dialog({
      position: dposition,   
      resizable: false,
      height: "auto",
      width: dwidth,
      modal: false,
      open: function() { SetupDialog() },  
      buttons: 
      {
//        "Ok": function() {  
//            execSim("#dlglogin");
//        },
//        "Cancela": function() 
//        {
//            $("#dlglogin").dialog("destroy");
//        }
      }
  });
}
////////////////////////////////////////////////////////////////////////////////
function ParkingFitHomePage()
{
//    if(isLogged()!=="" )
//        alert("Logado como: "+isLogged());
//    else
//        alert("Não Logado");
    SelectLanguage = CreateSelectLanguage();
    
        
    
    DialogShowInfo("ParkingFit",i2l("Solução completa para gerenciamento geográfico, fiscalização e tarifação de estacionamentos locais ou Municipais.<br><br>Para gestores Municipais ou Estacionamentos:<br><br>") +MsgDialogProvider()+
     i2l("<br><br>Para motoristas:<br><br>")+idMsgDialogUser+SelectLanguage);

}
////////////////////////////////////////////////////////////////////////////////
function GetPaymentOrderId(context)
{
    // Numero de identificação do Pedido. Text Max 50
    //   dtabuff = GetDateTime();
    //   dtabuff = dtabuff.replace(' ','');
    //   return context + dtabuff;
    //   
   dtabuff = GetDateTime();
   dtabuff = dtabuff.replace(' ','');
   dtabuff = dtabuff.replace('-',''); dtabuff = dtabuff.replace('-',''); // tira as duas ocorrencias
   dtabuff = dtabuff.replace(':',''); dtabuff = dtabuff.replace(':','');
   // alert(dtabuff);
   return dtabuff;

}
////////////////////////////////////////////////////////////////////////////////
function DescOnCardPayment()
{
    // Max 13 chars only for Master and Visa
    return "ParkingFit";
}
////////////////////////////////////////////////////////////////////////////////
function PayProviderDebt(date,amount)
{
    paymentData = {};
    paymentData.val2pay = amount;
    paymentData.date = date;    
    paymentData.orderid = GetPaymentOrderId('ParkingFitProvider');
    paymentData.namecustomer=isLogged('ParkingFitProvider');
    paymentData.PayForWho="System";
    
    paymentData.SoftDescriptor = DescOnCardPayment(); // paymentData.date = date; 
    //    paymentData.type = "CreditCard";  
    //    Data generated on payment dialog
    //    paymentData.cardtoken;
    //    paymentData.cvc;
    //    paymentData.brand;      

    //    
    paymentData.CallBackFunc = function(data)
    {
        if(data.response === "ErrorPayingValueWithCardToken")
        {
            ShowErrorToast("Erro pagamento");
        }   
        else
        {
            RegisterProviderDebitPayment(paymentData);
            ShowInfoToast("Pagamento autorizado");
            dataTableProfit = mountVetProfitServer(paymentData.namecustomer,$('#id_year').val());         
            $("#divProfit").html(dataTableProfit);
            $( "#dlgprovpagmethod" ).dialog("destroy");
        }
};
    
    
    DialogManagePaymentMethods('ParkingFitProvider',paymentData);
}
////////////////////////////////////////////////////////////////////////////////
function RegisterProviderDebitPayment(paymentData)   
{
      userEmail=isLogged('ParkingFitProvider');

    /*
    drop table IF EXISTS `ParkingFitProviderPaidFee`;
    CREATE TABLE `ParkingFitProviderPaidFee` (
            `emailProvider` VARCHAR(255) NULL,
            `RefDate` VARCHAR(50) NULL,
            `Status` VARCHAR(50) NULL DEFAULT 'Not paid',
            `Date` DATETIME not NULL DEFAULT CURRENT_TIMESTAMP,
            INDEX `Index 1` (`Date`, `emailProvider`, `RefDate`)
    )
    */
      // `ParkingFitProviderPaidFee`
      
    var dataSql = { };    
        
    dttime = GetDateTime();
    dataSql[0 ] = {Name:"emailProvider"     ,Value:userEmail,   Type:""   };
    dataSql[1 ] = {Name:"RefDate"  ,Value:paymentData.date,  Type:""   };
    dataSql[2 ] = {Name:"Status"     ,Value:"Paid",   Type:""   };
    dataSql[3 ] = {Name:"Date"      ,Value:dttime,    Type:""   };
     
    sql = SqlInsert("ParkingFitProviderPaidFee",dataSql); 
    PostExecSql(sql,null);

}
////////////////////////////////////////////////////////////////////////////////
function mountVetProfitServer(provider, year)
{
    // sqlDebit = "(select case when (select RawDataPayment from `ParkingFitProviderPaidFee` where `RefDate`='2017-05' and `emailProvider`='" + provider + "') IS NULL then 'Not Paid' end)";
    height = jQuery(window).height()-250;
    vet = [[],[],[]];
    fdatProcess = function (data)
    {
        if (data.response == "DataProfitProvider")
        {
            // ShowInfoToast("Infração registrada: "+data.placa); 
            vet = SqlJsonResult2Vet(data);
        }
    }
    
    var datSend = {};
    datSend.provider = provider;
    datSend.year = year;
    // TODO show only provit with more of 40 days, to avoid early payments -> TIMESTAMPDIFF(DAY,CURDATE(),DateTimePark)>40 and 

    PostGenericAction("MountDataProfitProvider",datSend,fdatProcess);

    // "Data"  "Estacionamentos"  "Recebido"  "ParkingFit %"     "ParkingFit Fee"  "Pago"
    il = 1;
    while (typeof vet[il][5] !== 'undefined')
    {
        if(vet[il][5] === 'Devedor')
        {    
           imgpay =" <img src=\"/ChildMonitor/Img/IconPay.png\" alt=\"\" height=\"13\" width=\"15\"> "; 
           date = vet[il][0]; amount = vet[il][4]; 
           vet[il][5] = "<font onclick=\"PayProviderDebt('"+date+"','"+amount+"');\"; style=\"color:rgb(255,0,0); font-weight: bold;  font-style: italic; \" >Devedor  "+imgpay+" </font> ";
        }
        else
        {
           vet[il][5] = 'Ok';
        }
        il++;
    }
    dataTableProfit = VetToTableInt("id_profitmontly",vet," width:98%; height:"+height+"px; ");
    return dataTableProfit;
}
////////////////////////////////////////////////////////////////////////////////
function DialogProviderProfit()
{
    
//    size = " width:98%; height:"+height+"px; ";	
//    Info = SqlToTable("select `idUser`,`idCar`,`ParkPaid`,`Address` ,`City` ,`State`,`Country`,`DateTimePark` from `ParkingDataPayment`",size); 
//    DialogShowInfo("Faturamento",Info);
   
   CreateFormBackground();
   id="dlgProviderProfit";
   LastActiveDlg="#"+id;
   
   provider = isLogged('ParkingFitProvider')
   
   /*
       "select DATE_FORMAT(DateTimePark,'%Y-%m') as Data,count(*) as `Estacionamentos`, sum(valuePaid) as `Recebido`, format(sum(valuePaid)*(select ParkingFitFee from ParkingProvider where email='"+provider+"'),2) as `ParkingFit Fee` from ParkingData where DATE_FORMAT(DateTimePark,'%Y')='"+year+"' and idProvider='"+provider+"'  group by DATE_FORMAT(DateTimePark,'%Y-%m');";
   */
  
   /*
      set @temp = ;
      (select case when (select RawDataPayment from `ParkingFitProviderPaidFee` where `RefDate`='2017-05' and `emailProvider`='2017-05') IS NULL then 'Not Paid' end);
   */
  
   
//    function mountSqlProfit(provider, year)
//    {
//        // sqlDebit = "(select case when (select RawDataPayment from `ParkingFitProviderPaidFee` where `RefDate`='2017-05' and `emailProvider`='" + provider + "') IS NULL then 'Not Paid' end)";
//        height = jQuery(window).height()-250;
//        sql = "select DATE_FORMAT(DateTimePark,'%Y-%m') as Data,count(*) as `Estacionamentos`, " +
//                " sum(valuePaid) as `Recebido`,(select ParkingFitFee from ParkingProvider where email='" + provider + "') " +
//                " as `ParkingFit %` , format(sum(valuePaid)*(select ParkingFitFee " +
//                "from ParkingProvider where email='" + provider + "')*(1/100.00),2) as `ParkingFit Fee` from " +
//                " ParkingData where DATE_FORMAT(DateTimePark,'%Y')='" + year + "' and idProvider='" + provider + "' " +
//                " group by DATE_FORMAT(DateTimePark,'%Y-%m');";
//        
//        vet = SqlToVet(sql);
//        
//        dataTableProfit = VetToTableInt(vet," width:98%; height:"+height+"px; ");
//        return dataTableProfit;
//    }
    

    // TODO show only provit with more of 40 days, to avoid early payments -> TIMESTAMPDIFF(DAY,CURDATE(),DateTimePark)>40 and 
   dataTableProfit = mountVetProfitServer(provider,GetYear());
   
     sqldate = "select distinct  DATE_FORMAT(DateTimePark,'%Y') as Data,DATE_FORMAT(DateTimePark,'%Y') from ParkingData where idProvider='"+provider+"' order by Data desc;";
   
   datMontlyProfit =  VetToTable([[i2l("Selecione ano:"),CreateSelectFromSql("id_year", sqldate," class=\"formdlg backBlack\"  style=\"width:60px; \" ")],[]])+
                      "<div id=\"divProfit\">"+ dataTableProfit + "</div>";
              
   cardselectedtopay = "<div id=\"divProfit\">"+ dataTableProfit + "</div>"          

   title = i2l("Faturamento");
   dlgdata  = "<div id=\""+id+"\" title=\""+title+"\">";
   dlgdata +=  CreateJqTabs("id_tabsprofit", [i2l("Mensal")], [datMontlyProfit]);    // CreateJqTabs("tabsPark",["Cartão","User"], [vardatacard,""])
   dlgdata += " </div>"; // end dlg
   

   
   var execSim = function(isNovo)
   {
       

   }
   
   function SetupDialog()
   { 
        // SetUITransparent(); 

        SetUITransparent(); 

        // $( "#id_year" ).selectmenu();
        $('#id_year').on('change', function() 
        {
            dataTableProfit = mountVetProfitServer(provider,$('#id_year').val());         
            $("#divProfit").html(dataTableProfit);
        })
        
//        $(document.body).on('change',"#id_year",function (e) {
//           //doStuff
//            alert(" dsdsdsdd" );
//        });

        // $( "#id_year" ).val(GetDateLocale());
       
        $( "#id_tabsprofit" ).tabs(); 
        
        JqueryDlgAcceptEnter("#dlgProviderProfit",execSim);     
       
   }
    
   dwidth = "550px";
   if(isTestMobile())
   {
       
       // dwidth = $(window).width()+"px";
       dwidth = "100%";
       dposition = { my: 'top', at: 'top+48' }; 
   }
   else     
       dposition = { my: "center", at: "center", of: window };    

   $( dlgdata ).dialog({
      position: dposition,   
      resizable: true,
      height: "auto",
      width: dwidth,
      modal: false,
      open: function() { SetupDialog() },  
      buttons: 
      {
        "Fecha": function() 
        {
            // ParkingFitHomePage();
            // execSim(true);
            // alert("zczc");
            $("#dlgProviderProfit").dialog("destroy");
            ParkingFitHomePage();

        }
      }
  });

}
////////////////////////////////////////////////////////////////////////////////
function DialogShowInfo(Title,Info)
{
   CreateFormBackground();
   id="dlgDialogShowInfo";
   LastActiveDlg="#"+id;
   $(LastActiveDlg).dialog("destroy");
   
   dlgdata  = "<div id=\""+id+"\" title=\""+Title+"\">";
   dlgdata += Info;
   dlgdata += " </div>";
   var execSim = function(id)
   {

   }

   function SetupDialog()
   {
        JqueryDlgAcceptEnter("dlgDialogShowInfo",execSim);
        SetUITransparent(); 
        
        
   }

   dwidth = "99%";  

   
   dposition = { my: 'top', at: 'top+48' }; 
   
   $( dlgdata ).dialog({
      position: dposition,   
      resizable: true,
      height: "auto",
      width: dwidth,
      modal: false,
      open: function() { SetupDialog() },  
      buttons: 
      {
//        "Ok": function() {  
//            execSim("#dlglogin");
//        },
//        "Cancela": function() 
//        {
//            $("#dlglogin").dialog("destroy");
//        }
      }
  });
}

////////////////////////////////////////////////////////////////////////////////
function CreateFormBackground(lines)
{
   DestroyLastDlg();
   $(ActiveFormWindow).hide();
   ActiveFormWindow = "#idBackFormProvider";
   $(ActiveFormWindow).show();
   if(lines===undefined)
       lines=0;

   // $(ActiveFormWindow).css('height',height);
   
   datazero = "<br>";
   for (i = 0; i < lines; i++)
      datazero+="<br>";
   $('#idBackFormProvider').html(datazero);
   

}
////////////////////////////////////////////////////////////////////////////////
function SelectCurrency(id)
{
    buffer = "<select style=' z-index:2; width:120px;' id='"+id+"' name='"+id+"'> \
          <option value='AUD'>Australian Dollar</option>\
          <option value='BRL'>Brazilian Real </option>\
          <option value='CAD'>Canadian Dollar</option>\
          <option value='CZK'>Czech Koruna</option>\
          <option value='DKK'>Danish Krone</option>\
          <option value='EUR'>Euro</option>\
          <option value='HKD'>Hong Kong Dollar</option>\
          <option value='HUF'>Hungarian Forint </option>\
          <option value='ILS'>Israeli New Sheqel</option>\
          <option value='JPY'>Japanese Yen</option>\
          <option value='MYR'>Malaysian Ringgit</option>\
          <option value='MXN'>Mexican Peso</option>\
          <option value='NOK'>Norwegian Krone</option>\
          <option value='NZD'>New Zealand Dollar</option>\
          <option value='PHP'>Philippine Peso</option>\
          <option value='PLN'>Polish Zloty</option>\
          <option value='GBP'>Pound Sterling</option>\
          <option value='SGD'>Singapore Dollar</option>\
          <option value='SEK'>Swedish Krona</option>\
          <option value='CHF'>Swiss Franc</option>\
          <option value='TWD'>Taiwan New Dollar</option>\
          <option value='THB'>Thai Baht</option>\
          <option value='TRY'>Turkish Lira</option>\
          <option value='USD' SELECTED='YES'>U.S. Dollar</option>\
        </select> ";
    return buffer;
}
////////////////////////////////////////////////////////////////////////////////
function SelectLang(id)
{
    buffer = "<select style=' z-index:2; width:120px;' id='"+id+"' name='"+id+"'> \
          <option SELECTED='YES' value='PT-BR'>Portugues</option>\
          <option value='US'>Ingles</option>\
        </select> ";
    return buffer;
}


//////////////////////////////////////////////////////////////////////////////// 
function LoadProviderData()
{
    log_email = isLogged();
    // log_email = "tes@sdhsd.sdds";
    
    if(log_email=="")
        return;
    // alert("Enviado: "+log_email); 
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    

    // alert("Size json: "+json_str.length);
    ok_pay=false;
    fdatProcess = function(data) 
    {   // alert(data.response); 
        // alert(data.period); 
        if(data.response=="ParkProviderData")
        {
            // {"email":"dfsd@adsa.sfdsd","password":"dfdfgf","NomeProvider":"gfdgf","NomeResponsavel":"fgdddg","TelefoneResponsavel":"fggfd","IdResponsavel":"fgfdgd","city":"Niteroi","address":"R. Goitacases, 39 - SÃ£o Francisco, NiterÃ³i - RJ, 24360-350, Brazil","country":"Brazil","countryCode":"BR","AutorizationLegalDocuments":"[{},{},{}]","Observacoes":"ObservaÃ§Ãµes","DateInclusion":"0000-00-00 00:00:00","Active":"1"}}
            // alert("Retornado: "+data.sql_dat.NomeProvider);
            
            $('#emailDta').hide();
              $('#id_email').val(data.sql_dat.email);    
              $('#id_emailcmp').val(data.sql_dat.email); 
              $('#id_passwd').val(data.sql_dat.password); 
            
            $('#id_nomeprovider').val(data.sql_dat.NomeProvider);
            $('#id_nomeresp').val(data.sql_dat.NomeResponsavel);
            $('#id_telefoneresp').val(data.sql_dat.TelefoneResponsavel);
            $('#id_cpfresp').val(data.sql_dat.IdResponsavel);
            $('#id_address').val(data.sql_dat.address);
            $('#id_city').val(data.sql_dat.city);
            $('#id_country').val(data.sql_dat.country);
            $('#id_obs').val(data.sql_dat.Observacoes);

            $("#idvalorhora").val(data.sql_dat.HourValue);
            $("#idpercinfraction").val(data.sql_dat.StaffPercHour);
            $("#idSelCurr").val(data.sql_dat.Currency); $("#idSelCurr").selectmenu("refresh");
            $("#idSelLang").val(data.sql_dat.Language); $("#idSelLang").selectmenu("refresh");
            
            $('#id_cielomerid').val(data.sql_dat.cielomerid);
            $('#id_cielomerkey').val(data.sql_dat.cielomerkey);
            
            // glbCountry;
            dataFile = JSON.parse(data.sql_dat.AutorizationLegalDocuments);  
            // console.log(dataFile);
            // alert(dataFile[0].name);
            document.getElementById("id_email").readOnly = true;
            document.getElementById("id_passwd").readOnly = true;
            
            // SetCheckBox(checkbox,bval)
            // GetCheckBoxState(checkbox)
            // alert(data.sql_dat.useRangeOperation);
            if(data.sql_dat.useRangeOperation==="1")
                SetCheckBox("#checkRegion",true);  
            else
                SetCheckBox("#checkRegion",false);
            
            console.log("SetupMap() on loading data");
            
            SetupMap();
            $("#idcountry_operation").val(data.sql_dat.countryOperation);
            $("#idcity_operation").val(data.sql_dat.cityOperation);
            $("#idlat_operation").val(data.sql_dat.latOperation);
            $("#idlon_operation").val(data.sql_dat.lonOperation);            
            $("#idradius_operation").val(data.sql_dat.rangeOperation); 
            
            SetupMapProvider(data.sql_dat.latOperation,data.sql_dat.lonOperation,data.sql_dat.rangeOperation);
        }

    };
    execAjax(url,{ type:"ParkLoadProvider",email:log_email},false,fdatProcess); 
}
////////////////////////////////////////////////////////////////////////////
var mapMan = MapManager; 
function SetupMapProvider(lat,lon,rad)
{
    console.log("SetupMapProvider("+lat+","+lon+","+rad+") on loading data");
    $("#idlat_operation").val(lat);
    $("#idlon_operation").val(lon);
    mapMan.mapCircleRad = rad;
    $("#idradius_operation").val(mapMan.mapCircleRad); 
    mapMan.mapCircle.setRadius(mapMan.mapCircleRad);
    // mapMan.map.panTo(new L.LatLng(lat, lon));
    mapMan.map.setView(new L.LatLng(lat, lon), 20);
    mapMan.SetMarkerOnNewLocation(lat, lon);
    SetupMapMove();
    hideRegion();
}

////////////////////////////////////////////////////////////////////////////
function SetupMap()
{
    if(mapMan.map!==null)
    {
       console.log("SetupMap() --> mapMan.map===null");
       return;
    }
    $("#idlat_operation").val(glbLat);
    $("#idlon_operation").val(glbLng);
    $("#idradius_operation").val(mapMan.mapCircleRad);
    // 
    // PutInputMask("#idradius_operation","999999");  
    hideRegion();
    mapMan.ShowOpenMap('mapRegions', glbLat, glbLng);

    $("#ui-id-5").click(function () { /* Redraw map bug */
        mapMan.map._onResize();
    });
    // mapMan.map.dragging.disable();
    mapMan.map.setView(new L.LatLng(glbLat, glbLng), 20);
    mapMan.SetMarkerOnNewLocation(glbLat,glbLng);

    // https://leaflet.github.io/Leaflet.draw/docs/leaflet-draw-latest.html

}
////////////////////////////////////////////////////////////////////////////////
function SetupMapMove()
{

    mapMan.SetMapDragActions(dragendRegProvider, mapmoveRegProvider);  
    
}
////////////////////////////////////////////////////////////////////////////////
function hideRegion()
{
   // alert("xcxcc");
   // $('#checkRegion').prop('checked', false); $('#checkRegion').button( "refresh" )
   // 
   // SetCheckBox(checkbox,bval)
   // GetCheckBoxState(checkbox)

   if($("#checkRegion").is(':checked'))
   {    
      $("#mapRegionsData").show();
      if(mapMan.map!==null)
        mapMan.map._onResize();   
  }   
   else
      $("#mapRegionsData").hide(); 

}
////////////////////////////////////////////////////////////////////////////////
function dragendRegProvider(e)
{
    // CreateMapTimer();
    // ActionOnControls(["#start","#divOriDst"],"show");


    if (mapMan.markerMe !== null)
    {
        var position = mapMan.markerMe.getLatLng();
        lat = Number(position['lat']).toFixed(10);
        lng = Number(position['lng']).toFixed(10);

        $("#idlat_operation").val(lat);
        $("#idlon_operation").val(lng);
        
        
        var updateAddressGoogle = function (strbuf, country, estado, city, street, countryLong)
        {
            if (strbuf === undefined || strbuf == "")
            {

            }
            else
            {
                $("#idcountry_operation").val(retirarAcento(country));
                $("#idcity_operation").val(retirarAcento(city));
            }
        };
        GetGoogleAddress(lat, lng, updateAddressGoogle);
    }
}
////////////////////////////////////////////////////////////////////////////
function mapmoveRegProvider() 
{
   // StopMapTimer();
    if(mapMan.markerMe!==null)
    {
       mapMan.markerMe.setLatLng(mapMan.map.getCenter());
       mapMan.mapCircle.setLatLng(mapMan.map.getCenter());
       
       
    }
//    pos = mapMan.map.getCenter()
//    
//    
//    
//
//    if(pos.lat!=undefined && pos.lng!=undefined )
//    {
////                    this.glbLatNow = pos.lat;    
////                    this.glbLngNow = pos.lng; 
//    }

   //  CreateMapTimer();
}
////////////////////////////////////////////////////////////////////////////
var dataFile=[];
dataFile[0]={};
dataFile[1]={};
dataFile[2]={};
var func_update=null;
////////////////////////////////////////////////////////////////////////////////
function DialogCreateProvider(parm)
{
     
   if(parm==="hideRegion")
   {
       return hideRegion();
   }
   if(parm==="ChangeRadius")
   {
       return ChangeRadius();
   }

   mapMan.FreeAll();
   CreateFormBackground();
   id="dlgcreateprov";
   LastActiveDlg="#"+id;
   provider = isLogged('ParkingFitProvider');
   if(provider!=="")
       title = i2l("Atualiza provedor - ")+provider;
   else
       title = i2l("Cria provedor");
   
   
    SelectPayMethod = SelectPayment(); 
   
   
   payDataCielo = "<div id=\"payDataCielo\" > \
                           "+i2l("No Brasil receba diretamente os pagamentos com a solução de pagamento")+" <a href=\"https://www.cielo.com.br/seja-nosso-cliente/?gclid=CK-t4crXq9QCFQkGkQodfV4KUg\">Cielo - API Cielo e-commerce</a>. \
                           <p>Cielo merchand id:<textarea class=\"formdlg\" rows='4' cols='50' style=\" width:300px;\" id=\"id_cielomerid\">  </textarea></p>  \
                           <p>Cielo merchand key:<textarea class=\"formdlg\" rows='4' cols='50' style=\" width:300px;\" id=\"id_cielomerkey\">  </textarea></p>  \
                   </div>";
   payDataStripe = "<div id=\"payDataStripe\" > \
                           "+i2l("No Mundo receba diretamente os pagamentos com a solução de pagamento")+" <a href=\"https://stripe.com\">Stripe</a>. \
                           <p>Stripe merchand id:<textarea class=\"formdlg\" rows='4' cols='50' style=\" width:300px;\" id=\"id_stripeid\">  </textarea></p>  \
                   </div>"; 
   
   dlgdata  = "<div id=\""+id+"\" title=\""+title+"\">";
      dlgdata += "<div id=\"tabs\" >\n\
                    <ul> \
                    <li><a href=\"#tabs-1\">"+i2l("Dados")+"</a></li> \
                    <li><a href=\"#tabs-2\">"+i2l("Configuração")+"</a></li> \
                    <li><a href=\"#tabs-3\">"+i2l("Região")+"</a></li> \
                    </ul>";
       dlgdata += "<div id=\"tabs-1\">"; 
       dlgdata += "<div id=\"emailDta\"> \n\
                      <input class=\"formdlg\" style=\" width:300px;\" title=\"Email\" placeholder=\"Email\" id=\"id_email\"> <br>\
                      <input class=\"formdlg\" style=\" width:300px;\" title=\""+i2l("Repita o email")+"\" placeholder=\""+i2l("Repita o email")+"\" id=\"id_emailcmp\"> <br> \
                      <input type=\"password\" class=\"formdlg\" style=\" width:300px;\" title=\"Password\" placeholder=\"Password\" id=\"id_passwd\"> <br> \n\
                   </div>\n\
                   <input class=\"formdlg\" style=\" width:300px;\" title=\""+i2l("Nome Município ou Estabelecimento")+"\" placeholder=\""+i2l("Nome Município ou Estabelecimento")+"\" id=\"id_nomeprovider\"> <br>\n\
                   <input class=\"formdlg\" style=\" width:300px;\" title=\""+i2l("Nome Responsável")+"\" placeholder=\""+i2l("Nome Responsável")+"\" id=\"id_nomeresp\"> <br>\n\
                   <input class=\"formdlg\" style=\" width:300px;\" title=\""+i2l("Telefone Responsável")+"\" placeholder=\""+i2l("Telefone Responsável")+"\" id=\"id_telefoneresp\"> <br>\n\
                   <input class=\"formdlg\" style=\" width:300px;\" title=\""+i2l("CPF Responsável")+"\" placeholder=\""+i2l("CPF Responsável")+"\" id=\"id_cpfresp\"> <br>\n\
                   <input class=\"formdlg\" style=\" width:300px;\" title=\""+i2l("Endereço Correspondência")+"\" placeholder=\""+i2l("Endereço Correspondência")+"\" id=\"id_address\"> <br>\n\
                   <input class=\"formdlg\" style=\" width:300px;\" title=\""+i2l("Cidade")+"\" placeholder=\""+i2l("Cidade")+"\" id=\"id_city\"> <br>\n\
                   <input class=\"formdlg\" style=\" width:300px;\" title=\""+i2l("País")+"\" placeholder=\""+i2l("País")+"\" id=\"id_country\"> <br>\n\
                   <textarea title=\""+i2l("Observações")+"\" class=\"formdlg\" rows='4' cols='50' style=\" width:300px;\" placeholder=\""+i2l("Observações")+"\" id=\"id_obs\">  </textarea> <br>\n\
                   "+i2l("Leis Municipais ou Cópias Alvarás Funcionamento:")+"<br><br>"+InputFilePdf("idFile1",dataFile[0])+"<br>"+InputFilePdf("idFile2",dataFile[1])+"<br>"+InputFilePdf("idFile3",dataFile[2]);
       dlgdata += " </div>\n\
                   <div id=\"tabs-2\">\n\
                        "+VetToTable([[i2l("Valor Hora:"), i2l("Fiscal Percentual Hora(%):")],
                                      ["<input class='formdlg' style=' width:80px;' type='text' id='idvalorhora'>", "<input class='formdlg' style=' width:80px;' type='text' id='idpercinfraction'>"],
                                      [i2l("Moeda:"), i2l("Linguagem:")],
                                      [SelectCurrency("idSelCurr"),SelectLang("idSelLang")],[]])+" \
                        "+SelectPayMethod+payDataCielo+payDataStripe+"\
                    </div>\n\
                    <div id=\"tabs-3\"> \n\
                        <p>"+i2l("País de operação:")+" <input readonly class='formdlg' style=' width:300px;' title=\""+i2l("País de operação do serviço de estacionamento, deve ser automaticamente selecionado ou indicado a partir do mapa")+".\" type='text' id='idcountry_operation'>  </p> \
                        <p>"+i2l("City of operation:")+" <input readonly class='formdlg' style=' width:300px;' title=\""+i2l("Cidade de operação do serviço de estacionamento, deve ser automaticamente selecionado ou indicado a partir do mapa")+"\" type='text' id='idcity_operation'> </p> \n\
                        <p>"+i2l("Define região de atuação:")+" <input onclick='DialogCreateProvider(\"hideRegion\");' type='checkbox' name='checkRegion' id='checkRegion'> </p> \ \n\
                        <div id=\"mapRegionsData\" > \
                           <p > "+ VetToTable( [["Lat", "Lon", "Raio (m)"],
                                                ["<input readonly class='formdlg' style=' width:100px;' title=\"\" type='text' id='idlat_operation'>",
                                                 "<input readonly class='formdlg' style=' width:100px;' title=\"\" type='text' id='idlon_operation'>",
                                                 "<input onkeyup='DialogCreateProvider(\"ChangeRadius\");' class='formdlg' style=' width:50px;' title=\"\" type='text' id='idradius_operation'>"],[]]) +" </p> \
                           <div id=\"mapRegions\" style=\" width:98%; min-height:400px; \"  > </div> \
                        </div>\
                    </div>\
                </div>\n ";
                  

   
   function ChangeRadius()
   {    
       temp = $("#idradius_operation").val();
       if (isNaN(temp))
       {
           $("#idradius_operation").val(mapMan.mapCircleRad);
           return;
       }   
       mapMan.mapCircleRad = temp;
       mapMan.mapCircle.setRadius(temp);   
   }       
       
       
   var execSim = function(id)
   {
         var controls = {};
         controls[0] = {id:"#id_email"       , valtype:["email"],   name:"Email"};
         controls[1] = {id:"#id_passwd"      , valtype:["notnull"], name:"Password"};
         controls[2] = {id:"#id_nomeprovider", valtype:["notnull"], name:"Nome Município ou Estabelecimento"};
         controls[3] = {id:"#id_nomeresp"    , valtype:["notnull"], name:"Nome Responsável"};
         controls[4] = {id:"#id_telefoneresp", valtype:["notnull"], name:"Telefone Responsável"};
         controls[5] = {id:"#id_cpfresp"     , valtype:["notnull"], name:"CPF Responsável"};
         controls[6] = {id:"#id_address"     , valtype:["notnull"], name:"Endereço Correspondência"};
         controls[7] = {id:"#id_city"        , valtype:["notnull"], name:"Cidade"};
         controls[8] = {id:"#id_country"     , valtype:["notnull"], name:"País"};
         controls[9] = {id:"#id_obs"         , valtype:["notnull"], name:"Observações"};
         
        if($('#id_email').val()!==$('#id_emailcmp').val())      
        {
           ShowErrorToast("Emails informados são diferentes");
           return; 
        }    
            
        if(!ValidateControls(controls))
           return;

        dtHoraPedido = new Date();

        var datPark = { };

        datPark.email       = trim($('#id_email').val());     
        datPark.passwd = $('#id_passwd').val(); 
        datPark.nomeprovider = $('#id_nomeprovider').val();
        datPark.nomeresp    = $('#id_nomeresp').val();
        datPark.telefoneresp = $('#id_telefoneresp').val();
        datPark.cpfresp     = $('#id_cpfresp').val();
        datPark.address     = $('#id_address').val();
        datPark.passwd      = $('#id_passwd').val();
        datPark.city        = $('#id_city').val();
        datPark.obs         = $('#id_obs').val();
        datPark.country     = $('#id_country').val();
        datPark.countrycode = glbCountry;
        datPark.idvalorhora = adjustNumberDB($("#idvalorhora").val());
        datPark.idpercinfraction = adjustNumberDB($("#idpercinfraction").val());
        datPark.idSelCurr = $("#idSelCurr").val();
        datPark.idSelLang = $("#idSelLang").val();
        datPark.providerLogged = isLogged('ParkingFitProvider');
        
          
        if(GetCheckBoxState("#checkRegion")) 
           datPark.useRangeOperation = 1;
        else
           datPark.useRangeOperation = 0;
        datPark.countryOperation = $("#idcountry_operation").val();
        datPark.cityOperation = $("#idcity_operation").val();
        datPark.latOperation = $("#idlat_operation").val();    
        datPark.lonOperation = $("#idlon_operation").val();  
        datPark.rangeOperation = $("#idradius_operation").val();  

        datPark.cielomerid        = trim($('#id_cielomerid').val());
        datPark.cielomerkey        = trim($('#id_cielomerkey').val());
 
        datPark.dataFile    = JSON.stringify(dataFile);
        
         if(PostSendProviderCreation(datPark))
         {
            DialogProvider();
            // $( LastActiveDlg ).dialog("destroy");
         }
         else
         {
         // $( this ).dialog("destroy");
         }
   }

    ////////////////////////////////////////////////////////////////////////////
    func_update = function ()
    {
        $('#id_address').val(glbAddress);
        $('#id_city').val(glbCity);
        $('#id_country').val(glbCountryLong);
        $('#idcountry_operation').val(glbCountry);
        $('#idcity_operation').val(glbCity);  
        console.log("SetupMap() on geolocation");
        SetupMap();
        SetupMapMove();
        hideRegion();
    }
   ////////////////////////////////////////////////////////////////////////////
   function SetupDialog()
   { 
        JqueryDlgAcceptEnter(LastActiveDlg,execSim);

        $("#tabs").tabs();
        
        $( "#selStripe" ).click(function() 
        {  
           $("#payDataCielo").hide();
           $("#payDataStripe").show();
        });

        $( "#selCielo" ).click(function() 
        {
           $("#payDataCielo").show();
           $("#payDataStripe").hide();
        });
        
        SetUITransparent(); 
        $("#idSelCurr").selectmenu();  
        $("#idSelLang").selectmenu(); 
        $('#idSelCurr').val('BRL');
        $("#idSelCurr").selectmenu("refresh");
        $('#idvalorhora').val('3,50');
        $('#idpercinfraction').val('5');
        
        $( document ).tooltip();

        LoadProviderData();
        
        if(mapMan.map===null)
          GetAllLocationData(func_update);
                
        

        SetupInputFilePdf("idFile1",dataFile[0]);
        SetupInputFilePdf("idFile2",dataFile[1]);
        SetupInputFilePdf("idFile3",dataFile[2]);  
   }

   dwidth = "350px";
   if(isTestMobile())
   {
       
       // dwidth = $(window).width()+"px";
       dwidth = "100%";
       dposition = { my: 'top', at: 'top+48' }; 
   }
   else     
       dposition = { my: "center", at: "center", of: window };    

    var buttons = [,];
    buttons['OK'] = i2l('Ok');
    buttons['Cancel'] = i2l('Cancela');

    var buttonArray = {};
    buttonArray[buttons['OK']] = function() {
        //Set OK function here
        execSim(LastActiveDlg);
    };
    buttonArray[buttons['Cancel']] = function() {
        //Set Cancel function here
        ParkingFitHomePage();
    };




   $( dlgdata ).dialog({
      position: dposition,   
      resizable: true,
      height: "auto",
      width: dwidth,
      modal: false,
      open: function() { SetupDialog() },  
      buttons: buttonArray
  });
}
////////////////////////////////////////////////////////////////////////////////
function PostSendProviderCreation(datPark)
{   
    
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    
    json_str = JSON.stringify(datPark);
    // alert("Size json: "+json_str.length);
    ok_pay=false;
    fdatProcess = function(data) 
    {   // alert(data.response); 
        // alert(data.period); 
        if(data.response==="ProviderCreatedOk")
        {
           ShowInfoToast("Provedor criado, email de ativação em breve será enviado ",1); 
           ok_pay=true;
           return;
        }
        if(data.response==="ProviderUpdatedOk")
        {
           ShowInfoToast("Provedor atualizado ",0); 
           ok_pay=true;
           return;
        }
        if(data.response==="AlreadyExists")
        {
           ShowInfoToast("Email já cadastrado a outro provedor",1); 
           ok_pay=false;
           return;
        }
        else
        {
          ShowErrorToast("Falha criação do provedor");
          ok_pay=false;
        }
    };
    execAjax(url,{ type:"ParkCreateProvider",json:json_str},false,fdatProcess); 
    return ok_pay;
}
////////////////////////////////////////////////////////////////////////////////
function LoadTableWork()  
{
    provider = isLogged('ParkingFitProvider'); 
    $("#id_tabela").html(SqlToTable("select idCar as `"+i2l("Placa")+"`,Address,DateTimePark as `"+i2l("Data Estacionamento")+"`,dateinfraction as `"+i2l("Data Infração")+"` from ParkingDataPenalty where DATE_FORMAT(dateinfraction,'%Y-%m-%d')='"+ConvertDate2DB($("#datework").val())+"' and emailstaff='"+selStaff+"' "));
   
    sql = "select DATE_FORMAT(dateinfraction,'%Y-%m') as `"+i2l("Data")+"`,count(*) as `"+i2l("Número Infrações")+"`, sum(staffMoney) as `"+i2l("Pagamento Mensal")+"` from ParkingDataPenalty where  emailstaff='"+selStaff+"'  group by DATE_FORMAT(dateinfraction,'%Y-%m');";
    $("#id_tabMontlyRel").html(SqlToTable(sql));
}
//////////////////////////////////////////////////////////////////////////////// 
function GetPFitProviderFromStaff(staff)
{
    sql = "select emailProvider from ParkingProviderStaff where email='"+staff+"'";
    return GetSqlValue(sql);
}
////////////////////////////////////////////////////////////////////////////////
var selStaff;
function DialogProviderStaff(context)
{
   if(typeof context === 'undefined' ||  context === '' ||  context === null)
   {    
      CreateFormBackground();
   }
   if(context === 'ParkingFitStaff')
   {
       provider = GetPFitProviderFromStaff(isLogged('ParkingFitStaff'));
       
   }    
   
   id="dlgprovstaff";
   LastActiveDlg="#"+id;
   $("#dlgprovstaff").dialog("destroy");

   title = i2l("Gerencia Fiscais");
   dlgdata  = "<div id=\""+id+"\" title=\""+title+"\">";
   dlgdata += "<select name=\"staff\" id=\"staff\" style=\" width:300px;\" ></select><br>\
                <div id=\"tabs\" >  <ul> \
                <li><a href=\"#tabs-1\">"+i2l("Dados")+"</a></li> \
                <li><a href=\"#tabs-2\">"+i2l("Trabalho")+"</a></li> \
                <li><a href=\"#tabs-3\">"+i2l("Mensal")+"</a></li> \
                </ul>";
   dlgdata += "<div id=\"tabs-1\">\n\
                 "+i2l("Email")+":<input class=\"formdlg\" style=\" width:300px;\" placeholder=\""+i2l("Email")+"\" id=\"id_email\"> <br>\n\
                 "+i2l("Password")+":<input class=\"formdlg\" type=\"password\" style=\" width:300px;\" placeholder=\""+i2l("Password")+"\" id=\"id_passwd\"> <br>\n\
                 "+i2l("Telefone")+":<input class=\"formdlg\"  style=\" width:300px;\" placeholder=\""+i2l("Telefone")+"\" id=\"id_phone\"> <br>\n\
                 "+i2l("CPF")+":<input class=\"formdlg\"  style=\" width:300px;\" placeholder=\""+i2l("CPF")+"\" id=\"id_cardid\"> <br>\n\
                 "+i2l("Banco")+":<input class=\"formdlg\" style=\" width:300px;\" placeholder=\""+i2l("Banco")+"\" id=\"id_bank\"> <br>\n\
                 "+i2l("Agência")+":<input class=\"formdlg\" style=\" width:300px;\" placeholder=\""+i2l("Agência")+"\" id=\"id_agency\"> <br>\n\
                 "+i2l("Conta")+":<input class=\"formdlg\" style=\" width:300px;\" placeholder=\""+i2l("Conta")+"\" id=\"id_account\"> <br>\n\
               </div>\n\
               <div id=\"tabs-2\">\n\
                 <p>"+i2l("Data")+": <input onchange='LoadTableWork()' class='formdlg' style=' width:80px;' type='text' id='datework'></p> \n\
                 <div id=\"id_tabela\"></div>\n\
               </div> \
               <div id=\"tabs-3\">\
                 <div id=\"id_tabMontlyRel\"></div>\
               </div>";
   dlgdata += "</div>"; // end tabs 
   dlgdata += " </div>"; // end dlg
   

   
   var execSim = function(isNovo)
   {
       
         // Print2PrinterElem("#sql_table","Relatório produção fiscal");
         
         var controls = {};
         controls[0] = {id:"#id_email"       ,valtype:["email"],   name:"Email"};
         controls[1] = {id:"#id_passwd"      ,valtype:["notnull"], name:"Password"};
         controls[2] = {id:"#id_phone"      ,valtype:["notnull"], name:"Fone"};
         controls[3] = {id:"#id_cardid"      ,valtype:["notnull"], name:"CPF"};
         controls[4] = {id:"#id_bank"        ,valtype:["notnull"], name:"Banco"};
         controls[5] = {id:"#id_agency"      ,valtype:["notnull"], name:"Agência"};
         controls[6] = {id:"#id_account"     ,valtype:["notnull"], name:"Conta"};
         
         if(!ValidateControls(controls))
           return;
       
        providerEmail = isLogged();

        var dataSql = { };    
        
        dataSql[0 ] = {Name:"email"     ,Value:$('#id_email').val(),   Type:""   };
        dataSql[1 ] = {Name:"password"  ,Value:$('#id_passwd').val(),  Type:""   };
        dataSql[2 ] = {Name:"Phone"     ,Value:$('#id_phone').val(),   Type:""   };
        dataSql[3 ] = {Name:"Card_id"   ,Value:$('#id_cardid').val(),  Type:""   };
        dataSql[4 ] = {Name:"Bank"      ,Value:$('#id_bank').val(),    Type:""   };
        dataSql[5 ] = {Name:"Agency"    ,Value:$('#id_agency').val(),  Type:""   };
        dataSql[6 ] = {Name:"Account"   ,Value:$('#id_account').val(), Type:""   };
        dataSql[7 ] = {Name:"emailProvider"     ,Value:providerEmail,   Type:""   };
       


        
//        idvalorhora
//        idpercinfraction
//        idSelCurr
//        idSelLang
        
        if(CheckExistence("select email from ParkingProviderStaff where  emailProvider='"+providerEmail+"' and email='"+$('#id_email').val()+"' "))
        {    
            ShowInfoToast("Staff já existe",1);
            return;
        }    
        if(isNovo===false)
        {    
           sql = SqlUpdate("ParkingProviderStaff",dataSql," emailProvider='"+providerEmail+"' and email='"+$('#id_email').val()+"' "); 
       }
       else
        {    
           sql = SqlInsert("ParkingProviderStaff",dataSql);  
       }   
        // alert(sql);
        
        fdatProcess = function(data)  
        {   
            // alert(data.response); 
            // alert(data.period); 
            if(data.response==="SqlResult")
            {
               if(isNovo) 
               {    
                   $( LastActiveDlg ).dialog("destroy");
                   DialogProviderStaff();
                   ShowInfoToast("Novo staff criado",1);
                   creatStaffFase=0;
               }
               else
               {    
                  ShowInfoToast("Atualizou - "+selStaff,1);  
               }   
            }

        };     
         if(PostExecSql(sql,fdatProcess))
         {
            // DialogProvider();
            // $( LastActiveDlg ).dialog("destroy");
         }
         else
         {
         // $( this ).dialog("destroy");
         }
   };
   
   var StaffApaga = function ()
   {
        fdatProcess = function(data)  
        {   
            // alert(data.response); 
            // alert(data.period); 
            if(data.response==="SqlResult")
            {
               ShowInfoToast("Apagou - "+selStaff,1); 
               $( LastActiveDlg ).dialog("destroy");
               DialogProviderStaff();
            }

        }; 
       sql = "delete from ParkingProviderStaff where email='"+selStaff+"' and emailProvider='"+isLogged()+"' "; 
       PostExecSql(sql,fdatProcess);
   };
   
   var stafftimer;
   function LoadStaff()
   {
        // alert("LoadStaff()");
        fdatLoadStaffProcess = function(data) 
        {   
            // alert(data.response); 
            // alert(data.period); 
            if(data.response==="SqlResult")
            {
               i=0;             
               while(typeof data.sql[i]!=='undefined')
               {                      
                   $("#staff").append("<option  val="+i+">"+data.sql[i].email+"</option>");
                   i++;
               }
            }

        };
       
       sql = "";
       if(context === 'ParkingFitStaff')
       {    
          staff = isLogged('ParkingFitStaff'); 
          provider = GetPFitProviderFromStaff(staff); 
          
          sql="select * from ParkingProviderStaff where `emailProvider`='"+provider+"' and `email`='"+staff+"' ;";
       }   
       else  
       {
          provider = isLogged(); 
          sql="select * from ParkingProviderStaff where `emailProvider`='"+provider+"' ;";
       }   
       
       PostExecSql(sql,fdatLoadStaffProcess);
       
       $("#staff").selectmenu();
       
       
       function checkStaff()
       {
           if(selStaff!==$( "#staff option:selected" ).text())
           {
               console.log( $( "#staff option:selected" ).text());
               selStaff = $( "#staff option:selected" ).text();
               LoadSelStaffData(selStaff);
           }
       };
       
       // $('.ui-button:contains(Ok)').hide();
       stafftimer = setInterval(checkStaff, 500);
   }
   
   function LoadSelStaffData(selStaff)
   {
       if(trim(selStaff)==="")
            return;
       fdatProcessLoadSelStaffData = function(data) 
       {   
            // alert(data.response); 
            // alert(data.period); 
            if(data.response==="SqlResult")
            {
                i=0;
                if(typeof data.sql[i]==='undefined')
                    return;
                
                $('#id_email').val(data.sql[i].email);   
                $('#id_passwd').val(data.sql[i].password); 
                $('#id_phone').val(data.sql[i].Phone );  
                $('#id_cardid').val(data.sql[i].Card_id); 
                $('#id_bank').val(data.sql[i].Bank);   
                $('#id_agency').val(data.sql[i].Agency);
                $('#id_account').val(data.sql[i].Account);
                // select idCar,Address,DateTimePark as `Data Estacionamento`,dateinfraction as `Data Infração` from ParkingDataPenalty where DATE_FORMAT(dateinfraction,'%Y-%m-%d')='2017-04-12' and emailstaff='andrefiscal@teste.com'  
                LoadTableWork();
                
            }
      }; 
      
       if(context === 'ParkingFitStaff')
       {    
           ///staff = isLogged('ParkingFitStaff'); 
           provider = GetPFitProviderFromStaff(staff); 
          
           sql = "select email,password,Phone,Card_id,Bank,Agency,Account from ParkingProviderStaff where emailProvider='"+provider+"' and email='"+selStaff+"'";  
       }   
       else  
       {
           provider = isLogged('ParkingFitProvider');
           sql = "select email,password,Phone,Card_id,Bank,Agency,Account from ParkingProviderStaff where emailProvider='"+provider+"' and email='"+selStaff+"'";  
       }   
      
      PostExecSql(sql,fdatProcessLoadSelStaffData);
   }
   
   function destroyTimer()
   {
      clearInterval(stafftimer);
   }
   function SetupDialog()
   { 
        // SetUITransparent(); 
        $( document ).tooltip();
        $( "#tabs" ).tabs();
        
        if(typeof context === 'undefined' ||  context === '' ||  context === null)
        {  
            SetUITransparent();
        }
        else
        {
            SetUI_Black();
        } 
        
        $(LastActiveDlg).css('padding','0 0');
        LoadStaff();
        $( "#datework" ).datepicker({ dateFormat: "dd/mm/yy"});
        $( "#datework" ).val(GetDateLocale());
       
       
        LoadTableWork();
//        sql = "select idCar das `Placa`,Address,DateTimePark as `Data Estacionamento`,dateinfraction as `Data Infração` from ParkingDataPenalty where emailstaff='"+selStaff+"' ";
//        $("#id_tabela").html(SqlToTable(sql));
//  
//        sql = "select DATE_FORMAT(dateinfraction,'%Y-%m') as Data,count(*) as `Número Infrações`, sum(staffMoney) as `Pagamento Mensal` from ParkingDataPenalty where  emailstaff='"+selStaff+"'  group by DATE_FORMAT(dateinfraction,'%Y-%m');";
//        $("#id_tabMontlyRel").html(SqlToTable(sql));
       
       
       // select DATE_FORMAT(dateinfraction,'%Y-%m'),count(*) as `Número Infrações`, sum(staffMoney) as Valor from ParkingDataPenalty where  emailstaff='andre@teste.com' group by emailstaff;
        // $("#id_tabMontlyRel").printThis();
        
        JqueryDlgAcceptEnter(LastActiveDlg,execSim);
        creatStaffFase=0;
        $("#staff-button").show();  
        $('.ui-button:contains(Atualiza)').show();
        $('.ui-button:contains(Apaga)').show();
        
        if(context === 'ParkingFitStaff')
        {
           $('.ui-button:contains('+i2l('Novo')+')').hide();
           $('.ui-button:contains('+i2l('Atualiza')+')').hide();
           $('.ui-button:contains('+i2l('Apaga')+')').hide();
        }    
//        PutInputMask("#id_placa","AAA-9999");      
//        PutInputMask("#id_dtexp","99/9999"); 
//        PutInputMask("#id_cvc","999"); 
//        PutInputMask("#id_card","9999.9999.9999.9999");      
 
        bufcontrols = ['#id_email','#id_passwd','#id_phone','#id_cardid','#id_bank',   
                       '#id_agency','#id_account'];
        ActionOnControls(bufcontrols,"readonly");
        
        PutInputMask("#id_phone","(99)9999999999w");       
 
//                $('#id_email').val(data.sql[i].email);   
//                $('#id_passwd').val(data.sql[i].password); 
//                $('#id_phone').val(data.sql[i].Phone );  
//                $('#id_cardid').val(data.sql[i].Card_id); 
//                $('#id_bank').val(data.sql[i].Bank);   
//                $('#id_agency').val(data.sql[i].Agency);
//                $('#id_account').val(data.sql[i].Account);       
         LoadSelStaffData(selStaff)
   }
    
    var creatStaffFase=0;
    function ManageNewStaff()
    {
        if(creatStaffFase===0)
        {
            bufcontrols = ['#id_email','#id_passwd','#id_phone','#id_cardid','#id_bank',   
                       '#id_agency','#id_account'];
            ActionOnControls(bufcontrols,"notreadonly");
        
            $('#id_email').val("");   
            $('#id_passwd').val("");    
            $('#id_phone').val("");    
            $('#id_cardid').val("");   
            $('#id_bank').val("");   
            $('#id_agency').val("");   
            $('#id_account').val("");  
            
            $("#staff-button").hide();
            $('.ui-button:contains('+i2l('Atualiza')+')').hide();
            $('.ui-button:contains('+i2l('Apaga')+')').hide();
            creatStaffFase=1;
            return;
        }  
        if(creatStaffFase===1)
        {
            execSim(true);
        }
    }
   dwidth = "auto";
   dposition = {  };
   if(isTestMobile())
   {
       
       // dwidth = $(window).width()+"px";
       dwidth = "100%";
       dposition = { my: 'top left', at: 'top+48' }; 
   }

    var buttons = [,];
    buttons['Novo'] = i2l('Novo');
    buttons['Atualiza'] = i2l('Atualiza');
    buttons['Apaga'] = i2l('Atualiza');
    buttons['Fecha'] = i2l('Fecha');


    var buttonArray = {};
    buttonArray[buttons['Novo']] = function() {
        // ParkingFitHomePage();
        ManageNewStaff();
        // execSim(true);
        // DialogProvider();
    };
    buttonArray[buttons['Atualiza']] = function() {
       execSim(false);
    };
    buttonArray[buttons['Apaga']] = function() {
       ConfirmDialog(i2l("Apaga fiscal: ")+selStaff+"?",StaffApaga);
    };
    buttonArray[buttons['Fecha']] = function() {
        $( LastActiveDlg ).dialog("destroy");
        if(typeof context === 'undefined' ||  context === '' ||  context === null)
           ParkingFitHomePage();  
    };



   $( dlgdata ).dialog({
      position: dposition,   
      resizable: true,
      height: "auto",
      width: dwidth,
      modal: false,
      open: function() { SetupDialog() },  
      buttons:buttonArray
  });
}
////////////////////////////////////////////////////////////////////////////////
var userEmail=null;
var Application=null;
var AppContext=null;
function DialogNewCard(context,refreshfunc)
{
   // CreateFormBackground();
   if(context==="ParkingFitProvider")
   {
      userEmail=isLogged('ParkingFitProvider');
      Application = "ParkingFit";
      AppContext = "CardProvider";
   }
   if(context==="ParkingFitUser")
   {
      userEmail=isLogged('ParkingFitUser');
      Application = "ParkingFit";
      AppContext = "CardProvider";
   }
   
   
   
   id="dlgNewCard";
   LastActiveDlg="#"+id;
   
   // "select  `emailUser`,`Aplication`,`AppContext`,`CardNumber`,`Holder`,`CardToken`,`Brand` from `PaymentCardsStored`;";
   title = i2l("Novo Cartão");
   dlgdata  = "<div id=\""+id+"\" title=\""+title+"\">";
          dlgdata += GenerateInputCardData();
   dlgdata += " </div>"; // end dlg
   
   var execSim = function()
   {
       
        var controls = {};
        controls[0] = {id: "#id_card", valtype: ["notnull","creditcard"], name: "Número cartão"};
        controls[1] = {id: "#id_cvc", valtype: ["notnull", "int"], name: "Codigo cartão"};
        controls[2] = {id: "#id_dtexp", valtype: ["notnull"], name: "Data expiração cartão"};
        controls[3] = {id: "#id_nome", valtype: ["notnull"], name: "Nome no cartão"};


        if (!ValidateControls(controls))
            return;
        
//        if(CheckExistence("select email from `PaymentCardsStored` where  emailUser='"+userEmail+"' and email='"+$('#id_email').val()+"' "))
//        {    
//            ShowInfoToast("Cartão já cadastrado",1);
//            return;
//        }    
        
        var datCard = {};

        datCard.userEmail = userEmail; 
        datCard.Application = Application;
        datCard.AppContext = AppContext;
        datCard.namecard = $('#id_nome').val();
        datCard.cardnumber = $('#id_card').val();
        datCard.brand = $('#idflag').attr('alt');
        datCard.cvc = $('#id_cvc').val();
        datCard.dtexp = $('#id_dtexp').val(); 
        datCard.dthorapedido = GetDateTime();
        
        if (context==="ParkingFitUser")
        {
            // TODO --- Acertos pagamento passar merchand provider email 
            // setCookie("glbSelectedProvider", $('#id_providersel').val(), 1000);
            datCard.PaymentMerchantUserId = GetCieloMerchantId("ParkingFitProvider",getCookie("glbSelectedProvider"));
            datCard.PaymentMerchantUserKey = GetCieloMerchantKey("ParkingFitProvider",getCookie("glbSelectedProvider"));
        }
        if (context === "ParkingFitProvider")
        {
            datCard.PaymentMerchantUserId = GetCieloMerchantId("System");
            datCard.PaymentMerchantUserKey = GetCieloMerchantKey("System");
        }        
        
        fdatProcess = function(data)  
        {   
            // alert(data.response); 
            // alert(data.period); 
            if(data.response==="PaymentCardRegistered")
            {
               if(refreshfunc !== undefined)
                   refreshfunc();
               
               // DialogProviderStaff();
               ShowInfoToast("Cartão cadastrado",1);
               $( LastActiveDlg ).dialog("destroy");  
            }
            if(data.response==="ErrorGetCardToken")
               ShowErrorToast("Erro solicitando token do cartão",1);

        };     
        PostGenericAction("RegisterNewPaymentCard",datCard,fdatProcess);

   };
   

   
   function SetupDialog()
   { 
        // SetUITransparent(); 
        $( document ).tooltip();
        // $( "#id_tabcards" ).tabs();
        // SetUITransparent(); 
        // $(LastActiveDlg).css('padding','0 0');
       
        $('.ui-widget-overlay').css('opacity', '0.1');
        JqueryDlgAcceptEnter(LastActiveDlg,execSim);
        InputCardDataFlag();

   }
    
   dwidth = "auto";
   dposition = {  }; 
   if(isTestMobile())
   {
       
       // dwidth = $(window).width()+"px";
       dwidth = "100%";
       dposition = { my: 'top left', at: 'top+48' }; 
   }

   $( dlgdata ).dialog({
      position: dposition,   
      resizable: true,
      height: "auto",
      width: dwidth,
      modal: true,
      open: function() { SetupDialog() },  
      buttons: 
      {
        "Ok": function() 
        {
            // ParkingFitHomePage();
            execSim();
            // DialogProvider();
            
        },
        "Cancela": function() 
        {
            $( LastActiveDlg ).dialog("destroy");  
        }
      }
  });
}
////////////////////////////////////////////////////////////////////////////////
LoadPaymentCards = function()
{
   $("#id_listcards").html(ListPaymentData(sql));

};  
////////////////////////////////////////////////////////////////////////////////
var vetDataPay=null;
function ListPaymentData(sql)
{
    sql = "select `CardNumber`,`ExpirationDate`, cvc,`Brand`,`Holder`, `DefaultSel` as `Padrão` from `PaymentCardsStored` where emailUser='"+userEmail+"' and Application='"+Application+"' and AppContext='"+AppContext+"' order by Data desc;";
    vetDataPay = SqlToVet(sql);
    
    il = 1;
    ic = 5;
    while (typeof vetDataPay[il][ic] !== 'undefined')
    {
        if(vetDataPay[il][ic]==="1")
        {
           vetDataPay[il][ic] =  "<img onclick=\"checkItemPaymentData(\'"+vetDataPay[il][0]+"\',\'"+vetDataPay[il][2]+"\',\'"+vetDataPay[il][3]+"\');\"  src=\"/ChildMonitor/Img/checked.png\" alt=\"Default\" height=\"22\" width=\"22\"> ";
        }   
        else
        {
           vetDataPay[il][ic]  =  "<img onclick=\"checkItemPaymentData(\'"+vetDataPay[il][0]+"\',\'"+vetDataPay[il][2]+"\',\'"+vetDataPay[il][3]+"\');\" src=\"/ChildMonitor/Img/unchecked.png\" alt=\"\" height=\"22\" width=\"22\"> ";
           vetDataPay[il][ic] +=  "<img onclick=\"DeleteThisCard(\'"+vetDataPay[il][0]+"\',\'"+vetDataPay[il][2]+"\',\'"+vetDataPay[il][3]+"\');\" src=\"/ChildMonitor/Img/Trash.png\" alt=\"\" height=\"22\" width=\"22\"> ";
           
        }  
            
       il++;
    }
    output = VetToTableInt("id_paymentlist",vetDataPay," width:98%; height:150px; ");
    
    VetToTableIntClickOnDataFunc["id_paymentlist"] = function(data,data2)
    {
        
        $("#id_cardpay").html(data);
        $("#id_expdt").html(data2);  

    };  
    return output;
}
////////////////////////////////////////////////////////////////////////////////
function checkItemPaymentData(CardNumber,cvc,Brand)
{
    sql = "update `PaymentCardsStored` set `DefaultSel`=0 where emailUser='"+userEmail+"' and Application='"+Application+"' and AppContext='"+AppContext+"';";
    PostExecSql(sql,null);
    sql = "update `PaymentCardsStored` set `DefaultSel`=1 where Brand='"+Brand+"' and  cvc='"+cvc+"' and CardNumber='"+CardNumber+"' and  emailUser='"+userEmail+"' and Application='"+Application+"' and AppContext='"+AppContext+"';";
    PostExecSql(sql,null);
    LoadPaymentCards();
}
////////////////////////////////////////////////////////////////////////////////
var UseCieloSandBox=0;
merchantidSB="684b2bb4-be71-4477-a54b-a2222123e529";
merchantkeySB="LUUSGHMKUNIBZTIQFKSDPQZGEPTTUIXXTLUAOHJF";
merchantid="7de82818-1601-4b12-aa61-d662e4fa9e2e"; // 7de82818-1601-4b12-aa61-d662e4fa9e2e
merchantkey="5uNMRYPLKbQTVIV0TgnzhjdIC15mgv1rjoGk6MMb"; // 5uNMRYPLKbQTVIV0TgnzhjdIC15mgv1rjoGk6MMb
////////////////////////////////////////////////////////////////////////////////
function GetCieloMerchantId(MerchantType,userMerchant)
{
    if(MerchantType==="ParkingFitProvider")
    {
        // select cielomerid from ParkingProvider where email ='andre@teste.com';
        sql = "select cielomerid from ParkingProvider where email ='"+userMerchant+"';";
        szbuffer = GetSqlValue(sql);
        if(szbuffer!=="")
        {    
           return szbuffer;
        }
        else // Se voltou vazio registra para mim
        {
            if(UseCieloSandBox===1)
               return merchantidSB;
            else
               return merchantid;             
        }
    }
    if(MerchantType==="System")
    {
        if(UseCieloSandBox===1)
           return merchantidSB;
        else
           return merchantid; 
    }    
}
////////////////////////////////////////////////////////////////////////////////
function GetCieloMerchantKey(MerchantType,userMerchant)
{       
    if(MerchantType==="ParkingFitProvider")
    {
        sql = "select cielomerkey from ParkingProvider where email ='"+userMerchant+"';";
        szbuffer = GetSqlValue(sql);
        if(szbuffer!=="")
        {    
           return szbuffer;
        }
        else // Se voltou vazio registra para mim
        {
            if(UseCieloSandBox===1)
               return merchantkeySB; 
            else
               return merchantkey;              
        }
    }
    if(MerchantType==="System")
    {
        if(UseCieloSandBox===1)
           return merchantkeySB; 
        else
           return merchantkey;   
    }    
}
////////////////////////////////////////////////////////////////////////////////
function DeleteThisCard(CardNumber,cvc,Brand)
{
    DeleteThisCardFunc = function()
    {
        sql = "delete from `PaymentCardsStored` where Brand='" + Brand + "' and  cvc='" + cvc + "' and CardNumber='" + CardNumber + "' and  emailUser='" + userEmail + "' and Application='" + Application + "' and AppContext='" + AppContext + "';";
        PostExecSql(sql, null);
        LoadPaymentCards();
    };  
    ConfirmDialog(i2l("Confirma o apagamento do cartão: ")+CardNumber+" ?", DeleteThisCardFunc);
}
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
function GenerateOrderID(Application)
{
    return GetDateTime()+"_"+Application; 
}
////////////////////////////////////////////////////////////////////////////////

function DialogManagePaymentMethods(context,paymentData)
{
   // CreateFormBackground();
   if(context==="ParkingFitProvider")
   {
      userEmail=isLogged('ParkingFitProvider');
      Application = "ParkingFit";
      AppContext = "CardProvider";
   }
   if(context==="ParkingFitUser")
   {
      userEmail=isLogged('ParkingFitUser');
      Application = "ParkingFit";
      AppContext = "CardProvider";
   }
   //////////////////////////////////////////
   function ManageFastPayment(paymentData)
   {
       // paymentData.val2pay;
       // paymentData.date;  
       // paymentData.CallBackFunc; 
       if(paymentData===undefined || paymentData===null)
       {
           $("#id_selcard").hide();
           return;
       }
       $("#id_pag").button();
       $("#id_pag").click(function()
       {
           datSend = {};

           datSend.userEmail = userEmail;
           datSend.Application = "ParkingFit";
           datSend.AppContext = "CardProvider";
           datSend.PaymentProvider = "Cielo";
           if(paymentData.PayForWho==="Provider")
           {
              datSend.PaymentMerchantUserId = GetCieloMerchantId("ParkingFitProvider",paymentData.MerchantToPay);
              datSend.PaymentMerchantUserKey = GetCieloMerchantKey("ParkingFitProvider",paymentData.MerchantToPay);
           }
           if(paymentData.PayForWho==="System")
           {
               datSend.PaymentMerchantUserId = GetCieloMerchantId("System");
               datSend.PaymentMerchantUserKey = GetCieloMerchantKey("System");             
           }
           
           datSend.date = GetDateTime();     
           datSend.orderid = paymentData.orderid; 
           datSend.namecustomer = paymentData.namecustomer; 
           datSend.type = "CreditCard" ; //"CreditCard"  
           datSend.amount = paymentData.val2pay*100;
           datSend.SoftDescriptor =  paymentData.SoftDescriptor;
         
           cardNum = $("#id_cardpay").html();
           expDt = $("#id_expdt").html();
           
           sql = "select `CardToken` from `PaymentCardsStored` where `ExpirationDate` = '"+expDt+"' and `CardNumber` = '"+cardNum+"' and emailUser='"+userEmail+"' and Application='"+Application+"' and AppContext='"+AppContext+"';";
           datSend.cardtoken = GetSqlValue(sql);
           sql = "select `cvc` from `PaymentCardsStored` where `ExpirationDate` = '"+expDt+"' and `CardNumber` = '"+cardNum+"' and emailUser='"+userEmail+"' and Application='"+Application+"' and AppContext='"+AppContext+"';";
           datSend.cvc = GetSqlValue(sql);
           sql = "select `Brand` from `PaymentCardsStored` where `ExpirationDate` = '"+expDt+"' and `CardNumber` = '"+cardNum+"' and emailUser='"+userEmail+"' and Application='"+Application+"' and AppContext='"+AppContext+"';";
           datSend.brand = GetSqlValue(sql);
           
           PostGenericAction("PayValueWithCardToken",datSend,paymentData.CallBackFunc);
       }); 
   }
   //////////////////////////////////////////
   id="dlgprovpagmethod";
   LastActiveDlg="#"+id;
   
   
   // "select  `emailUser`,`Aplication`,`AppContext`,`CardNumber`,`Holder`,`CardToken`,`Brand` from `PaymentCardsStored`;";
   title = i2l("Métodos de Pagamento");
   // cardslist =   CreateJqTabs("#id_tabcards", [["Cartões"],[]], [[SqlToTable(sql)],[]])  
  
   val2pay=0;
   if(paymentData!==undefined && paymentData!==null)
   {
       val2pay = paymentData.val2pay;
   }    
   
   sql = "select `CardNumber` from `PaymentCardsStored` where `DefaultSel`=1 and emailUser='"+userEmail+"' and Application='"+Application+"' and AppContext='"+AppContext+"';";
   sqlDate = "select `ExpirationDate` from `PaymentCardsStored` where `DefaultSel`=1 and emailUser='"+userEmail+"' and Application='"+Application+"' and AppContext='"+AppContext+"';";
   selectedCard = "<div id=\"id_selcard\" style=\" background-color:rgb(25, 0, 0);\"  >Pagar o valor <label id='id_valpay'>"+val2pay+"</label> Reais, com o cartão:<br><b><label id='id_cardpay'>"+GetSqlValue(sql)+"</label>   -   <label id='id_expdt'>"+GetSqlValue(sqlDate)+"</label></b>?<br><br> <button id=\"id_pag\">Ok</button> </div><br>";
   
    
   sql = "select `CardNumber`, cvc,`Brand`,`Holder`,`DefaultSel` as `Padrão` from `PaymentCardsStored` where emailUser='"+userEmail+"' and Application='"+Application+"' and AppContext='"+AppContext+"';";
   cardlist =   ListPaymentData(sql);

   dlgdata  = "<div id=\""+id+"\" title=\""+title+"\">";
        dlgdata += selectedCard;
        dlgdata += "<div id=\"id_listcards\"  >"+cardlist+"</div>"; 
   dlgdata += " </div>"; // end dlg
   
   var execSim = function(isNovo)
   {
       
   };
   

   
   function SetupDialog()
   { 
        // SetUITransparent(); 
        $( document ).tooltip();
        // $( "#id_tabcards" ).tabs();
        $('.ui-widget-overlay').css('opacity', '0.1');
        // This dialog has some white controls on background. Should be better became less transparente
        $('.ui-widget-content').css('background', 'rgba(0,0,0,0.99)');
        
        ManageFastPayment(paymentData);
   }
    
   dwidth = "auto";
   var dposition = {};
   if(isTestMobile())
   {
       
       // dwidth = $(window).width()+"px";
       dwidth = "100%";
       dposition = { my: 'top left', at: 'top+48' }; 
   }

   $( dlgdata ).dialog({
      position: dposition,   
      resizable: true,
      height: "auto",
      width: dwidth,
      modal: true,
      open: function() { SetupDialog(); },  
      buttons: 
      {
        "Novo": function() 
        {
            DialogNewCard(context,LoadPaymentCards);
        },
        "Cancela": function() 
        {
            $( "#dlgprovpagmethod" ).dialog("destroy");  
        }
      }
  });
}
