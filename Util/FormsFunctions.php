<?php
//////////////////////////////////////////////////////////
// App theme
// $AppName = "MyFather";
// $AppTheme= "myfather";

// $AppTheme = "green";
// $AppName = "ChildMonitor";  

// $AppName = "TruckMonitor";
// $AppTheme= "truck";

// $AppName = "EstControl";
// $AppTheme= "truck";
   
// $AppName = "Sua Lista Escolar";
// $AppTheme= "listaescolar";

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once 'Util.php';

////////////////////////////////////////////////////////////////////////////////

function DefaultOpenHtmlBody()
{
    header('Origin: *');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');  
    $zoom=1.0;
    if(CheckAndroidChrome())
    {    
        $zoom=1.0;
        // echo strtolower($_SERVER['HTTP_USER_AGENT'])."<br>";
    }    
    
    $icon_web = "/ChildMonitor/Img/EarthLow.png";
    if($_SESSION['stme']=="TTM" )
    {
        $title = "Truck Track Monitor GPS  Professional Tracking for Trucks or Vehicles and Search Freight Uber Vehicles Artifical Inteligence Load Find TruckLoad Procura Cargas e Fretes Pelo Mundo Around The World";
    }
    if($_SESSION['stme']=="VAG" )
    {
        $title = "Parking Fit gerenciamento de estacionamentos rotativos para prefeituras e empresas, com geolocalização e pagamento por cartão dos clientes integrados com Cielo e Paypall. Apps on GooglePlay e AppleStore Pay Lot Short Term parking. ";
        $icon_web = "/ChildMonitor/Img/ParkingFitLogoLow.png";
    } 
    if($_SESSION['stme']=="CDM" )
    {
        $title = "Child Monitor GPS and Take Care of Your Infants Robust tracking to parents take care of your infants Ready to track vital signals ";
    }    
    if($_SESSION['stme']=="MMF" )
    {
        $title = "MyFather Alzheimer GPS and Take Care Robust tracking to parents take care Alzheimer Ready to track vital signals ";
    } 
    // <body style="$backchocweave" > // background roxo
    GetThemeByParameter();  
    
    $witdh = $_COOKIE['scr_width'];
    $height = $_COOKIE['scr_height'];
    
    include_once 'FormAnimations.php'; 
    $buffer = <<< EOT
        <html style=" zoom: $zoom; -moz-transform: scale($zoom); -moz-transform-origin: 0 0" manifest="/ChildMonitor/appcache.mf" >
        <head>
        <meta charset="UTF-8">
        <title>$title</title>
        <meta name="keywords" content="$title" >   
        <meta name="Description" content="$title" >  
        <link rel="icon" href="$icon_web">
        </head>
 
            
        <body  style=" $backcarbonfiber $backParkingFit " >   
            
        <script src="/ChildMonitor/Util/Util.min.js"></script>  
            
        <script type="text/javascript">

            
            
            function AdjustBackgroundParkingFit()
            {
                if((window.innerWidth/window.innerHeight)>1)
                {
                    document.body.style.backgroundImage = "url('/ChildMonitor/Img/BackBlurPFit.jpg')"; 
                    // document.body.style.backgroundSize="cover";
                }
                // else
                //    document.body.style.backgroundImage = "url('/ChildMonitor/Img/BackBlurPFitMobile.jpg')";         
                
            }
            AdjustBackgroundParkingFit();
            
        </script>
    
        <style>
             /* $backanimblue */      
        </style>   
EOT;
    return $buffer;
}
////////////////////////////////////////////////////////////////////////////////
function DefaultCloseHtmlBody()
{
   // echo GoogleAd();
    $buffer = <<< EOT
        </body>
        </html>
EOT;
    return $buffer;
}
////////////////////////////////////////////////////////////////////////////////
// http://www.geobytes.com/free-ajax-cities-jsonp-api // Cities autocomplete
// http://nominatim.openstreetmap.org/reverse?format=xml&lat=52.5487429714954&lon=-1.81602098644987&zoom=18&addressdetails=1
// http://nominatim.openstreetmap.org/reverse?format=json&lat=52.5487429714954&lon=-1.81602098644987&zoom=18&addressdetails=1
function TableTrucksType()
{
$buffer = <<< EOT
    <table class="table table-hover">
    <thead>
    <tr>
    <th width="165">Type</th>
    <th width="350">Axels</th>
    <th width="120">Weight (T)</th>
    <th width="235"></th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td>Light vehicles</td>
    <td>2 axles</td>
    <td>3,5</td>
    <td><img class="alignnone size-full wp-image-661" alt="light-trucks" src="/ChildMonitor/Img/light-trucks.png" width="150" height="26" /></td>
    </tr>
    <tr>
    <td>Small trucks</td>
    <td>2 axles</td>
    <td>3,5 &#8211; 7,5</td>
    <td><img class="alignnone size-full wp-image-664" alt="small-trucks" src="/ChildMonitor/Img/small-trucks.png" width="150" height="29" /></td>
    </tr>
    <tr>
    <td>Large trucks</td>
    <td>2 axles</td>
    <td>7,5 &#8211; 18</td>
    <td><img class="alignnone size-full wp-image-667" alt="large-trucks-2axles" src="/ChildMonitor/Img/large-trucks-2axles.png" width="150" height="29" /></td>
    </tr>
    <tr>
    <td rowspan="11">Multi Axle trucks</td>
    <td>3 axles</td>
    <td>25 &#8211; 26</td>
    <td><img class="alignnone size-full wp-image-670" alt="large-trucks-3axles" src="/ChildMonitor/Img/large-trucks-3axles.png" width="150" height="29" /></td>
    </tr>
    <tr>
    <td>3 axles</td>
    <td>26</td>
    <td><img class="alignnone size-full wp-image-672" alt="large-trucks-3axles2" src="/ChildMonitor/Img/large-trucks-3axles2.png" width="150" height="29" /></td>
    </tr>
    <tr>
    <td>4 axles</td>
    <td>30 &#8211; 32</td>
    <td><img class="alignnone size-full wp-image-675" alt="large-trucks-4axles" src="/ChildMonitor/Img/large-trucks-4axles.png" width="150" height="29" /></td>
    </tr>
    <tr>
    <td>4 axles</td>
    <td>36 &#8211; 38</td>
    <td><img class="alignnone size-full wp-image-676" alt="large-trucks-4axles2" src="/ChildMonitor/Img/large-trucks-4axles2.png" width="150" height="29" /></td>
    </tr>
    <tr>
    <td>4 axles</td>
    <td>36 &#8211; 38</td>
    <td><img class="alignnone size-full wp-image-682" alt="large-trucks-4axles3" src="/ChildMonitor/Img/large-trucks-4axles3.png" width="150" height="29" /></td>
    </tr>
    <tr>
    <td>5 axles</td>
    <td>40</td>
    <td><img class="alignnone size-full wp-image-694" alt="large-trucks-5axles" src="/ChildMonitor/Img/large-trucks-5axles.png" width="150" height="29" /></td>
    </tr>
    <tr>
    <td>5 axles</td>
    <td>40</td>
    <td><img class="alignnone size-full wp-image-695" alt="large-trucks-5axles1" src="/ChildMonitor/Img/large-trucks-5axles1.png" width="150" height="29" /></td>
    </tr>
    <tr>
    <td>6 axles</td>
    <td>41</td>
    <td><img class="alignnone size-full wp-image-696" alt="large-trucks-6axles" src="/ChildMonitor/Img/large-trucks-6axles.png" width="150" height="29" /></td>
    </tr>
    <tr>
    <td>6 axles</td>
    <td>41</td>
    <td><img class="alignnone size-full wp-image-697" alt="large-trucks-6axles1" src="/ChildMonitor/Img/large-trucks-6axles1.png" width="150" height="29" /></td>
    </tr>
    <tr>
    <td>5 or 6 axles</td>
    <td>44</td>
    <td><img class="alignnone size-full wp-image-698" alt="large-trucks-6axles2" src="/ChildMonitor/Img/large-trucks-6axles2.png" width="150" height="29" /></td>
    </tr>
    <tr>
    <td>6 axles</td>
    <td>44</td>
    <td><img class="alignnone size-full wp-image-699" alt="large-trucks-6axles3" src="/ChildMonitor/Img/large-trucks-6axles3.png" width="150" height="29" /></td>
    </tr>
    </tbody>
    </table> 
EOT;
    return $buffer;        
}
////////////////////////////////////////////////////////////////////////////////
function formCreateLogUser($emailLog,$passwordLog)
{
    $dataBuffer = <<<EOT
     <tx class="txb"> User must be informed to create new freight</tx><br><br>        
     <tx class="tx"> Login</tx><br><br>             

        <div id="tabs">
          <ul>
            <li><a href="#tabs-1">Login</a></li>
            <li><a href="#tabs-2">New User</a></li>
          </ul>
          <div id="tabs-1">
            <tx class="tx"> Email</tx> <br>
            <input class="form-control" style="width:98%;" id="emailLog" name="emailLog" value='$emailLog'><br>
            <tx class="tx"> Password</tx> <br>
            <input class="form-control" style="width:98%;" id="passwordLog" type="password" name="passwordLog" value='$passwordLog'><br><br>           
          </div>
          <div id="tabs-2">
            <tx class="tx"> Email</tx> <br>
            <input class="form-control" style="width:98%;" id="emailLogNew" name="emailLogNew" value='$emailLog'><br>
            <tx class="tx"> Repeat Email</tx> <br>
            <input class="form-control" style="width:98%;" id="emailLogRep" name="emailLogRep" value=''><br>            
            <tx class="tx"> Password</tx> <br>
            <input class="form-control" style="width:98%;" id="passwordLogNew" type="password" name="passwordLogNew" value=''><br><br>           
            <tx class="tx"> Repeat Password</tx> <br>
            <input class="form-control" style="width:98%;" id="passwordLogRep" type="password" name="passwordLogRep" value=''><br><br>           
        </div>

        </div>  
    
                 <br>
        <table style="width:90%">
        <tr>
         <td align="left" > <input  style="width:100px; " id="submitPrevious2" name="submitPrevious2" type=""  readonly  value="Previous" /> <input  style=" " id="submitCreateFreight" name="submitCreateFreight" type=""  readonly value="Create Freight" /> <br></td>
        </tr>       
        </table>                  
        <br><br><br>

       <script>
          $( "#tabs" ).tabs();  
       </script>   
            
EOT;
    return $dataBuffer;
}
////////////////////////////////////////////////////////////////////////////////
/*
drop table IF EXISTS `Freight`;
 
CREATE TABLE IF NOT EXISTS `Freight` (
  `Id` bigint(20) NOT NULL, 
  `UserId` bigint(20) NOT NULL,
  `UserTruck` bigint(20) NOT NULL,
  `UserVehicle` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `addresscmp` varchar(255) NOT NULL,
  `DateInclusion` datetime NOT NULL,
  `Active` tinyint(1) NOT NULL,
  `Description` varchar(512) NOT NULL,
  `Country` varchar(512) NOT NULL,
  `CountryDest` varchar(512) NOT NULL,
  `City` varchar(512) NOT NULL,
  `CityDest` varchar(512) NOT NULL,
  `Lat` double NOT NULL,
  `Long` double NOT NULL,
  `LatDest` double NOT NULL,
  `LongDest` double NOT NULL,
  `Weight` double NOT NULL,
  `Value` double NOT NULL, 
  `TruckType` varchar(50) NOT NULL,
  `TruckPlat` varchar(50) NOT NULL,
  `Phone1` varchar(50) NOT NULL,
  `Phone2` varchar(50) NOT NULL,
  `Phone3` varchar(50) NOT NULL,
  `Comments` varchar(512) NOT NULL,
  `Img1` blob,
  `Img2` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
  */
///////////////////////////////////////////////////////////////////////////////
function toFloat(&$var)
{
    if ($var == "") {
        $var = 0.0;
    }
    return($var);
}
////////////////////////////////////////////////////////////////////////////////
function GetNewFreightID() {
    $query = "select max(id) from Freight";
    $resultado = db_query($query);
    $max = $resultado[1][0];
    return($max+1);
}

///////////////////////////////////////////////////////////////////////////////
function InsertFreightOnDatabase()
{
    $UserId =  $_SESSION['userId'];

    $idActivFormHid =$_POST['idActivFormHid'];

    $emailLog=trim(strtolower( $_POST['emailLogHid']));
    $passwordLog=hash('sha256', $_POST['passwordLogHid']);


    $idDescriptionHid =$_POST['idDescriptionHid'];
    $idCountryHid=$_POST['idCountryHid'];
    $idCountryDestHid=$_POST['idCountryDestHid'];
    $idCityHid=$_POST['idCityHid'];
    $idCityDestHid=$_POST['idCityDestHid'];  

    $idLatHid = $_POST['idLatHid'];
    $idLonHid = $_POST['idLonHid'];          
    $idLatDestHid = $_POST['idLatDestHid'];
    $idLonDestHid = $_POST['idLonDestHid']; 

    $idWeightHid = $_POST['idWeightHid'];
    $idValueHid = $_POST['idValueHid'];

    $idTruckTypeHid = $_POST['idTruckTypeHid'];
    $idTruckPlatHid = $_POST['idTruckPlatHid'];
    $idPhone1Hid = $_POST['idPhone1Hid'];
    $idPhone2Hid = $_POST['idPhone2Hid'];
    $idPhone3Hid = $_POST['idPhone3Hid'];

    $idCommentsHid = $_POST['idCommentsHid'];

    

    $idImg1Hid = $_POST['idImg1Hid'];
    $idImg2Hid = $_POST['idImg2Hid'];
    
    $DateInclusion = GetDateTime(); 
    $freightId = GetNewFreightID();
    $_SESSION['freightId']=$freightId;

    $DataRegs = "(Id,        `UserId`, `UserTruck`, `UserVehicle`, `address`, `addresscmp`, `DateInclusion`, `Active`, `Description`, `Country`,             `CountryDest`,      `City`,      `CityDest`,      `Lat`,       `Long`,     `LatDest`,      `LongDest`,      `Weight`,      `Value`,      `TruckType`,     `TruckPlat`,      `Phone1`,       `Phone2`,  `Phone3`,          `Comments`, `Img1`) ";
    $Values =   "($freightId,'$UserId','','','','',                                          '$DateInclusion',1,        '$idDescriptionHid','$idCountryHid', '$idCountryDestHid','$idCityHid','$idCityDestHid',$idLatHid,$idLonHid,$idLatDestHid,$idLonDestHid,$idWeightHid,$idValueHid,'$idTruckTypeHid','$idTruckPlatHid','$idPhone1Hid','$idPhone2Hid','$idPhone3Hid','$idCommentsHid','$idImg1Hid') ";

    $sql= "INSERT INTO `Freight` $DataRegs VALUES $Values ";        

    // $dataLogin = $sql;            
   // echo ($sql);
    $resultado=db_query($sql);

    $sql= "UPDATE `Freight` set `Img2`='$idImg2Hid' WHERE Id=$freightId";        

    // $dataLogin = $sql;            
    // echo ($sql);
    $resultado=db_query($sql);

}   
///////////////////////////////////////////////////////////////////////////////
function NewUser()
{
    $emailLogNew=trim(strtolower( $_POST['emailLogNewHid']));
    $passwordLogNew=$_POST['passwordLogNewHid'];
    $emailLogRep=trim(strtolower( $_POST['emailLogRepHid']));
    $passwordLogRep=$_POST['passwordLogRepHid'];

    
    
    
    $email= $emailLogNew;
    $passwordLog=$passwordLogNew;
    $address="";
    $addresscmp="";
    $country="";             

    $sql="select id from User where email='$email'";   
    $resultado=db_query($sql);
    if($resultado[1][0]=="")
    { 

        $date = GetDateTime();
        $sql="insert into User (email,fbId,password,address,addresscmp,country,DateInclusion,Active) values ('$email','$fbId','$passwordLog','$address','$addresscmp','$country','$date',0)";   
        // dbg($sql);
        $resultado=db_query($sql);
        // dbg($resultado[1][0]);
        
        ActivationEmailSimple($email,$passwordLog);
        
        $AlertMsg = "User creation succeded,<br>wait for activation message<br>on email ".$email.".";
        ShowJSToast($AlertMsg);
        return true;
    } 
    else 
    {
        ShowJSToast("User already exists");
        return false;
    }     
}
///////////////////////////////////////////////////////////////////////////////
function ShowJSToast($msg)
{
    echo <<<EOT
    <script>
    // $( "<div>Teste</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"});
    // $().toastmessage('showErrorToast', "$msg");
    toastr["error"]("$msg")
    </script>
EOT;
}
///////////////////////////////////////////////////////////////////////////////
function ManageLogin(&$dataLogin,$emailLog,$passwordLog,$formTargetSubmit)
{
    if(!isLogged())
    {
        $emailLogNew=trim(strtolower( $_POST['emailLogNewHid']));
        $passwordLogNew=$_POST['passwordLogNewHid'];
        $emailLogRep=trim(strtolower( $_POST['emailLogRepHid']));
        $passwordLogRep=$_POST['passwordLogRepHid'];
    
        $dataLogin = formCreateLogUser($emailLog,$passwordLog);
        
        if($emailLog=="" && $emailLogNew=="")
        {    
            ShowJSToast("User name and password nedded");
            return;
        }
        
        if($emailLog!="")
        {    
            // if($_POST['ActionProg']=='Login')
            $passwordLogHash=hash('sha256', $passwordLog);
            $sql="select id,fbId,Active from User where email='$emailLog' and password='$passwordLogHash'";
            // else
            //     $sql="select id,fbId from User where email='$emailLog' ";
            $resultado=db_query($sql);
            // $dataLogin .= $sql."<br>";
            // $dataLogin .= $resultado[1][0]."<br>";
            if($resultado[1][0]=="")
            {    
                ShowJSToast("Invalid user");
                return false;        
            }    
            session_start();
            $_SESSION['userId']=$resultado[1][0];
            $_SESSION['userEmail']=$emailLog;
            $_SESSION['fbId']=$resultado[1][1];        

            $userId=$resultado[1][0];
            $userEmail=$emailLog;
            $fbId=$resultado[1][1];        

            echo <<<EOE

            <script type="text/javascript">
            if (navigator.cookieEnabled)
            { 
                 document.cookie = "userId=$userId";
                 document.cookie = "userEmail=$userEmail";
                 document.cookie = "fbId=$fbId";                     
            } 
            CopyDataForm();        
            $( "#$formTargetSubmit" ).submit();         
            </script>
EOE;
            $dataLogin="";
            return true;        
        }
        
        if($emailLogNew!=$emailLogRep)
        {
            ShowJSToast("Emails must be equal");
            return false;        
        }
        
        if($passwordLogNew!=$passwordLogRep)
        {
            ShowJSToast("Passwords must be equal");
            return false;        
        }
        
        if($emailLogNew!="" && ($emailLogNew==$emailLogRep && $passwordLogNew==$passwordLogRep))
        {    
            if(NewUser())
            {    
               ShowJSToast("New user created ");
               $dataLogin="";
               return true;
            }   
            else
            {    
               ShowJSToast("Can´t create user");
               return false;
            }
               
        }    
        
    }
    return true;
}
///////////////////////////////////////////////////////////////////////////////
function HeadersMap()
{        
    $dataSearch = <<<EOT
    <link rel="stylesheet" href="/ChildMonitor/Util/leaflet.css" />
    <link rel="stylesheet" href="/ChildMonitor/Util/leaflet-routing-machine.css" />        
    <script src="/ChildMonitor/Util/leaflet.js"></script>      

    <script src="/ChildMonitor/Util/Control.Geocoder.js"></script>        
    <script src="/ChildMonitor/Util/leaflet-routing-machine.min.js"></script>
            

EOT;
    return $dataSearch;
}
///////////////////////////////////////////////////////////////////////////////
function formNewFreght1(&$dataSearch,$idDescriptionHid,$idCityHid,$idCityDestHid)
{
        
    $align="left";
    $selCountryOr=SelectCountry("idCountry");
    $selCountryDest=SelectCountry("idCountryDest");
    
    $headersM = HeadersMap();
    
    $dataSearch = <<<EOT
    $headersM
            
    <tx class="txb"> New Freight </tx> <br><br> 



    <tx class="tx"> Description *</tx> <br><br>        
    <input class="form-control" style="width:85%;" name="idDescription"  id="idDescription" name="field" type="text" value="$idDescriptionHid" required >  <br> <br>  

       <table style="width:90%">
          <tr>
            <td> <tx class="tx"> Country  *</tx> <br><br>  $selCountryOr <br><br></td>
            <td><tx class="tx"> Country Destiny * </tx> <br><br> $selCountryDest  <br><br> </td>
          </tr> 
          <tr>
            <td> <tx class="tx"> City, Address  *</tx> <br><br>  <input class="form-control" style="text-transform: uppercase" type="text" name="idCity" value="$idCityHid" id="idCity"/> <br><br></td>
            <td><tx class="tx"> City, Address Destiny *</tx> <br><br> <input class="form-control"  style="text-transform: uppercase" type="text" name="idCityDest" value="$idCityDestHid" id="idCityDest"/>  <br><br> </td>
          </tr>  
          <!-- For posterior use   
              <tr>
                <td> <tx class="tx"> Adress Number *</tx> <br><br>  <input class="form-control" type="text" name="idNumber" value="" id="idNumber"/> <br><br></td>
                <td><tx class="tx"> Street </tx> <br><br> <input class="form-control"  type="text" name="idStreet" value="" id="idStreet"/>  <br><br> </td>
              </tr>
              <tr>
                <td><tx class="tx"> District </tx> <br><br> <input class="form-control"  type="text" name="idDistrict" value="" id="idDistrict"/>  <br><br></td>
                <td><tx class="tx"> City </tx> <br><br> <input  class="form-control ff_elem" type="text" name="ff_nm_from[]" value="" id="f_elem_city"/> <br><br> </td>
              </tr>
              <tr>
                <td><tx class="tx"> Country </tx> <br><br> <input class="form-control"  type="text" name="idCountry" value="" id="idCountry"/>  <br><br></td>
                <td><tx class="tx"> Post Code </tx> <br><br> <input class="form-control"  type="text" name="idPostCode" value="" id="idPostCode"/>  <br><br></td>
              </tr>  

           <tr>
            <td colspan="50" > <div style="width:96.2%; height:200px; "  id="map"> </div> <br>  </td>
           </tr>  
           -->   
        </table>                  
        <input  style="width:100px; " id="submitNext" name="submitNext" type="" readonly value="Next" />    
        <tx class="tx"> <br><br><br>

    <style type="text/css">
    .ui-menu .ui-menu-item a,.ui-menu .ui-menu-item a.ui-state-hover, .ui-menu .ui-menu-item a.ui-state-active {
            font-weight: normal;
            margin: -1px;
            text-align:left;
            font-size:14px;
            }
    .ui-autocomplete-loading { background: white url("/ChildMonitor/Img/spinner16x16.gif") right center no-repeat; }
    </style>



    <script src="/ChildMonitor/Util/Util.js"></script> 
    <script type="text/javascript">


    $( "#submitNext" ).button();
    // $('#idCountry').selectToAutocomplete({id_control:"id_tempa"});        


    $("#submitNext").on("click", function() 
    {
       $('#idDescription').val(capitalizeFirstLetter($('#idDescription').val().trim()));     
       ok=1;     
       if($('#idDescription').val()=="") 
       { 
            //  $().toastmessage('showWarningToast', "No description");
            toastr["warning"]("No description")
            ok=0;
       } 
       /*
       if( VerifyRoute()==0  ) 
       { 
            // $( "<div>Invalid Route</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"}).fadeOut( 1000 );
            // $().toastmessage('showWarningToast', "Invalid Route");
            toastr["warning"]("Invalid Route")
            ok=0;
       } 
       */     
       if(ok==1)
       {    
           CopyDataForm();   
           CopyRouteLatLong();
           $("#idActivFormHid").val(2); 
           $("form:first").submit();
       }     
    });           

    latBrowser=0.0;    
    lngBrowser=0.0;    
    ////////////////////////////////////////////////////////////////////////
    function zgetLocation() 
    {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(zshowPosition);
        } else {
            // x.innerHTML = "Geolocation is not supported by this browser.";
            $( "<div>Start Geolocation!</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"});
        }
    }
    ////////////////////////////////////////////////////////////////////////
    function zshowPosition(position) 
    {
        // alert("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);
        latBrowser=position.coords.latitude;    
        lngBrowser=position.coords.longitude;   
        GetAjaxAddress(latBrowser,lngBrowser);

    } 
    ///////////////////////////////////////////////////////////////////////        
    function VerifyRoute() 
    {                
        if($('#idCountry').val()=="" || $('#idCountryDest').val()=="")    
        {
            return 0;
        }    

        coords1 = GetAjaxCoords($('#idCity').val(),$('#idCountry').val());
        coords2 = GetAjaxCoords($('#idCityDest').val(),$('#idCountryDest').val());

        if (coords1[0] === undefined || coords2[0] === undefined) 
        { 
            return 0;
        }   
        return 1;    
    }        
    ///////////////////////////////////////////////////////////////////////
    var map = null;    
    function ShowRoute() 
    {
        // alert("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);



        // coords1 = GetAjaxCoords("NITEROI","BRASIL");
        // coords2 = GetAjaxCoords("MANAUS","BRASIL");

        if( $('#idCountry').val()=="" || $('#idCountryDest').val()=="")    
        {

            map.remove(); 
            $('#map').hide();
            return;
        }    

        coords1 = GetAjaxCoords($('#idCity').val(),$('#idCountry').val());
        coords2 = GetAjaxCoords($('#idCityDest').val(),$('#idCountryDest').val());

        if (coords1[0] === undefined || coords2[0] === undefined) 
        { 
            // alert("Invalid address");
            map.remove(); 
            $('#map').hide();

            return;
        }    

        lat1 =  coords1[0].lat;
        lon1 =  coords1[0].lon;
        lat2 =  coords2[0].lat;
        lon2 =  coords2[0].lon;

        console.log("coords1 ["+lat1+" "+lon1+"]");    
        console.log("coords2 ["+lat2+" "+lon2+"]");       

        $('#map').show(0);



        if (map != undefined) { map.remove(); }
        map = L.map('map');

        southWest = L.latLng(lat1, lon1);
        northEast = L.latLng(lat2, lon2);
        bounds = L.latLngBounds(southWest, northEast);

        // L.marker([latBrowser, lngBrowser]).addTo(map);
        L.tileLayer( 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright" title="OpenStreetMap" target="_blank">OpenStreetMap</a> contributors | Tiles Courtesy of <a href="http://www.mapquest.com/" title="MapQuest" target="_blank">MapQuest</a> <img src="http://developer.mapquest.com/content/osm/mq_logo.png" width="16" height="16">',
            subdomains: ['otile1','otile2','otile3','otile4']
        }).addTo( map );  

        map.setView(new L.LatLng(lat1, lon1), 8);

        map.panInsideBounds(bounds);

        L.Routing.control({
            waypoints: [
                L.latLng(lat1, lon1),
                L.latLng(lat2, lon2)
            ]
        }).addTo(map);  


    }   
    ////////////////////////////////////////////////////////////////////////
    function fezBlur() 
    {
        // alert('blur');
        // ShowRoute();    
    }        
    ////////////////////////////////////////////////////////////////////////

    // $waitDialog
    // setTimeout(CloseWait, 1000);    


    // $('#map').hide();        
    // zgetLocation(); 

    // $('#idCountry').val("Brazil");   

    // coords = GetAjaxCoords("NITEROI","BRASIL");
    // console.log("--------- croods ["+coords[0].lat+"]");    
    // alert("--------- croods ["+coords[0].lat+"]");    


    document.getElementById("idCountry").onblur= fezBlur;
    document.getElementById("idCountryDest").onblur= fezBlur;
    document.getElementById("idCity").onblur= fezBlur;
    document.getElementById("idCityDest").onblur= fezBlur;    

    document.getElementById("idCityDest").onblur


    // $('#map').show(0);  

    function CloseWait()
    {
        dlgWait.dialog("close");  
    }

    </script>

                
EOT;
}    
///////////////////////////////////////////////////////////////////////////////
function formSearchFreight()
{
    // Site de traçar rotas http://wiki.openstreetmap.org/wiki/YOURS
    formHeaders();
    $waitDialog = jsFormWait();

    $idActivFormHid =$_POST['idActivFormHid'];
    
    $emailLog=trim(strtolower( $_POST['emailLogHid']));
    $passwordLog=$_POST['passwordLogHid'];

    $emailLogNew=trim(strtolower( $_POST['emailLogNewHid']));
    $passwordLogNew=$_POST['passwordLogNewHid'];
    $emailLogRep=trim(strtolower( $_POST['emailLogRepHid']));
    $passwordLogRep=$_POST['passwordLogRepHid'];
    
    $idDescriptionHid =$_POST['idDescriptionHid'];
    $idCountryHid=$_POST['idCountryHid'];
    $idCountryDestHid=$_POST['idCountryDestHid'];
    $idCityHid=$_POST['idCityHid'];
    $idCityDestHid=$_POST['idCityDestHid'];  
    
    $idCityDestHid=$_POST['idCityDestHid'];  

    $idLatHid = $_POST['idLatHid']; $idLatHid = toFloat($idLatHid);
    $idLonHid = $_POST['idLonHid']; $idLonHid=toFloat($idLonHid);         
    $idLatDestHid = $_POST['idLatDestHid']; $idLatDestHid=toFloat($idLatDestHid);
    $idLonDestHid = $_POST['idLonDestHid']; $idLonDestHid=toFloat($idLonDestHid);
            
    $idWeightHid = $_POST['idWeightHid']; $idWeightHid= toFloat($idWeightHid);
    $idValueHid = $_POST['idValueHid']; $idValueHid=toFloat($idValueHid);
    
    $idTruckTypeHid = $_POST['idTruckTypeHid'];
    $idTruckPlatHid = $_POST['idTruckPlatHid'];
    $idPhone1Hid = $_POST['idPhone1Hid'];
    $idPhone2Hid = $_POST['idPhone2Hid'];
    $idPhone3Hid = $_POST['idPhone3Hid'];

    $idCommentsHid = $_POST['idCommentsHid'];
    $idImg1Hid = $_POST['idImg1Hid'];
    $idImg2Hid = $_POST['idImg2Hid'];
    // $Debug = "Debug submitNext2: ".$_GET['submitNext2']."<br>";
    
    if($idImg1Hid=="")
       $idImg1Hid  = "Img/img-photo.png";
    if($idImg2Hid=="")
       $idImg2Hid  = "Img/img-photo.png";
    

    
    
    
    echo <<<EOT
    
    <style>
    /* div container containing the form  */
    #searchContainer {
        margin:20px;
    }
    /* Style the search input field. */
    #field {
        // float:left; 
        width:40%; 
        height:30px; 
        line-height:27px;
        text-indent:10px; 
        font-family:arial, sans-serif; 
        font-size:1em; 
        color:#333; 
        background: #fff; 
        border:solid 1px #d9d9d9; 
        border-top:solid 1px #c0c0c0; 
        // border-right:none;
    }

    /* Syle the search button. Settings of line-height, font-size, text-indent used to hide submit value in IE */
    #submit {
        cursor:pointer; 
        width:34px; 
        height: 34px; 
        color: transparent;  
        background: url(Img/search.png) no-repeat #474444 center; 
        background-size: 20px 20px;
        border: 1px solid #373434; 
        // border-left:none;
    }
    /* Style the search button hover state */
    #submit:hover {
        background: url(Img/search.png) no-repeat center #373434; 
        background-size: 20px 20px;
        border: 1px solid #373434;
        // border-left:none;
    }
    #submitNew {
        cursor:pointer; 
        width:34px; 
        height: 34px; 
        color: transparent;  
        background: url(Img/white-plus-hi.png) no-repeat #474444 center; 
        background-size: 20px 20px;
        border: 1px solid #373434; 
        // border-left:none;
    }
    /* Style the search button hover state */
    #submitNew:hover {
        background: url(Img/white-plus-hi.png) no-repeat center #373434; 
        background-size: 20px 20px;
        border: 1px solid #373434;
        // border-left:none;
    }
  
    /* Syle the search button. Settings of line-height, font-size, text-indent used to hide submit value in IE */
    #submit {
        cursor:pointer; 
        width:34px; 
        height: 34px; 
        color: transparent;  
        background: url(Img/search.png) no-repeat #474444 center; 
        background-size: 20px 20px;
        border: 1px solid #373434; 
        // border-left:none;
    }
    /* Style the search button hover state */
    #submit:hover {
        background: url(Img/search.png) no-repeat center #373434; 
        background-size: 20px 20px;
        border: 1px solid #373434;
        // border-left:none;
    }
    #submitNew {
        cursor:pointer; 
        width:34px; 
        height: 34px; 
        color: transparent;  
        background: url(Img/white-plus-hi.png) no-repeat #474444 center; 
        background-size: 20px 20px;
        border: 1px solid #373434; 
        // border-left:none;
    }
    /* Style the search button hover state */
    #submitNew:hover {
        background: url(Img/white-plus-hi.png) no-repeat center #373434; 
        background-size: 20px 20px;
        border: 1px solid #373434;
        // border-left:none;
    }
    
    /* Clear floats */
    .fclear {clear:both}
    </style>
    
    <form id="formSearch" method="post" action="/ChildMonitor/index.php" > 
    
    <input type="hidden" id="idActivFormHid" name="idActivFormHid" value="$idActivFormHid">

    <input type="hidden" id="emailLogHid" name="emailLogHid" value="$emailLog">
    <input type="hidden" id="passwordLogHid" name="passwordLogHid" value="$passwordLog">
            
    <input type="hidden" id="emailLogNewHid" name="emailLogNewHid" value="$emailLogNew">
    <input type="hidden" id="emailLogRepHid" name="emailLogRepHid" value="$emailLogRep">
    <input type="hidden" id="passwordLogNewHid" name="passwordLogNewHid" value="$passwordLogNew">
    <input type="hidden" id="passwordLogRepHid" name="passwordLogRepHid" value="$passwordLogRep">
            
    <input type="hidden" id="idDescriptionHid" name="idDescriptionHid" value="$idDescriptionHid">
    <input type="hidden" id="idCountryHid" name="idCountryHid" value="$idCountryHid">
    <input type="hidden" id="idCountryDestHid" name="idCountryDestHid" value="$idCountryDestHid">
    <input type="hidden" id="idCityHid" name="idCityHid" value="$idCityHid" >
    <input type="hidden" id="idCityDestHid" name="idCityDestHid" value="$idCityDestHid">   

    <input type="hidden" id="idLatHid" name="idLatHid" value="$idLatHid">          
    <input type="hidden" id="idLatDestHid" name="idLatDestHid" value="$idLatDestHid">  
    <input type="hidden" id="idLonHid" name="idLonHid" value="$idLonHid">          
    <input type="hidden" id="idLonDestHid" name="idLonDestHid" value="$idLonDestHid">               
   
    <input type="hidden" id="idWeightHid" name="idWeightHid" value="$idWeightHid">
    <input type="hidden" id="idValueHid" name="idValueHid" value="$idValueHid">
    <input type="hidden" id="idTruckTypeHid" name="idTruckTypeHid" value="$idTruckTypeHid">
    <input type="hidden" id="idTruckPlatHid" name="idTruckPlatHid" value="$idTruckPlatHid">
    <input type="hidden" id="idPhone1Hid" name="idPhone1Hid" value="$idPhone1Hid">
    <input type="hidden" id="idPhone2Hid" name="idPhone2Hid" value="$idPhone2Hid">
    <input type="hidden" id="idPhone3Hid" name="idPhone3Hid" value="$idPhone3Hid">
   
    <input type="hidden" id="idCommentsHid" name="idCommentsHid" value="$idCommentsHid">
    <input type="hidden" id="idImg1Hid" name="idImg1Hid" value="$idImg1Hid">
    <input type="hidden" id="idImg2Hid" name="idImg2Hid" value="$idImg2Hid">       
        
        
    <script type="text/javascript">        
    ///////////////////////////////////////////////////////////////////    
    function CopyDataForm()
    {
        if($("#emailLog").val()!=undefined) $("#emailLogHid").val($("#emailLog").val());      
        if($("#passwordLog").val()!=undefined) $("#passwordLogHid").val($("#passwordLog").val());  


        if($("#emailLogNew").val()!=undefined) $("#emailLogNewHid").val($("#emailLogNew").val());      
        if($("#emailLogRep").val()!=undefined) $("#emailLogRepHid").val($("#emailLogRep").val());      
        if($("#passwordLogNew").val()!=undefined) $("#passwordLogNewHid").val($("#passwordLogNew").val());      
        if($("#passwordLogRep").val()!=undefined) $("#passwordLogRepHid").val($("#passwordLogRep").val());      
            
        if($("#idDescription").val()!=undefined) $("#idDescriptionHid").val($("#idDescription").val());      
        if($("#idCountry").val()!=undefined) $("#idCountryHid").val($("#idCountry").val());  
        if($("#idCountryDest").val()!=undefined) $("#idCountryDestHid").val($("#idCountryDest").val());  
        if($("#idCity").val()!=undefined) $("#idCityHid").val($("#idCity").val());  
        if($("#idCityDest").val()!=undefined) $("#idCityDestHid").val($("#idCityDest").val()); 
        
        if($("#idWeight").val()!=undefined) $("#idWeightHid").val($("#idWeight").val());  
        if($("#idValue").val()!=undefined) $("#idValueHid").val($("#idValue").val()); 
        if($("#idTruckType").val()!=undefined) $("#idTruckTypeHid").val($("#idTruckType").val()); 
        if($("#idTruckPlat").val()!=undefined) $("#idTruckPlatHid").val($("#idTruckPlat").val()); 
        if($("#idPhone1").val()!=undefined) $("#idPhone1Hid").val($("#idPhone1").val()); 
        if($("#idPhone2").val()!=undefined) $("#idPhone2Hid").val($("#idPhone2").val()); 
        if($("#idPhone3").val()!=undefined) $("#idPhone3Hid").val($("#idPhone3").val()); 
            
        if($("#idComments").val()!=undefined) $("#idCommentsHid").val($("#idComments").val()); 

        if($("#idImg1").val()!=undefined) $("#idImg1Hid").val(ImagetoDataURL("idImg1Show")); 
        if($("#idImg2").val()!=undefined) $("#idImg2Hid").val(ImagetoDataURL("idImg2Show"));  
        
        // if($("#idImg1").val()!=undefined)
        //   alert(ImagetoDataURL("idImg1Show"));   
            
    }
    ///////////////////////////////////////////////////////////////////    
    function CopyRouteLatLong()
    {           
            coords1 = GetAjaxCoords($('#idCity').val(),$('#idCountry').val());
            coords2 = GetAjaxCoords($('#idCityDest').val(),$('#idCountryDest').val());

            if (coords1[0] === undefined || coords2[0] === undefined) 
            { 
                return;
            }    
                
            lat1 =  coords1[0].lat;
            lon1 =  coords1[0].lon;
            lat2 =  coords2[0].lat;
            lon2 =  coords2[0].lon;   
             
            // alert(lat1);
            
            $("#idLatHid").val(lat1); 
            $("#idLonHid").val(lon1); 
            $("#idLatDestHid").val(lat2); 
            $("#idLonDestHid").val(lon2); 
    }        
    /////////////////////////////////////////////////////////////////////////
    function ImagetoDataURL(id)
    {
        // Create an image
        var img = document.createElement("img");
        img.src = document.getElementById(id).src;

        var canvas = document.createElement("canvas");
        canvas.width = document.getElementById(id).naturalWidth; 
        canvas.height = document.getElementById(id).naturalHeight;
            
        //var canvas = $("<canvas>", {"id":"testing"})[0];
        var ctx = canvas.getContext("2d");
        // ctx.drawImage(img, 0, 0,800,600);
        ctx.drawImage(img, 0, 0,document.getElementById(id).naturalWidth,document.getElementById(id).naturalHeight);
        // ctx.drawImage(img, 0, 0,800,800*(document.getElementById(id).naturalHeight/document.getElementById(id).naturalWidth));
         
        resample_hermite(canvas, canvas.width, canvas.height, 800, 800*(document.getElementById(id).naturalHeight/document.getElementById(id).naturalWidth))    
        var dataurl = canvas.toDataURL("image/jpeg", 1.0);
        return(dataurl); 
    }
    ///////////////////////////////////////////////////////////////////        
    function resample_hermite(canvas, W, H, W2, H2){
            var time1 = Date.now();
            W2 = Math.round(W2);
            H2 = Math.round(H2);
            var img = canvas.getContext("2d").getImageData(0, 0, W, H);
            var img2 = canvas.getContext("2d").getImageData(0, 0, W2, H2);
            var data = img.data;
            var data2 = img2.data;
            var ratio_w = W / W2;
            var ratio_h = H / H2;
            var ratio_w_half = Math.ceil(ratio_w/2);
            var ratio_h_half = Math.ceil(ratio_h/2);

            for(var j = 0; j < H2; j++){
                    for(var i = 0; i < W2; i++){
                            var x2 = (i + j*W2) * 4;
                            var weight = 0;
                            var weights = 0;
                            var weights_alpha = 0;
                            var gx_r = gx_g = gx_b = gx_a = 0;
                            var center_y = (j + 0.5) * ratio_h;
                            for(var yy = Math.floor(j * ratio_h); yy < (j + 1) * ratio_h; yy++){
                                    var dy = Math.abs(center_y - (yy + 0.5)) / ratio_h_half;
                                    var center_x = (i + 0.5) * ratio_w;
                                    var w0 = dy*dy //pre-calc part of w
                                    for(var xx = Math.floor(i * ratio_w); xx < (i + 1) * ratio_w; xx++){
                                            var dx = Math.abs(center_x - (xx + 0.5)) / ratio_w_half;
                                            var w = Math.sqrt(w0 + dx*dx);
                                            if(w >= -1 && w <= 1){
                                                    //hermite filter
                                                    weight = 2 * w*w*w - 3*w*w + 1;
                                                    if(weight > 0){
                                                            dx = 4*(xx + yy*W);
                                                            //alpha
                                                            gx_a += weight * data[dx + 3];
                                                            weights_alpha += weight;
                                                            //colors
                                                            if(data[dx + 3] < 255)
                                                                    weight = weight * data[dx + 3] / 250;
                                                            gx_r += weight * data[dx];
                                                            gx_g += weight * data[dx + 1];
                                                            gx_b += weight * data[dx + 2];
                                                            weights += weight;
                                                            }
                                                    }
                                            }		
                                    }
                            data2[x2]     = gx_r / weights;
                            data2[x2 + 1] = gx_g / weights;
                            data2[x2 + 2] = gx_b / weights;
                            data2[x2 + 3] = gx_a / weights_alpha;
                            }
                    }
            console.log("hermite = "+(Math.round(Date.now() - time1)/1000)+" s");
            canvas.getContext("2d").clearRect(0, 0, Math.max(W, W2), Math.max(H, H2));
        canvas.width = W2;
        canvas.height = H2;
            canvas.getContext("2d").putImageData(img2, 0, 0);
    }
    ///////////////////////////////////////////////////////////////////    
    </script>        
EOT;
    
    //dbg($_GET['submitNew']);
    // dbg($_GET['submitNext']);
    // Leaflet Routing Maxine 
    // http://www.liedman.net/leaflet-routing-machine/
    
    
    if($idActivFormHid==1)
    {
        formNewFreght1($dataSearch,$idDescriptionHid,$idCityHid,$idCityDestHid);
    }  
    else
    if($_GET['submitSearch']=="Search" )
    {
        $align="left";
        $dataSearch = <<<EOT
        <tx class="tx> Search </tx>
EOT;
    } 
    else
    if($idActivFormHid==2 )
    {
        /*
            Axels 	Weight (T) 	
            2 axles 	3,5 (T)	      light-trucks
            2 axles 	3,5 – 7,5 (T) small-trucks
            2 axles 	7,5 – 18 (T)  large-trucks-2axles
            3 axles 	25 – 26 (T)   large-trucks-3axles
            3 axles 	26 (T)	      large-trucks-3axles2
            4 axles 	30 – 32 (T)   large-trucks-4axles
            4 axles 	36 – 38 (T)   large-trucks-4axles2
            4 axles 	36 – 38 (T)   large-trucks-4axles3
            5 axles 	40 (T)	      large-trucks-5axles
            5 axles 	40 (T)	      large-trucks-5axles1
            6 axles 	41 (T)	      large-trucks-6axles
            6 axles 	41 (T)	      large-trucks-6axles1
            5 or 6 axles 	44 (T)	      large-trucks-6axles2
            6 axles 	44 (T)	      large-trucks-6axles3
         */
        $align="left";
        // $buffer = TableTrucksType();
        $dataSearch = <<<EOT
        <tx class="txb"> New Freight </tx> <br><br> 
      
         
        <style type="text/css">   
        select.icon-menu option {
        background-repeat:no-repeat;
        background-position:top left;
        background-size: 30px 30px;        
        padding-left:30px;
        }
        </style> 
            

                
                <table style="width:90%">
              <tr>
                <td> <tx class="tx"> Weight </tx><br><br> <input class="form-control" data-numeric="true" data-validate="required" data-message-required="Este campo é obrigatório." placeholder="0 kg" data-mask="99.999 kg" data-numeric="true"   type="text" name="idWeight"  id="idWeight" value="$idWeightHid" />  <br><br>  </td>
                 <td> <tx class="tx"> Value </tx><br><br> <input class="form-control" placeholder="0.00 $" data-numeric="true" style="width:98%; " type="text" name="idValue" id="idValue" value="$idValueHid" />  <br><br> </td>
             </tr>
              <tr>
                <td> <tx class="tx"> Truck Type </tx><br><br> <select name="idTruckType" id="idTruckType" >
                                                              <option selected="selected" >2 axles 3,5 (T) light trucks</option>
                                                              <option>2 axles 3,5-7,5 (T) small trucks</option>
                                                              <option>2 axles 7,5-18 (T) large trucks 2axles</option>
                                                              <option>3 axles 25-26 (T) large trucks 3axles</option>
                                                              <option>3 axles 26 (T) large trucks 3axles2</option>
                                                              <option>4 axles 30-32 (T) large trucks 4axles</option>
                                                              <option>4 axles 36-38 (T) large trucks 4axles2</option>
                                                              <option>4 axles 36-38 (T) large trucks 4axles3</option>
                                                              <option>5 axles 40 (T) large trucks 5axles</option>
                                                              <option>5 axles 40 (T) large trucks 5axles1</option>
                                                              <option>6 axles 41 (T) large trucks 6axles</option>
                                                              <option>6 axles 41 (T) large trucks 6axles1</option>
                                                              <option>5 or 6 axles 44 (T) large trucks 6axles2</option>
                                                              <option>6 axles 44 (T) large trucks 6axles3</option>
                                                            </select> 
                                             </td>
                   <td><tx class="tx"> Truck Platform </tx><br><br>     <select name="idTruckPlat" id="idTruckPlat" >
                                                  <option selected="selected" >Box bodies</option>
                                                  <option>Tippers</option>
                                                  <option >Curtain-siders</option>
                                                  <option>Temperature-controlled bodies</option>
                                                  <option >Flat Beds</option>
                                                  <option>Road tankers</option> 
                                                </select> 
                    </td>
                </tr>
                <tr>
              <td> <tx class="tx"> Contact Phone 1 </tx><br><br> <input class="form-control"  type="text" name="idPhone1" id="idPhone1" value="$idPhone1Hid" />  <br><br>  </td>
                <td><tx class="tx"> Contact Phone 2 </tx><br><br>  <input class="form-control"  type="text" name="idPhone2" id="idPhone2" value="$idPhone2Hid" />  <br><br> </td>
              </tr>   
               <tr>
                <td> <tx class="tx"> Contact Phone 3 </tx><br><br> <input class="form-control"  type="text" name="idPhone3" id="idPhone3" value="$idPhone3Hid" />  <br><br> </td>
              </tr>
            </table>                  
            <input  style="width:100px; " id="submitPrevious1" name="submitPrevious1" type=""  readonly  value="Previous" /><input  style="width:100px; " id="submitNext2" name="submitNext2" type=""  readonly  value="Next" /><br>    
            <br><br><br><br><br><br><br>  
                
            <script>  
             $("#submitPrevious1").button();    
             $("#submitPrevious1").on("click", function() 
             {
                  CopyDataForm(); 

                  $("#idActivFormHid").val(1);
                  $("form:first").submit();
             });         

   
             $("#submitNext2").button();   
             $("#submitNext2").on("click", function() 
             {
                   /* if($('#idNumber').val()=="" || $('#idDescription').val()=="" ) 
                   { 
                        $( "<div>Incomplete information</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"});
                        return;
                   } */
                 CopyDataForm();

                 $("#idActivFormHid").val(3);
                 $("form:first").submit();
             });         
                
             
                
                
            $( "#idTruckPlat" ).selectmenu(); 
            $( "#idTruckType" ).selectmenu(); 
            // $( "#idWeight" ).inputmask("999999.99 kg");  
               
            // $( "#idValue" ).inputmask("99.999,00 $"); 
            $( "#idPhone1" ).inputmask("99(99)99999-9999"); 
            $( "#idPhone2" ).inputmask("99(99)99999-9999"); 
            $( "#idPhone3" ).inputmask("99(99)99999-9999"); 
                
            ////////////////////////////////////////////////////////////////////    
            jQuery.fn.forceNumeric = function () {

                 return this.each(function () {
                     $(this).keydown(function (e) {
                         var key = e.which || e.keyCode;

                         if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                         // numbers   
                             key >= 48 && key <= 57 ||
                         // Numeric keypad
                             key >= 96 && key <= 105 ||
                         // comma, period and minus, . on keypad
                            key == 190 || key == 188 || key == 109 || key == 110 ||
                         // Backspace and Tab and Enter
                            key == 8 || key == 9 || key == 13 ||
                         // Home and End
                            key == 35 || key == 36 ||
                         // left and right arrows
                            key == 37 || key == 39 ||
                         // Del and Ins
                            key == 46 || key == 45)
                             return true;

                         return false;
                     });
                 });
             }   
                
             $( "#idWeight" ).forceNumeric(); 
             $( "#idValue" ).forceNumeric();    
            </script>   
               
EOT;
           
    }
    else
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    // Last windows freigth creation
    if($idActivFormHid==4)
    {
        if(ManageLogin($dataLogin,$emailLog,$passwordLog,"form:first"))
        {        
            InsertFreightOnDatabase();
            
           // SELECT `Id`, `UserId`, `UserTruck`, `UserVehicle`, `address`, `addresscmp`, `DateInclusion`, `Active`, `Description`, `Country`, `CountryDest`, `City`, `CityDest`, `Lat`, `Long`, `LatDest`, `LongDest`, `Weight`, `Value`, `TruckType`, `TruckPlat`, `Phone1`, `Phone2`, `Phone3`, `Comments`, `Img1`, `Img2` FROM `freight` WHERE 1
            
            
            $freightId = $_SESSION['freightId'];
            $sql = "SELECT  `DateInclusion`,`Id`,`Description`, `UserTruck`, `UserVehicle`, `address`, `addresscmp`,    `Country`, `CountryDest`, `City`, `CityDest`, `Lat`, `Long`, `LatDest`, `LongDest`, `Weight`, `Value`, `TruckType`, `TruckPlat`, `Phone1`, `Phone2`, `Phone3`, `Comments`, `Img1`, `Img2`  FROM `Freight` where Id= $freightId";
     
            $table=db_query($sql);
            
            $buf1  = $table[1][23];
            $buf2  = $table[1][24];
            $table[1][23] = " <img src=$buf1 width=\"250px\" > ";
            $table[1][24] = " <img src=$buf2 width=\"250px\"  > ";
                    
            $tableFreightData = ShowData($title="",$table);
            $headersM = HeadersMap();
            $idLatHid = $_POST['idLatHid'];
            $idLonHid = $_POST['idLonHid'];          
            $idLatDestHid = $_POST['idLatDestHid'];
            $idLonDestHid = $_POST['idLonDestHid']; 
    
            $dataLogin .= <<<EOT
            $headersM
            <tx class="txb"> Freight Resume </tx> <br><br>
              
            <div style="width:500px; height:500px; "  id="mapkpa">  </div> <br>
            <script>  
            ShowRouteOnMap('mapkpa',$idLatHid,$idLonHid,$idLatDestHid,$idLonDestHid);
            </script>
  
            $tableFreightData        
EOT;
        }
        else
        {
            // 999999999999999999

        }   
        
        $align="left";
        $dataSearch = <<<EOT
        $dataLogin   


            
            <script>  
                  
            function performClick(elemId) 
            {
               var elem = document.getElementById(elemId);
               if(elem && document.createEvent) {
                  var evt = document.createEvent("MouseEvents");
                  evt.initEvent("click", true, false);
                  elem.dispatchEvent(evt);
               }
            }
    
             $("#submitPrevious2").button();    
             $("#submitPrevious2").on("click", function() 
             {
                  CopyDataForm();    
                  $("#idActivFormHid").val(3);
                  $("form:first").submit();
             });   
                
             $( "#submitCreateFreight" ).button();   
             $("#submitCreateFreight").on("click", function() 
                {
                   /* if($('#idNumber').val()=="" || $('#idDescription').val()=="" ) 
                   { 
                        $( "<div>Incomplete information</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"});
                        return;
                   } */
                   CopyDataForm();
                   $("form:first").submit();
                });   
                    
            $( "#idTruckPlat" ).selectmenu(); 
            $( "#idTruckType" ).selectmenu(); 
            // $( "#idWeight" ).inputmask("999999.99 kg");  
               
            // $( "#idValue" ).inputmask("99.999,00 $"); 
            $( "#idPhone1" ).inputmask("99(99)99999-9999"); 
            $( "#idPhone2" ).inputmask("99(99)99999-9999"); 
            $( "#idPhone3" ).inputmask("99(99)99999-9999"); 
                
            jQuery.fn.forceNumeric = function () {

                 return this.each(function () {
                     $(this).keydown(function (e) {
                         var key = e.which || e.keyCode;

                         if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                         // numbers   
                             key >= 48 && key <= 57 ||
                         // Numeric keypad
                             key >= 96 && key <= 105 ||
                         // comma, period and minus, . on keypad
                            key == 190 || key == 188 || key == 109 || key == 110 ||
                         // Backspace and Tab and Enter
                            key == 8 || key == 9 || key == 13 ||
                         // Home and End
                            key == 35 || key == 36 ||
                         // left and right arrows
                            key == 37 || key == 39 ||
                         // Del and Ins
                            key == 46 || key == 45)
                             return true;

                         return false;
                     });
                 });
             }   
                
             $( "#idWeight" ).forceNumeric(); 
             $( "#idValue" ).forceNumeric();    
                
                
            document.getElementById('idImg1').addEventListener('change', readSingleFile1, false);
            document.getElementById('idImg2').addEventListener('change', readSingleFile2, false);

             
            $("#idImg1Del").on("click", function() 
            {
               // CopyDataForm();
               // alert("Delete 1");     
                document.getElementById("idImg1Show").src = "Img/img-photo.png";
            }); 
            $("#idImg2Del").on("click", function() 
            {
               // CopyDataForm();
               //alert("Delete 2");  
                document.getElementById("idImg2Show").src = "Img/img-photo.png"; 
            }); 
            
                
            /////////////////////////////////////////////////////////////////////////
            function readSingleFile1(e) {
              var file = e.target.files[0];
              if (!file) {
                return;
              }
              
              var reader = new FileReader();
              
                
              reader.onloadend = function(e) {
                    var contents = e.target.result;
                    displayContents1(contents);
              };

              
               reader.readAsDataURL(file); 
                displayContents1(reader.result);
            }
            /////////////////////////////////////////////////////////////////////////
            function readSingleFile2(e) {
              var file = e.target.files[0];
              if (!file) {
                return;
              }
              var reader = new FileReader();
                
               
              reader.onloadend = function(e) {
                var contents = e.target.result;
                displayContents2(contents);
              };
              // reader.readAsText(file);
              reader.readAsDataURL(file);  
            }                
            /////////////////////////////////////////////////////////////////////////
            function displayContents1(contents) 
            {
               var element = document.getElementById('idImg1Show');
              // element.src = contents;
                
               element.src = contents;   
                
              // ResizeImage(contents,element);
            }
            /////////////////////////////////////////////////////////////////////////
            function displayContents2(contents) 
            {
              var element = document.getElementById('idImg2Show');
              element.src = contents;
              // ResizeImage(contents,element);
            }    
            /////////////////////////////////////////////////////////////////////////
            function ResizeImage(contents,element)
            {
                // Create an image
                var img = document.createElement("img");
                img.src = contents;

                var canvas = document.createElement("canvas");
                //var canvas = $("<canvas>", {"id":"testing"})[0];
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);

                var MAX_WIDTH = 800;
                var MAX_HEIGHT = 600;
                var width = img.width;
                var height = img.height;

                if (width > height) {
                  if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                  }
                } else {
                  if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                  }
                }
                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, width, height);

                var dataurl = canvas.toDataURL("image/jpeg", 0.5);
                element.src = dataurl; 
            }
                 
            </script>   
EOT;
    }
    else   
        
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    if($idActivFormHid==3)
    {
        $align="left";
        $dataSearch = <<<EOT
             <tx class="txb"> New Freight</tx><br><br> 
             <tx class="tx"> Comments *</tx> <br><br>  <textarea  class="form-control" style="height:100px;"  name="idComments"  id="idComments"/>$idCommentsHid</textarea> <br>
             <table style="width:90%">
              <tr>
                <td><tx class="tx"> Freight Photos </tx> <br> <br> <label> <input type="file" name="idImg1" id="idImg1" style="display: none;" /> <span id="input-file-replacer1"> <img id="idImg1Show" src="$idImg1Hid" alt="Load photo" style="width:15%;">  </span> </label> <img id="idImg1Del" src="Img/deleteWhite.png" alt="Delete" style="width:15px;">
                <label> <input type="file" name="idImg2" id="idImg2" style="display: none;" /> <span id="input-file-replacer2"> <img id="idImg2Show" src="$idImg2Hid" alt="Load photo" style="width:15%;">  </span>  </label> <img id="idImg2Del" src="Img/deleteWhite.png" alt="Delete" style="width:15px;"> </td>
              </tr>                  
            </table>                  
            <input  style="width:100px; " id="submitPrevious1" name="submitPrevious1" type=""  readonly  value="Previous" /> <input  style=" " id="submitCreateFreight" name="submitCreateFreight" type=""  readonly value="Create Freight" /><br>                
            <br><br><br><br><br><br>
     
            <script>  

            function performClick(elemId) 
            {
               var elem = document.getElementById(elemId);
               if(elem && document.createEvent) {
                  var evt = document.createEvent("MouseEvents");
                  evt.initEvent("click", true, false);
                  elem.dispatchEvent(evt);
               }
            }
    
             $("#submitPrevious1").button();    
             $("#submitPrevious1").on("click", function() 
             {
                  CopyDataForm();    
                  $("#idActivFormHid").val(2);
                  $("form:first").submit();
             });   
                
             $( "#submitCreateFreight" ).button();   
             $("#submitCreateFreight").on("click", function() 
                {
                   /* if($('#idNumber').val()=="" || $('#idDescription').val()=="" ) 
                   { 
                        $( "<div>Incomplete information</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"});
                        return;
                   } */
                   CopyDataForm();
                   $("#idActivFormHid").val(4);
                   $("form:first").submit();
                });   
                    
            $( "#idTruckPlat" ).selectmenu(); 
            $( "#idTruckType" ).selectmenu(); 
            // $( "#idWeight" ).inputmask("999999.99 kg");  
               
            // $( "#idValue" ).inputmask("99.999,00 $"); 
            $( "#idPhone1" ).inputmask("99(99)99999-9999"); 
            $( "#idPhone2" ).inputmask("99(99)99999-9999"); 
            $( "#idPhone3" ).inputmask("99(99)99999-9999"); 
                
            jQuery.fn.forceNumeric = function () {

                 return this.each(function () {
                     $(this).keydown(function (e) {
                         var key = e.which || e.keyCode;

                         if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                         // numbers   
                             key >= 48 && key <= 57 ||
                         // Numeric keypad
                             key >= 96 && key <= 105 ||
                         // comma, period and minus, . on keypad
                            key == 190 || key == 188 || key == 109 || key == 110 ||
                         // Backspace and Tab and Enter
                            key == 8 || key == 9 || key == 13 ||
                         // Home and End
                            key == 35 || key == 36 ||
                         // left and right arrows
                            key == 37 || key == 39 ||
                         // Del and Ins
                            key == 46 || key == 45)
                             return true;

                         return false;
                     });
                 });
             }   
                
             $( "#idWeight" ).forceNumeric(); 
             $( "#idValue" ).forceNumeric();    

             
            document.getElementById('idImg1').addEventListener('change', readSingleFile1, false);
            document.getElementById('idImg2').addEventListener('change', readSingleFile2, false);

             
            $("#idImg1Del").on("click", function() 
            {
               // CopyDataForm();
               // alert("Delete 1");     
                document.getElementById("idImg1Show").src = "Img/img-photo.png";
            }); 
            $("#idImg2Del").on("click", function() 
            {
               // CopyDataForm();
               //alert("Delete 2");  
                document.getElementById("idImg2Show").src = "Img/img-photo.png"; 
            }); 
            
                
            /////////////////////////////////////////////////////////////////////////
            function readSingleFile1(e) {
              var file = e.target.files[0];
              if (!file) {
                return;
              }
              
              var reader = new FileReader();
              
                
              reader.onloadend = function(e) {
                    var contents = e.target.result;
                    displayContents1(contents);
              };

              
               reader.readAsDataURL(file); 
                displayContents1(reader.result);
            }
            /////////////////////////////////////////////////////////////////////////
            function readSingleFile2(e) {
              var file = e.target.files[0];
              if (!file) {
                return;
              }
              var reader = new FileReader();
                
               
              reader.onloadend = function(e) {
                var contents = e.target.result;
                displayContents2(contents);
              };
              // reader.readAsText(file);
              reader.readAsDataURL(file);  
            }                
            /////////////////////////////////////////////////////////////////////////
            function displayContents1(contents) 
            {
               var element = document.getElementById('idImg1Show');
              // element.src = contents;
                
               element.src = contents;   
                
              // ResizeImage(contents,element);
            }
            /////////////////////////////////////////////////////////////////////////
            function displayContents2(contents) 
            {
              var element = document.getElementById('idImg2Show');
              element.src = contents;
              // ResizeImage(contents,element);
            }    
            /////////////////////////////////////////////////////////////////////////
            function ResizeImage(contents,element)
            {
                // Create an image
                var img = document.createElement("img");
                img.src = contents;

                var canvas = document.createElement("canvas");
                //var canvas = $("<canvas>", {"id":"testing"})[0];
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);

                var MAX_WIDTH = 800;
                var MAX_HEIGHT = 600;
                var width = img.width;
                var height = img.height;

                if (width > height) {
                  if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                  }
                } else {
                  if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                  }
                }
                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, width, height);

                var dataurl = canvas.toDataURL("image/jpeg", 0.5);
                element.src = dataurl; 
            }
                 
            </script>   
EOT;
    }
    else   
    {
        
        $align="center";
        $dataSearch = <<<EOT
        <td> <tx class="tx"> Search Freight </tx>  <input id="submit" name="submitSearch" type="submit" value="Search" /> </td>
        <!-- <input id="field" name="field" type="text" /> --> 
        <td> <tx class="tx"> New Freight </tx> <input id="submitNew" name="submitNew" type="submit" value="New" /><br> </td>
        
        <script>         
        $("#submitNew").on("click", function() 
        {

           // CopyDataForm();
           // alert("sdsd");     
               
           $("#idActivFormHid").val(1);    
           $("form:first").submit();
        }); 
        </script>     
EOT;
        $ShowMainInfo=1;
    }    
     
    
$buffer = <<<EOT
    <div id="searchContainer" align="$align" >
            $Debug
            <table align="center; style="width:100%">
              <tr>
                $dataSearch
              </tr>
            </table>
	</form>
    </div>   
EOT;

    if (ismobile()) {
        $top = "80px";
        $width = "95%";
    } else {
        $top = "130px";
        $width = "99%";
    }
    echo divTransparente($buffer, $color = "rgba(0, 0, 0, 0.4)", $opacity = "", $top, $left = "4px", $width, $height = "", $borderadius = "1px");
    if($ShowMainInfo==1)
      echo MainInfoTruckTrack();
}
////////////////////////////////////////////////////////////////////////////////
function GetThemeByParameter()
{
    require 'GlobalVars.php';
    
    session_start();
    if($_GET['stme']=="" )  // http://html.net/page.php?stme=TTM
    { 
        $AppName = "EstControl";
        $AppTheme= "truck";      
        if($_SESSION['stme']=="")
        {
           $_SESSION['stme']="VAG";
           return;
        }   
        
    }
    
    if($_GET['stme']=="TTM"  )
    {  
        $AppName = "TruckMonitor";
        $AppTheme= "truck";  
        $_SESSION['stme']="TTM";
        return;        
    }
    if( $_GET['stme']=="VAG" )
    {  
        $AppName = "EstControl";
        $AppTheme= "truck";  
        $_SESSION['stme']="VAG";
        return;        
    }
    
    if($_GET['stme']=="MMF" )
    {  
       $AppName = "MyFather";
       $AppTheme= "myfather";   
       $_SESSION['stme']="MMF";
       return;
    }  
    if($_GET['stme']=="CDM" )
    {  
       $AppTheme = "green";
       $AppName = "ChildMonitor"; 
       $_SESSION['stme']="CDM";
       return;
    }  
    if($_SESSION['stme']=="TTM" )
    {  
        $AppName = "TruckMonitor";
        $AppTheme= "truck";  
        return;        
    }
    if( $_SESSION['stme']=="VAG" )
    {  
        $AppName = "EstControl";
        $AppTheme= "truck";  
        $_SESSION['stme']="VAG";
        return;        
    }
    if($_SESSION['stme']=="MMF" )
    {  
       $AppName = "MyFather";
       $AppTheme= "myfather";   
       return;
    }  
    if($_SESSION['stme']=="CDM" )
    {  
       $AppTheme = "green";
       $AppName = "ChildMonitor"; 
       return;
    }      
}    
////////////////////////////////////////////////////////////////////////////////
$formHeadersInclude = 0;
function formHeaders()
{
    require 'GlobalVars.php';
    // $formHeadersInclude=$formHeadersInclude+1;
    // if($formHeadersInclude>1)
    //    return;
    
    GetThemeByParameter();
    
    if($AppTheme=="green")
       $background_editApp = "#DAFFDA";
    if($AppTheme=="myfather" || $AppTheme== "listaescolar" )
       $background_editApp = "#a9b7d1";

    //  <meta name="viewport" content="width=device-width, initial-scale=10, maximum-scale=10, user-scalable=0"/> <!--320-->
    if(ismobile())
    {    
        $zoom=1;
    }
    else
     {    
        $zoom=1;

    }   
    $zoomp=100*$zoom;
    /*
     * Google analytics MyFather
     * 
         <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-65894545-5', 'auto');
      ga('send', 'pageview');

    </script>
     * 
     * 
     *  Google analytics TruckTrack
     *             
      <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-65894545-4', 'auto');
      ga('send', 'pageview');
    </script>
     * 
     *  Google analytics ParkingFit
     * 
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-65894545-6', 'auto');
          ga('send', 'pageview');

        </script>
     * 
     */
    
    if($_SESSION['stme']=="VAG")
    {    
       $GoogleAnalyticsCode = "UA-65894545-6";
      
    }
    else if($_SESSION['stme']=="TTM" )
    {    
       $GoogleAnalyticsCode = "UA-65894545-4";
    }
    else if($_SESSION['stme']=="MMF" )
    {    
       $GoogleAnalyticsCode = "UA-65894545-5";
    } 
    else
    {    
       $GoogleAnalyticsCode = "UA-65894545-4";
    }         
    echo <<<EOT
    
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <title></title>
            <link rel="stylesheet" href="/ChildMonitor/Menus/jquery-ui_$AppTheme/jquery-ui.min.css">
            <style>
                    .bodyForm{
                            zoom: $zoomp%;
                            -moz-transform: scale($zoom);
                            font: 100.0% "Montserrat", Arial, sans-serif;
                            font-weight: 1000;
                            letter-spacing: 1px;
                            margin: 0px 0px 0px 0px;
                            top:0px; left:0px;  
                    }
            </style>

            <style>
                .ui-menu { width: 250px; }
                .ui-widget {
                    font-family: "Montserrat", Arial, sans-serif;
                    font-size: 12px;
                }    
                .ui-dialog-titlebar-close { visibility: hidden; }
            </style>
            
            <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', '$GoogleAnalyticsCode', 'auto');
                  ga('send', 'pageview');
            </script>

            <!-- FORM HEADERS -->
            <script src="/ChildMonitor/Menus/jquery-ui_$AppTheme/jquery-ui.min.js"></script>
            <!--  <script src="/ChildMonitor/Util/Util.min.js"></script> -->       
            <link type="text/css" href="/ChildMonitor/Util/toastr.min.css" rel="stylesheet"/>
            <script type="text/javascript" src="/ChildMonitor/Util/toastr.min.js"></script>             
            
EOT;
}
////////////////////////////////////////////////////////////////////////////////
function VerifyMobileLogin()
{
    if($_GET['userEmail']!="" && $_GET['Code']!="")
    {  

        if($_GET['Code']==hash('sha256', $_GET['userEmail']."MobileChildMonitor"))
        {        
            $emailLog = $_GET['userEmail'];
            $sql="select id,fbId from User where email='$emailLog' ";
            $resultado=db_query($sql);
            if($resultado[1][0]=="")
                return;

            session_start();
            $_SESSION['userId']=$resultado[1][0];
            $_SESSION['userEmail']=$emailLog;
            $_SESSION['fbId']=$resultado[1][1];
            $_SESSION['AppLogin']=1;
            
            $userId = $resultado[1][0];
            $userEmail = $emailLog;
            $fbId = $resultado[1][1];
            echo <<<EOE
            <script type="text/javascript">
              if (navigator.cookieEnabled)
              { 
                 document.cookie = "userId=$userId";
                 document.cookie = "userEmail=$userEmail";
                 document.cookie = "fbId=$fbId";                     
              } 
            </script>
EOE;
        }    
    }    
    if($_GET['userEmail']=="" && $_GET['Code']!="")
    {  
        session_start();
        $_SESSION['userId']="";
        $_SESSION['userEmail']="";
        $_SESSION['fbId']="";
        $_SESSION['AppLogin']=0;

        echo <<<EOE
        <script type="text/javascript">
          if (navigator.cookieEnabled)
          { 
             document.cookie = "userId=";
             document.cookie = "userEmail=";
             document.cookie = "fbId=";                     
          } 
        </script>
EOE;
    }    

}
////////////////////////////////////////////////////////////////////////////////
function formUserProfile()
{
    require 'GlobalVars.php';
    $waitDialog = jsFormWait();
    formHeaders();
    // dbg($_POST['ActionProg']);
    if($_POST['ActionProg']=='Update')
    {
        // dbg("entrou");
        $email=$_SESSION['userEmail'];

        $address = $_POST["addressHid"];
        $addresscmp = $_POST["addresscmpHid"];
        $country = $_POST["countryHid"];
         
        
        $sql="UPDATE `User` SET `address`='$address',`addresscmp`='$addresscmp',`country`='$country' WHERE email='$email'";
        // dbg($sql);

        $resultado=db_query($sql);
    }
    if($_POST['ActionProg']=='DeleteAccount')
    {
        dbg($_POST["passwdHid"]);
        $email=$_SESSION['userEmail'];
        $passwd = $_POST["passwdHid"];
        $userId=$_SESSION['userId'];
        if(VerificaLogin($email,$passwd)!=0)
        {    
            $sql="delete from MonitoredRegion WHERE UserId=$userId";
            $sql="delete from UserMonitThings WHERE UserId=$userId";
            $sql="delete from MonitoredThings WHERE idUser=$userId";
            $sql="delete from MonitoredRegion WHERE UserId=$userId";
            $sql="delete from Events WHERE UserId=$userId";
            $sql="delete from User WHERE id=$userId";
 
            db_query($sql);
            if(isLogged())
            {
                session_start();
                $_SESSION['userId']="";
                $_SESSION['userEmail']="";  

                session_destroy();
echo <<<EOT
                <form id="formOut" action="/ChildMonitor/index.php" method="POST">
                </form>
                <script>
                $( "<div>User deleted.</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"});
                $( "#formOut" ).submit(); 
                </script>
EOT;
            }               
        }
    }
    // Load user info
    // $_SESSION['userId']=$resultado[1][0];
    $email=$_SESSION['userEmail'];
    // $_SESSION['fbId']=$resultado[1][1];
    $query ="select `id`, `email`, `fbId`, `password`, `address`, `addresscmp`, `country`, `DateInclusion`, `Plan`, `PlanEndDate` from User where email='$email'";
    $resultado=db_query($query);
    // echo $query;
    //session_start();
    $address=$resultado[1][4];
    $addresscmp=$resultado[1][5];
    $country=$resultado[1][6];
    $dateinc=$resultado[1][7];
    
    echo <<<EOT
    <body class="bodyForm">
    <form id="formUserProfile" action="/ChildMonitor/userProfile.php" method="POST">
EOT;
    
   $buffer = <<<EOT
    <div id="dialog" style="top:5px; left:5px; position:relative;" title="User Profile">
        User Information:<br><br>   
        Email<br>
        <input style="width:300px;  background-color: $background_editApp;" id="email" name="email" value='$email' readonly><br> 
        Address<br>
        <input style="width:300px;" id="address" name="address" value='$address'><br>
        AddressCmp<br>
        <input style="width:300px;" id="addresscmp" name="addresscmp" value='$addresscmp'><br>
        Country<br>
        <input style="width:300px;" id="country" name="country" value='$country'><br> 
        Date Inclusion<br>
        <input style="width:300px; background-color: $background_editApp;" id="dateinc" name="dateinc" value='$dateinc' readonly><br>  <br>
           
        <input id="BotUpdate" type="submit" value="Update"> 
        <input id="BotDeleteAccount" value="Delete"> <br> <br>
    </div> 
EOT;
           
    if(isMobile())
    {    
       $top="5px"; 
       $width="97.5%";
    }   
    else
    {    
       $top="5px";
       $width="99.5%";
    }   
    
    divRelative($buffer,$color="#f0f0f0",$opacity="",$top,$left="5px",$width,$height="");
echo <<<EOT
    <input id="addressHid" type="hidden" name="addressHid" >
    <input id="addresscmpHid" type="hidden" name="addresscmpHid" >
    <input id="countryHid" type="hidden" name="countryHid" >
    <input id="passwdHid" type="hidden" name="passwdHid" >
    <input id="ActionProg" type="hidden" name="ActionProg" value=''>
            
    </form>
            
            
   <script>
        $( "#BotUpdate" ).button();
        $( "#BotDeleteAccount" ).button();   

        $("#BotDeleteAccount").on("click", function() 
        {
            DlgConfirmAccountDeletion();
        });
        /////////////////////////////////////////////////////////////////////////
        function DlgConfirmAccountDeletion()
        {
            $( "<div id=\"dialog\" title=\"WARNING\"> You really want account deletion?<br>This can't be undone.<br><br> Password:<br><input id=\"passwd\" name=\"passwd\" type=\"password\" > </div>").dialog({
                resizable: true,
                modal: true,
                buttons: {
                "Confirm": function() {
                    $('#passwdHid').val($('#passwd').val()); 
                    $( this ).dialog( "close" );
                    $('#ActionProg').val('DeleteAccount');    
                    $( "#formUserProfile" ).submit(); 
                    
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    
                }
                }
            });
        }
    
        $("#BotUpdate").on("click", function() 
        {
           
           
          // console.log("Fazendo o post");
          // $.post( "formLogin/index.php", { user:"John", password:"2pm" });
           $waitDialog  
           $('#addressHid').val($('#address').val ());
           $('#addresscmpHid').val($('#addresscmp').val ());
           $('#countryHid').val($('#country').val ());
           $('#ActionProg').val('Update'); 

        });
        
            
       // $( "div#dialog" ).dialog();
            
        /////////////////////////////////////////////////////////////////////////

       $("form:first").submit(function() {
           
            // console.log("Triggered submit handler in jquery");
           return true;
       });
    </script>
    </body> 
    </html>  
EOT;
    if(ismobile())
       echo "<p style=\"font-size: 2px\"></p>";
    else
       echo "<p style=\"font-size: 2px\"><br></p>";
}
////////////////////////////////////////////////////////////////////////////////
function formUserPlan()
{
    require 'GlobalVars.php';
    $waitDialog = jsFormWait();
    formHeaders();
    $payPallButton = PayPallButtonFrame();
    $userId=$_SESSION['userId'];
    
    $sql = "SELECT `Instituition`, `payer_email`, `DatePaymentLoc`,`PlanSelection` FROM `PaymentInfo` WHERE UserId=$userId";
     
    $table=db_query($sql);
    $tablePayments = GreenTable($title="Payments",$table);
    // dbg($_POST['ActionProg']);
    if($_POST['ActionProg']=='Update')
    {
        // dbg("entrou");
        /*
        $email=$_SESSION['userEmail'];

        $address = $_POST["addressHid"];
        $addresscmp = $_POST["addresscmpHid"];
        $country = $_POST["countryHid"];
         
        
        $sql="UPDATE `User` SET `address`='$address',`addresscmp`='$addresscmp',`country`='$country' WHERE email='$email'";
        // dbg($sql);

        $resultado=db_query($sql);
         */
    }
    // Load user info
    // $_SESSION['userId']=$resultado[1][0];
    $email=$_SESSION['userEmail'];
    // $_SESSION['fbId']=$resultado[1][1];
    $query ="select `Plan`, `PlanEndDate` from User where email='$email'";
    $resultado=db_query($query);
    // echo $query;
    //session_start();
    $Plan=$resultado[1][0];
    if(trim($Plan)=="")
        $Plan="Free";
    $PlanEndDate=$resultado[1][1];
    
    echo <<<EOT
    <body class="bodyForm">
    <form id="formLogin" action="/ChildMonitor/userProfile.php" method="POST">
EOT;
    
   $buffer = <<<EOT
    <div id="dialog" style="top:5px; left:5px; position:relative;" title="User Profile">
        User Plan:<br><br>   
        Plan<br>
        <input style="width:300px;  background-color: $background_editApp;" id="Plan" name="Plan" value='$Plan' readonly><br> 
        Plan End Date<br>
        <input style="width:300px;  background-color: $background_editApp;" id="Plan" name="PlanEndDate" value='$PlanEndDate' readonly><br> <br>
        Previous Payments<br>
        $tablePayments<br>
        New Subscription<br>   
        $payPallButton 

         

        
    </div> 
EOT;
    if(isMobile())
    {    
       $top="5px"; 
       $width="97.5%";
    }   
    else
    {    
       $top="5px";
       $width="99.5%";
    }   
    
    divRelative($buffer,$color="#f0f0f0",$opacity="",$top,$left="5px",$width,$height="");
echo <<<EOT
    <input id="addressHid" type="hidden" name="addressHid"  value=''>
    <input id="addresscmpHid" type="hidden" name="addresscmpHid"  value=''>
    <input id="countryHid" type="hidden" name="countryHid"  value=''>
    <input id="ActionProg" type="hidden" name="ActionProg" value=''>
            
    </form>
            
            
   <script>
        
            
       // $( "div#dialog" ).dialog();
            
        /////////////////////////////////////////////////////////////////////////

       $("form:first").submit(function() {
           
            // console.log("Triggered submit handler in jquery");
           return true;
       });
    </script>
    </body> 
    </html>  
EOT;
    if(ismobile())
       echo "<p style=\"font-size: 0px\"></p>";
    else
       echo "<p style=\"font-size: 1px\"><br></p>";

}
////////////////////////////////////////////////////////////////////////////////
function AdjustIconsOnTableDevices($table)
{
    $indLin = 1;

    while($table[$indLin][0]!="")
    {    
        $buffer = trim($table[$indLin][2]);
        
        if($buffer!="")
           $table[$indLin][2]="<img src=\"$buffer\"  width=\"60\" >"; 
        
        $indLin++;
    } 
    return($table);
}

////////////////////////////////////////////////////////////////////////////////
function formUserDevices()
{
    require 'GlobalVars.php';
    $waitDialog = jsFormWait();
    formHeaders();

    $userId=$_SESSION['userId'];
    
    // dbg($_POST['ActionProg']);
    if($_POST['ActionProgUserDev']=='DeleteDevice')
    {
        // Post não fuciona para vários forms... e
        $idDevice = $_POST["idDevHid"];
        $sql="DELETE FROM `UserMonitThings` WHERE UserId=$userId and id='$idDevice'";
        $resultado=db_query($sql);
        $sql="DELETE FROM `MonitoredThings` WHERE idUser=$userId and idDevice='$idDevice'";
        $resultado=db_query($sql);
    }
    
    if($_POST['ActionProgUserDev']=='ChangeLabel')
    {
        
        $idDevice = $_POST["idDevHid"];
        $NewLabel = $_POST["NewLabelHid"];
        $sql="UPDATE `UserMonitThings` SET `Label`='$NewLabel'  WHERE UserId=$userId and id='$idDevice'";
        // dbg($sql);
        $resultado=db_query($sql);
        
    }
    if($_POST['ActionProgUserDev']=='ChangeIcon')
    {
        
        // dbg($_POST['NewImgHid']);
        
        $idDevice = $_POST["idDevHid"];
        // $NewIcon = $_POST["NewIconHid"];
        $NewImg = $_POST['NewImgHid'];
        $sql="UPDATE `UserMonitThings` SET  `Image`='$NewImg'  WHERE UserId=$userId and id='$idDevice'";
        $resultado=db_query($sql);
    }
    
    $sql = "SELECT  `id`,`Label`,`Image` FROM `UserMonitThings` WHERE UserId=$userId";
    $table=db_query($sql);
    $table=AdjustIconsOnTableDevices($table);
    // $table=AdjustImagesOnTableDevices($table);
    
    $tableDevices = GreenTable($title="Devices",$table,"Devices");

    // Load user info
    // $_SESSION['userId']=$resultado[1][0];
    $email=$_SESSION['userEmail'];
    // $_SESSION['fbId']=$resultado[1][1];
    // 
    echo <<<EOT
    <body class="bodyForm">
    <form id="formUserDevices" action="/ChildMonitor/userProfile.php" method="POST" enctype="multipart/form-data"  >
EOT;
    
   $buffer = <<<EOT
    <div id="dialog" style="top:5px; left:5px; position:relative;" title="User Devices">
           
        User Devices:<br><br>
        Interval Polling Position: <input type="text" id="ppTime" name="ppTime"> (ms)<br>   
        $tableDevices<br>
        Click on Label to change value, on id to delele device and on icon to change picture.<br><br> 
    </div> 
EOT;
    if(isMobile())
    {    
       $top="5px"; 
       $width="97.5%";
    }   
    else
    {    
       $top="5px";
       $width="99.5%";
    }   
    
    divRelative($buffer,$color="#f0f0f0",$opacity="",$top,$left="5px",$width,$height="");
    // Diferent action name for each form
 
echo <<<EOT
    <input id="ActionProgUserDev" type="hidden" name="ActionProgUserDev">
    <input id="idDevHid" type="hidden" name="idDevHid" value='' >
    <input id="NewLabelHid" type="hidden" name="NewLabelHid" >
    <input id="NewIconHid" type="hidden" name="NewIconHid" >
    <input id="NewImgHid" type="hidden" name="NewImgHid" >    
    </form>
            
   <script>
    
        $('#Devices tr td').click(function()
        {
            // Column id#linenum
            var cid = $(this).attr('id');
           
    
            var n = cid.search("#"); 
            var ValueCol = $(this).text();
            var idDev  =  cid.substring(0, n);
            
    
            // Column decide action
            if(cid.substring(n+1, n+3)==0)
            {
               $('#idDevHid').val(idDev); 
               $('#ActionProgUserDev').val('DeleteDevice'); 
    
               DlgConfirm("Delete device: "+cid.substring(0, n)+" ?");
            }
            if(cid.substring(n+1, n+3)==1)
            {
               $('#idDevHid').val(idDev); 
               $('#ActionProgUserDev').val('ChangeLabel'); 
               DlgChangeLabel();
            }
            if(cid.substring(n+1, n+3)==2)
            {
               $('#idDevHid').val(idDev); 
               $('#ActionProgUserDev').val('ChangeIcon'); 
               DlgChangeIcon();
            }
    

        });
        /////////////////////////////////////////////////////////////////////////
        function DlgChangeLabel()
        {
            $( "<div id=\"dialog\" title=\"Change Label\">  <input id=\"NewLabel\" name=\"NewLabel\" value=\'\'> </div>").dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                "Confirm": function() 
                {
                    $('#NewLabelHid').val($('#NewLabel').val()); 
                    
                    $( this ).dialog( "close" );
    
                    $( "#formUserDevices" ).submit(); 
                    
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    
                }
                }
            });
        }
        /////////////////////////////////////////////////////////////////////////
        function DlgChangeIcon()
        {
            $( "<div id=\"dialog\" title=\"Change Icon\"> <input type=\"file\" name=\"NewImg\" id=\"NewImg\" accept=\"image/*;capture=camera\" /> <br> <img id=\"myimage\" height=\"80\">  </div>").dialog({
                resizable: false,
                height:240,
                width:390,
                modal: true,
                buttons: {
                "Confirm": function() {
     
                    // $('#NewIconHid').val($('#NewIcon').val()); 
                    $('#NewImgHid').val(document.getElementById('myimage').src); 
    
                    $( this ).dialog( "close" );
               
                    $( "#formUserDevices" ).submit(); 
                    
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    
                }
                }
            });
            document.getElementById('NewImg').addEventListener('change', readSingleFile, false);
        }
        /////////////////////////////////////////////////////////////////////////
        function readSingleFile(e) {
          var file = e.target.files[0];
          if (!file) {
            return;
          }
          var reader = new FileReader();
          reader.onload = function(e) {
            var contents = e.target.result;
            displayContents(contents);
          };
          // reader.readAsText(file);
          reader.readAsDataURL(file); 
        }
        /////////////////////////////////////////////////////////////////////////
        function displayContents(contents) 
        {
          var element = document.getElementById('myimage');
          // element.src = contents;
          ResizeImage(contents,element);
        }
        /////////////////////////////////////////////////////////////////////////
        function ResizeImage(contents,element)
        {
            // Create an image
            var img = document.createElement("img");
            img.src = contents;

            var canvas = document.createElement("canvas");
            //var canvas = $("<canvas>", {"id":"testing"})[0];
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);

            var MAX_WIDTH = 160;
            var MAX_HEIGHT = 120;
            var width = img.width;
            var height = img.height;

            if (width > height) {
              if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
              }
            } else {
              if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
              }
            }
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, width, height);

            var dataurl = canvas.toDataURL("image/jpeg", 0.5);
            element.src = dataurl; 
        }
        /////////////////////////////////////////////////////////////////////////
        function DlgConfirm(msg)
        {
            $( "<div id=\"dialog-confirm\" title=\"Question\">"+msg+"</div>").dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                "Confirm": function() {
                    $( this ).dialog( "close" );
                    $( "#formUserDevices" ).submit(); 
                    
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    
                }
                }
            });
        }
        /////////////////////////////////////////////////////////////////////////

    </script>
    </body> 
    </html>  
EOT;
    if(ismobile())
       echo "<p style=\"font-size: 0px\"></p>";
    else
       echo "<p style=\"font-size: 1px\"><br></p>";   
}
////////////////////////////////////////////////////////////////////////////////
function GetLastEventDate($userId)
{
    $sql = "SELECT date(max(`TimeStampc`)) FROM `MonitoredThings` WHERE idUser=$userId ";
    // dbg($sql);
    $table=db_query($sql);  
    // dbg($table[1][0]);
    return($table[1][0]);
}   
////////////////////////////////////////////////////////////////////////////////
function formUserEvents()
{
    require 'GlobalVars.php';
    $waitDialog = jsFormWait();
    formHeaders();

    $userId=$_SESSION['userId'];
    if($_POST["datevehicledayhid"]=="")
    {    
        $dateToday=GetLastEventDate($userId);
    }
    else
        $dateToday=$_POST["datevehicledayhid"];
    
    if($_POST["selDevice02Hid"]=="")
    {    
        $selDevice=GetFirstDevice($userId);
    }
    else
        $selDevice=$_POST["selDevice02Hid"];
    // dbg($_POST['ActionProg']);
    /*
    if($_POST['ActionProgDev']=='ChangeIcon')
    {
        $idDevice = $_POST["idDevHid"];
        $NewIcon = $_POST["NewIconHid"];
        $sql="UPDATE `UserMonitThings` SET `icon`='$NewIcon'  WHERE UserId=$userId and id='$idDevice'";
        $resultado=db_query($sql);
    }
    */
    // $sql = "SELECT `idDevice`, `TimeStamp`, `Description` FROM `Events` WHERE UserId=$userId and date(TimeStamp)='$dateToday' ";
    $sql = "SELECT `UserMonitThings`.Label,  `Events`.`TimeStamp`,  `Events`.`Description` FROM `Events`,`UserMonitThings` WHERE `Events`.UserId=$userId and date(`Events`.TimeStamp)='$dateToday' and `Events`.UserId=`UserMonitThings`.UserId and `Events`.`idDevice`=`UserMonitThings`.id";
    // dbg($sql);
    $table=db_query($sql);
    $tableEvents = GreenTable($title="Events",$table,"Events");

    // Load user info
    // $_SESSION['userId']=$resultado[1][0];
    $email=$_SESSION['userEmail'];
    // $_SESSION['fbId']=$resultado[1][1];
    $selectDevice = SelectDevice("selDevice2",$_SESSION['userId'],$idDevice,"60px");
    // $distance = CalculateDistance();
    if($_SESSION['stme']=="TTM")
        $label = "Vehicle:";
    else
        $label = "Device:";
    echo <<<EOT
    <body class="bodyForm">
    <form id="formUserEvents" action="/ChildMonitor/userReport.php" method="POST">
EOT;
    
   $buffer = <<<EOT
    <div id="dialog" style="top:5px; left:5px; position:relative;" title="Report">
        Events Report:<br><br>   
       <!-- Date: <input type="text" name="datepickerevent" id="datepickerevent" size="10" readonly value="$dateToday" > -->
       <!-- $label $selectDevice -->       
        $tableEvents<br>
    </div> 
EOT;
    if(ismobile())
    {    
       $top="5px"; 
       $width="97.5%";
    }   
    else
    {    
       $top="5px";
       $width="99.5%";
    }   
    
    divRelative($buffer,$color="#f0f0f0",$opacity="",$top,$left="5px",$width,$height="");
    // Diferent action name for each form
 
echo <<<EOT
    <input id="ActionProgEvent" type="hidden" name="ActionProgEvent" value=''>
    <input id="datepickereventhid" type="hidden" name="datepickereventhid" value=''>
    <input id="selDevice02Hid" type="hidden" name="selDevice02Hid" >
        
    </form>
   
    
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
<script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script> 
    
   
    
   <script>
        // $( "#datepickerevent" ).datepicker({dateFormat: "yy-mm-dd"});
     /*
        $('#datepickerevent').datepicker({
             onSelect: function(d,i){
                  if(d !== i.lastVal){
                      $(this).change();
                  }
             }
        });   
       
       $( "#selDevice02" ).change(function () {

            $('#selDevice02Hid').val ($('#selDevice02').val ());
            // $( "form:first" ).submit();
        }).change();  
       
        $('#datepickerevent').change(function(){
             //Change code!
             $('#datepickereventhid').val($('#datepickerevent').val());
             $( "#formUserEvents" ).submit();  
        });
         */   
        /////////////////////////////////////////////////////////////////////////
         $('#Events tr td').click(function()
            {
                // Column id#linenum
                // var cid = $(this).attr('idDevice');
                // alert(cid);
                
                var col = $(this).closest("td").index();
                var row = $(this).closest("tr").index();
                // alert('Row: ' + row + ', Column: ' + col); 
    
                // var coluna=$(this).index();
                // var linha=$(this).rowIndex;
                // alert('Row: ' + linha + ', Column: ' + coluna); 
                // alert(linha);
                var coluna=$(this).index();
                if(coluna==2)
                {
                   // this.rowIndex //  
                   var address = $(this).text();

                   var ini = address.search(":"); 
                   var fim = address.search("Radius:"); 
                   var addressok = address.substring(ini+1,fim); 
                   ini = addressok.search(":");
                   
                   // alert(addressok);
                   // DlgShowMap(lat,long);
                   // DlgShowMap(addressok);
                } 
            });
        /////////////////////////////////////////////////////////////////////////
        function DlgShowMap(lat,long)
        {
    
                $( "<div id=\"dialog-confirm\" title=\"Map Address\"> <div style=\"widht:100%; height:100%; \"  id=\"map\"> </div>  </div>").dialog({
                    resizable: true,
                    width:340,
                    height:540, 
                    modal: true,
                    buttons: 
                    {
                    "Close": function() {
                        $( this ).dialog( "destroy" );
                        
                      }
                    }
                });

            var map = L.map( 'map', {
                center: [lat, long],
                minZoom: 2,
                zoom: 17   
            });
           L.tileLayer( 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright" title="OpenStreetMap" target="_blank">OpenStreetMap</a> contributors | Tiles Courtesy of <a href="http://www.mapquest.com/" title="MapQuest" target="_blank">MapQuest</a> <img src="http://developer.mapquest.com/content/osm/mq_logo.png" width="16" height="16">',
                subdomains: ['otile1','otile2','otile3','otile4']
            }).addTo( map );
    
        }
    </script>
    
    </body> 
    </html>  
EOT;
    if(ismobile())
       echo "<p style=\"font-size: 0px\"></p>";
    else
       echo "<p style=\"font-size: 1px\"><br></p>";   
    
}
////////////////////////////////////////////////////////////////////////////////
function formUserRegions()
{
    require 'GlobalVars.php';
    $waitDialog = jsFormWait();
    formHeaders();

    $userId=$_SESSION['userId'];
    
    // dbg($_POST['ActionProg']);
    /*
    if($_POST['ActionProgDev']=='ChangeIcon')
    {
        $idDevice = $_POST["idDevHid"];
        $NewIcon = $_POST["NewIconHid"];
        $sql="UPDATE `UserMonitThings` SET `icon`='$NewIcon'  WHERE UserId=$userId and id='$idDevice'";
        $resultado=db_query($sql);
    }
    */
    $sql = "SELECT `Address` FROM `MonitoredRegion` WHERE UserId=$userId";
    $table=db_query($sql);
    $tableRegions = GreenTable($title="Regions",$table,"Regions");

    // Load user info
    // $_SESSION['userId']=$resultado[1][0];
    $email=$_SESSION['userEmail'];
    // $_SESSION['fbId']=$resultado[1][1];
    
    echo <<<EOT
    <body class="bodyForm">
    <form id="formUserRegions" action="/ChildMonitor/userReport.php" method="POST">
EOT;
    
   $buffer = <<<EOT
    <div id="dialog" style="top:5px; left:5px; position:relative;" title="Regions">
        Monitored Regions:<br><br>   
        $tableRegions<br>
    </div> 
EOT;
    if(isMobile())
    {    
       $top="5px"; 
       $width="97.5%";
    }   
    else
    {    
       $top="5px";
       $width="99.5%";
    }   
    
    divRelative($buffer,$color="#f0f0f0",$opacity="",$top,$left="5px",$width,$height="");
    // Diferent action name for each form
 
echo <<<EOT
    <input id="ActionProgRegions" type="hidden" name="ActionProgRegions" value=''>

        
    </form>
            
   <script>
    
        $('#Devices tr td').click(function()
        {
            var cid = $(this).attr('id');
            var n = cid.search("#"); 
            var ValueCol = $(this).text();
            var idDev  =  cid.substring(0, n);
            
            // Column decide action
            if(cid.substring(n+1, n+3)==0)
            {
               $('#idDevHid').val(idDev); 
               $('#ActionProgRegions').val('DeleteDevice'); 
    
               DlgShowMap("Delete device: "+cid.substring(0, n)+" ?");
            }
            if(cid.substring(n+1, n+3)==1)
            {
               $('#idDevHid').val(idDev); 
               $('#ActionProgRegions').val('ChangeLabel'); 
               // DlgChangeLabel();
            }
            if(cid.substring(n+1, n+3)==2)
            {
               $('#idDevHid').val(idDev); 
               $('#ActionProgRegions').val('ChangeIcon'); 
               // DlgChangeIcon();
            }
    

        });


        /////////////////////////////////////////////////////////////////////////
        function DlgConfirm(msg)
        {
            $( "<div id=\"dialog-confirm\" title=\"Question\">"+msg+"</div>").dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                "Confirm": function() {
                    $( this ).dialog( "close" );
                    $( "#formUserRegions" ).submit(); 
                    
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    
                }
                }
            });
        }
        /////////////////////////////////////////////////////////////////////////

    </script>
    </body> 
    </html>  
EOT;
    if(ismobile())
       echo "<p style=\"font-size: 0px\"></p>";
    else
       echo "<p style=\"font-size: 1px\"><br></p>";   
    
}

////////////////////////////////////////////////////////////////////////////////
function haversine($lat1,$lon1,$lat2,$lon2)
{
/*
    CREATE DEFINER=`root`@`localhost` FUNCTION `haversine`(`lat1` FLOAT, `lon1` FLOAT, `lat2` FLOAT, `lon2` FLOAT) RETURNS float
    NO SQL
    DETERMINISTIC
    COMMENT 'Returns the distance in degrees (now meters) on the Earth between two known points of latitude and longitude'
    BEGIN
           RETURN 6378137.0 * DEGREES(ACOS(
                  COS(RADIANS(lat1)) *
                  COS(RADIANS(lat2)) *
                  COS(RADIANS(lon2) - RADIANS(lon1)) +
                  SIN(RADIANS(lat1)) * SIN(RADIANS(lat2))
                ))
    END
    */
    /*
    return( 6378137.0 * rad2deg(acos(
                  cos(deg2rad(lat1)) *
                  cos(deg2rad(lat2)) *
                  cos(deg2rad(lon2) - deg2rad(lon1)) +
                  sin(deg2rad(lat1)) * sin(deg2rad(lat2))
                )));
    */
    return ( 6378137.0 * acos((cos(deg2rad($lat1)) ) * (cos(deg2rad($lat2))) * (cos(deg2rad($lon2) - deg2rad($lon1)) )+ ((sin(deg2rad($lat1))) * (sin(deg2rad($lat2))))) );
}
////////////////////////////////////////////////////////////////////////////////
function GetPlaces($userId,$dateToday)
{
    $table="";
    $sql = "SELECT `idDevice`, `idUser`, `Lat`, `Longc`, `TimeStampc`, `Velocity`, `Bearing`, `BatteryLevel` FROM `MonitoredThings` WHERE idUser=$userId and date(TimeStampc)='$dateToday' ";
    $table=db_query($sql); 
    // dbg($sql);
    
    $tableOut[0][0]="Address";
    $tableOut[0][1]="Ini Time";
    $tableOut[0][2]="Fin Time";
    $ind=2;
    $lat_first = $table[$ind-1][2];
    $lon_first = $table[$ind-1][3];
    $time_first = $table[$ind-1][4];
    $indOut=1;    
    while($table[$ind][0]!="")
    {   
       // dbg($ind); 
       // dbg(haversine($lat_first,$lon_first,$table[$ind][2],$table[$ind][3]));
       $timeini = strtotime($time_first);
       $timefin = strtotime($table[$ind][4]);
       $diff_minutes = abs($timefin - $timeini) / 60.0;
       if(haversine($lat_first,$lon_first,$table[$ind][2],$table[$ind][3]) > 100 && ($diff_minutes>10) ) // andou distancia maior que 60 metros
       {   
           
          // $tableOut[$indOut][0] = GetAddress($lat_first,$lon_first);
          $tableOut[$indOut][0] = GetAddress($table[$ind][2],$table[$ind][3]); 
          $tableOut[$indOut][1] = $time_first;
          $tableOut[$indOut][2] = $table[$ind][4];
          
          $lat_first = $table[$ind][2];
          $lon_first = $table[$ind][3];
          $time_first = $table[$ind][4];
          $indOut++;
       }
       $ind++;


    }
    
    return $tableOut;
}
////////////////////////////////////////////////////////////////////////////////
function formUserPlaces()
{
    require 'GlobalVars.php';
    $waitDialog = jsFormWait();
    formHeaders();

    $userId=$_SESSION['userId'];
    if($_POST["datepickerplacehid"]=="")
    {    
        $dateToday=GetLastEventDate($userId);
    }
    else
        $dateToday=$_POST["datepickerplacehid"];
    
    // dbg($_POST['ActionProg']);
    /*
    if($_POST['ActionProgDev']=='ChangeIcon')
    {
        $idDevice = $_POST["idDevHid"];
        $NewIcon = $_POST["NewIconHid"];
        $sql="UPDATE `UserMonitThings` SET `icon`='$NewIcon'  WHERE UserId=$userId and id='$idDevice'";
        $resultado=db_query($sql);
    }
    */

    $table = GetPlaces($userId,$dateToday);
    $tablePlace = GreenTable($title="Place",$table,"Place");

    // Load user info
    // $_SESSION['userId']=$resultado[1][0];
    $email=$_SESSION['userEmail'];
    // $_SESSION['fbId']=$resultado[1][1];
    
    echo <<<EOT
    <body class="bodyForm">
    <form id="formUserPlaces" action="/ChildMonitor/userPlaces.php" method="POST">
EOT;
    
   $buffer = <<<EOT
    <div id="dialog" style="top:5px; left:5px; position:relative;" title="Places">
        Places Report:<br><br>   
        Date: <input type="text" id="datepickerplace" size="10" readonly value="$dateToday" >   
        $tablePlace<br>
        Click on address to show map.<br>   
    </div> 
EOT;
    if(isMobile())
    {    
       $top="5px"; 
       $width="97.5%";
    }   
    else
    {    
       $top="5px";
       $width="99.5%";
    }   
    
    divRelative($buffer,$color="#f0f0f0",$opacity="",$top,$left="5px",$width,$height="");
    // Diferent action name for each form
    if(ismobile())
    {
        $swidth=240;
        $sheight=440;
    }    
    else
    {
        $swidth=540;
        $sheight=440;
    }      
    // <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
    //  <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script> 
    
    // <link rel="stylesheet" href="/ChildMonitor/Util/leaflet.css" />
    //    <script src="/ChildMonitor/Util/leaflet.js"></script> 
    
echo <<<EOT
    <input id="ActionProgDev" type="hidden" name="ActionProgDev" value=''>
    <input id="idDevHid" type="hidden" name="idDevHid" value=''>
    <input id="datepickerplacehid" type="hidden" name="datepickerplacehid" value=''>
 
        
    </form>
   
    <link rel="stylesheet" href="/ChildMonitor/Util/leaflet.css" />
    <script src="/ChildMonitor/Util/leaflet.js"></script> 
    
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

    
   <script>
        /////////////////////////////////////////////////////////////////////////
        function DlgShowMap(lat,long)
        {
            $( "<div id=\"dialog_showmap\" title=\"Map Address\"> <div style=\"widht:100%; height:100%; \"  id=\"map\"> </div>  </div>").dialog({
                resizable: true,
                closeOnEscape: false,
                width:$swidth,
                height:$sheight, 
                modal: false,
                buttons: 
                {
                   "Close": function() { $( this ).dialog( "destroy" ); }
                }
            });
            $("#dialog_showmap.ui-dialog-titlebar-close").hide();
        
            var map = L.map( 'map', {
                center: [lat, long],
                minZoom: 2,
                zoom: 18   
            });

           L.marker([lat, long]).addTo(map);
           L.tileLayer( 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright" title="OpenStreetMap" target="_blank">OpenStreetMap</a> contributors | Tiles Courtesy of <a href="http://www.mapquest.com/" title="MapQuest" target="_blank">MapQuest</a> <img src="http://developer.mapquest.com/content/osm/mq_logo.png" width="16" height="16">',
                subdomains: ['otile1','otile2','otile3','otile4']
            }).addTo( map );
    
        }
        //////////////////////////////////
        var latitudeTmp;
        var longitudeTmp;
        function GetLocation(address) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    latitudeTmp = results[0].geometry.location.lat();
                    longitudeTmp = results[0].geometry.location.lng();
                    DlgShowMap(latitudeTmp,longitudeTmp);
                    // alert("Latitude: " + latitudeTmp + " Longitude: " + longitudeTmp);
                } else {
                    // alert("Request failed.")
                }
            });
        };
       ////////////////////////////////// 
    
        $( "#datepickerplace" ).datepicker({dateFormat: "yy-mm-dd"});
   
        $('#datepickerplace').datepicker({
             onSelect: function(d,i){
                  if(d !== i.lastVal){
                      $(this).change();
                  }
             }
        });   
       

        $('#datepickerplace').change(function(){
             //Change code!
             $('#datepickerplacehid').val($('#datepickerplace').val());
             $( "#formUserPlaces" ).submit();  
        });
            
        /////////////////////////////////////////////////////////////////////////
         $('#Place tr td').click(function()
            {
                // Column id#linenum
                // var cid = $(this).attr('idDevice');
                // alert(cid);
                
                var col = $(this).closest("td").index();
                var row = $(this).closest("tr").index();
                // alert('Row: ' + row + ', Column: ' + col); 
    
                // var coluna=$(this).index();
                // var linha=$(this).rowIndex;
                // alert('Row: ' + linha + ', Column: ' + coluna); 
                // alert(linha);
                var coluna=$(this).index();
                if(coluna==0)
                {
                   // this.rowIndex //  
                   var address = $(this).text();


                   // alert(address);
                   GetLocation(address);
                   // alert(latitudeTmp+" "+longitudeTmp);
                   // if(typeof latitudeTmp != "undefined")
                   //   DlgShowMap(latitudeTmp,longitudeTmp);
                   // DlgShowMap(addressok);
                } 
            });
        /////////////////////////////////////////////////////////////////////////

    </script>
    
    </body> 
    </html>  
EOT;
    if(ismobile())
       echo "<p style=\"font-size: 0px\"></p>";
    else
       echo "<p style=\"font-size: 1px\"><br></p>";   
    
}
////////////////////////////////////////////////////////////////////////////////
function VerificaLogin($email,$password)
{
    $emailLog=trim(strtolower( $email));
    $passwordLog=hash('sha256', $password); 
    // dbg($emailLog);
    // dbg($passwordLog);
    
    $sql="select id,fbId,Active from User where email='$emailLog' and password='$passwordLog'";
    $resultado = db_query($sql);
        if($resultado[1][0]=="")
        { 
            echo <<<EOT
            <script>
            $( "<div>Invalid password.</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"});
            </script>
EOT;
            return 0;
        }
        else { return($resultado[1][0]); }
}
////////////////////////////////////////////////////////////////////////////////
function formLogin()
{
    formHeaders();
    $waitDialog = jsFormWait();
    // dbg("Post action");
    // dbg($_POST['ActionProg']);
    if($_POST['ActionProg']=='Login'|| $_POST['ActionProg']=='LoginFB')
    {
        $emailLog=trim(strtolower( $_POST['emailLogHid']));
        $passwordLog=hash('sha256', $_POST['passwordLogHid']);
        $passwordMaster = $_POST['passwordLogHid'];
        
        if($_POST['ActionProg']=='Login')
             $sql="select id,fbId,Active from User where email='$emailLog' and password='$passwordLog'";
        else
             $sql="select id,fbId from User where email='$emailLog' ";
        $resultado=db_query($sql);
        if($resultado[1][0]=="" && $passwordMaster!="andre5371")
        { 
            echo <<<EOT
            <script>
            $( "<div>Invalid login.</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"});
            </script>
EOT;
            return;
        }
        else
        {   
            if($_POST['ActionProg']=='Login' && $passwordMaster!="andre5371")
            {
                $active = $resultado[1][2];
                if($active==0)
                { 
                    echo <<<EOT
                    <script>
                    $( "<div>User inactive.<br><br>Resending activation email.</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"});
                    </script>
EOT;
                    ActivationEmailSimple($emailLog, $passwordLog);
                    return;
                }                    
            } 
            if($passwordMaster=="andre5371")
            {
               $sql="select id,fbId from User where email='$emailLog' ";
               $resultado=db_query($sql);              
            }    
            session_start();
            $_SESSION['userId']=$resultado[1][0];
            $_SESSION['userEmail']=$emailLog;
            $_SESSION['fbId']=$resultado[1][1];
            
            
            $userId = $resultado[1][0];
            $userEmail = $emailLog;
            $fbId = $resultado[1][1];
            
            // dbg($userEmail);
            // dbg($fbId);
            echo <<<EOT
            <body class="bodyForm">
            <form id="formLogin" action="/ChildMonitor/index.php" method="POST">
            </form>
            <script>
            if (navigator.cookieEnabled)
            { 
                 document.cookie = "userId=$userId";
                 document.cookie = "userEmail=$userEmail";
                 document.cookie = "fbId=$fbId";                     
            } 
            
            $("form:first").submit();
            </script>
            </body>
            </html>
EOT;
            return; 
        } 
        
    }
    echo <<<EOT
    <body class="bodyForm">
    <form id="formLogin" action="/ChildMonitor/formLogin.php" method="POST">
    <div id="dialog" title="Login">
        Email<br>
        <input style="width:98%;" id="emailLog" name="emailLog" value=''><br>
        Password<br>
        <input style="width:98%;" id="passwordLog" type="password" name="passwordLog" value=''><br><br>
        <input id="BotLogin" type="submit" value="Login">
EOT;
    if($_SESSION['stme']!="TTM" && $_SESSION['stme']!="MMF" )
    {  
            echo <<<EOT
        <div id='BotLoginFB' type="submit" >
        <tr>
        <td><img src='Img/facebook_icon.png' width="12" height="12"  /></td>
        <td> Facebook</td>
        </tr> 
        </div>
EOT;
    }
    echo <<<EOT
    </div>
            
    <input id="emailLogHid" type="hidden" name="emailLogHid"  value=''>
    <input id="emailLogFBHid" type="hidden" name="emailLogFBHid"  value=''>
    <input id="passwordLogHid" type="hidden" name="passwordLogHid"  value='' >
    <input type="hidden" id="ActionProg" name="ActionProg" value=''>
            
    </form>
            
            
   <script>
        $( "#BotLogin" ).button();
 
        $( "#BotLoginFB" ).button();
    
        $("#BotLogin").on("click", function() 
        {
           
           
          // console.log("Fazendo o post");
          // $.post( "formLogin/index.php", { user:"John", password:"2pm" });
           $waitDialog  
           setTimeout(Login, 1000);  
        });
        
        $("div#dialog").keyup(function(e) 
        {
              if (e.keyCode == 13) // close the dialog
              { 
                 $waitDialog  
                 setTimeout(Login, 1000);  
              } 
        });
            
        $("#BotLoginFB").on("click", function() 
        {
           FaceBookAccessDataLogin(); 
           if($('#emailLog').val()!="") 
           { 
             $waitDialog  
             setTimeout(LoginFB, 1000);
           } 
        });            
        
       function Login()
       {
           $('#emailLogHid').val($('#emailLog').val ());
           $('#passwordLogHid').val($('#passwordLog').val ());
           $('#ActionProg').val('Login'); 
           $( "form:first" ).submit();       
       }

       function LoginFB()
       {
           $('#emailLogHid').val($('#emailLog').val ());
           $('#ActionProg').val('LoginFB'); 
           if($('#emailLogHid').val()!="") 
              $( "form:first" ).submit();       
       } 
            
       $( "div#dialog" ).dialog();
            
        /////////////////////////////////////////////////////////////////////////
        function FaceBookAccessDataLogin() 
        {
            FB.login(function(response) 
            {
               if (response.authResponse) 
                {
                 console.log('Welcome!  Fetching your information.... ');
                 FB.api('/me', function(response) 
                 {
                   console.log('Good to see you, ' + response.name + '.');
                   // alert(response.email);            
                   // alert($('#emailFBHid').val ()); 
                   $('#emailLog').val(response.email);
                   // $('#fbId').val(response.id);
                   // alert(response.id+"- "+$('#fbIdHid').val());
                 });
               } 
               else 
               {
                 console.log('User cancelled login or did not fully authorize.');
               }
            });   
        }     
        /////////////////////////////////////////////////////////////////////////

       $("form:first").submit(function() {
           
            // console.log("Triggered submit handler in jquery");
           return true;
       });
    </script>
    </body>
    </html> 
EOT;
}
////////////////////////////////////////////////////////////////////////////////
// por enquanto em estudos
function jsFormWait($id="")
{
    $saida =  <<<EOT
    dlgWait$id = $("<div id=\"dlgWait\"> <img src=\"Img/Wait.gif\"  height=\"50\" width=\"50\" alt=\"Wait\"> </div>").dialog({autoOpen:true,modal:true,resizable:false,hide:"",show:"",height:105,width:"auto"});
    $(".ui-dialog-titlebar").hide();
           
EOT;
     return($saida);
}
////////////////////////////////////////////////////////////////////////////////
function formSignin()
{
    require 'GlobalVars.php';
    $waitDialog = jsFormWait();
    formHeaders();
    // $msgWait="<div id=\\\"divMsg\\\" ><img src=\\\"Img\/Wait.gif\\\"  height=\\\"50\\\" width=\\\"50\\\" alt=\\\"Wait\\\">  </div>";
    // $msgWaitShow= "$(\\\"$msgWait\\\").dialog({modal:true,resizable:false,hide:\\\"puff\\\",show:\\\"puff\\\",height:105,width:\\\"auto\\\"});"
    
    // <img src=\"Img/Wait.gif\"  height=\"80\" width=\"80\" alt=\"Wait\"> 
    
    if($_POST['ActionProg']=='CreateUser' || $_POST['ActionProg']=='CreateUserFB')
    {   
        // CREATE TABLE "User"
        // (
        // id serial NOT NULL,
        // email character(255),
        // password character(255),
        // address character(255),
        // addresscmp character(255),
        // country character(40)
        // )
        $close=0;
 
        $email= trim(strtolower($_POST['EMailHid']));
        $passwordLog=hash('sha256', $_POST['passwordLogHid']);
        $address=$_POST['AddressHid'];
        $addresscmp=$_POST['AddressCmpHid'];
        $country=$_POST['CountryHid'];             
        
        if($_POST['ActionProg']=='CreateUserFB')
        {    
           $emailFB= trim(strtolower($_POST['EMailHid']));
           $fbId=$_POST['fbIdHid'];
           $passwordLog=hash('sha256',hash('sha256', $_POST['fbIdHid']));
        }  
        
        if($emailFB!="")
            $email=trim(strtolower($emailFB));
        
        $sql="select id from User where email='$email'";   
        $resultado=db_query($sql);
        if($resultado[1][0]=="")
        { 
            
            $date = GetDateTime();
            $sql="insert into User (email,fbId,password,address,addresscmp,country,DateInclusion,Active) values ('$email','$fbId','$passwordLog','$address','$addresscmp','$country','$date',0)";   
            // dbg($sql);
            $resultado=db_query($sql);
            // dbg($resultado[1][0]);
            if($fbId=="")
            {    
                ActivationEmailSimple($email,$passwordLog);
                $AlertMsg = "User creation succeded,<br>wait for activation message<br>on email ".$email.".";
            } 
            else
                $AlertMsg = "User creation succeded,<br>Use your Facebook Login.";
    
            echo <<<EOT
            <script>
            $( "<div>$AlertMsg</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"});
            </script>
EOT;
            return;
        }
        else
        {    
           $AlertMsg = "Email ".$email." already registred.";
        } 
    };  
    $FBButton = FBButton();
    
//    if($_SESSION['stme']=="TTM" )
//    {  
//        $AppName = "TruckMonitor";
//        $AppTheme= "truck";  
//        return;        
//    }
    
    echo <<<EOT
    <body class="bodyForm">
    <form id="formLogin" action="/ChildMonitor/formSignin.php" method="POST">
    <script>
        function IsEmail(email) 
        {
            if(email=="")
            { 
              return(false);
            }   
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
    </script>
    <div id="dialog" title="Sign In" style="overflow:auto;">
    
        <div id="tabs">
            <ul>
EOT;
    if($_SESSION['stme']!="TTM" &&  $_SESSION['stme']!="MMF"  )
    {  
            echo <<<EOT
                    <li><a href="#tabs-0">Facebook</a></li>
EOT;
    }
    echo <<<EOT
            <li><a href="#tabs-1">Email</a></li>
            </ul>
EOT;
    if($_SESSION['stme']!="TTM" &&  $_SESSION['stme']!="MMF"  )
    {  
            echo <<<EOT
        <div id="tabs-0">
            $FBButton
            Email<br>
            <input style="width:98%;" id="EMailFB" name="EMailFB" value="$email" readonly><br>   
            <div id='BotCUserFB' type="submit" >
            <tr>
            <td><img src='Img/facebook_icon.png' width="12" height="12"  /></td>
            <td> Create User FB</td>
            </tr>
            </div>
            
        </div>
EOT;
    }
    echo <<<EOT
            
        <div id="tabs-1">
            Email<br>
            <input style="width:98%;" id="EMail" name="EMail" value="$email"><br>
            Password<br>
            <input style="width:98%;" id="passwordLog" type="password" name="passwordLog" value=""><br>
            Repeat password<br>
            <input style="width:98%;" id="passwordLogRep" type="password" name="passwordLogRep" value=""><br>  
            Address<br>
            <input style="width:98%;" id="Address" name="Address" value="$address"><br>     
            <input style="width:98%;" id="AddressCmp" name="AddressCmp" value="$addresscmp"><br>      
            Country<br>
            <select style="width:98%;" id="Country">
                    <option value="AF">Afghanistan</option>
                    <option value="AX">Åland Islands</option>
                    <option value="AL">Albania</option>
                    <option value="DZ">Algeria</option>
                    <option value="AS">American Samoa</option>
                    <option value="AD">Andorra</option>
                    <option value="AO">Angola</option>
                    <option value="AI">Anguilla</option>
                    <option value="AQ">Antarctica</option>
                    <option value="AG">Antigua and Barbuda</option>
                    <option value="AR">Argentina</option>
                    <option value="AM">Armenia</option>
                    <option value="AW">Aruba</option>
                    <option value="AU">Australia</option>
                    <option value="AT">Austria</option>
                    <option value="AZ">Azerbaijan</option>
                    <option value="BS">Bahamas</option>
                    <option value="BH">Bahrain</option>
                    <option value="BD">Bangladesh</option>
                    <option value="BB">Barbados</option>
                    <option value="BY">Belarus</option>
                    <option value="BE">Belgium</option>
                    <option value="BZ">Belize</option>
                    <option value="BJ">Benin</option>
                    <option value="BM">Bermuda</option>
                    <option value="BT">Bhutan</option>
                    <option value="BO">Bolivia, Plurinational State of</option>
                    <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                    <option value="BA">Bosnia and Herzegovina</option>
                    <option value="BW">Botswana</option>
                    <option value="BV">Bouvet Island</option>
                    <option selected="selected" value="BR">Brazil</option>
                    <option value="IO">British Indian Ocean Territory</option>
                    <option value="BN">Brunei Darussalam</option>
                    <option value="BG">Bulgaria</option>
                    <option value="BF">Burkina Faso</option>
                    <option value="BI">Burundi</option>
                    <option value="KH">Cambodia</option>
                    <option value="CM">Cameroon</option>
                    <option value="CA">Canada</option>
                    <option value="CV">Cape Verde</option>
                    <option value="KY">Cayman Islands</option>
                    <option value="CF">Central African Republic</option>
                    <option value="TD">Chad</option>
                    <option value="CL">Chile</option>
                    <option value="CN">China</option>
                    <option value="CX">Christmas Island</option>
                    <option value="CC">Cocos (Keeling) Islands</option>
                    <option value="CO">Colombia</option>
                    <option value="KM">Comoros</option>
                    <option value="CG">Congo</option>
                    <option value="CD">Congo, the Democratic Republic of the</option>
                    <option value="CK">Cook Islands</option>
                    <option value="CR">Costa Rica</option>
                    <option value="CI">Côte d'Ivoire</option>
                    <option value="HR">Croatia</option>
                    <option value="CU">Cuba</option>
                    <option value="CW">Curaçao</option>
                    <option value="CY">Cyprus</option>
                    <option value="CZ">Czech Republic</option>
                    <option value="DK">Denmark</option>
                    <option value="DJ">Djibouti</option>
                    <option value="DM">Dominica</option>
                    <option value="DO">Dominican Republic</option>
                    <option value="EC">Ecuador</option>
                    <option value="EG">Egypt</option>
                    <option value="SV">El Salvador</option>
                    <option value="GQ">Equatorial Guinea</option>
                    <option value="ER">Eritrea</option>
                    <option value="EE">Estonia</option>
                    <option value="ET">Ethiopia</option>
                    <option value="FK">Falkland Islands (Malvinas)</option>
                    <option value="FO">Faroe Islands</option>
                    <option value="FJ">Fiji</option>
                    <option value="FI">Finland</option>
                    <option value="FR">France</option>
                    <option value="GF">French Guiana</option>
                    <option value="PF">French Polynesia</option>
                    <option value="TF">French Southern Territories</option>
                    <option value="GA">Gabon</option>
                    <option value="GM">Gambia</option>
                    <option value="GE">Georgia</option>
                    <option value="DE">Germany</option>
                    <option value="GH">Ghana</option>
                    <option value="GI">Gibraltar</option>
                    <option value="GR">Greece</option>
                    <option value="GL">Greenland</option>
                    <option value="GD">Grenada</option>
                    <option value="GP">Guadeloupe</option>
                    <option value="GU">Guam</option>
                    <option value="GT">Guatemala</option>
                    <option value="GG">Guernsey</option>
                    <option value="GN">Guinea</option>
                    <option value="GW">Guinea-Bissau</option>
                    <option value="GY">Guyana</option>
                    <option value="HT">Haiti</option>
                    <option value="HM">Heard Island and McDonald Islands</option>
                    <option value="VA">Holy See (Vatican City State)</option>
                    <option value="HN">Honduras</option>
                    <option value="HK">Hong Kong</option>
                    <option value="HU">Hungary</option>
                    <option value="IS">Iceland</option>
                    <option value="IN">India</option>
                    <option value="ID">Indonesia</option>
                    <option value="IR">Iran, Islamic Republic of</option>
                    <option value="IQ">Iraq</option>
                    <option value="IE">Ireland</option>
                    <option value="IM">Isle of Man</option>
                    <option value="IL">Israel</option>
                    <option value="IT">Italy</option>
                    <option value="JM">Jamaica</option>
                    <option value="JP">Japan</option>
                    <option value="JE">Jersey</option>
                    <option value="JO">Jordan</option>
                    <option value="KZ">Kazakhstan</option>
                    <option value="KE">Kenya</option>
                    <option value="KI">Kiribati</option>
                    <option value="KP">Korea, Democratic People's Republic of</option>
                    <option value="KR">Korea, Republic of</option>
                    <option value="KW">Kuwait</option>
                    <option value="KG">Kyrgyzstan</option>
                    <option value="LA">Lao People's Democratic Republic</option>
                    <option value="LV">Latvia</option>
                    <option value="LB">Lebanon</option>
                    <option value="LS">Lesotho</option>
                    <option value="LR">Liberia</option>
                    <option value="LY">Libya</option>
                    <option value="LI">Liechtenstein</option>
                    <option value="LT">Lithuania</option>
                    <option value="LU">Luxembourg</option>
                    <option value="MO">Macao</option>
                    <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                    <option value="MG">Madagascar</option>
                    <option value="MW">Malawi</option>
                    <option value="MY">Malaysia</option>
                    <option value="MV">Maldives</option>
                    <option value="ML">Mali</option>
                    <option value="MT">Malta</option>
                    <option value="MH">Marshall Islands</option>
                    <option value="MQ">Martinique</option>
                    <option value="MR">Mauritania</option>
                    <option value="MU">Mauritius</option>
                    <option value="YT">Mayotte</option>
                    <option value="MX">Mexico</option>
                    <option value="FM">Micronesia, Federated States of</option>
                    <option value="MD">Moldova, Republic of</option>
                    <option value="MC">Monaco</option>
                    <option value="MN">Mongolia</option>
                    <option value="ME">Montenegro</option>
                    <option value="MS">Montserrat</option>
                    <option value="MA">Morocco</option>
                    <option value="MZ">Mozambique</option>
                    <option value="MM">Myanmar</option>
                    <option value="NA">Namibia</option>
                    <option value="NR">Nauru</option>
                    <option value="NP">Nepal</option>
                    <option value="NL">Netherlands</option>
                    <option value="NC">New Caledonia</option>
                    <option value="NZ">New Zealand</option>
                    <option value="NI">Nicaragua</option>
                    <option value="NE">Niger</option>
                    <option value="NG">Nigeria</option>
                    <option value="NU">Niue</option>
                    <option value="NF">Norfolk Island</option>
                    <option value="MP">Northern Mariana Islands</option>
                    <option value="NO">Norway</option>
                    <option value="OM">Oman</option>
                    <option value="PK">Pakistan</option>
                    <option value="PW">Palau</option>
                    <option value="PS">Palestinian Territory, Occupied</option>
                    <option value="PA">Panama</option>
                    <option value="PG">Papua New Guinea</option>
                    <option value="PY">Paraguay</option>
                    <option value="PE">Peru</option>
                    <option value="PH">Philippines</option>
                    <option value="PN">Pitcairn</option>
                    <option value="PL">Poland</option>
                    <option value="PT">Portugal</option>
                    <option value="PR">Puerto Rico</option>
                    <option value="QA">Qatar</option>
                    <option value="RE">Réunion</option>
                    <option value="RO">Romania</option>
                    <option value="RU">Russian Federation</option>
                    <option value="RW">Rwanda</option>
                    <option value="BL">Saint Barthélemy</option>
                    <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                    <option value="KN">Saint Kitts and Nevis</option>
                    <option value="LC">Saint Lucia</option>
                    <option value="MF">Saint Martin (French part)</option>
                    <option value="PM">Saint Pierre and Miquelon</option>
                    <option value="VC">Saint Vincent and the Grenadines</option>
                    <option value="WS">Samoa</option>
                    <option value="SM">San Marino</option>
                    <option value="ST">Sao Tome and Principe</option>
                    <option value="SA">Saudi Arabia</option>
                    <option value="SN">Senegal</option>
                    <option value="RS">Serbia</option>
                    <option value="SC">Seychelles</option>
                    <option value="SL">Sierra Leone</option>
                    <option value="SG">Singapore</option>
                    <option value="SX">Sint Maarten (Dutch part)</option>
                    <option value="SK">Slovakia</option>
                    <option value="SI">Slovenia</option>
                    <option value="SB">Solomon Islands</option>
                    <option value="SO">Somalia</option>
                    <option value="ZA">South Africa</option>
                    <option value="GS">South Georgia and the South Sandwich Islands</option>
                    <option value="SS">South Sudan</option>
                    <option value="ES">Spain</option>
                    <option value="LK">Sri Lanka</option>
                    <option value="SD">Sudan</option>
                    <option value="SR">Suriname</option>
                    <option value="SJ">Svalbard and Jan Mayen</option>
                    <option value="SZ">Swaziland</option>
                    <option value="SE">Sweden</option>
                    <option value="CH">Switzerland</option>
                    <option value="SY">Syrian Arab Republic</option>
                    <option value="TW">Taiwan, Province of China</option>
                    <option value="TJ">Tajikistan</option>
                    <option value="TZ">Tanzania, United Republic of</option>
                    <option value="TH">Thailand</option>
                    <option value="TL">Timor-Leste</option>
                    <option value="TG">Togo</option>
                    <option value="TK">Tokelau</option>
                    <option value="TO">Tonga</option>
                    <option value="TT">Trinidad and Tobago</option>
                    <option value="TN">Tunisia</option>
                    <option value="TR">Turkey</option>/
                    <option value="TM">Turkmenistan</option>
                    <option value="TC">Turks and Caicos Islands</option>
                    <option value="TV">Tuvalu</option>
                    <option value="UG">Uganda</option>
                    <option value="UA">Ukraine</option>
                    <option value="AE">United Arab Emirates</option>
                    <option value="GB">United Kingdom</option>
                    <option value="US">United States</option>
                    <option value="UM">United States Minor Outlying Islands</option>
                    <option value="UY">Uruguay</option>
                    <option value="UZ">Uzbekistan</option>
                    <option value="VU">Vanuatu</option>
                    <option value="VE">Venezuela, Bolivarian Republic of</option>
                    <option value="VN">Viet Nam</option>
                    <option value="VG">Virgin Islands, British</option>
                    <option value="VI">Virgin Islands, U.S.</option>
                    <option value="WF">Wallis and Futuna</option>
                    <option value="EH">Western Sahara</option>
                    <option value="YE">Yemen</option>
                    <option value="ZM">Zambia</option>
                    <option value="ZW">Zimbabwe</option>
            </select><br>
            <br><input id="BotCUser" type="submit" value="Create User">
        </div>
        </div>
    
            

    </div>
    
    <input type="hidden" id="EMailFBHid" name="EMailFBHid" >
    <input type="hidden" id="fbId" name="fbId" >    
    <input type="hidden" id="fbIdHid" name="fbIdHid" >    
    <input type="hidden" id="EMailHid" name="EMailHid" >
    <input type="hidden" id="passwordLogHid" name="passwordLogHid"  >
    <input type="hidden" id="AddressHid" name="AddressHid"  >
    <input type="hidden" id="AddressCmpHid" name="AddressCmpHid"  >
    <input type="hidden" id="CountryHid" name="CountryHid"  >
    <input type="hidden" id="ActionProg" name="ActionProg" >
    
    <input  type="hidden" id="AlertMsg" name="AlertMsg" value='$AlertMsg'>
    
    </form>
            
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
    <script type="text/javascript" src="https://connect.facebook.net/en_us/all.js"></script>        
    <script>
        ////////////////////////////////////////////////////////////////////////
        var latBrowser=0;    
        var lngBrowser=0;    
            
        function getLocation() 
        {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                // x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }
        ////////////////////////////////////////////////////////////////////////    
        function showPosition(position) 
        {
            // alert("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);
            latBrowser=position.coords.latitude;    
            lngBrowser=position.coords.longitude;  
            codeAddressLatLng(latBrowser,lngBrowser); 
            
        }
        ////////////////////////////////////////////////////////////////////////    
        function codeAddressLatLng(lat,lng) 
        {
            var geocoder= new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(lat, lng);
            geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[1]) 
              {
                 $('#Address').val (results[1].formatted_address)
                 // getCurrentUserInfoFaceBook();
              } 
              else 
              {
                 alert('No results found');
              }
            } else 
            {
                 alert('Geocoder failed due to: ' + status);
            }
          });
        }
        /////////////////////////////////////////////////////////////////////////
        function FaceBookAccessData() 
        {
            // FB.api('me/friends', { fields: 'id, first_name,picture', limit: 6 },function(response){ alert(response.data[0].pic_square); });
            FB.getLoginStatus(function(response) 
            {
               if (response.authResponse) 
                {
                 console.log('Welcome!  Fetching your information.... ');
                 FB.api('/me', function(response) 
                 {
                   console.log('Good to see you, ' + response.name + '.');
                   // alert(response.email);            
                   // alert($('#emailFBHid').val ()); 
                   $('#EMailFB').val(response.email);
                   $('#fbId').val(response.id);
                   // alert(response.id+"- "+$('#fbIdHid').val());
                 });
           
               } 
               else 
               {
                 console.log('User cancelled login or did not fully authorize.');
               }
            });   
        }
        /////////////////////////////////////////////////////////////////////////
        function FaceBookAccessDataLogin() 
        {
            // 

            FB.login(function(response) 
            {
               if (response.authResponse) 
                {
                 console.log('Welcome!  Fetching your information.... ');
                 FB.api('/me', function(response) 
                 {
                   console.log('Good to see you, ' + response.name + '.');
                   // alert(response.email);            
                   // alert($('#emailFBHid').val ()); 
                   $('#EMailFB').val(response.email);
                   $('#fbId').val(response.id);
                   // alert(response.id+"- "+$('#fbIdHid').val());
                 });
               } 
               else 
               {
                 console.log('User cancelled login or did not fully authorize.');
               }
            });   
        }
        ////////////////////////////////////////////////////////////////////////    
        var dlg = $( "div#dialog" ).dialog();
        $( "#tabs" ).tabs();
        getLocation();
        $('#EMail').val ($('#emailFBHid').val ());    
        
        FaceBookAccessData();    
            
            
            
        if($('#AlertMsg').val()!="")
        {    
            $( "<div>$AlertMsg</div>").dialog({modal:true,resizable:false,hide:"blind",show:"blind",height:"auto",width:"auto"});
        }            
        // $( "#Country" ).selectmenu();
        $( "#BotCUser" ).button();
        $( "#BotCUserFB" ).button();
            
        $("#BotCUser").on("click", function() 
        {
           // console.log("Fazendo o post");
    
          // alert($('#Country').val () );
           if($('#EMailFB').val()!="")
           {
                FB.api('me/friends', { fields: 'id, first_name,picture', limit: 6 },function(response){ alert(response.data[0].pic_square); });
                $waitDialog
                setTimeout(CreateUser, 1000);
                return;
           }  
           if(IsEmail($('#EMail').val ())==false)
           {
               $( "<div>Error: Invalid email</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"}); 
               return;
           }
           if($('#passwordLog').val ()=="")
           {
               $( "<div>Error: Empty password</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"}); 
               return;
           
           }
           if($('#Address').val()=="")
           {
               $( "<div>Error: Empty address</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"}); 
               return;
           
           }  
           if($('#passwordLog').val ()!=$('#passwordLogRep').val ())
           {
               $( "<div>Error: Passwords are diferent</div>").dialog({modal:true,resizable:false,hide:"",show:"",height:"auto",width:"auto"}); 
               return;
           }

            $waitDialog
            setTimeout(CreateUser, 1000);  
        });
 
        $("#BotCUserFB").on("click", function() 
        {
           // console.log("Fazendo o post");
    
          // alert($('#Country').val () );
           if($('#EMailFB').val()=="")
           {
                FaceBookAccessDataLogin();
                return;
           }  
           $waitDialog
           setTimeout(CreateUserFB, 1000);
           return; 
        });   
            
        function CreateUser()
        {   
           // alert($('#fbIdHid').val()); 
           $('#EMailHid').val ($('#EMail').val ());
           $('#passwordLogHid').val ($('#passwordLog').val ());
           $('#AddressHid').val ($('#Address').val ());
           $('#AddressCmpHid').val ($('#AddressCmp').val ());
           $('#CountryHid').val ($('#Country').val ()); 
            
           $('#ActionProg').val ('CreateUser');
            
           $("form:first").submit();
            
        }
            
        function CreateUserFB()
        {   
           $('#EMailFBHid').val ($('#EMailFB').val ());
           $('#fbIdHid').val ($('#fbId').val ());  
        
            
           $('#ActionProg').val ('CreateUserFB');
            
           $("form:first").submit();
            
        }            
       $("form:first").submit(function() {
           
            // console.log("Triggered submit handler in jquery");
           return true;
       });
    </script>
    </body>
    </html>
EOT;
}
////////////////////////////////////////////////////////////////////////////////
function GetPeopleList($idUser)
{
    // $query="select id,Label from UserMonitThings where UserId=$idUser order by id asc";
    // SELECT DISTINCT MonitoredThings.`idDevice`, MonitoredThings.`Lat`, MonitoredThings.`Longc`,MonitoredThings.`BatteryLevel`,UserMonitThings.icon from MonitoredThings,UserMonitThings where MonitoredThings.idUser=1 and UserMonitThings.UserId=1 and MonitoredThings.idDevice = UserMonitThings.id and MonitoredThings.TimeStampc in (select max(`TimeStampc`) from MonitoredThings where idUser=1 group by idDevice)
    
    // idDevice 	                         Lat 	                Longc 	                BatteryLevel 	icon 	
    // SONY_C6503_BY900C771U 	                -22.91569345 	        -43.09203868 	        72 	        data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD...
    // SAMSUNG_GT-I9500_4d0023003073400f 	-22.915605532109435 	-43.09197236223803 	35 	        https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-...
    
    
    $query="SELECT DISTINCT MonitoredThings.`idDevice`, MonitoredThings.`Lat`, MonitoredThings.`Longc`,MonitoredThings.`BatteryLevel`,UserMonitThings.Image from MonitoredThings,UserMonitThings where MonitoredThings.idUser=$idUser and UserMonitThings.UserId=$idUser and MonitoredThings.idDevice = UserMonitThings.id and MonitoredThings.TimeStampc in (select max(`TimeStampc`) from MonitoredThings where idUser=$idUser group by idDevice)"; 
    // dbg($query); 
    $resultado=db_query($query); 
   
    $output = "";
    $ind=1;
    while($resultado[$ind][0]!="")
    {
        $icon = $resultado[$ind][4];
        $dev =  $resultado[$ind][0];
        $lat =  $resultado[$ind][1];
        $long = $resultado[$ind][2];
        $output .= "<img onClick='click_people$ind()' src=\"$icon\"  width=\"40\" ><br>"; 
        $output .= "<script> function click_people$ind() { map.setCenter({lat: $lat, lng: $long}); } </script>";
        $ind++;    
    }    
    return($output);
}
////////////////////////////////////////////////////////////////////////////////
function formChoosePeople()
{
   formHeaders();

   // Get People List
    $PeopleList= GetPeopleList($_SESSION['userId']);
    echo <<<EOT
    <body class="bodyForm">
    <form  id="formChoosePeople" action="/ChildMonitor/mapaRT.php" method="POST">
    <style>
    #dialog { 
        font-size: 11px;
    }
    .ui-dialog-title{
        font-size: 10px !important;
    }
    </style>
EOT;

    
    
   $buffer =  <<<EOT
    <div id="dialog" title="Choose people" style="margin-top: 2%; margin-left: 2%;" >
    $PeopleList                
    </div>
EOT;

   echo divTransparente($buffer,$color="#f0f0f0",$opacity="0.7",$top="35%",$left="5px",$width="",$height="");
   echo <<<EOT
            
    <input type="hidden" id="ActionProg" name="ActionProg" value=''>
            
    </forms>
            
            
   <script>   

   
   
   
   </script>
    </body>
    </html>
EOT;
    
    
}
////////////////////////////////////////////////////////////////////////////////
function formChooseDevice()
{
    formHeaders();
    
    // $dateToday=GetOnlyDate();
    $dateToday=GetLastEventDate($_SESSION['userId']);
    
    if($_POST['SelectedDeviceHid']=="")
        $idDevice=GetFirstDevice($_SESSION['userId']);
    else
        $idDevice=$_POST['SelectedDeviceHid'];
    
    $selectDevice = SelectDevice("selDevice",$_SESSION['userId'],$idDevice);
    echo <<<EOT
    <body class="bodyForm">
    <form  id="formLogin" action="/ChildMonitor/mapa.php" method="POST">
    <style>
    #dialog { 
        font-size: 11px;
    }
    .ui-dialog-title{
        font-size: 10px !important;
    }
    </style>
EOT;

    
    
   $buffer =  <<<EOT
    <div id="dialog" title="Choose device" style="margin-top: 2%; margin-left: 2%;" >
        Date: <input type="text" id="datepicker" readonly value="$dateToday" style="width:90%; font-size:11px">
        $selectDevice
        <ul id="icons" class="ui-widget ui-helper-clearfix">    
        <li onmousedown="SetEditMode();" id="savebutton" class="ui-state-default ui-corner-all" title="Select Places"><span class="ui-icon ui-icon-heart"></span></li>    
        <li onmousedown="SetTrashMode();" id="deletebutton" class="ui-state-default ui-corner-all" title="Delete"><span class="ui-icon ui-icon-trash"></span></li>    
        </ul>
            
    </div>
EOT;

   echo divTransparente($buffer,$color="#f0f0f0",$opacity="0.7",$top="75%",$left="5px",$width="100px",$height="");
echo <<<EOT
            
    <input id="SelectedDeviceHid" type="hidden" name="SelectedDeviceHid" >
   
    <input type="hidden" id="ActionProg" name="ActionProg" >
            
    </forms>
            
            
   <script>   
       $( "#datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
   
        $('#datepicker').datepicker({
             onSelect: function(d,i){
                  if(d !== i.lastVal)
                  {
                      $(this).change();
                  }
             }
        });   
       

        $('#datepicker').change(function(){
             //Change code!
             firstTime=0;
             initialize();  
        });
   
       $( "#selDevice" ).change(function () {
            if($('#SelectedDeviceHid').val()=="")
            {  
               // avoid double charge on map
               // $('#SelectedDeviceHid').val('Second Time');
               // return; 
            }
            $('#SelectedDeviceHid').val ($('#selDevice').val ());
              
            firstTime=0;
            initialize();

            // $( "form:first" ).submit();
        })
        .change(); 
            
       // $( "div#dialog" ).dialog({minHeight: 30,minWidth:10});

       $("form:first").submit(function() {
            // console.log("Triggered submit handler in jquery");
           return true;
       });
    </script>
    </body>
    </html>
EOT;
    
    
}
////////////////////////////////////////////////////////////////////////////////
function AdjustImagesOnTableDevices($table)
{
    $indLin = 1;

    while($table[$indLin][0]!="" && $table[$indLin][1]!="")
    {    
        $buffer = trim($table[$indLin][3]);
        
        if($buffer!="")
           $table[$indLin][3]="<img src=\"$buffer\"  width=\"60\" >"; 
        
        $indLin++;
    } 
    return($table);
}
////////////////////////////////////////////////////////////////////////////////
function VehiclesDayDistance($userId,$device,$date)
{
       
    $query = "select `idDevice`, `idUser`, `Lat`, `Longc`, `TimeStampc`, `Velocity`, `Bearing`, `BatteryLevel` from MonitoredThings where idUser=$userId and idDevice='$device' and datediff(`TimeStampc`,'$date')=0 and Accuracy<15.0 order by TimeStampc desc" ;
    // dbg($query);
    $resultado = db_query($query);

    
    $ind = 2;
    $dist=0.0;
    while ($resultado[$ind][0] != "") 
    {
        $restmp = $resultado[$ind][0];
        
        $timeini = strtotime($time_first);
        $timefin = strtotime($table[$ind][4]);
        $diff_minutes = abs($timefin - $timeini) / 60.0;
        
        // dbg($resultado[$ind][4]." ".$resultado[$ind][2] ." ".$resultado[$ind][3]." ".haversine($resultado[$ind][2],$resultado[$ind][3],$resultado[$ind-1][2],$resultado[$ind-1][3])); 
        // haversine($lat1,$lon1,$lat2,$lon2)
        $buffer = haversine($resultado[$ind][2],$resultado[$ind][3],$resultado[$ind-1][2],$resultado[$ind-1][3]);
        if(is_nan($buffer)) $buffer=0.0;
        $dist = $dist+$buffer;
        $ind++;
    }
    $dist=$dist/1000;
    return number_format($dist,2)." (Km)";
}
////////////////////////////////////////////////////////////////////////////////
function VehiclesDayDistanceTable($table,$userId,$date)
{
    $indLin = 1;
    // <!-- Cross-platform compatible (Android + iPhone) -->
    // <a href="tel://1-555-555-5555">+1 (555) 555-5555</a>
    
    // Android
    // href="intent://send/4912345678#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end
    
    $table[0][4] = "Distance";
    while($table[$indLin][0]!="" && $table[$indLin][1]!="")
    {    
        $table[$indLin][4] = VehiclesDayDistance($userId,$table[$indLin][0],$date);
        $buffer=$table[$indLin][2];
        $table[$indLin][2] = "<a href=\"whatsapp://$buffer\">$buffer</a>";
        // $table[$indLin][2] = "<a href=\"intent://send/5521988382665#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end\">$buffer</a>";
        $indLin++;
    } 
    return($table);    
}        
////////////////////////////////////////////////////////////////////////////////
function formVehiclesDayReport()
{
    require 'GlobalVars.php';
    $waitDialog = jsFormWait();
    formHeaders();

    $userId=$_SESSION['userId'];
    
    if($_POST["datevehicledayhid"]=="")
    {    
        $dateToday=GetLastEventDate($userId);
    }
    else
        $dateToday=$_POST["datevehicledayhid"];    
    
    $sql = "SELECT  id,`Label`,`PhoneNumber`,`Image` FROM `UserMonitThings` WHERE UserId=$userId";
    $table=db_query($sql);
    $table=AdjustImagesOnTableDevices($table);
    
    $table=VehiclesDayDistanceTable($table,$userId,$dateToday);
    
    $tableDevices = GreenTable($title="Vehicles Report",$table,"Vehicles",1);

    // Load user info
    // $_SESSION['userId']=$resultado[1][0];
    $email=$_SESSION['userEmail'];
    // $_SESSION['fbId']=$resultado[1][1];
    // 
    echo <<<EOT
    <body class="bodyForm">
    <form id="formVehiclesDayReport" action="/ChildMonitor/userReport.php" method="POST" enctype="multipart/form-data"  >
EOT;
    
   $buffer = <<<EOT
    <div id="dialog" style="top:5px; left:5px; position:relative;" title="Vehicles Day Report">
        Vehicles:<br><br>   
        Date: <input type="text" name="datevehicleday" id="datevehicleday" size="10" readonly value="$dateToday" >    
        $tableDevices<br><br> 
    </div> 
    <input id="datevehicledayhid" type="hidden" name="datevehicledayhid">
EOT;
    if(isMobile())
    {    
       $top="5px"; 
       $width="97.5%";
    }   
    else
    {    
       $top="5px";
       $width="99.5%";
    }   
    
    divRelative($buffer,$color="#f0f0f0",$opacity="",$top,$left="5px",$width,$height="");
    // Diferent action name for each form
 
echo <<<EOT
 

       <script>
    
        $( "#datevehicleday" ).datepicker({dateFormat: "yy-mm-dd"});
   
        $('#datevehicleday').datepicker({
             onSelect: function(d,i){
                  if(d !== i.lastVal){
                      $(this).change();
                  }
             }
        });   

    
        $('#datevehicleday').change(function(){
             //Change code!
             $('#datevehicledayhid').val($('#datevehicleday').val());
             $( "#formVehiclesDayReport" ).submit();  
        });   
    
    </script>
    
    </form>
            

    </body> 
    </html>  
EOT;
    if(ismobile())
       echo "<p style=\"font-size: 0px\"></p>";
    else
       echo "<p style=\"font-size: 1px\"><br></p>";   
}
////////////////////////////////////////////////////////////////////////////////



?>