/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
////////////////////////////////////////////////////////////////////////////////
// $.getScript("/ChildMonitor/Util/fingerprint2.min.js", function(){ /* alert("Script loaded but not necessarily executed."); */ });

function jsRequire(url) 
{
    var ajax = new XMLHttpRequest();
    ajax.open('GET', url, false); // <-- the 'false' makes it synchronous
    ajax.onreadystatechange = function () {
        var script = ajax.response || ajax.responseText;
        if (ajax.readyState === 4) {
            switch (ajax.status) {
                case 200:
                    eval.apply(window, [script]);
                    console.log("script loaded: ", url);
                    break;
                default:
                    console.log("ERROR: script not loaded: ", url);
            }
        }
    };
    ajax.send(null);
}
////////////////////////////////////////////////////////////////////////////////
// jsRequire("/ChildMonitor/Util/Jquery/jquery.min.js"); 

$.ajax({
    async: false,
    url: "/ChildMonitor/Util/Multilanguage.min.js",
    dataType: "script"
});

$.ajax({
    async: true,
    url: "/ChildMonitor/Util/fingerprint2.min.js",
    dataType: "script"
});

$.ajax({
    async: true,
    url: "/ChildMonitor/Util/vanila-masker.min.js",
    dataType: "script"
});

$.ajax({
    async: true,
    url: "/ChildMonitor/Util/sprintf.min.js",
    dataType: "script"
});

$.ajax({
    async: true,
    url: "/ChildMonitor/Util/moment.min.js",
    dataType: "script"
});

$.ajax({
    async: false,
    url: "/ChildMonitor/Util/MapFunctions.min.js",
    dataType: "script"
});

$.ajax({
    async: false,
    url: "/ChildMonitor/Util/FileUpload.min.js",
    dataType: "script"
});

$.ajax({
    async: false,
    url: "/ChildMonitor/Util/printThis.min.js",
    dataType: "script"
});

$.ajax({
    async: false,
    url: "/ChildMonitor/Util/moip.min.js",
    dataType: "script"
});

//$.ajax({
//    async: false,
//    url: "/ChildMonitor/Util/compass.min.js",
//    dataType: "script"
//});
////////////////////////////////////////////////////////////////////////////
var ActiveFormWindow=null;
////////////////////////////////////////////////////////////////////////////
// Used to detect whether the users browser is an mobile browser
function isTestMobile() 
{
    ///<summary>Detecting whether the browser is a mobile browser or desktop browser</summary>
    ///<returns>A boolean value indicating whether the browser is a mobile browser or not</returns>

    if (sessionStorage.desktop) // desktop storage 
        return false;
    else if (localStorage.mobile) // mobile storage
        return true;

    // alternative
    var mobile = ['iphone','ipad','android','blackberry','nokia','opera mini','windows mobile','windows phone','iemobile']; 
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) return true;

    // nothing found.. assume desktop
    return false;
}
////////////////////////////////////////////////////////////////////////////////
function adjustNumberDB(num)
{
    return num.replace(",",".");
}
////////////////////////////////////////////////////////////////////////////////
function GetDateTime()
{
   dtBuffer = new Date();
   return moment(dtBuffer).format('YYYY-MM-DD HH:mm:ss');
}
////////////////////////////////////////////////////////////////////////////////
function GetDateLocale()
{
   dtBuffer = new Date();
   return moment(dtBuffer).format('DD/MM/YYYY');
}
////////////////////////////////////////////////////////////////////////////////
function GetYear()
{
   dtBuffer = new Date();
   return moment(dtBuffer).format('YYYY');
}
////////////////////////////////////////////////////////////////////////////////
function setCookie(cname, cvalue, exdays) 
{
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
} 
////////////////////////////////////////////////////////////////////////////////
function deleteCookie(name) 
{
    setCookie(name,"",-1);
}
////////////////////////////////////////////////////////////////////////////////
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
} 
////////////////////////////////////////////////////////////////////////////////
function VerifyLoginOnLoad(context)
{
    if(isLogged(context)==="")
        $(document).ready(function(){ /*code here*/ DialogLogin(context); }) 
}
////////////////////////////////////////////////////////////////////////////////
function isLogged(context)             
{
    
    if(typeof context==='undefined')
       return getCookie('userEmailProviderParkFit'); 
    if(typeof context==='')
       return getCookie('userEmailProviderParkFit');
    if(context==='ParkingFitProvider')
       return getCookie('userEmailProviderParkFit');    

    if(context==='ParkingFitUser')
       return getCookie('userEmailParkFit'); 

    if(context==='ParkingFitStaff')
       return getCookie('userEmailStaffParkFit'); 
    return "";
}
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
function isParkingStaffLogged()
{
    if(getCookie('userEmailProviderParkFit'))
        
    return getCookie('userEmailProviderParkFit'); 
}
////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////
function haversineDistance(coords1, coords2)
{
    function toRad(x) {
        return x * Math.PI / 180;
    }

    var lon1 = coords1[0];
    var lat1 = coords1[1];

    var lon2 = coords2[0];
    var lat2 = coords2[1];

    var R = 6371000.00; // m

    var x1 = lat2 - lat1;
    var dLat = toRad(x1);
    var x2 = lon2 - lon1;
    var dLon = toRad(x2)
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;

     
    return sprintf("%.2f",d);
}
//////////////////////////////////////////////////////////////////////////////// 
function CenterObject(id)
{
    $(id).css('position','absolute');
    $(id).css('top',($(window).width() - $(id).width()) / 2);
    $(id).css('left', ($(window).height() - $(id).height()) / 2);
}   

////////////////////////////////////////////////////////////////////////////////
/*
    Dynamic mask

    Sometimes you need swap the mask according with value. You can see the sample below on codepen.io
    http://codepen.io/lagden/pen/meemqG

    function inputHandlerVmask(masks, max, event) {
      var c = event.target;
      var v = c.value.replace(/\D/g, '');
      var m = c.value.length > max ? 1 : 0;
      VMasker(c).unMask();
      VMasker(c).maskPattern(masks[m]);
      c.value = VMasker.toPattern(v, masks[m]);
    }

    var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
    var tel = document.querySelector('#tel');
    VMasker(tel).maskPattern(telMask[0]);
    tel.addEventListener('input', inputHandlerVmask.bind(undefined, telMask, 14), false);

    var docMask = ['999.999.999-999', '99.999.999/9999-99'];
    var doc = document.querySelector('#doc');
    VMasker(doc).maskPattern(docMask[0]);
    doc.addEventListener('input', inputHandlerVmask.bind(undefined, docMask, 14), false);
*/
////////////////////////////////////////////////////////////////////////////////
function PutInputMask(id,Mask)
{
    // var tel = document.querySelector(id);
    // VMasker(tel).maskPattern(Mask);
    $(id).on('keyup', function() 
    {
        buffertmp =  VMasker.toPattern($(id).val() , Mask);
        // alert(buffertmp);
        $(id).val(buffertmp);
    });  
   
    // alert(VMasker.toPattern(1099911111, "(99) 9999-9999"));
}
////////////////////////////////////////////////////////////////////////////////
function ActionOnControls(controls,action)
{
  // ActionOnControls(["#start","#divOriDst"],"hide");
  ind=0;

  // console.log("ActionOnControls "+action); 
  while(!(controls[ind]===undefined))  
  {
      if(action==="hide")
      {
          $(controls[ind]).fadeOut();
      }
      if(action==="show")
      {
          $(controls[ind]).fadeIn();
      }
      if(action==="readonly")
      {
          $(controls[ind]).prop("readonly", true);
      }
      if(action==="notreadonly")
      {
          $(controls[ind]).prop("readonly", false);
      }
      ind++;
  }
}
////////////////////////////////////////////////////////////////////////////////
function isInt(x) {
    if(x=="") return false;
    return !isNaN(x) && eval(x).toString().length == parseInt(eval(x)).toString().length
}
////////////////////////////////////////////////////////////////////////////////
function isFloat(x) {
    if(x=="") return false;
    return !isNaN(x) && !isInt(eval(x)) && x.toString().length > 0
}
////////////////////////////////////////////////////////////////////////////////
function isEmail(email) 
{
    if(email=="")
    { 
      return(false);
    }   
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
////////////////////////////////////////////////////////////////////////////////
function ValidateControls(controls)
{
   ind=0;
   valid=true;
   while(!(controls[ind]===undefined))  
   {
       $(controls[ind].id).val($(controls[ind].id).val().trim());
       indval=0;
       while(!(controls[ind].valtype[indval]===undefined)) 
       {        
           if(controls[ind].valtype[indval]=="creditcard")
           {
               if($(controls[ind].id).val()!=="0000.0000.0000.0001" )
               if($(controls[ind].id).val()!=="0000.0000.0000.0002" )    
               if(!moip.creditCard.isValid($(controls[ind].id).val()) )
               {
                   valid=false;
                   ShowErrorToast(controls[ind].name+" inválido");
               }    
           }             
           if(controls[ind].valtype[indval]=="email")
           {
               if(!isEmail($(controls[ind].id).val()))
               {
                   valid=false;
                   ShowErrorToast(controls[ind].name+" inválido");
               }    
           }  
           if(controls[ind].valtype[indval]=="notnull")
           {
               if($(controls[ind].id).val()=="")
               {
                   valid=false;
                   ShowErrorToast(controls[ind].name+" vazio");
               }    
           }  
           if(controls[ind].valtype[indval]=="size")
           {
               tmpbuffer = $(controls[ind].id).val();
               if(tmpbuffer.length < controls[ind].size)
               {
                   valid=false;
                   ShowErrorToast(controls[ind].name+" tamanho inválido "+controls[ind].size);
               }    
           }   
           if(controls[ind].valtype[indval]=="int")
           {
               if(!isInt($(controls[ind].id).val()))
               {
                   valid=false;
                   ShowErrorToast(controls[ind].name+" inválido");
               }    
           }  
           if(controls[ind].valtype[indval]=="float")
           {
               if(!isFloat($(controls[ind].id).val()))
               {
                   if(!isInt($(controls[ind].id).val()))
                   {
                      valid=false;
                      ShowErrorToast(controls[ind].name+" inválido");
                   }
               }    
           }  
           indval++;
       }  
       ind++;
   }
   return valid;
}  
////////////////////////////////////////////////////////////////////////////////
function retirarAcento(strbuffer) 
{
    var varString = new String(strbuffer);
    var stringAcentos = new   String('àâêôûãõáéíóúçüÀÂÊÔÛÃÕÁÉÍÓÚÇÜ');
    var stringSemAcento = new String('aaeouaoaeioucuAAEOUAOAEIOUCU');

    var i = new Number();
    var j = new Number();
    var cString = new String();
    var varRes = '';

    for (i = 0; i < varString.length; i++) {
        cString = varString.substring(i, i + 1);
        for (j = 0; j < stringAcentos.length; j++) {
            if (stringAcentos.substring(j, j + 1) == cString) {
                cString = stringSemAcento.substring(j, j + 1);
            }
        }
        varRes += cString;
    }
    return varRes;
}
////////////////////////////////////////////////////////////////////////////
function timeDiffMilissec(dFinal, dInicial) 
{
    var d1 = dFinal.getTime();
    var d2 = dInicial.getTime();
    var df = (d1 - d2);

    return df;
}
////////////////////////////////////////////////////////////////////////////////
function PeriodoExpirado(strDateNow,strDatePark,period)
{
    var dtNow  = new Date(strDateNow);
    var dtPark = new Date(strDatePark);
    diffHours = Math.abs(dtNow - dtPark)*(1/1000)*(1/60)*(1/60);
    if(diffHours>period)
        return true;
    else
        return false;
}    
////////////////////////////////////////////////////////////////////////////////
function RegisterPlateEndTime(datPark)
{
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    
    json_str = JSON.stringify(datPark);
    fdatProcess = function(data) 
    {   // alert(data.response); 
        if(data.response=="PenaltyRegistred")
          ShowInfoToast("Infração registrada: "+data.placa); 
        else
        if(data.response=="PenaltyAlreadyRegistred")
          ShowInfoToast("Infração já registrada hoje: "+data.placa); 
        else
          ShowErrorToast("Erro registrando infraçao");  
    };
    
    // alert(datPark.DateTimePark);
    execAjax(url,{ type:"RegisterPlateEndTime",json:json_str},false,fdatProcess);
}
////////////////////////////////////////////////////////////////////////////////               
function PostGetPlateLastStatus(datPark,fdatProcess)
{   
    tempData = datPark;  
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    

    json_str = JSON.stringify(datPark);

    execAjax(url,{ type:"GetPlateLastStatus",json:json_str},false,fdatProcess);

}
////////////////////////////////////////////////////////////////////////////////               
function PostGenericAction(action,datSend,fdatProcess)
{   
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    json_str = JSON.stringify(datSend);
    execAjax(url,{ type:action,json:json_str},false,fdatProcess);
}
////////////////////////////////////////////////////////////////////////////////
function PostGeoServiceParkingPay(datPark)
{   
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    if(isNaN(datPark.heading))
       datPark.heading="Null";
    if(datPark.heading=="")
       datPark.heading="Null"; 
    if(datPark.heading===null) // chrome
       datPark.heading="Null";
    json_str = JSON.stringify(datPark);
    ok_pay=false;
    fdatProcess = function(data) 
    {   // alert(data.response); 
        // alert(data.period); 
        if(data.response=="PaymentOk")
        {
           ShowInfoToast("Pagamento aprovado - "+data.period,1); 
           ok_pay=true;
        }
        else
        {
          ShowErrorToast("Pagamento não aprovado<br>"+data.ReturnMessage);  
        }
    };
    execAjax(url,{ type:"ParkingPay",json_str:json_str},false,fdatProcess); 
    return ok_pay;
}
////////////////////////////////////////////////////////////////////////////////
function PostGetCarsInThisStreet(glbLatNow,glbLngNow,glbCountry,glbCity,glbStreet,datetime,fdatProcess)
{
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    

    execAjax(url,{ type:"GetCarsInThisStreet",lat:glbLatNow,lon:glbLngNow,
                   country:glbCountry,city:glbCity,street:glbStreet,datetime:datetime},true,fdatProcess);
    return "";
}
////////////////////////////////////////////////////////////////////////////////
function execAjax(url,xdata,async,functionExec)
{
     async = false;
    if(!async) {  WaitBmp(1); }
    
    dataJson = JSON.stringify(xdata)
    console.log( "execAjax : " + url + " "+ dataJson  );
    len = dataJson.length/(1024);
    console.log( "    JSON size : "+len+" Kbytes"  );
    $.ajax
    ({
        url: url,
       data: xdata,
       type: "POST",
       async: async,
       dataType : "json",
        success: function( json ) {
            console.log( "Data :" + JSON.stringify(json)  );
            if(functionExec!==null)
               functionExec(json);
        },
        error: function( xhr, status, errorThrown ) {
             console.log( "Ajax error :" + errorThrown );
        },
        complete: function( xhr, status ) 
        { 
             console.log( "Ajax ok");
        }
    });
    if(!async) WaitBmp(0);
}
////////////////////////////////////////////////////////////////////////////////

if (document.getElementById('dlgWaitTmp_999') === null) 
    document.write("<div id=\"dlgWaitTmp_999\"; width:10px; height:10px; style=\" position:absolute; z-index:999; \" > <img src=\"Img/load.gif\"  height=\"50\" width=\"50\" alt=\"Wait\"> </div>" );

$("#dlgWaitTmp_999").hide();


function WaitBmp(state)
{
   // $( ).dialog();
   // if(_WaitBmpFirstTime_==0)
   //   document.write("<div id=\"dlgWaitTmp_999\"; width:10px; height:10px; style=\" position:absolute; z-index:999;\" > <img src=\"Img/load.gif\"  height=\"50\" width=\"50\" alt=\"Wait\"> </div>" );
   
  
   var altura_tela = $(window).height(); /*cria variável com valor do altura da janela*/
   var largura_tela = $(window).width(); 
   
   $("#dlgWaitTmp_999").css('top',(altura_tela/2) - 25);
   $("#dlgWaitTmp_999").css('left', (largura_tela/2) - 25 );
   if(state==1)
   {    
       // console.log( "Show wait" );
       $("#dlgWaitTmp_999").show();
   }
   else
   {
       // console.log( "Hide wait" );
       $("#dlgWaitTmp_999").hide(); 
   }
   
}
///////////////////////////////////////////////////////////////////////////
var glbDeviceId=null;
function GetDeviceId() 
{
    // <script src="/ChildMonitor/Util/fingerprint2.min.js"></script>
    var fp2 = new Fingerprint2();
    fp2.get(function(result) {
        glbDeviceId=result;
        // alert(result);
        // $("#fp2").text(result);
    });
} 
////////////////////////////////////////////////////////////////////////////////
var datUser = { };
datUser.glbLat = 0.0;  
datUser.glbLng = 0.0; 
datUser.glbHeading= 0.0;
datUser.glbAccuracy = 0.0;
datUser.glbSpeed= 0.0;
datUser.glbCity = ""; 
datUser.glbCountry = "";     
////////////////////////////////////////////////////////////////////////////////   
function CollectUserData()
{
    datUser.glbLat = glbLat;  
    datUser.glbLng = glbLng; 
    datUser.glbHeading = glbHeading;
    datUser.glbAccuracy = glbAccuracy;
    datUser.glbSpeed = glbSpeed;
    datUser.menuState= $('#LabelFromHere').html(); 
    datUser.glbLatNow   = glbLatNow;    
    datUser.glbLngNow   = glbLngNow; 
    datUser.idOrilatGlb = idOrilatGlb;    
    datUser.idOrilonGlb = idOrilonGlb;   
    datUser.idDstlatGlb = idDstlatGlb;    
    datUser.idDstlonGlb = idDstlonGlb;      
    return(datUser);
}
////////////////////////////////////////////////////////////////////////////////   
function SaveUserLastStatus($data)
{
    //    datUser.texto = "maskasd asdfkasdf asdfksdf asdfksdf asdfk";        
    //    json_str = JSON.stringify(datUser);
    //    alert(JSON.stringify(datUser));
    //    bufDat = JSON.parse(json_str);
    //    alert(bufDat.texto);   
    // This post is asynchronous   
    
    // TODO: Decidir se vale a pena existir esta função
    return;
    
    
    json_str = JSON.stringify($data);
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    
    $.post(url, { type:"SaveLastUserStatus", data:json_str }, 
             function(data) {
                 // alert(data.response);
                WaitBmp(0);
            }, 'json');
 }
////////////////////////////////////////////////////////////////////////////////   
function PostGeoRequestService(UserId,Lat,Lon,LatDest,LonDest,addressOri,addressDest,Service)
{
    // This post is asynchronous  
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    
    $.post(url, { type:"RequestCar",userid:UserId,lat:Lat,lon:Lon,latdest:LatDest,londest:LonDest,addressOri:addressOri,addressDest:addressDest,service: Service }, 
             function(data) {
                 // alert(data.response);
                WaitBmp(0);
            }, 'json');
 }
////////////////////////////////////////////////////////////////////////////////   
function PostGeoServiceLastPos(UserId,idDevice,Lat,Lon,Heading,Service)
{
    // This post is asynchronous 
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    
    if(isNaN(Heading))
       Heading="Null";
    if(Heading=="")
       Heading="Null"; 
    if(Heading===null) // chrome
       Heading="Null";

    $.post(url, { type:"SendLastPos",userid:UserId,iddevice:idDevice, lat:Lat,lon:Lon,heading:Heading,service: Service }, 
            function(data) {
                 // alert(data.response);
                // WaitBmp(0); 
            }, 'json');     
}
////////////////////////////////////////////////////////////////////////////////   
function PostGetProvidersInRegion(UserId,Lat,Lon,Service,funcData)
{
    // This post is asynchronous 
    //    class ItemProvider {
    //    public $aLat = 0.0;
    //    public $aLong = 0.0;
    //    public $Heading = 0.0; }
    
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    $.post(url, { type:"GetProviders",userid:UserId, lat:Lat,lon:Lon,service: Service }, function(data) {
                 // alert(data.vet[0].aLat);
                 // alert(data.numItens);
                 // WaitBmp(0);
                 funcData(data);
            }, 'json');    
}
////////////////////////////////////////////////////////////////////////////////   
function PostGetProviderStatus(UserId,Lat,Lon,Service,funcData)
{
    // This post is asynchronous 
    //    class ItemProvider {
    //    public $aLat = 0.0;
    //    public $aLong = 0.0;
    //    public $Heading = 0.0;
    //}
    
    url = window.location.protocol+'//'+location.host+'/ChildMonitor/Util/PostGeoRequestService.php';
    $.post(url, { type:"GetProviderStatus",userid:UserId, lat:Lat,lon:Lon,service: Service }, function(data) {
                 // alert(data.vet[0].aLat);
                 // alert(data.numItens);
                 // WaitBmp(0);
                 funcData(data);
            }, 'json');    
}

////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////

function RotateImage(contents,degrees,scale)
{
    // Create an image
    var img = document.createElement("img");
    img.src = contents;

    var canvas = document.createElement("canvas");
    //var canvas = $("<canvas>", {"id":"testing"})[0];
    var ctx = canvas.getContext("2d");
    
    width = img.width * scale;
    height = img.height * scale;
    
    ctx.width = width;
    ctx.height = height;
    
    
    // ctx.rotate((Math.PI/180)*25);
    ctx.translate(width / 2, height / 2);
    ctx.rotate((Math.PI/180)*degrees);
    ctx.translate(-width / 2, -height / 2);
    ctx.drawImage(img, 0, 0, width, height);
    
    var dataurl = canvas.toDataURL("image/png");
    return dataurl;
}
//////////////////////////////////////////////////////////////////////// 
function RemoveMarker(markerMe) 
{
    if(markerMe!=null)  
       map.removeLayer(markerMe)   
    return null;
}          
////////////////////////////////////////////////////////////////////////
function PutCarMarker(markerGlb, map,Lat,Lon,angle) 
{
    // angle = 360 * Math.random();
    if(isNaN(angle)) 
    {
       return (PutMarker(markerGlb, map,"Img/MapMarker_Flag4_Right_Black.png",20,20,Lat,Lon));
    }     
    if(markerGlb!=null)    
       map.removeLayer(markerGlb)    
    markerGlb = L.marker([Lat, Lon]);

    angle=angle-90.0;
    // alert(angle);  
    var bufimg = RotateImage('Img/iconcar0.png',angle,0.27);
    var carIcon = L.icon({
        iconUrl: bufimg
    });

    /*
    var carIcon = L.icon({
        iconUrl: '$img',
        iconSize:     [25, 25] // size of the icon
    });
    */

    markerGlb.setIcon(carIcon);    
    markerGlb.addTo(map);  
    map.addLayer(markerGlb);   
    return markerGlb;
}
////////////////////////////////////////////////////////////////////////
function PutMarkerLocalization(markerMe, map,glbLatNow,glbLngNow)
{
    return PutMarker(markerMe, map,"Img/Black_Marker.png",20,66,glbLatNow,glbLngNow);
}
////////////////////////////////////////////////////////////////////////
function PutMarker(markerTst, map,imgurl,szx,szy,Lat,Lon) {

    // bufnada = "Rota = ("+idOrilatGlb+","+idOrilonGlb+"),("+idDstlatGlb+","+idDstlonGlb+")";
    // console.log(bufnada);            
 
    if(markerTst!=null)
    {
        map.removeLayer(markerTst)
    }    
    markerTst = L.marker([Lat, Lon]);
    
    var imgIcon = L.icon({
      iconUrl: imgurl,
      iconSize: [szx, szy]
    });    
    
//    var imgIcon = L.icon({
//      iconUrl: imgurl,
//      iconSize: [szx, szy],
//      shadowUrl: '../Img/marker-shadow.png',
//      shadowSize:   [szx, szy],
//      shadowAnchor: [szx/2, szy/2]
//    });
    
    /*
     var carIcon = L.icon({
        iconUrl: '$img',
        iconSize:     [25, 25] // size of the icon
    }); 
    */
    markerTst.setIcon(imgIcon);    
    markerTst.addTo(map);  
    map.addLayer(markerTst);    
    return markerTst;
}
//////////////////////////////////////////////////////////////////////// 
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
//////////////////////////////////////////////////////////////////////// 
String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
//////////////////////////////////////////////////////////////////////// 
var map = null;    
function ShowRouteOnMap(mapid,lat1,lon1,lat2,lon2) 
{
    map = L.map(mapid);

    southWest = L.latLng(lat1, lon1);
    northEast = L.latLng(lat2, lon2);
    bounds = L.latLngBounds(southWest, northEast);

    // L.marker([latBrowser, lngBrowser]).addTo(map);
    L.tileLayer( 'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://osm.org/copyright" title="OpenStreetMap" target="_blank">OpenStreetMap</a> contributors | Tiles Courtesy of <a href="https://www.mapquest.com/" title="MapQuest" target="_blank">MapQuest</a> <img src="https://developer.mapquest.com/content/osm/mq_logo.png" width="16" height="16">',
        subdomains: ['otile1','otile2','otile3','otile4']
    }).addTo( map );  

    map.setView(new L.LatLng(lat1, lon1), 8);

    map.panInsideBounds(bounds);

//    var routing = new L.Routing();
//    routing.control({
//        waypoints: [
//            L.latLng(lat1, lon1),
//            L.latLng(lat2, lon2)
//        ]
//    }).addTo(map);  
    
    L.Routing.control({
        waypoints: [
            L.latLng(lat1, lon1),
            L.latLng(lat2, lon2)
        ]
    }).addTo(map);  
}
///////////////////////////////////////////////////////////////////////////////
function AjaxLoadStyle(urlfile)
{
    $.ajax({
            url:urlfile,
            dataType:"script",
            success:function(data){
                    $("head").append("<style>" + data + "</style>");
                    //loading complete code here
            }
    });
}
///////////////////////////////////////////////////////////////////////////////
function AjaxLoadScript(urlfile)
{
    $.ajax({
    async: false,
    url: urlfile,
    dataType: "script"
    });
}
///////////////////////////////////////////////////////////////////////////////
function HeadersMap()
{        
    AjaxLoadStyle("/ChildMonitor/Util/leaflet.css");
    AjaxLoadStyle("/ChildMonitor/Util/leaflet-routing-machine.css");
    
    AjaxLoadScript("/ChildMonitor/Util/leaflet.js")
    AjaxLoadScript("/ChildMonitor/Util/Control.Geocoder.js")
    AjaxLoadScript("/ChildMonitor/Util/leaflet-routing-machine.min.js")

}
///////////////////////////////////////////////////////////////////////////////
function ShowOpenMap(mapid,lat1,lon1) 
{
    map = L.map(mapid, { zoomControl:false, minZoom:-9 });
    
    L.tileLayer( 'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://osm.org/copyright" title="OpenStreetMap" target="_blank">OpenStreetMap</a> contributors | Tiles Courtesy of <a href="https://www.mapquest.com/" title="MapQuest" target="_blank">MapQuest</a> <img src="https://developer.mapquest.com/content/osm/mq_logo.png" width="16" height="16">',
        subdomains: ['otile1','otile2','otile3','otile4']
    }).addTo( map );  
    
    
    // Layers examples
    // http://leaflet-extras.github.io/leaflet-providers/preview/
    
    /*
       var Esri_WorldStreetMap = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
        });
    
         var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
	maxZoom: 18,
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    
      var Esri_WorldStreetMap = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
    });
    
    L.tileLayer( 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright" title="OpenStreetMap" target="_blank">OpenStreetMap</a> contributors | Tiles Courtesy of <a href="http://www.mapquest.com/" title="MapQuest" target="_blank">MapQuest</a> <img src="http://developer.mapquest.com/content/osm/mq_logo.png" width="16" height="16">',
        subdomains: ['otile1','otile2','otile3','otile4']
    }).addTo( map );  
    
    var OpenTopoMap = L.tileLayer('http://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
	maxZoom: 17,
	attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
    });
    
    var CartoDB_Positron = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
	subdomains: 'abcd',
	maxZoom: 19
    });
    
    var HERE_hybridDay = L.tileLayer('http://{s}.{base}.maps.cit.api.here.com/maptile/2.1/{type}/{mapID}/hybrid.day/{z}/{x}/{y}/{size}/{format}?app_id={app_id}&app_code={app_code}&lg={language}', {
	attribution: 'Map &copy; 1987-2014 <a href="http://developer.here.com">HERE</a>',
	subdomains: '1234',
	mapID: 'newest',
	app_id: '<your app_id>',
	app_code: '<your app_code>',
	base: 'aerial',
	maxZoom: 20,
	type: 'maptile',
	language: 'eng',
	format: 'png8',
	size: '256'
    });
    
    var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
    });
    
    var Esri_WorldStreetMap = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
    });
    */
    
    map.setView(new L.LatLng(lat1, lon1), 16);

   // map.panInsideBounds(bounds);

    return (map);
    


}
////////////////////////////////////////////////////////////////////////
function xinspect(o,i){
    // alert(xinspect(document));
    if(typeof i=='undefined')i='';
    if(i.length>50)return '[MAX ITERATIONS]';
    var r=[];
    for(var p in o){
        var t=typeof o[p];
        r.push(i+'"'+p+'" ('+t+') => '+(t=='object' ? 'object:'+xinspect(o[p],i+'  ') : o[p]+''));
    }
    return r.join(i+'\n');
}
////////////////////////////////////////////////////////////////////////////////
//coords.latitude	The latitude as a decimal number (always returned)
//coords.longitude	The longitude as a decimal number (always returned)
//coords.accuracy	The accuracy of position (always returned)
//coords.altitude	The altitude in meters above the mean sea level (returned if available)
//coords.altitudeAccuracy	The altitude accuracy of position (returned if available)
//coords.heading	The heading as degrees clockwise from North (returned if available)
//coords.speed	The speed in meters per second (returned if available)
//timestamp	The date/time of the response (returned if available)
////////////////////////////////////////////////////////////////////////////////
var getLocationTimeOut=0;
function getLocation() 
{
    /*
    var options = { enableHighAccuracy: true, maximumAge: 100, timeout: 60000 };
    if( navigator.geolocation) {
       var watchID = navigator.geolocation.watchPosition( gotPos, gotErr, options );
       var timeout = setTimeout( function() { navigator.geolocation.clearWatch( watchID ); }, 5000 );
    } else {
       gotErr();
    }
    */

    if (navigator.geolocation) 
    {
        navigator.geolocation.getCurrentPosition(getLatLon);
    } else 
    {
        ShowErrorToast("Fail on geolocation");        
    }
    if(glbLat===0.0 && glbLng===0.0)
    {
        getLocationTimeOut++;
        if(getLocationTimeOut>10)
           ShowErrorToast("Ativar Geolocalização no Dispositivo");
    }   
    else
    {
        getLocationTimeOut=0;
    }    
}
////////////////////////////////////////////////////////////////////////
var glbLat = 0.0;  
var glbLng = 0.0; 
var glbHeading= 0.0;
var glbAccuracy = 0.0;
var glbSpeed= 0.0;
var glbCity = ""; 
var glbEstado = ""; 
var glbCountry = ""; 
var glbCountryLong = ""; 
var glbStreet = ""; 
var glbAddress = "";
function getLatLon(position) 
{
    // alert("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);
    glbLat = position.coords.latitude;    
    glbLng = position.coords.longitude; 
    glbHeading = position.coords.heading;
    glbSpeed = position.coords.speed;
    glbAccuracy = position.coords.accuracy;

    if(glbLat===0.0 && glbLng===0.0)
    {
        ShowErrorToast("Ativar Geolocalização no Dispositivo");
    }    
    // ucodeAddressLatLng(latBrowser,lngBrowser); 
    // GetAjaxAddress(window.glbLatBrowser,window.glbLngBrowser);
}
////////////////////////////////////////////////////////////////////////////////
function GetAllLocationData(func_update)
{
    var updateAddressGoogle = function (strbuf, country, estado, city, street,countryLong)
    {
        if (strbuf === undefined || strbuf == "")
        {
            // $('#divAddress').slideUp();  
            // $('#idAddress').text("");
        }
        else
        {
            // $('#divAddress').show();  
            // $('#divAddress').slideUp(); 
            // $('#idAddress').text(strbuf.substring(0, 400));  

            glbEstado = retirarAcento(estado);
            glbCity = retirarAcento(city);
            glbCountry = retirarAcento(country);
            glbCountryLong = retirarAcento(countryLong);
            glbStreet = retirarAcento(street);
            glbAddress = strbuf.substring(0, 400);
            func_update();
        }
    };
    
    function getLatLonCmp(position) 
    {
        // alert("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);
        glbLat = position.coords.latitude;    
        glbLng = position.coords.longitude; 
        glbHeading = position.coords.heading;
        glbSpeed = position.coords.speed;
        glbAccuracy = position.coords.accuracy;

        GetGoogleAddress(glbLat, glbLng, updateAddressGoogle);
    }
    
    if (navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(getLatLonCmp);
    } 
    else
    {
        alert("Fail on geolocation");
    }



    // strbuf = GetOpenMapAddress(glbLatNow,glbLngNow,updateAddress);   

    
}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
function ugetLocation() 
{
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(ushowPosition);
    } else {
        // x.innerHTML = "Geolocation is not supported by this browser.";
    }
}
////////////////////////////////////////////////////////////////////////
function ushowPosition(position) 
{
    // alert("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);
    window.glbLatBrowser=position.coords.latitude;    
    window.glbLngBrowser=position.coords.longitude;  
    // ucodeAddressLatLng(latBrowser,lngBrowser); 
    GetAjaxAddress(window.glbLatBrowser,window.glbLngBrowser);
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
function ucodeAddressLatLng(lat,lng) 
{
    var geocoder= new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[1]) 
      {
         // $('#Address').val (results[1].formatted_address)
         // results[].types
         // results[0].formatted_address: "275-291 Bedford Ave, Brooklyn, NY 11211, USA",
         // results[1].formatted_address: "Williamsburg, NY, USA",
         // results[2].formatted_address: "New York 11211, USA",
         // results[3].formatted_address: "Kings, New York, USA",
         // results[4].formatted_address: "Brooklyn, New York, USA",
         // results[5].formatted_address: "New York, New York, USA",
         // results[6].formatted_address: "New York, USA",
         // results[7].formatted_address: "United States"
         AddressData = results[1];
         $('#idCountry').val (results[7].formatted_address);
         
         // alert(results[6].formatted_address);
         
         // getCurrentUserInfoFaceBook();
      } 
      else 
      {
         alert('No address results found');
         // return null;
      }
    } else 
    {
        alert('Geocoder failed due to: ' + status);
         // return null;
    }
  });
}
/////////////////////////////////////////////////////////////////////////
function GetAjaxAddress(latBrowser,lngBrowser)
{
    console.log("http://nominatim.openstreetmap.org/reverse?format=json&accept-language=en&lat="+latBrowser+"&lon="+lngBrowser+"&zoom=18&addressdetails=1");
    var sjson="";
    $.ajax
    ({
        url: "http://nominatim.openstreetmap.org/reverse?format=json&accept-language=en&lat="+latBrowser+"&lon="+lngBrowser+"&zoom=18&addressdetails=1&accept-language=en",
       data: {   },
       type: "GET",
       async: false,
       dataType : "json",
        success: function( json ) {
            sjson = json;
            console.log("Depurando JSON ["+json.address.country+"]");
            $('#idCountry').val(json.address.country);
            $('#idCountryDest').val(json.address.country);
            // $('#idNumber').val (json.address.house_number);
            // $('#idStreet').val (json.address.road);
            // $('#idDistrict').val (json.address.suburb);
            // $('#idPostCode').val (json.address.postcode);
            
            // lLat = json.vet[0].Lat;
            // lLong = json.vet[0].Long;
            // idDevice = json.vet[0].idDevice;
            // dTimeStamp = json.vet[0].TimeStamp;


                      
     
            // console.log(lLat);
            // console.log(lLong);
            // console.log(dTimeStamp);                       
                },
        error: function( xhr, status, errorThrown ) {

            // console.log( "Erro json :" + errorThrown );
            // console.dir( xhr );
            // alert("Erro ajax");
        },
        complete: function( xhr, status ) 
        { 

             // alert("Ajax OK");
        }
    }); 
    return sjson;
}
/////////////////////////////////////////////////////////////////////////
function GetOpenMapGeoComplete(sName,latBrowser,lngBrowser)
{
   // http://photon.komoot.de/  autocompletamento de endereço 
   /*
   http://photon.komoot.de:2322/api?q=gay&lat=-22.802671&lon=-43.0722389
    
   http://photon.komoot.de:2322/api?q=moreira%20cesar&lat=-22.802671&lon=-43.0722389
    
    -22.802671,-43.0722389 
    
   Exemplo json
   {"features":[{"geometry":{"coordinates":[-7.778894167015766,54.48179625],"type":"Point"},"type":"Feature","properties":{"osm_id":3976704,"osm_type":"R","extent":[-7.7812793,54.4827301,-7.7754431,54.4809188],"country":"United Kingdom","osm_key":"place","osm_value":"island","name":"Gay Island","state":"Northern Ireland"}},{"geometry":{"coordinates":[58.4405186,51.4714422],"type":"Point"},"type":"Feature","properties":{"osm_id":148471713,"osm_type":"W","extent":[58.3997374,51.4941014,58.4766417,51.4444596],"country":"Russia","osm_key":"place","osm_value":"town","name":"Gay","state":"Orenburg Oblast"}},{"geometry":{"coordinates":[3.1608333,49.6605556],"type":"Point"},"type":"Feature","properties":{"osm_id":153697,"osm_type":"R","extent":[3.1342899,49.6751648,3.1794048,49.6494893],"country":"France","osm_key":"place","osm_value":"village","postcode":"02300","name":"Ugny-le-Gay","state":"Picardy"}},{"geometry":{"coordinates":[-7.778894167015766,54.48179625],"type":"Point"},"type":"Feature","properties":{"osm_id":3976704,"osm_type":"R","extent":[-7.7812793,54.4827301,-7.7754431,54.4809188],"country":"United Kingdom","osm_key":"boundary","osm_value":"administrative","name":"Gay Island","state":"Northern Ireland"}},{"geometry":{"coordinates":[7.2791233,44.8816607],"type":"Point"},"type":"Feature","properties":{"osm_id":2027744859,"osm_type":"N","country":"Italy","osm_key":"place","osm_value":"village","name":"Gay","state":"Piemont"}},{"geometry":{"coordinates":[-73.555055,45.5194111],"type":"Point"},"type":"Feature","properties":{"osm_id":1069247679,"osm_type":"N","country":"Canada","osm_key":"place","city":"Montreal city","osm_value":"neighbourhood","postcode":"H2L 3B5 ","name":"Gay Village","state":"Quebec"}},{"geometry":{"coordinates":[0.450452,44.0786159],"type":"Point"},"type":"Feature","properties":{"osm_id":3254492583,"osm_type":"N","country":"France","osm_key":"place","osm_value":"hamlet","name":"Gay","state":"Aquitaine"}},{"geometry":{"coordinates":[9.358093,13.479146],"type":"Point"},"type":"Feature","properties":{"osm_id":473436383,"osm_type":"N","country":"Niger","osm_key":"place","osm_value":"village","name":"Gay","state":"Zinder"}},{"geometry":{"coordinates":[14.55709,10.48322],"type":"Point"},"type":"Feature","properties":{"osm_id":2933225782,"osm_type":"N","country":"Cameroon","osm_key":"place","osm_value":"village","name":"Gay Gay","state":"Far-North"}},{"geometry":{"coordinates":[3.8955825,45.513566],"type":"Point"},"type":"Feature","properties":{"osm_id":3259564375,"osm_type":"N","country":"France","osm_key":"place","city":"Saint-Anthème","osm_value":"isolated_dwelling","postcode":"63660","name":"Gay","state":"Auvergne"}},{"geometry":{"coordinates":[-112.1183018,43.0482499],"type":"Point"},"type":"Feature","properties":{"osm_id":150952778,"osm_type":"N","country":"United States of America","osm_key":"place","osm_value":"hamlet","name":"Gay","state":"Idaho"}},{"geometry":{"coordinates":[-83.2732046,35.2914879],"type":"Point"},"type":"Feature","properties":{"osm_id":154007755,"osm_type":"N","country":"United States of America","osm_key":"place","osm_value":"hamlet","name":"Gay","state":"North Carolina"}},{"geometry":{"coordinates":[-95.6213524,33.9534361],"type":"Point"},"type":"Feature","properties":{"osm_id":151846670,"osm_type":"N","country":"United States of America","osm_key":"place","osm_value":"hamlet","name":"Gay","state":"Oklahoma"}},{"geometry":{"coordinates":[47.543611,34.386389],"type":"Point"},"type":"Feature","properties":{"osm_id":3525486068,"osm_type":"N","country":"Iran","osm_key":"place","osm_value":"village","name":"Gay Kol","state":"Kermanshah"}},{"geometry":{"coordinates":[2.2511161,43.714047],"type":"Point"},"type":"Feature","properties":{"osm_id":1907539619,"osm_type":"N","country":"France","osm_key":"place","city":"Montredon-Labessonnié","osm_value":"isolated_dwelling","postcode":"81360","name":"Gay","state":"Languedoc-Roussillon-Midi-Pyrénées"}}],"type":"FeatureCollection"}
   */
   /* Exemplo do Photon
        <!doctype html>
        <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>Photon, search-as-you-type with OpenStreetMap</title>
            <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.css"/>
            <link rel="stylesheet" href="http://photon.komoot.de/static/leaflet.photon/leaflet.photon.css">
            <link rel="stylesheet" href="http://photon.komoot.de/static/css/app.css">
        </head>
        <body>
        <div class="map" id="map"></div>
        <script src="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet-src.js"></script>
        <script src="http://photon.komoot.de/static/leaflet.photon/leaflet.photon.js"></script>
        <script src="http://photon.komoot.de/static/js/app.js"></script>
        </body>
        </html>
    */
   
}
/////////////////////////////////////////////////////////////////////////
function GetOpenMapAddress(latBrowser,lngBrowser,updateFunction)
{
    console.log("https://nominatim.openstreetmap.org/reverse?format=json&accept-language=en&lat="+latBrowser+"&lon="+lngBrowser+"&zoom=18&addressdetails=1");
    var sjson="";
    $.ajax
    ({
        url: "https://nominatim.openstreetmap.org/reverse?format=json&accept-language=en&lat="+latBrowser+"&lon="+lngBrowser+"&zoom=18&addressdetails=1&accept-language=en",
       data: {   },
       type: "GET",
       async: true,
       dataType : "json",
        success: function( json ) {
            sjson = json;
            // console.log("Depurando JSON ["+json.display_name+"]");
            updateFunction(json.display_name,json.address.country,json.address.city);
            // $('#idCountry').val(json.address.country);
            // $('#idCountryDest').val(json.address.country);
            // $('#idNumber').val (json.address.house_number);
            // $('#idStreet').val (json.address.road);
            // $('#idDistrict').val (json.address.suburb);
            // $('#idPostCode').val (json.address.postcode);
            
            // lLat = json.vet[0].Lat;
            // lLong = json.vet[0].Long;
            // idDevice = json.vet[0].idDevice;
            // dTimeStamp = json.vet[0].TimeStamp;


                      
     
            // console.log(lLat);
            // console.log(lLong);
            // console.log(dTimeStamp);                       
                },
        error: function( xhr, status, errorThrown ) {

            // console.log( "Erro json :" + errorThrown );
            // console.dir( xhr );
            // alert("Erro ajax");
        },
        complete: function( xhr, status ) 
        { 

             // alert("Ajax OK");
        }
    }); 
    return sjson.display_name;
}
/////////////////////////////////////////////////////////////////////////
function GetShortNameFromGoogleResults(json,info,indresults)
{
    for (i=0;i<json.results[indresults].address_components.length;i++){
        for (j=0;j<json.results[indresults].address_components[i].types.length;j++){
           if(json.results[indresults].address_components[i].types[j]==info)
               return(json.results[indresults].address_components[i].short_name);
              // return(json.results[indresults].address_components[i].long_name);
        }
    }
}
/////////////////////////////////////////////////////////////////////////
function GetLongNameFromGoogleResults(json,info,indresults)
{
    for (i=0;i<json.results[indresults].address_components.length;i++){
        for (j=0;j<json.results[indresults].address_components[i].types.length;j++){
           if(json.results[indresults].address_components[i].types[j]==info)
               return(json.results[indresults].address_components[i].long_name);
        }
    }
}
///////////////////////////////////////////////////////////////////////// 
function GetGoogleAddress(latBrowser,lngBrowser,updateFunction)
{
    // https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=AIzaSyBS6rSkiRWc6hoSB5E3PJZk9rms4-86ayM
    strurl = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latBrowser+","+lngBrowser+"&key=AIzaSyBS6rSkiRWc6hoSB5E3PJZk9rms4-86ayM";
    console.log(strurl);
    var sjson="";
    $.ajax
    ({
       url: strurl,
       data: {   },
       type: "GET",
       async: true,
       dataType : "json",
        success: function( json ) {
            sjson = json;
            // console.log("Depurando JSON ["+json.display_name+"]");
            // data.results[ind].formatted_address
            updateFunction(json.results[0].formatted_address,
                           GetShortNameFromGoogleResults(json,"country",0),
                           GetShortNameFromGoogleResults(json,"administrative_area_level_1",0),
                           GetShortNameFromGoogleResults(json,"administrative_area_level_2",0),
                           GetShortNameFromGoogleResults(json,"route",0),
                           GetLongNameFromGoogleResults(json,"country",0)
                           );
            // alert(strurl); 
            // alert(json.results[0].formatted_address);
            // alert(json.results[0].formatted_address);
            
            // $('#idCountry').val(json.address.country);
            // $('#idCountryDest').val(json.address.country);
            // $('#idNumber').val (json.address.house_number);
            // $('#idStreet').val (json.address.road);
            // $('#idDistrict').val (json.address.suburb);
            // $('#idPostCode').val (json.address.postcode);
            
            // lLat = json.vet[0].Lat;
            // lLong = json.vet[0].Long;
            // idDevice = json.vet[0].idDevice;
            // dTimeStamp = json.vet[0].TimeStamp;


                      
     
            // console.log(lLat);
            // console.log(lLong);
            // console.log(dTimeStamp);                       
                },
        error: function( xhr, status, errorThrown ) {

            // console.log( "Erro json :" + errorThrown );
            // console.dir( xhr );
            // alert("Erro ajax");
        },
        complete: function( xhr, status ) 
        { 

             // alert("Ajax OK");
        }
    }); 
    return sjson.display_name;
}
/////////////////////////////////////////////////////////////////////////
function GetAjaxCoords(city,country)
{
    // http://nominatim.openstreetmap.org/search?q=niteroi+brazil&format=json&accept-language=en&polygon=0&addressdetails=1
    // Usar esse acima
    urli = encodeURI("https://nominatim.openstreetmap.org/search?q="+city+"+"+country+"&format=json&accept-language=en&polygon=0&addressdetails=1");
    console.log(urli);
    var buffer;
    $.ajax
    ({
       url: urli,
       data: {   },
       type: "GET",
       async: true,
       dataType : "json",
        success: function( json ) 
        {
            // console.log("---------------- Depurando JSON ["+json[0].lat+"]");
            buffer=json;
            // $('#idCountry').val(json.address.country);
            // $('#idCountryDest').val(json.address.country);
            // $('#idNumber').val (json.address.house_number);
            // $('#idStreet').val (json.address.road);
            // $('#idDistrict').val (json.address.suburb);
            // $('#idPostCode').val (json.address.postcode);
            
            // lLat = json.vet[0].Lat;
            // lLong = json.vet[0].Long;
            // idDevice = json.vet[0].idDevice;
            // dTimeStamp = json.vet[0].TimeStamp;
            // console.log(lLat);
            // console.log(lLong);
            // console.log(dTimeStamp);                       
        },
        error: function( xhr, status, errorThrown ) 
        {

            // console.log( "Erro json :" + errorThrown );
            // console.dir( xhr );
            // alert("Erro ajax");
        },
        complete: function( xhr, status ) 
        { 

             // alert("Ajax OK");
        }
    }); 
    return buffer;
}
/////////////////////////////////////////////////////////////////////////
function GetLatLonFromAdressGoogle(saddress,updateFunction)
{ 

    // Geocode GoogleApi
    sSite = "https://maps.googleapis.com/maps/api/geocode/json?address="+saddress+"&&key=AIzaSyBS6rSkiRWc6hoSB5E3PJZk9rms4-86ayM";

    // urli = encodeURI("http://nominatim.openstreetmap.org/search?q="+city+"+"+country+"&format=json&accept-language=en&polygon=0&addressdetails=1");
    console.log(sSite);
    var buffer;
    $.ajax
    ({
       url: sSite,
       data: {   },
       type: "GET",
       async: false,
       dataType : "json",
        success: function( json ) {
            // console.log("---------------- Depurando JSON ["+json[0].lat+"]");
            // alert(json.results[0].geometry.location.lat);
            if(json.results[0]===undefined)
            {
               lat = 0.0;
               lon = 0.0;                
            }
            else
            {
               lat = json.results[0].geometry.location.lat;
               lon = json.results[0].geometry.location.lng;
            }   
            updateFunction(lat,lon);
            // $('#idCountry').val(json.address.country);
            // $('#idCountryDest').val(json.address.country);
            // $('#idNumber').val (json.address.house_number);
            // $('#idStreet').val (json.address.road);
            // $('#idDistrict').val (json.address.suburb);
            // $('#idPostCode').val (json.address.postcode);
            
            // lLat = json.vet[0].Lat;
            // lLong = json.vet[0].Long;
            // idDevice = json.vet[0].idDevice;
            // dTimeStamp = json.vet[0].TimeStamp;


                      
     
            // console.log(lLat);
            // console.log(lLong);
            // console.log(dTimeStamp);                       
                },
        error: function( xhr, status, errorThrown ) {

            // console.log( "Erro json :" + errorThrown );
            // console.dir( xhr );
            // alert("Erro ajax");
        },
        complete: function( xhr, status ) 
        { 

             // alert("Ajax OK");
        }
    }); 
    return buffer;
}

/////////////////////////////////////////////////////////////////////////
$.ajax({
    async: false,
    url: "/ChildMonitor/Util/UserInterfaceControls.min.js",
    dataType: "script"
});
/////////////////////////////////////////////////////////////////////////
$.ajax({
    async: false,
    url: "/ChildMonitor/Util/FormsProvider.min.js",
    dataType: "script"
});
/////////////////////////////////////////////////////////////////////////
$.ajax({
    async: false,
    url: "/ChildMonitor/Util/FormsMaps.min.js",
    dataType: "script"
});
/////////////////////////////////////////////////////////////////////////