<?php
include_once 'Util.php';
////////////////////////////////////////////////////////////////////////////////
class ColumnData
{
   public $Name=""; 
   public $Value="";
   public $Type="Char"; 
}
ini_set('post_max_size', '100M');
ini_set('upload_max_filesize', '100M');
ini_set('memory_limit', '128M');
ini_set('session.gc_maxlifetime','50800');
ini_set('max_input_time','50800');
ini_set('max_execution_time','50800');
////////////////////////////////////////////////////////////////////////////////
function DailyParkingDatabaseSanityCheck()
{
   /*
   $sql = "INSERT INTO `ParkingDataPenaltyHistory` select * from `ParkingDataPenalty` WHERE DATEDIFF(CURDATE(),DateTimePark)>4";	
   db_query($sql);
    
   $sql = "DELETE FROM `ParkingDataPenalty` WHERE DATEDIFF(CURDATE(),DateTimePark)>4;";
   db_query($sql);

   $sql = "INSERT INTO `ParkingDataHistory` select * from `ParkingData` WHERE DATEDIFF(CURDATE(),DateTimePark)>2";	
   db_query($sql);
    
   $sql = "DELETE FROM `ParkingData` WHERE DATEDIFF(CURDATE(),DateTimePark)>2;";
   db_query($sql);   
    */
}
//////////////////////////////////////////////////////////////////////////////// 
function PostGeoRequestServiceMain()
{
    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);
    
    /* Table definition
    
    //////////////////////////////////////////////////////////////////////////////
    drop table IF EXISTS `RequestedService`;
    CREATE TABLE IF NOT EXISTS `RequestedService` (
      `Id` bigint(20) NOT NULL, 
      `UserId` bigint(20) NOT NULL,
      `addressOri` varchar(255) NOT NULL,
      `addressDest` varchar(255) NOT NULL,
      `DateInclusion` datetime NOT NULL, 
      `Service` varchar(512) NOT NULL,
      `Lat` double NOT NULL,
      `Long` double NOT NULL,
      `LatDest` double NOT NULL,
      `LongDest` double NOT NULL,
      `cStatus` varchar(255) NOT NULL,
      `Active` tinyint(1) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

     INSERT INTO `RequestedService`(`Id`, `UserId`, `addressOri`, `addressDest`, `DateInclusion`, `Description`, `Lat`, `Long`, `LatDest`, `LongDest`, `Active`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9],[value-10],[value-11])

     *      */
    // error_log("Teste");
    // error_log($_POST["type"]);
    
    $Type =$_POST["type"];
    if($Type=="RequestCar")
        return PostGeoRequestService();
    if($Type=="SendLastPos")
        return PostGeoRequestSendLastPos(); 
    if($Type=="GetProviders")
        return PostGetProvidersInRegion();   
    if($Type=="GetProviderStatus")
        return PostGetProviderStatus();  
    if($Type=="SaveLastUserStatus")
        return PostSaveLastUserStatus();      
    if($Type=="ParkingPay")
        return PostParkingPay();   
    if($Type=="GetCarsInThisStreet")
        return PostGetCarsInThisStreet(); 
    if($Type=="CheckPlateParking")
        return PostCheckPlateParking(); 
    if($Type=="RegisterPlateEndTime")
        return RegisterPlateEndTime(); 
    if($Type=="GetPlateLastStatus")
        return GetPlateLastStatus(); 
    if($Type=="ParkCreateProvider")
        return ParkCreateProvider();    
    if($Type=="ParkLoadProvider")      
        return ParkLoadProvider();    
    if($Type=="CheckLogin")      
        return CheckLogin();    
    if($Type=="ExecuteSQL")      
        return PostExecuteSQL();   
    if($Type=="ResetPassword")      
        return PostResetPassword(); 
    if($Type=="SavePassword")      
        return PostSavePassword();
    if($Type=="MountDataProfitProvider")      
        return PostMountDataProfitProvider();   
    if($Type=="RegisterNewPaymentCard")      
        return PostRegisterNewPaymentCard();   
    if($Type=="PayValueWithCardToken")      
        return PostPayValueWithCardToken();   
     

    
    echo "{\"response\":\"Invalid Request Type\"}";
}
////////////////////////////////////////////////////////////////////////////////
function PostMountDataProfitProvider()
{
    $json   = $_POST["json"];
  
    // sqlDebit = "(select case when (select RawDataPayment from `ParkingFitProviderPaidFee` where `RefDate`='2017-05' and `emailProvider`='" + provider + "') IS NULL then 'Not Paid' end)";

    $datadec = json_decode($json);    
    
    $provider = $datadec->provider;     
    $year = $datadec->year;  
        
    if($datadec->provider=="")
    {    
        echo "{\"response\":\"RequestError\",\"extradata\":\"null\"}";
        return;
    }    
      // TODO show only provit of closed months, to avoid early payments -> DATE_FORMAT(DateTimePark,'%Y-%m')!=DATE_FORMAT(CURDATE(),'%Y-%m')  and 

    // TODO mostrar apenas meses passados sem incluir mes atual
    $sql = "select DATE_FORMAT(DateTimePark,'%Y-%m') as Data,count(*) as `Estacionamentos`, " .
            " sum(valuePaid) as `Recebido`,(select ParkingFitFee from ParkingProvider where email='" . $provider . "') " .
            " as `ParkingFit %` , format(sum(valuePaid)*(select ParkingFitFee " .
            "from ParkingProvider where email='" . $provider . "')*(1/100.00),2) as `ParkingFit Fee` from " .
            " ParkingData where DATE_FORMAT(DateTimePark,'%Y-%m')!=DATE_FORMAT(CURDATE(),'%Y-%m') and DATE_FORMAT(DateTimePark,'%Y')='" . $year . "' and idProvider='" . $provider . "' " .
            " group by DATE_FORMAT(DateTimePark,'%Y-%m');";
    
    // error_log($sql);
    
    $resultado = db_query($sql);
    // "Data"  "Estacionamentos"  "Recebido"  "ParkingFit %"     "ParkingFit Fee"       
    $resultado[0][5] = "Pago";
    $i=1;
    
    while ($resultado[$i][0]!==null)
    {
        $query = "(select case when (select Date from `ParkingFitProviderPaidFee` where `RefDate`='".$resultado[$i][0]."' and `emailProvider`='".$provider."') IS NULL then 'Devedor' end)";
        $temp = GetValue($query);
        $resultado[$i][5] = GetValue($query); 
        $i=$i+1;
    }
    $sqlRows = CreateJsonSqlRowsNames($resultado);
    $sqlData = CreateJsonFromSqlCursor($resultado);
    echo "{\"response\":\"DataProfitProvider\",\"sqlrows\":$sqlRows,\"sql\":$sqlData}"; 
}

////////////////////////////////////////////////////////////////////////////////
function CreateJsonSqlRowsNames($data)
{
    $ic = 0;
    $json = "[";
    while ($data[0][$ic]!="") 
    {
        if($ic>0)
           $json .= ",";
        $namerow = $data[0][$ic];

        $json .= "\"$namerow\" ";
        $ic++;
    }  
    $json .= "]";
    return $json;
}


////////////////////////////////////////////////////////////////////////////////
function PostExecuteSQL()
{
    $json   = $_POST["json"];
  
    // dataSQL.code = "DSCLAD8724FN4KJF4NKF145243524K2RFK345JK423KFN23K5J4K2352MF3K4J534RE243K5245";
    
    $datadec = json_decode($json);    
    if($datadec->code!="DSCLAD8724FN4KJF4NKF145243524K2RFK345JK423KFN23K5J4K2352MF3K4J534RE243K5245")
    {    
        echo "{\"response\":\"ErrorSQL\",\"null:\"null\"}";
        return;
    }    
    
    $sql = $datadec->sql;
    
    // echo $sql."<br>";
    
    $resultado = db_query($sql);

    $sqlRows = CreateJsonSqlRowsNames($resultado);
    $sqlData = CreateJsonFromSqlCursor($resultado);
    
    // error_log("{\"response\":\"SqlResult\",\"sqlrows\":$sqlRows,\"sql\":$sqlData}");
    echo "{\"response\":\"SqlResult\",\"sqlrows\":$sqlRows,\"sql\":$sqlData}"; 

}
////////////////////////////////////////////////////////////////////////////////
function CheckUserParkingFitAndCreate($datadec)
{
    $resultado = db_query("select count(Active) from `ParkingFitUser` WHERE email='$datadec->email'");
    if ($resultado[1][0] == 0) 
    {
        $data[] = new ColumnData();  
      
        $data[0 ]->Name="email";                            $data[0 ]->Value=$datadec->email;   
        $data[1 ]->Name="password";                         $data[1 ]->Value=$datadec->passwd;        
    
   
        $resp = SqlInsert ("ParkingFitUser",$data);
        $resultado = db_query($sql);

       ActivationEmailSimple($datadec->email, hash('sha256', $datadec->passwd . "ParkingFit"),"ParkingFitUser"); 
       echo "{\"response\":\"UserCreated\",\"sql\":\"\"}"; 
       return "GoOut";
    }    
    $resultado = db_query("select Active from `ParkingFitUser` WHERE email='$datadec->email'");
    if ($resultado[1][0] == 0) 
    {
       echo "{\"response\":\"User Inactive\",\"sql\":\"\"}"; 
       return "GoOut";
    }        
}
////////////////////////////////////////////////////////////////////////////////
function CheckLogin()
{
    $json   = $_POST["json"];
  
    $datadec = json_decode($json); 
        
    if($datadec->context=="")
    {
        $resultado = db_query("select count(Active) from `ParkingProvider` WHERE email='$datadec->email'");
        if ($resultado[1][0] == 0) 
        {
           echo "{\"response\":\"UserCreated\",\"sql\":\"$sql\"}"; 
           return;
        }
        $resultado = db_query("select Active from `ParkingProvider` WHERE email='$datadec->email'");
        if ($resultado[1][0] == 0) 
        {
           echo "{\"response\":\"User Inactive\",\"sql\":\"$sql\"}"; 
           return;
        }        
        $sql = "select count(email) from `ParkingProvider` WHERE email='$datadec->email' and password='$datadec->passwd' ";
    }
  
    if($datadec->context=="ParkingFitStaff")
    {
       $sql = "select count(email) from `ParkingProviderStaff` WHERE email='$datadec->email' and password='$datadec->passwd' ";
    }   

    if($datadec->context=="ParkingFitUser")
    {
        if(CheckUserParkingFitAndCreate($datadec)=="GoOut")
        {
           return;
        }
        $sql = "select count(email) from `ParkingFitUser` WHERE email='$datadec->email' and password='$datadec->passwd' ";
    } 
    // echo $sql."<br>";
    $resultado = db_query($sql);
    if ($resultado[1][0] == 0) 
    {
       echo "{\"response\":\"InvalidLogin\",\"sql\":\"$sql\"}"; 
       return;
    }

 
    echo "{\"response\":\"LoginOk\",\"email\":\"$datadec->email\"}";  
    
}
////////////////////////////////////////////////////////////////////////////////
function CreateJsonFromSqlResult($data)
{
    //  {"col1":"val1", "col12":"val2" } 
    $il = 0; $ic = 0;
    $json = "{";
    while ($data[$il][$ic]!="") 
    {
        if($ic>0)
           $json .= ",";
        $namerow = $data[$il][$ic];
        $datrow = $data[$il+1][$ic];
        $json .= "\"$namerow\":\"$datrow\"";
        $ic++;
    }  
    $json .= "}";
    return $json;
}
////////////////////////////////////////////////////////////////////////////////
function CreateJsonFromSqlCursor($data)
{
    //  {"col1":"val1", "col12":"val2" } 
    $il = 0; $ic = 0;
    while ($data[0][$ic]!="") 
        $ic++;
    
    $size_ic = $ic;
    $il = 1; $ic = 0;
    
    $json = "[";
    while ($data[$il][0]!="") 
    {
        if($ic==0)
           $json .= "{"; 
        if($ic>0)
           $json .= ",";
        $namerow = $data[0][$ic];
        $datrow = $data[$il][$ic];
        $json .= "\"$namerow\":\"$datrow\"";
        $ic++;
        if($ic==$size_ic)
        {
            $ic=0;
            $il++;
            if($data[$il][0]=="")
            {
                $json .= "}";
                break;
            }
            $json .= "},";
        }
    }  
    $json .= "]";
    return $json;
}

////////////////////////////////////////////////////////////////////////////////
function ParkLoadProvider()
{
    $email    = $_POST["email"];

     
    // $datadec = json_decode(unescapeJsonString($dataJson)); 
    // echo $datadec->dataFile."<br>";
    
    // $email="tes@sdhsd.sdds";
        
    $sql = "select count(email) from `ParkingProvider` WHERE email='$email' ";
    
    // echo $sql."<br>";
    $resultado = db_query($sql);
    if ($resultado[1][0] == 0) 
    {
       echo "{\"response\":\"ProviderDontExists\",\"email\":\"$email\"}"; 
       return;
    }
    $sql = "select * from `ParkingProvider` WHERE email='$email' ";
    
    $resultado = db_query($sql);
 
    //echo "<br> Antes<br>".$resultado[1][10]."<br>";

    $resultado[1][10] = escapeJsonString($resultado[1][10]);
    // echo "<br>Depois<br>".$resultado[1][10]."<br>";
    $json =  CreateJsonFromSqlResult($resultado);
 
    echo "{\"response\":\"ParkProviderData\",\"sql_dat\":$json}";  
    
}
////////////////////////////////////////////////////////////////////////////////
function PostSavePassword()
{
    $dataJson    = $_POST["json"];
    $datadec = json_decode($dataJson);    
 

//    datPark.email = email;
//    datPark.passwd = $('#id_passwd').val();
//    datPark.context = context;
 
    $data[] = new ColumnData();  
      
    $data[0 ]->Name="email";                            $data[0 ]->Value=$datadec->email;   
    $data[1 ]->Name="password";                         $data[1 ]->Value=$datadec->passwd; 
    
    if($datadec->context=="ParkingFitUser")
         $table = "ParkingFitUser";
   
    if($datadec->context=="ParkingFit")
         $table = "ParkingProvider";
        
   
    $resp = SqlUpdate ($table,$data," email='$datadec->email' ");
    $resultado = db_query($sql);
    echo "{\"response\":\"Password Updated\",\"email\":\"$datadec->email\"}"; 

}
////////////////////////////////////////////////////////////////////////////////
function PostResetPassword()
{
    $dataJson    = $_POST["json"];
    $datadec = json_decode($dataJson);    
 
    // datPark.email  
    // datPark.context  ParkingFitProvider ParkingFitUser 
    // error_log("PostResetPassword - datPark.context :".datPark.context);
    
    if($datadec->context==='' || $datadec->context==='ParkingFitProvider')
    {        
       $sql = "select count(email) from `ParkingProvider` WHERE email='$datadec->email' ";
    }  
    if($datadec->context==='ParkingFitUser')
    {        
       $sql = "select count(email) from `ParkingFitUser` WHERE email='$datadec->email' ";
    }   
    $resultado = db_query($sql);

    if ($resultado[1][0] > 0) 
    {
       if($datadec->context==='' || $datadec->context==='ParkingFitProvider')
       {        
          ActivationEmailSimple($datadec->email, hash('sha256', $datadec->email . "ParkingFit"),"VAGPROVRESET"); 
          echo "{\"response\":\"ResetPasswordDone\",\"email\":\"$datadec->email\"}"; 
       }   
       if($datadec->context==='ParkingFitUser')
       {        
          ActivationEmailSimple($datadec->email, hash('sha256', $datadec->email . "ParkingFit"),'ParkingFitUserReset'); 
          echo "{\"response\":\"ResetPasswordDone\",\"email\":\"$datadec->email\"}"; 
       }   
       // return;
    }
}
////////////////////////////////////////////////////////////////////////////////
function ParkCreateProvider()
{
    
//    datPark.email = $('#id_login').val();
//    datPark.passwd = $('#id_passwd').val();
//    datPark.email       = $('#id_email').val();       
//    datPark.passwd      = $('#id_passwd').val();      
//    datPark.nomeprovider = $('#id_nomeprovider').val();
//    datPark.nomeresp    = $('#id_nomeresp').val();
//    datPark.telefoneresp = $('#id_telefoneresp').val();
//    datPark.cpfresp     = $('#id_cpfresp').val();
//    datPark.address     = $('#id_address').val();
//    datPark.passwd      = $('#id_passwd').val();
//    datPark.city        = $('#id_city').val();
//    datPark.obs         = $('#id_obs').val();

//    datPark.idvalorhora = $("idvalorhora").val()
//    datPark.idpercinfraction = $("idpercinfraction").val()
//    datPark.idSelCurr = $("idSelCurr").val()
//    datPark.idSelLang = $("idSelLang").val()
    
    $dataJson    = $_POST["json"];
    // $dataJson    = "{\"email\":\"andre@teste.com\",\"passwd\":\"andre\",\"nomeprovider\":\"adfdasfds\",\"nomeresp\":\"dasfsdfd\",\"telefoneresp\":\"1321233324\",\"cpfresp\":\"132423342124\",\"address\":\"R. Goitacases, 39 - São Francisco, Niterói - RJ, 24360-350, Brazil\",\"city\":\"Niteroi\",\"obs\":\"Observações\",\"country\":\"Brazil\",\"countrycode\":\"BR\",\"idvalorhora\":\"3.50\",\"idpercinfraction\":\"5\",\"idSelCurr\":\"BRL\",\"idSelLang\":\"PT-BR\",\"dataFile\":\"[{\\\"name\\\":\\\"\\\",\\\"contents\\\":\\\"\\\"},{},{}]\"}";    
    
    $datadec = json_decode($dataJson); 

        
    // echo $datadec->dataFile."<br>";

    $sql = "select count(email) from `ParkingProvider` WHERE email='$datadec->email' ";
    
    // echo $sql."<br>";
    $update = false;
    if ($datadec->providerLogged === "") 
    {
        $resultado = db_query($sql);
        $update = false;
        if ($resultado[1][0] > 0) 
        {
            $update = true;
            echo "{\"response\":\"AlreadyExists\",\"email\":\"$datadec->email\"}";
            return;
        }
    }
    else 
    {
       $update = true; 
    }
    $data[] = new ColumnData();  
      
    $data[0 ]->Name="email";                            $data[0 ]->Value=$datadec->email;   
    $data[1 ]->Name="password";                         $data[1 ]->Value=$datadec->passwd;        // $data[2 ]->Type="Num";  
    $data[2 ]->Name="NomeProvider";                     $data[2 ]->Value=$datadec->nomeprovider;         //  $data[3 ]->Type="Num";
    $data[3 ]->Name="NomeResponsavel";                  $data[3 ]->Value=$datadec->nomeresp;
    $data[4 ]->Name="TelefoneResponsavel";              $data[4 ]->Value=$datadec->telefoneresp;      
    $data[5 ]->Name="IdResponsavel";                    $data[5 ]->Value=$datadec->cpfresp;     
    $data[6 ]->Name="city";                             $data[6 ]->Value=$datadec->city;    
    $data[7 ]->Name="address";                          $data[7 ]->Value=$datadec->address;  
    $data[8 ]->Name="country";                          $data[8 ]->Value=$datadec->country;   
    $data[9 ]->Name="countryCode";                      $data[9 ]->Value=$datadec->countrycode;  
    $data[10]->Name="Observacoes";                      $data[10]->Value=$datadec->obs;       
    $data[11]->Name="Active";                           $data[11]->Value=0;                     $data[11]->Type="Num";  
    $data[12]->Name="AutorizationLegalDocuments";       $data[12]->Value=$datadec->dataFile;       

    $data[13]->Name="StaffPercHour";                    $data[13]->Value=$datadec->idpercinfraction;  $data[13]->Type="Num";
    $data[14]->Name="Currency";                         $data[14]->Value=$datadec->idSelCurr;       
    $data[15]->Name="Language";                         $data[15]->Value=$datadec->idSelLang;                        
    $data[16]->Name="HourValue";                        $data[16]->Value=$datadec->idvalorhora;     $data[16]->Type="Num";  

    
    $data[17]->Name="useRangeOperation"; $data[17]->Value=$datadec->useRangeOperation; $data[17]->Type="Num";  
    $data[18]->Name="countryOperation";  $data[18]->Value=$datadec->countryOperation; 
    $data[19]->Name="cityOperation";     $data[19]->Value=$datadec->cityOperation; 
    $data[20]->Name="latOperation";      $data[20]->Value=$datadec->latOperation;    $data[20]->Type="Num";  
    $data[21]->Name="lonOperation";      $data[21]->Value=$datadec->lonOperation;    $data[21]->Type="Num"; 
    $data[22]->Name="rangeOperation";    $data[22]->Value=$datadec->rangeOperation;  $data[22]->Type="Num"; 

    $data[23]->Name="cielomerid";    $data[23]->Value=$datadec->cielomerid;    
    $data[24]->Name="cielomerkey";   $data[24]->Value=$datadec->cielomerkey; 
    
    
    if($update==false)
    {
        $data[25]->Name="DateInclusion";  $data[25]->Value=GetDateTime(); 
        $resp = SqlInsert ("ParkingProvider",$data);
        ActivationEmailSimple($datadec->email, hash('sha256', $datadec->passwd . 'ParkingFit'),'VAG'); 
        echo "{\"response\":\"ProviderCreatedOk\",\"sql\":\"\"}"; 
    }
    else
    {
        $data[11]->Name="Active";                           $data[11]->Value=1;                     $data[11]->Type="Num";  

        $resp = SqlUpdate ("ParkingProvider",$data," email='$datadec->email' ");
        // error_log("Sql do update ".$resp);
        echo "{\"response\":\"ProviderUpdatedOk\",\"sql\":\"Nada\"}"; 
    }
    // echo $resp;
    // $resp = escapeJsonString($resp);
     
    
}
////////////////////////////////////////////////////////////////////////////////
function ParkCreateStaff()
{
    
//    datPark.email = $('#id_login').val();
//    datPark.passwd = $('#id_passwd').val();
    
    $dataJson    = $_POST["json"];
    
    // $dataJson    = "{\"email\":\"sasd@asdasd.asdasd\",\"passwd\":\"asdasd\",\"nomeprovider\":\"asdas\",\"nomeresp\":\"asdasd\",\"telefoneresp\":\"adsasd\",\"cpfresp\":\"asddsa\",\"address\":\"Chafariz do Mte. Valentim - Centroasddas, Rio de Janeiro - RJ, Brasil\",\"city\":\"Rio de Janeiro\",\"obs\":\"Observações\",\"country\":\"Brasil\",\"countrycode\":\"BR\",\"dataFile\":\"[{\\\"name\\\":\\\"Teste.pdf\\\",\\\"contents\\\":\\\"data:application/pdf;base64,VGVzdGU=\\\"},{},{}]\"}";
    $datadec = json_decode($dataJson); 
     
    // $datadec = json_decode(unescapeJsonString($dataJson)); 
        
    // echo $datadec->dataFile."<br>";

    $sql = "select count(email) from `ParkingProvider` WHERE email='$datadec->email' ";
    
    // echo $sql."<br>";
    $resultado = db_query($sql);
    $update=false;
    if ($resultado[1][0] > 0) 
    {
       $update=true;
       // echo "{\"response\":\"AlreadyExists\",\"email\":\"$datadec->email\"}"; 
       // return;
    }
    
    $data[] = new ColumnData();  
   
//    datPark.passwd = $('#id_passwd').val();
//    datPark.email       = $('#id_email').val();       
//    datPark.passwd      = $('#id_passwd').val();      
//    datPark.nomeprovider = $('#id_nomeprovider').val();
//    datPark.nomeresp    = $('#id_nomeresp').val();
//    datPark.telefoneresp = $('#id_telefoneresp').val();
//    datPark.cpfresp     = $('#id_cpfresp').val();
//    datPark.address     = $('#id_address').val();
//    datPark.city        = $('#id_city').val();
//    datPark.obs         = $('#id_obs').val();
      
    $data[0 ]->Name="email";                            $data[0 ]->Value=$datadec->email;   
    $data[1 ]->Name="password";                         $data[1 ]->Value=$datadec->passwd;        // $data[2 ]->Type="Num";  
    $data[2 ]->Name="NomeProvider";                     $data[2 ]->Value=$datadec->nomeprovider;         //  $data[3 ]->Type="Num";
    $data[3 ]->Name="NomeResponsavel";                  $data[3 ]->Value=$datadec->nomeresp;
    $data[4 ]->Name="TelefoneResponsavel";              $data[4 ]->Value=$datadec->telefoneresp;      
    $data[5 ]->Name="IdResponsavel";                    $data[5 ]->Value=$datadec->cpfresp;     
    $data[6 ]->Name="city";                             $data[6 ]->Value=$datadec->city;    
    $data[7 ]->Name="address";                          $data[7 ]->Value=$datadec->address;  
    $data[8 ]->Name="country";                          $data[8 ]->Value=$datadec->country;   
    $data[9 ]->Name="countryCode";                      $data[9 ]->Value=$datadec->countrycode;  
    $data[10]->Name="Observacoes";                      $data[10]->Value=$datadec->obs;       
    $data[11]->Name="Active";                           $data[11]->Value=1;                     $data[11]->Type="Num";  
    $data[12]->Name="AutorizationLegalDocuments";       $data[12]->Value=$datadec->dataFile;       
    
       
    // echo "data[6]->Value=idDevice; - ".$idDevice."\n";

    if($update==false)
    {
        $data[13]->Name="DateInclusion";                    $data[13]->Value=GetDateTime(); 
        $resp = SqlInsert ("ParkingProvider",$data);
        echo "{\"response\":\"ProviderCreatedOk\",\"sql\":\"NADA\"}"; 
    }
    else
    {
        $resp = SqlUpdate ("ParkingProvider",$data," email='$datadec->email' ");
        echo "{\"response\":\"ProviderUpdatedOk\",\"sql\":\"NADA\"}"; 
    }
    // $resp = escapeJsonString($resp);
     
    
}
////////////////////////////////////////////////////////////////////////////////
class ItemParkedCar
{
    public $aLat = 0.0;
    public $aLng = 0.0;
    public $idCar = 0.0;
    public $period = 0.0;
    public $DateTimePark = "";
    public $dist = 0;
}

////////////////////////////////////////////////////////////////////////////////
function DiffTime($DateIni,$DateFin)
{
    $timeini = strtotime($DateIni);
    $timefin = strtotime($DateFin);
    $diff_minutes = ($timefin - $timeini) / 60.0;
    return $diff_minutes;
}
////////////////////////////////////////////////////////////////////////////////
class ParkedCar
{
    public $response = 0.0;
    public $aLat = 0.0;
    public $aLng = 0.0;
    public $idCar = "";
    public $period = 0.0;
    public $DateTimePark = "";
    public $RemainingTime = 0;
}
////////////////////////////////////////////////////////////////////////////////
function GetPlateLastStatus()
{

    $json_dat=$_POST["json"];
    // $json_dat= "{\"cardnumber\":\"0000.0000.0000.0001\",\"placa\":\"ZZZ-3232\",\"estado\":\"RJ\",\"cidade\":\"Niteroi\",\"pais\":\"BR\"}";
    // $json_dat= "{\"cardnumber\":\"0000.0000.0000.0001\",\"placa\":\"ZZZ-1234\",\"estado\":\"RJ\",\"cidade\":\"Rio de Janeiro\",\"pais\":\"BR\"}";
    
    $datadec = json_decode($json_dat);  
    
    $idPlate = $datadec->placa;
    $cidade   = $datadec->cidade;       
    $estado   = $datadec->estado; 
    $pais  = $datadec->pais; 
    
    $query = "select idCar,Lat,Longc,Period,DateTimePark  from `ParkingData` where idCar='$datadec->placa' and City='$datadec->cidade' and Country='$datadec->pais' order by DateTimePark desc limit 30";
    
    
        
    $resultado = db_query($query);
    
    $idCar = $resultado[$i + 1][0];
    $aLat = $resultado[$i + 1][1];
    $aLng = $resultado[$i + 1][2];
    $period = $resultado[$i + 1][3];
    $DateTimePark = $resultado[$i + 1][4];
    
    
    $resp = new ParkedCar();  
    
    if($idCar=="")
    {
        $resp->response = "Nothing"; 
   
        echo json_encode($resp);   
        return;
    }    
    // echo $DateTimePark."<br>";
    // echo GetDateTime()."<br>";
    
    
    $diffTime = (DiffTime($DateTimePark,GetDateTime())/60.0);
    
    // echo $diffTime."<br>";
    if($diffTime < $period && $diffTime>0) // não acabou o tempo
    {
        $resp->response = "RemainingPeriod";
        $resp->aLat = $aLat;
        $resp->aLng = $aLng;
        $resp->period = $period; 
        $resp->idCar = $idCar; 
        $resp->RemainingTimeHs = $period-(DiffTime($DateTimePark,GetDateTime())/60.0);           
    }  
    else
    {
        $resp->response = "Nothing"; 

    }
    // print_r($resultado); 
    echo json_encode($resp);   
    return;
}
////////////////////////////////////////////////////////////////////////////////


function RegisterPlateEndTime()
{
    
//    INSERT INTO tbl_temp2 (fld_id)
//    SELECT tbl_temp1.fld_order_id
//    FROM tbl_temp1 WHERE tbl_temp1.fld_order_id > 100;
    

    
    $dataJson    = $_POST["json"];
    // $dataJson    = "{\"placa\":\"QWQ-1221\",\"lat\":-22.9155315,\"lon\":-43.0918731,\"userid\":1,\"estado\":\"RJ\",\"cidade\":\"Niteroi\",\"pais\":\"BR\",\"street\":\"R. Goitacases\",\"endereco\":\"R. Goitacases, 39 - São Francisco, Niterói - RJ, 24360-350, Brazil\"}";
    // $datadec = json_decode($dataJson); 
     
    $datadec = json_decode(unescapeJsonString($dataJson)); 
        
//     datPark.placa = $('#id_placa').val().toUpperCase();
//     datPark.lat = glbLatNow; 
//     datPark.lon = glbLngNow;
//     datPark.userid = UserId;
//     datPark.estado = glbEstado;
//     datPark.cidade = glbCity;
//     datPark.pais = glbCountry;
//     datPark.street = glbStreet;
//     datPark.endereco = glbAddress;
    if ($datadec->DateTimePark == null) {
        // $datadec->DateTimePark = GetDateTime();
        $sql = "select count(idCar) from `ParkingDataPenalty` WHERE `idCar`='$datadec->placa' ";
    }
    else {
        $sql = "select count(idCar) from `ParkingDataPenalty` WHERE `idCar`='$datadec->placa' and TIMESTAMPDIFF(DAY,CURDATE(),DateTimePark)>0";
    }
    	
    $resultado = db_query($sql);
    if ($resultado[1][0] > 0) 
    {
       $plate = $datadec->placa;
       echo "{\"response\":\"PenaltyAlreadyRegistred\",\"placa\":\"$plate\",\"sql\":\"NADA\"}"; 
       return;
    }
    
    $data[] = new ColumnData();  
    
   
    $data[0 ]->Name="idCar";        $data[0 ]->Value=$datadec->placa;      
    $data[1 ]->Name="JsonDat";      $data[1 ]->Value=$dataJson;   
    $data[2 ]->Name="Lat";          $data[2 ]->Value=$datadec->lat;      $data[2 ]->Type="Num";  
    $data[3 ]->Name="Longc";        $data[3 ]->Value=$datadec->lon;      $data[3 ]->Type="Num";
    $data[4 ]->Name="Address";      $data[4 ]->Value=$datadec->endereco;
    $data[5 ]->Name="City";         $data[5 ]->Value=$datadec->cidade;      
    $data[6 ]->Name="State";        $data[6 ]->Value=$datadec->estado;     
    $data[7 ]->Name="Country";      $data[7 ]->Value=$datadec->pais;    
    $data[8 ]->Name="Street";       $data[8 ]->Value=$datadec->street;  
    $data[9 ]->Name="DateTimePark"; $data[9 ]->Value=$datadec->DateTimePark;    
    $data[10]->Name="emailstaff";   $data[10]->Value=$datadec->emailstaff;
    $data[11]->Name="dateinfraction";   $data[11]->Value=$datadec->dtHora;
    $data[12]->Name="emailProvider";   $data[12]->Value=$datadec->emailprovider;
    
    $sql= "select HourValue*(StaffPercHour/100)  as Value from ParkingProvider where email='$datadec->emailprovider'";
    $data[13]->Name="staffMoney";   $data[13]->Value=GetValue($sql); $data[13]->Type="Num";
     
    
    
    
    // datPark.emailprovider
    // echo "data[6]->Value=idDevice; - ".$idDevice."\n";

    $resp = SqlInsert ("ParkingDataPenalty",$data);
    // $resp = escapeJsonString($resp);
    $plate = $datadec->placa;
    echo "{\"response\":\"PenaltyRegistred\",\"placa\":\"$plate\",\"sql\":\"NADA\"}";    
}
////////////////////////////////////////////////////////////////////////////////
function PostCheckPlateParking() 
{
    // { type:"GetCarsInThisStreet",lat:glbLatNow,lon:glbLatNow,country:glbCountry,city:glbCity,street:glbStreet}   
    // select idCar,Lat,Longc,Period from `ParkingData` where Street='' and City='' and Country=''
    $json_dat=$_POST["json"];
    $datadec = json_decode($json_dat); 
    
    $idPlate = $datadec->placa;
    $lat = $datadec->lat;
    $lon   = $datadec->lon;    
    $endereço = $datadec->endereço;
    $cidade   = $datadec->cidade;       
    $estado   = $datadec->estado; 

    
    $query = "select idCar,Lat,Longc,Period,DateTimePark,haversine(Lat,Longc,$lat,$lon) as harvin from `ParkingData` where REPLACE(idCar,'-', '')=REPLACE('$datadec->placa','-', '') order by harvin asc limit 30";
    
    // echo $query;
    
    $resultado = db_query($query);

    // print_r($resultado); 

    $r = new vetItensR();
    $r->vet[] = new ItemParkedCar();

    $i = 0;

    $r->vet[$i]->idCar = $resultado[$i + 1][0];
    $r->vet[$i]->aLat = $resultado[$i + 1][1];
    $r->vet[$i]->aLng = $resultado[$i + 1][2];
    $r->vet[$i]->period = $resultado[$i + 1][3];
    $r->vet[$i]->DateTimePark = $resultado[$i + 1][4];
    $r->vet[$i]->dist = $resultado[$i + 1][5];

    $r->numItens = $i;                        
    $r->sql =$query;
    // $r->sql = $datadec->placa;
    if($r->vet[$i]->idCar!="")
       $r->response ="PlacaEncontrada";
    else
       $r->response ="PlacaNaoEncontrada";
    echo json_encode($r);
}
////////////////////////////////////////////////////////////////////////////////
function PostGetCarsInThisStreet() 
{
    // { type:"GetCarsInThisStreet",lat:glbLatNow,lon:glbLatNow,country:glbCountry,city:glbCity,street:glbStreet}   
    // select idCar,Lat,Longc,Period from `ParkingData` where Street='' and City='' and Country=''
    $lat=$_POST["lat"];
    $lon=$_POST["lon"];
    $country=$_POST["country"];
    $city=$_POST["city"];
    $street=$_POST["street"];
    $datetime=$_POST["datetime"];
    // $idDevice=$_GET['idDevice'];
    $Radius = 1000; // Raio de 1000m
    // $query = "select idProvider, Service, posLat, posLng, TimeStampc, Velocity, Heading, BatteryLevel, Accuracy, haversine(posLat,posLng,$lat,$lon) as harv  from `GeoServicesOnline` where haversine(posLat,posLng,$lat,$lon)<=$Radius and `cStatus`='Ready' order by harv LIMIT 15";
    // select haversine(-22.904748, -43.190300,-22.901081, -43.179150) = 1212.7547511759801 metros
    // select idProvider, Service, posLat, posLng, TimeStampc, Velocity, Bearing, BatteryLevel, Accuracy, haversine(posLat,posLng,-22.901081, -43.179150) as harv  from `GeoServicesOnline` where haversine(posLat,posLng,-22.901081, -43.179150)<=20000 order by harv
    // haversine(aLat,aLong,$Lat,$Long)<=Radius 
    $query = "select idCar,Lat,Longc,Period,DateTimePark,haversine(Lat,Longc,$lat,$lon) as harvin from `ParkingData` where  HOUR(TIMEDIFF('$datetime',  DateTimePark)) <= 24 and ((Street='$street' AND City='$city' AND Country='$country') OR haversine(Lat,Longc,$lat,$lon)<=$Radius)  order by harvin asc limit 30";
    
    // HOUR(TIMEDIFF('$datetime',  DateTimePark)) <= 24
    
    // echo $query;
    
    $resultado = db_query($query);

    // print_r($resultado);

    $r = new vetItensR();
    $r->vet[] = new ItemParkedCar();


    $i = 0;
    while ($resultado[$i + 1][0] != "") 
    {
        $r->vet[$i]->idCar = $resultado[$i + 1][0];
        $r->vet[$i]->aLat = $resultado[$i + 1][1];
        $r->vet[$i]->aLng = $resultado[$i + 1][2];
        $r->vet[$i]->period = $resultado[$i + 1][3];
        $r->vet[$i]->DateTimePark = $resultado[$i + 1][4];
        // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        // $r->vet[$i]->Address = $resultado[$i + 1][4];
        $i++;
    }
    $r->numItens = $i;
    $r->sql =$query;
    if($i!=0)
       $r->response ="CarList";
    echo json_encode($r);
}
////////////////////////////////////////////////////////////////////////////////
/**
 * @param $value
 * @return mixed
 */
function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($escapers, $replacements, $value);
    return $result;
}
////////////////////////////////////////////////////////////////////////////////
function unescapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($replacements,$escapers,  $value);
    return $result;
}
/////////////////////////////////////////////////////////////////////////////////
include_once 'GatewayPagamentos/Cielo/CieloApi.php';
/////////////////////////////////////////////////////////////////////////////////
function PostPayValueWithCardToken()
{
   $dataJson    = $_POST["json"];

    $datadec = json_decode($dataJson); 
    
//    datSend.amount = paymentData.val2pay;
//    datSend.date = GetDateTime();
    
    $cieloResp= CieloPayWithCardToken($datadec);
    
    // error_log($cieloResp->MerchantOrderId);
    // error_log($cieloResp->msg_curl_error);
    
    SaveTransactionOnPaymentLogs($datadec->date,$datadec->userEmail,
                                 $datadec->Application,$datadec->AppContext,"Cielo",
                                 $cieloResp->dbg);
    if($cieloResp->msg_curl_error!=="")
    {  
       // $buffret = $cieloResp->Payment->ReturnMessage;
       echo "{\"response\":\"ErrorPayingValueWithCardToken\",\"ReturnMessage\":\"null\",\"dbg\":\"null\"}";
       return;
    }
    

    echo "{\"response\":\"PaymentOk\",\"ReturnMessage\":\"null\",\"dbg\":\"null\"}";
    

    return;
    
    // PaymentCardsStored
    //  `emailUser` VARCHAR(255) NULL DEFAULT NULL,
    //	`Aplication` VARCHAR(255) NULL DEFAULT NULL,
    //	`AppContext` VARCHAR(255) NULL DEFAULT NULL,
    //	`CardNumber` VARCHAR(100) NULL DEFAULT NULL,
    //	`Holder` VARCHAR(100) NULL DEFAULT NULL,
    //	`ExpirationDate` VARCHAR(100) NULL DEFAULT NULL,
    //	`CardToken` VARCHAR(100) NULL DEFAULT NULL,
    //	`Brand` VARCHAR(25) NULL DEFAULT NULL,
    
        
    //    datCard.userEmail = userEmail; 
    //    datCard.Application = Application;
    //    datCard.AppContext = AppContext;
    //    datCard.namecard = $('#id_nome').val();
    //    datCard.periodo = $('#periodo').val();
    //    datCard.cardnumber = $('#id_card').val();
    //    datCard.cvc = $('#id_cvc').val();
    //    datCard.dtexp = $('#id_dtexp').val();
    //    datCard.placa = $('#id_placa').val().toUpperCase();
    //    datCard.dthorapedido = GetDateTime();
    
    
    $data[] = new ColumnData();           
   
    // "5148000000008765";   
    $datadec->cardnumber = str_replace(".", "", $datadec->cardnumber);
    $cardnumber = trim("****-****-****-" .substr($datadec->cardnumber,12,4));
    
    $data[0 ]->Name="emailUser";       $data[0 ]->Value=$datadec->userEmail;          
    $data[1 ]->Name="Application";     $data[1 ]->Value=$datadec->Application;      
    $data[2 ]->Name="AppContext";      $data[2 ]->Value=$datadec->AppContext;   
    $data[3 ]->Name="CardNumber";      $data[3 ]->Value=$cardnumber;            
    $data[4 ]->Name="Holder";          $data[4 ]->Value=strtoupper($datadec->namecard);          
    $data[5 ]->Name="ExpirationDate";  $data[5 ]->Value=$datadec->dtexp;
    $data[6 ]->Name="CardToken";       $data[6 ]->Value=$cieloResp; //  --- >> TOKEN HERE!!!!!!!!!!!!!!!!;    
    $data[7 ]->Name="Brand";           $data[7 ]->Value=$datadec->brand; //  --- >> BRAND HERE!!!!!!!!!!!!!!!!;     
    $data[8 ]->Name="cvc";             $data[8 ]->Value=$datadec->cvc;
    $data[9 ]->Name="Data";            $data[9 ]->Value=$datadec->dthorapedido;
    
    $tmpbuf = "select CardNumber from PaymentCardsStored where emailUser='".$datadec->userEmail."' and Application='".$datadec->Application."' and AppContext='".$datadec->AppContext."';";
    if (GetValue($tmpbuf) == "") 
    {
        $dtadefault = 1;
    } 
    else 
    {
        $dtadefault = 0;
    }

    $data[10]->Name="DefaultSel";         $data[10]->Value=$dtadefault;           $data[10]->Type="Num";  
    
    $resp = SqlInsert ("PaymentCardsStored",$data);
    $resp = escapeJsonString($resp);
    echo "{\"response\":\"PaymentCardRegistered\",\"sql\":\"$resp\"}";  
    
}
/////////////////////////////////////////////////////////////////////////////////
function SaveTransactionOnPaymentLogs($Date,$emailUser,$Application,$AppContext,$PaymentGateway,$RawPayment)
{
    $data[] = new ColumnData();           
    
    $data[0 ]->Name="Date";            $data[0 ]->Value=$Date;          
    $data[1 ]->Name="emailUser";       $data[1 ]->Value=$emailUser;      
    $data[2 ]->Name="Application";     $data[2 ]->Value=$Application;   
    $data[3 ]->Name="AppContext";      $data[3 ]->Value=$AppContext;            
    $data[4 ]->Name="PaymentGateway";  $data[4 ]->Value=$PaymentGateway;          
    $data[5 ]->Name="RawPayment";      $data[5 ]->Value=mysql_real_escape_string ($RawPayment);
    $retorno = SqlInsert ("PaymentLogs",$data);
    // error_log($retorno);
}   
/////////////////////////////////////////////////////////////////////////////////
function PostRegisterNewPaymentCard()
{
    
    $dataJson    = $_POST["json"];
       
    $datadec = json_decode($dataJson); 
    $cieloResp= CieloGetCardToken($datadec);
    
    SaveTransactionOnPaymentLogs($datadec->dthorapedido,$datadec->userEmail,
                                 $datadec->Application,$datadec->AppContext."-GetToken","Cielo",
                                 $cieloResp->dbg);
    

    
    if($cieloResp->CardToken=="")
    {  
       // $buffret = $cieloResp->Payment->ReturnMessage;
       echo "{\"response\":\"ErrorGetCardToken\",\"ReturnMessage\":\"null\",\"dbg\":\"null\"}";
       return;
    }
 
    // PaymentCardsStored
    //  `emailUser` VARCHAR(255) NULL DEFAULT NULL,
    //	`Aplication` VARCHAR(255) NULL DEFAULT NULL,
    //	`AppContext` VARCHAR(255) NULL DEFAULT NULL,
    //	`CardNumber` VARCHAR(100) NULL DEFAULT NULL,
    //	`Holder` VARCHAR(100) NULL DEFAULT NULL,
    //	`ExpirationDate` VARCHAR(100) NULL DEFAULT NULL,
    //	`CardToken` VARCHAR(100) NULL DEFAULT NULL,
    //	`Brand` VARCHAR(25) NULL DEFAULT NULL,
    
        
    //    datCard.userEmail = userEmail; 
    //    datCard.Application = Application;
    //    datCard.AppContext = AppContext;
    //    datCard.namecard = $('#id_nome').val();
    //    datCard.periodo = $('#periodo').val();
    //    datCard.cardnumber = $('#id_card').val();
    //    datCard.cvc = $('#id_cvc').val();
    //    datCard.dtexp = $('#id_dtexp').val();
    //    datCard.placa = $('#id_placa').val().toUpperCase();
    //    datCard.dthorapedido = GetDateTime();
    
    
    $data[] = new ColumnData();           
   
    // "5148000000008765";   
    $datadec->cardnumber = str_replace(".", "", $datadec->cardnumber);
    $cardnumber = trim("****-****-****-" .substr($datadec->cardnumber,12,4));
    
    $data[0 ]->Name="emailUser";       $data[0 ]->Value=$datadec->userEmail;          
    $data[1 ]->Name="Application";     $data[1 ]->Value=$datadec->Application;      
    $data[2 ]->Name="AppContext";      $data[2 ]->Value=$datadec->AppContext;   
    $data[3 ]->Name="CardNumber";      $data[3 ]->Value=$cardnumber;            
    $data[4 ]->Name="Holder";          $data[4 ]->Value=strtoupper($datadec->namecard);          
    $data[5 ]->Name="ExpirationDate";  $data[5 ]->Value=$datadec->dtexp;
    $data[6 ]->Name="CardToken";       $data[6 ]->Value=$cieloResp->CardToken; //  --- >> TOKEN HERE!!!!!!!!!!!!!!!!;    
    $data[7 ]->Name="Brand";           $data[7 ]->Value=$datadec->brand; //  --- >> BRAND HERE!!!!!!!!!!!!!!!!;     
    $data[8 ]->Name="cvc";             $data[8 ]->Value=$datadec->cvc;
    $data[9 ]->Name="Data";            $data[9 ]->Value=$datadec->dthorapedido;
    
    $tmpbuf = "select CardNumber from PaymentCardsStored where emailUser='".$datadec->userEmail."' and Application='".$datadec->Application."' and AppContext='".$datadec->AppContext."';";
    if (GetValue($tmpbuf) == "") 
    {
        $dtadefault = 1;
    } 
    else 
    {
        $dtadefault = 0;
    }

    $data[10]->Name="DefaultSel";         $data[10]->Value=$dtadefault;           $data[10]->Type="Num";  
    
    $resp = SqlInsert ("PaymentCardsStored",$data);
    $resp = escapeJsonString($resp);
    echo "{\"response\":\"PaymentCardRegistered\",\"sql\":\"$resp\"}";  

}
/////////////////////////////////////////////////////////////////////////////////
function PaymentParkDone($datadec)
{
    
    $dtatrans = new DataTrasactionCielo();

    $dtatrans->orderid=$datadec->placa."_".GetDateTime();
    $dtatrans->namecustomer=$datadec->namecard."_".$datadec->placa; 
    $dtatrans->type="CreditCard"; 
    
    $valuePerHour = GetValue("select HourValue from ParkingProvider where  email='$datadec->id_providersel';");
    $dtatrans->amount=$datadec->periodo*$valuePerHour*100; 
    $dtatrans->instalments=1; 
    $dtatrans->cardnumber=$datadec->cardnumber; 
    $dtatrans->holder=$datadec->namecard; 
    $dtatrans->expdate=$datadec->dtexp; 
    $dtatrans->cvc=$datadec->cvc; 
    $dtatrans->brand=$datadec->brand; 
    $dtatrans->requestid=$datadec->placa."_".GetDateTime();
               // $datadec->PaymentProvider = "Cielo";
    $dtatrans->merchantid=$datadec->PaymentMerchantUserId;
    $dtatrans->merchantkey=$datadec->PaymentMerchantUserKey;       

    // 
    $cieloResp = CieloTransaction("CardPayment",$dtatrans); 
    
    $cieloResp->dbg = escapeJsonString($cieloResp->dbg);
    if($cieloResp->Payment->Status == 2) // OK
    {
        // 
        $paid=1;
    }   
    else // Error
    {
        $paid=0;
    }
    
    SaveTransactionOnPaymentLogs(GetDateTime(),$datadec->userid,"ParkingFit","NoUserPayment - ".$datadec->placa,"Cielo",json_encode($cieloResp));
    
//    $data[] = new ColumnData();           
//   
//    $data[0 ]->Name="idUser";         $data[0]->Value=$datadec->userid;        $data[0 ]->Type="Num";  
//    $data[1 ]->Name="idCar";          $data[1]->Value=$datadec->placa;      
//    $data[2 ]->Name="Address";        $data[2]->Value=$datadec->endereco;
//    $data[3 ]->Name="City";           $data[3]->Value=$datadec->cidade;      
//    $data[4 ]->Name="State";          $data[4]->Value=$datadec->estado;     
//    $data[5 ]->Name="Country";        $data[5]->Value=$datadec->pais; 
//    $data[6 ]->Name="DateTimePark";   $data[6]->Value=GetDateTime();       
//    $data[7 ]->Name="JsonDatPark";    $data[7]->Value=json_encode($datadec);
//    $data[8 ]->Name="JsonDatPayment"; $data[8]->Value=json_encode($cieloResp);
//    $data[9 ]->Name="ParkPaid";       $data[9]->Value=$paid;
//    
//    // echo "data[6]->Value=idDevice; - ".$idDevice."\n";
//    $resp = SqlInsert ("ParkingDataPayment",$data);
        
    return $cieloResp;

    
}
////////////////////////////////////////////////////////////////////////////////
function PostParkingPay() 
{
    //    datPark.namecard = $('#id_nome').val();
    //    datPark.periodo = $('#periodo').val();
    //    datPark.cardnumber = $('#id_card').val();
    //    datPark.cvc = $('#id_cvc').val();
    //    datPark.dtexp = $('#id_dtexp').val();
    //    datPark.placa = $('#id_placa').val();
    //    datPark.lat = idOrilatGlb;
    //    datPark.lon = idOrilonGlb;
    //    datPark.heading = glbHeading;
    //    datPark.accuracy = glbAccuracy;
    //    datPark.userid = UserId;
    //    datPark.endereco
    //    datPark.cidade
    
    $dataJson    = $_POST["json_str"];
    // Depurando
    // $dataJson    = "{\"namecard\":\"12121212122112\",\"periodo\":\"3h\",\"cardnumber\":\"0000.0000.0000.0001\",\"cvc\":\"121\",\"dtexp\":\"12/221\",\"placa\":\"ZZZ-1211\",\"lat\":-22.91548207479761,\"lon\":-43.09187650680542,\"heading\":\"Null\",\"accuracy\":20,\"userid\":1,\"endereco\":\"R.+Goitacases,+39+-+São+Francisco,+Niterói+-+RJ,+24360-350,+Brazil\",\"estado\":\"RJ\",\"cidade\":\"Niteroi\",\"pais\":\"BR\",\"street\":\"R.+Goitacases\",\"dthorapedido\":\"2017-02-11T21:40:52.331Z\"}";
    // $dataJson = "{\"namecard\":\"zebedeu tarado\",\"periodo\":\"3h\",\"cardnumber\":\"0000.0000.0000.0001\",\"cvc\":\"123\",\"dtexp\":\"02/2022\",\"placa\":\"ZZZ-1234\",\"lat\":-22.902496799999998,\"lon\":-43.1740421,\"heading\":\"Null\",\"accuracy\":36,\"userid\":1,\"endereco\":\"Chafariz do Mte. Valentim - Centro, Rio de Janeiro - RJ, Brasil\",\"estado\":\"RJ\",\"cidade\":\"Rio de Janeiro\",\"pais\":\"BR\",\"street\":\"undefined\",\"dthorapedido\":\"2017-02-21T16:54:37.480Z\"}";
    
    $datadec = json_decode($dataJson); 
    
        // $cieloResp->Payment->Status == 2 Pago e capturado
    if($datadec->PaidByToken=="") 
    {
        // datPark.PaymentMerchantUserId = GetCieloMerchantId("ParkingFitProvider",datPark.id_providersel);
        // datPark.PaymentMerchantUserKey = GetCieloMerchantKey("ParkingFitProvider",datPark.id_providersel);
        $cieloResp = PaymentParkDone($datadec);
        if($cieloResp->Payment->Status != 2)
        {  
           $buffret = $cieloResp->Payment->ReturnMessage;
           echo "{\"response\":\"PaymentError\",\"ReturnMessage\":\"$buffret\",\"dbg\":\"$cieloResp->dbg\"}";
           return;
        }
    }
    
    $Lat = $datadec->lat;
    $Lon   = $datadec->lon;    
    $endereço = $datadec->endereço;
    $cidade   = $datadec->cidade;       
    $estado   = $datadec->estado; 
    
    $data[] = new ColumnData();           
   
    $data[0 ]->Name="idUser";       $data[0 ]->Value=$datadec->userid;       
    $data[1 ]->Name="idCar";        $data[1 ]->Value=$datadec->placa;      
    $data[2 ]->Name="JsonDat";      $data[2 ]->Value=$dataJson;   
    
    $data[3 ]->Name="Lat";          $data[3 ]->Value=$datadec->lat;           $data[3 ]->Type="Num";  
    $data[4 ]->Name="Longc";        $data[4 ]->Value=$datadec->lon;           $data[4 ]->Type="Num";
    $data[5 ]->Name="Address";      $data[5 ]->Value=$datadec->endereco;
    $data[6 ]->Name="City";         $data[6 ]->Value=$datadec->cidade;      
    $data[7 ]->Name="State";        $data[7 ]->Value=$datadec->estado;     
    $data[8 ]->Name="Country";      $data[8 ]->Value=$datadec->pais; 
    $data[9 ]->Name="Period";       $data[9 ]->Value=$datadec->periodo;       
    $data[10]->Name="Street";       $data[10]->Value=$datadec->street;    
    $data[11]->Name="DateTimePark"; $data[11]->Value=GetDateTime(); 
    $data[12]->Name="idProvider";   $data[12]->Value=$datadec->id_providersel;  
    
    $valuePerHour = GetValue("select HourValue from ParkingProvider where  email='$datadec->id_providersel';");
    $data[13]->Name="valuePaid";   $data[13]->Value=$datadec->periodo * $valuePerHour;  $data[13]->Type="Num";
    
    $resp = SqlInsert ("ParkingData",$data);
    $resp = escapeJsonString($resp);
    echo "{\"response\":\"PaymentOk\",\"period\":\"$datadec->periodo\",\"sql\":\"$resp\"}";  
}
////////////////////////////////////////////////////////////////////////////////
/*
    drop table IF EXISTS `UserLastStatus`;
    CREATE TABLE `UserLastStatus` (
            `idUser` BIGINT(20) NOT NULL,
            `idDevice`   VARCHAR(255) NULL,

            `JsonStatus`     MEDIUMBLOB NULL,
            INDEX `iidProvider` (`idUser`, `idDevice`)
    )
    COLLATE='latin1_swedish_ci '
    ENGINE=InnoDB;
 */ 
     
function PostSaveLastUserStatus() 
{
    $data    = $_POST["data"];
    $datadec = json_decode($data); 
    $idUser = $datadec->iduser;
    $idDevice = $datadec->iddevice;    
    
    $sql = "select count(idUser) from `UserLastStatus` WHERE idUser=$UserId and idDevice='$idDevice' ;";
    $resultado = db_query($sql);
    if ($resultado[1][0] == 0) 
        $flagType = "Insert";
    
    $data[] = new ColumnData();           
    
    $data[0]->Name="idUser";          $data[0]->Value=$UserId;       $data[0]->Type="Num";  
    $data[1]->Name="idDevice";        $data[1]->Value=$idDevice;       
    $data[2]->Name="JsonStatus";      $data[2]->Value=$data;            
    
    // echo "data[6]->Value=idDevice; - ".$idDevice."\n";
    if($flagType == "Insert") 
       $resp = SqlInsert ("UserLastStatus",$data);
    else
       $resp = SqlUpdate ("UserLastStatus",$data," idUser=$UserId and idDevice='$idDevice' ");

}
////////////////////////////////////////////////////////////////////////////////
class ItemProvider 
{
    public $aLat = 0.0;
    public $aLng = 0.0;
    public $Heading = 0.0;
}

//class vetItensR 
//{
//
//    public $vet;
//    public $numItens = 0;
//
//}

////////////////////////////////////////////////////////////////////////////////
function PostGetProvidersInRegion() 
{
    // { type:"GetProviders",userid:UserId, lat:Lat,lon:Lon,service: Service }   
    $UserId =$_POST["userid"];
    $lat=$_POST["lat"];
    $lon=$_POST["lon"];
    $service=$_POST["service"];

    // $idDevice=$_GET['idDevice'];
    $Radius = 10000; // Raio de 10km
    $query = "select idProvider, Service, posLat, posLng, TimeStampc, Velocity, Heading, BatteryLevel, Accuracy, haversine(posLat,posLng,$lat,$lon) as harv  from `GeoServicesOnline` where haversine(posLat,posLng,$lat,$lon)<=$Radius and `cStatus`='Ready' order by harv LIMIT 15";
    // select haversine(-22.904748, -43.190300,-22.901081, -43.179150) = 1212.7547511759801 metros
    // select idProvider, Service, posLat, posLng, TimeStampc, Velocity, Bearing, BatteryLevel, Accuracy, haversine(posLat,posLng,-22.901081, -43.179150) as harv  from `GeoServicesOnline` where haversine(posLat,posLng,-22.901081, -43.179150)<=20000 order by harv
    // haversine(aLat,aLong,$Lat,$Long)<=Radius 
    
    echo $query;
    
    $resultado = db_query($query);

    // print_r($resultado);

    $r = new vetItensR();
    $r->vet[] = new ItemProvider();


    $i = 0;
    while ($resultado[$i + 1][0] != "") 
    {
        $r->vet[$i]->aLat = $resultado[$i + 1][2];
        $r->vet[$i]->aLng = $resultado[$i + 1][3];
        if($resultado[$i + 1][6]==" ")
            $r->vet[$i]->Heading = "Null";
        else
            $r->vet[$i]->Heading = $resultado[$i + 1][6];
        // $r->vet[$i]->Address = $resultado[$i + 1][4];
        $i++;
    }
    $r->numItens = $i;


    // Uso
    // http://localhost/ChildMonitor/Util/GetGlobalPosition.php?id=12
    // Retorno
    // {"id":"12","Lat":"23.123212","Long":"12.129877","TimeStamp":"2000-11-11 13:23:02","Velocity":"3"} 
    // Decodificação em javascript
    // var json = '{"result":true,"count":1}',
    // obj = JSON && JSON.parse(json) || $.parseJSON(json);
    // alert(obj.count);

    echo json_encode($r);
}
////////////////////////////////////////////////////////////////////////////////
function CleanProvidersOffLineMoreThan5Minutes()
{
    // Unidade segundos
    // select CURRENT_TIMESTAMP,TimeStampc,(CURRENT_TIMESTAMP-TimeStampc)/60.0 from GeoServicesOnline;
    
    // Apaga os que estiverem 2 minutos offline
    $sql = "DELETE FROM `GeoServicesOnline` WHERE ((CURRENT_TIMESTAMP-TimeStampc)/60.0) > 5.0;";
    $resultado = db_query($sql);   
}
////////////////////////////////////////////////////////////////////////////////
function PostGeoRequestSendLastPos()
{
    // type:"SendLastPos",userid:UserId,lat:Lat,lon:Lon,heading:Heading,service: Service
    /*
     *  //////////////////////////////////////////////////////////////////////////////
        drop table IF EXISTS `GeoServicesOnline`;
        CREATE TABLE `GeoServicesOnline` (
                `idProvider` BIGINT(20) NOT NULL,
                `idDevice` varchar(255) NOT NULL,
                `Service` varchar(255) NOT NULL,
                `cStatus` varchar(255) NOT NULL,
                `posLat` DOUBLE NOT NULL,
                `posLng` DOUBLE NOT NULL,
                `DateInclusion` datetime NOT NULL, 
                `TimeStampc` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `Velocity` DOUBLE NULL DEFAULT NULL,
                `Heading` DOUBLE NULL DEFAULT NULL,
                `BatteryLevel` DOUBLE NULL DEFAULT NULL,
                `Accuracy` FLOAT NOT NULL,
                INDEX `tlastlong` (`TimeStampc`, `posLat`, `posLng`),
                INDEX `timestamp` (`TimeStampc`)
        ) COLLATE='latin1_swedish_ci ' ENGINE=InnoDB;
     */ 

    // On future turn this a cron job   
    CleanProvidersOffLineMoreThan5Minutes();
    
    
    $UserId =$_POST["userid"];
    $idDevice =$_POST["iddevice"];
    $lat=$_POST["lat"];
    $lon=$_POST["lon"];
    $service=$_POST["service"];
    $heading=$_POST["heading"];

    
//     $sql = "select id from `GeoServicesOnline` WHERE idProvider=$UserId and idDevice='$idDevice' ;";
//    $resultado = db_query($sql);
//    if ($resultado[1][0] != "") {
//        echo "Device already registred";
//        return;
//    }
    
    $sql = "select count(idProvider) from `GeoServicesOnline` WHERE idProvider=$UserId and idDevice='$idDevice' ;";
    $resultado = db_query($sql);
    if ($resultado[1][0] == 0) 
        $flagType = "Insert";

       
    
    $data[] = new ColumnData();           
    
    $data[0]->Name="idProvider";          $data[0]->Value=$UserId;       $data[0]->Type="Num";  
    $data[1]->Name="DateInclusion";       $data[1]->Value=GetDateTime();       
    $data[2]->Name="posLat";              $data[2]->Value=$lat;          $data[2]->Type="Num";  
    $data[3]->Name="posLng";              $data[3]->Value=$lon;          $data[3]->Type="Num";  
    $data[4]->Name="Service";             $data[4]->Value=$service; 
    $data[5]->Name="Heading";             $data[5]->Value=$heading;      $data[5]->Type="Num"; 
    $data[6]->Name="idDevice";            $data[6]->Value=$idDevice;     $data[6]->Type="Char"; 
    $data[7]->Name="cStatus";              $data[7]->Value="Ready";    
    
    // echo "data[6]->Value=idDevice; - ".$idDevice."\n";
    if ($flagType == "Insert") {
        $resp = SqlInsert("GeoServicesOnline", $data);
    } else {
        $resp = SqlUpdate("GeoServicesOnline", $data, " idProvider=$UserId and idDevice='$idDevice' ");
    }

    echo "{\"response\":\"$service-($lat,$lon) ($latdest,$londest) - \n $sql \n $resp\"}";    
}
////////////////////////////////////////////////////////////////////////////////
function ProcessListOfReadyProviders($UserId,$list)
{
    // $resultado[1][0] == 0
    $i=1;
    // echo "Aqui [".$list[0][0]."]\n";
    while($list[$i][0]!="")
    {
        // echo $list[$i][0]." ".$list[$i][1]." ".$list[$i][5]."\n";
         
        $i++;
    }    
}
////////////////////////////////////////////////////////////////////////////////
function SelectProviderAndCallUpon($UserId,$lat,$lon,$service)
{
    $Radius = 10000; // Raio de 10km
    $query = "select idProvider,idDevice, Service, posLat, posLng, TimeStampc, Velocity, Heading, BatteryLevel, Accuracy, haversine(posLat,posLng,$lat,$lon) as harv  from `GeoServicesOnline` where `Service`='$service' and `cStatus`='Ready' and haversine(posLat,posLng,$lat,$lon)<=$Radius order by harv LIMIT 15";
    $list = db_query($query);
    // if ($resultado[1][0] == 0) 
    //     $flagType = "Insert";  
    // echo $query."\n";
    // echo "Aqui 0 [".$list[1][1]."]\n";
    ProcessListOfReadyProviders($UserId,$list);
    
}
////////////////////////////////////////////////////////////////////////////////
function PostGeoRequestService()
{
    $UserId =$_POST["userid"];
    $service=$_POST["service"];
    $lat=$_POST["lat"];
    $lon=$_POST["lon"];
    $latdest=$_POST["latdest"];
    $londest=$_POST["londest"];
    $service=$_POST["service"];
    $addressOri=$_POST["addressOri"];
    $addressDest=$_POST["addressDest"];
   
    $active=1;
    
    $Id = GetNewRequestedServiceID ();

    
    $data[] = new ColumnData();           
    
    
    $data[0]->Name="Id";              $data[0]->Value=$Id;        $data[0]->Type="Num";   
    $data[1]->Name="UserId";          $data[1]->Value=$UserId;    $data[1]->Type="Num";  
    $data[2]->Name="addressOri";      $data[2]->Value=$addressOri;       
    $data[3]->Name="addressDest";     $data[3]->Value=$addressDest;       
    $data[4]->Name="DateInclusion";   $data[4]->Value=GetDateTime();       
    $data[5]->Name="Lat";             $data[5]->Value=$lat;       $data[5]->Type="Num";  
    $data[6]->Name="Long";            $data[6]->Value=$lon;       $data[6]->Type="Num";  
    $data[7]->Name="LatDest";         $data[7]->Value=$latdest;   $data[7]->Type="Num";  
    $data[8]->Name="LongDest";        $data[8]->Value=$londest;   $data[8]->Type="Num";  
    $data[9]->Name="Active";          $data[9]->Value=1;          $data[9]->Type="Num"; 
    $data[10]->Name="Service";        $data[10]->Value=$service; 
    // $data[10]->Name="cStatus";         $data[10]->Value= "WaitingProvider"; 
    
    $resp = SqlInsert ("RequestedService",$data);
   
    SelectProviderAndCallUpon($UserId,$lat,$lon,$service);
    /////////////////////////////////////////////////////////////////////
    
    echo "{\"response\":\"$service-($lat,$lon) ($latdest,$londest) - $resp\"}";    
}
////////////////////////////////////////////////////////////////////////////////
// $data= new ColumnData();
function SqlInsert($table, $data) {
    $conlumnsnames = "";
    $i = 0;
    while (trim($data[$i]->Name) != "") {
        if ($i == 0)
            $virg = "";
        else
            $virg = ",";
        $buffer = $data[$i]->Name;
        $conlumnsnames .= "$virg`$buffer` ";
        $i++;
    }
    $conlumnsvalues = "";
    $i = 0;
    while (trim($data[$i]->Name) != "") {
        if ($i == 0)
            $virg = "";
        else
            $virg = ",";
        $buffer = $data[$i]->Value;
     
        if ($data[$i]->Type == "Num")
            $conlumnsvalues .= "$virg$buffer ";
        if ($data[$i]->Type == "Char" || $data[$i]->Type == "Date" || trim($data[$i]->Type) == "")
            $conlumnsvalues .= "$virg'$buffer' ";
        $i++;
    }
    $sql = "INSERT INTO `$table` ($conlumnsnames) VALUES ($conlumnsvalues)";
    $resultado = db_query($sql);
    return($sql);
}
////////////////////////////////////////////////////////////////////////////////
function SqlUpdate($table,$data,$condition) 
{
    $conlumnsvalues = "";
    $i = 0;
    while (trim($data[$i]->Name) != "") 
    {
        if ($i == 0)
            $virg = "";
        else
            $virg = ",";
        $bufferName = $data[$i]->Name;
        $bufferData = $data[$i]->Value;
        
        
        if ($data[$i]->Type == "Num")
            $conlumnsvalues .= "$virg`$bufferName` = $bufferData ";
        if ($data[$i]->Type == "Char" || $data[$i]->Type == "Date" || trim($data[$i]->Type) == "")
            $conlumnsvalues .= "$virg`$bufferName` = '$bufferData' ";
        $i++;
    }
    $sql = "UPDATE `$table` SET $conlumnsvalues WHERE ($condition)";

    $resultado = db_query($sql);
    return($sql);
}
////////////////////////////////////////////////////////////////////////////////
function GetNewRequestedServiceID () 
{
    $query = "select max(Id) from RequestedService";
    $resultado = db_query($query);
    $max = $resultado[1][0];
    return($max+1);
}

////////////////////////////////////////////////////////////////////////////////
function PostGetProviderStatus() 
{
   // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);
    
    
    $UserId =$_POST["userid"];
    $service=$_POST["service"];
    $lat=$_POST["lat"];
    $lon=$_POST["lon"];
    // $latdest=$_POST["latdest"];
    // $londest=$_POST["londest"];
    $service=$_POST["service"];
    // $addressOri=$_POST["addressOri"];
    // $addressDest=$_POST["addressDest"];
  
    // $sql = "SELECT `UserId`, `aLat`, `aLong`, `Radius`, `Address` FROM `MonitoredRegion` WHERE UserId=$idUser and haversine(aLat,aLong,$Lat,$Long)>Radius";

   
    $sql = "select Id, UserId, addressOri, addressDest, DateInclusion, Service, Lat, `Long`, LatDest, LongDest, Active from `RequestedService`;";

    $resultado = db_query($sql);    
    
    echo "{\"response\":\"$service-($lat,$lon) ($latdest,$londest) - $resp\"}";
   
}
////////////////////////////////////////////////////////////////////////////////