/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

////////////////////////////////////////////////////////////////////////////////
var MapManager =
{
    map : null,
    markerMe : null,
    mapCircle : null,
    mapCircleRad : 10,
    // alert("assass");
    ////////////////////////////////////////////////////////////////////////////
    FreeAll: function() 
    {
        if(this.map !== null)
        {
            this.map.off();
            this.map.remove();
        }
        this.map = null;
        this.markerMe = null;
        this.mapCircle = null;
        this.mapCircleRad = 10;      
    },  
    //////////////////////////////////// 
    SetMarkerOnNewLocation: function(glbLatNow,glbLngNow)
    {
        // getLocation();      
        // alert(GetOpenMapAddress(glbLatNow,glbLngNow));   
            
        this.map.setView(new L.LatLng(glbLatNow,glbLngNow)); 
            
        // this.markerMe = PutMarker(this.markerMe, this.map,"Img/Black_Marker.png",20,33,glbLatNow,glbLngNow);
        
        this.markerMe =  PutMarkerLocalization(this.markerMe, this.map,glbLatNow,glbLngNow);                
        this.mapCircle = L.circle([glbLatNow, glbLngNow], this.mapCircleRad).addTo(this.map);
      
    },         
    ///////////////////////////////////////////////////////////////////////////////
    ShowOpenMap: function(mapid,lat1,lon1) 
    {
        this.map = L.map(mapid, { zoomControl:false, minZoom:-9 });

        L.tileLayer( 'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://osm.org/copyright" title="OpenStreetMap" target="_blank">OpenStreetMap</a> contributors | Tiles Courtesy of <a href="https://www.mapquest.com/" title="MapQuest" target="_blank">MapQuest</a> <img src="https://developer.mapquest.com/content/osm/mq_logo.png" width="16" height="16">',
            subdomains: ['otile1','otile2','otile3','otile4']
        }).addTo( this.map );  


        // Layers examples
        // http://leaflet-extras.github.io/leaflet-providers/preview/

        /*
           var Esri_WorldStreetMap = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
                attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
            });

             var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        });

          var Esri_WorldStreetMap = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
        });

        L.tileLayer( 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright" title="OpenStreetMap" target="_blank">OpenStreetMap</a> contributors | Tiles Courtesy of <a href="http://www.mapquest.com/" title="MapQuest" target="_blank">MapQuest</a> <img src="http://developer.mapquest.com/content/osm/mq_logo.png" width="16" height="16">',
            subdomains: ['otile1','otile2','otile3','otile4']
        }).addTo( map );  

        var OpenTopoMap = L.tileLayer('http://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
            maxZoom: 17,
            attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
        });

        var CartoDB_Positron = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
            subdomains: 'abcd',
            maxZoom: 19
        });

        var HERE_hybridDay = L.tileLayer('http://{s}.{base}.maps.cit.api.here.com/maptile/2.1/{type}/{mapID}/hybrid.day/{z}/{x}/{y}/{size}/{format}?app_id={app_id}&app_code={app_code}&lg={language}', {
            attribution: 'Map &copy; 1987-2014 <a href="http://developer.here.com">HERE</a>',
            subdomains: '1234',
            mapID: 'newest',
            app_id: '<your app_id>',
            app_code: '<your app_code>',
            base: 'aerial',
            maxZoom: 20,
            type: 'maptile',
            language: 'eng',
            format: 'png8',
            size: '256'
        });

        var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
        });

        var Esri_WorldStreetMap = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
        });
        */

       //  this.map.setView(new L.LatLng(lat1, lon1), 20);
       //  this.SetMarkerOnNewLocation(lat1,lon1);
       //  map.panInsideBounds(bounds);
        
        return (this.map);



    },
    ////////////////////////////////////////////////////////////////////////////
    SetMapDragActions: function (on_mapmove,on_dragend) 
    {
        this.map.on('move', on_mapmove);

        //Dragstart event of map for update marker position
        this.map.on('dragstart', function(e) {  
            // StopMapTimer();
            // ActionOnControls(["#start","#divOriDst"],"hide");          
        });


        //Dragend event of map for update marker position
        this.map.on('dragend', on_dragend);


    },
    ////////////////////////////////////////////////////////////////////////////
//    dragend: function(e) {
//        // CreateMapTimer();
//        // ActionOnControls(["#start","#divOriDst"],"show");
//
//        var cnt = this.map.getCenter();
//        if(this.markerMe!==null) 
//           var position = this.markerMe.getLatLng();
//        lat = Number(position['lat']).toFixed(5);
//        lng = Number(position['lng']).toFixed(5);
//        //console.log(position);
//        setLeafLatLong(lat, lng);
//
//    },
//    ////////////////////////////////////////////////////////////////////////////
//    mapmove: function () 
//    {
//       // StopMapTimer();
//        if(this.markerMe!==null) 
//           this.markerMe.setLatLng(this.map.getCenter());
//
//        pos = this.map.getCenter()
//
//        if(pos.lat!=undefined && pos.lng!=undefined )
//        {
////                    this.glbLatNow = pos.lat;    
////                    this.glbLngNow = pos.lng; 
//        }
//
//       //  CreateMapTimer();
//    },
    ////////////////////////////////////////////////////////////////////////////
    CleanMapDragActions: function() 
    {
        this.map.off('move', function (e) {

        });

        //Dragend event of map for update marker position
        this.map.off('dragend', function(e) {

        });          
    }
    ////////////////////////////////////////////////////////////////////////////


};

