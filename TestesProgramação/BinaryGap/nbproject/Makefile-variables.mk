#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=None-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/None-Windows
CND_ARTIFACT_NAME_Debug=binarygap
CND_ARTIFACT_PATH_Debug=dist/Debug/None-Windows/binarygap
CND_PACKAGE_DIR_Debug=dist/Debug/None-Windows/package
CND_PACKAGE_NAME_Debug=binarygap.tar
CND_PACKAGE_PATH_Debug=dist/Debug/None-Windows/package/binarygap.tar
# Release configuration
CND_PLATFORM_Release=None-Windows
CND_ARTIFACT_DIR_Release=dist/Release/None-Windows
CND_ARTIFACT_NAME_Release=binarygap
CND_ARTIFACT_PATH_Release=dist/Release/None-Windows/binarygap
CND_PACKAGE_DIR_Release=dist/Release/None-Windows/package
CND_PACKAGE_NAME_Release=binarygap.tar
CND_PACKAGE_PATH_Release=dist/Release/None-Windows/package/binarygap.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
