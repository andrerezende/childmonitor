/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: andre
 *
 * Created on 9 de Janeiro de 2017, 21:40
 */
#include <iostream>
#include <vector>
#include <cstdlib>
#include <bitset>

using namespace std;

std::string int2bin(int num)
{
    return std::bitset<8>(num).to_string();
}

unsigned long bin2int(std::string stchar)
{
    return std::bitset<8>(stchar).to_ulong();
}
/*
 * 
 */
int main(int argc, char** argv) 
{
    std::cout<<int2bin(99)<<"\n";

    std::cout<<bin2int(int2bin(99))<<"\n";
    
    std::cout<< "Estamos indo!"<< endl;
    return 0;
}

